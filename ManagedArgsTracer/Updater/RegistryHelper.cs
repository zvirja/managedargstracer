﻿#region

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

using ManagedArgsTracer.Helpers;

using Microsoft.Win32;

#endregion

namespace ManagedArgsTracer.Updater
{
  internal static class RegistryHelper
  {
    #region Constants

    private const string KeyPath = @"Software\Zvirja\ManagedArgsTracer";

    private const string LastUpdateCheckValueName = "LastUpdateCheck";

    private const string LatestKnownValueName = "LatestKnownVersion";

    #endregion

    #region Public Methods and Operators

    public static DateTime GetLastUpdateCheckDate()
    {
      RegistryKey key = GetKey(false);
      if (key == null)
      {
        return DateTime.MinValue;
      }
      var lastUpdateCheckRaw = key.GetValue(LastUpdateCheckValueName, null) as string;
      if (lastUpdateCheckRaw.IsNullOrEmpty())
      {
        return DateTime.MinValue;
      }
      return DateTime.Parse(lastUpdateCheckRaw, CultureInfo.InvariantCulture);
    }

    public static Version GetLatestKnownVersion()
    {
      RegistryKey key = GetKey(false);
      if (key == null)
      {
        return null;
      }
      var versionStr = key.GetValue(LatestKnownValueName, null) as string;
      if (versionStr == null)
      {
        return null;
      }
      Version version;
      if (!Version.TryParse(versionStr, out version))
      {
        return null;
      }
      return version;
    }

    public static void SetLastUpdateCheckDate()
    {
      RegistryKey key = GetKey(false);
      if (key == null)
      {
        return;
      }
      key.SetValue(LastUpdateCheckValueName, DateTime.Now.ToString(CultureInfo.InvariantCulture), RegistryValueKind.String);
    }

    public static bool StoreLatestVersion(Version version)
    {
      RegistryKey key = GetKey(true);
      if (key == null)
      {
        return false;
      }
      key.SetValue(LatestKnownValueName, version.ToString(), RegistryValueKind.String);
      return true;
    }

    #endregion

    #region Methods

    private static RegistryKey GetKey(bool writable)
    {
      return Registry.CurrentUser.CreateSubKey(KeyPath, writable ? RegistryKeyPermissionCheck.ReadWriteSubTree : RegistryKeyPermissionCheck.Default);
    }

    #endregion
  }
}