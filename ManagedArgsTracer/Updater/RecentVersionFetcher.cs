﻿#region

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

using ManagedArgsTracer.Diagnostics;
using ManagedArgsTracer.Helpers;

#endregion

namespace ManagedArgsTracer.Updater
{
  internal static class RecentVersionFetcher
  {
    #region Constants

    private const string NewVersionInfoUri = "https://bitbucket.org/zvirja/managedargstracer-binaries/downloads/LatestVersion_ForUpdater.info";

    #endregion

    #region Public Methods and Operators

    public static Version GetCurrentVersion()
    {
      Assembly executingAssembly = Assembly.GetExecutingAssembly();
      FileVersionInfo fileVersionInfo = FileVersionInfo.GetVersionInfo(executingAssembly.Location);
      string fileVersionStr = fileVersionInfo.FileVersion;

      return Version.Parse(fileVersionStr);
    }

    public static string GetUpdateMessageIfNeed()
    {
      try
      {
        Version currentVersion = GetCurrentVersion();
        var storedRecentVersion = IsLatestVersion(currentVersion);
        if (storedRecentVersion == null || storedRecentVersion.Value)
        {
          return null;
        }
        return
          "Your version of tool ({0}) is obsoleted. Refer to the https://bitbucket.org/zvirja/managedargstracer-binaries/ page to download the fresh version.".FormatWith(
            currentVersion);
      }
      catch (Exception ex)
      {
        Tracer.PrivateOutput("[Updater]Unable to get update message: " + ex);
        return null;
      }
    }

    public static void RunVersionFetch()
    {
      bool? isLatest = IsLatestVersion(GetCurrentVersion());
      //We know that current version is obsoleted. No sense to communicate server for one more time.
      if (isLatest.HasValue && !isLatest.Value)
      {
        return;
      }

      //Check only once per day.
      DateTime lastUpdateCheck = RegistryHelper.GetLastUpdateCheckDate();
      if (lastUpdateCheck.Date >= DateTime.Today)
      {
        return;
      }
      Task.Factory.StartNew(FetchVersionAsyncDo);
    }

    #endregion

    #region Methods

    private static void FetchVersionAsyncDo()
    {
      try
      {
        WebRequest request = WebRequest.Create(NewVersionInfoUri);
        WebResponse response = request.GetResponse();
        using (Stream responseStream = response.GetResponseStream())
        {
          if (responseStream == null)
          {
            Tracer.PrivateOutput("[Updater]Respose stream is null.");
            return;
          }
          using (var infoStream = new StreamReader(responseStream))
          {
            if (infoStream.EndOfStream)
            {
              Tracer.PrivateOutput("[Updater]Respose stream is empty.");
              return;
            }
            string versionLine = infoStream.ReadLine();
            if (versionLine.IsNullOrEmpty())
            {
              Tracer.PrivateOutput("[Updater]Unable to read version line.");
              return;
            }

            Version latestVersion;
            if (!Version.TryParse(versionLine, out latestVersion))
            {
              Tracer.PrivateOutput("[Updater]Unable to parse version line: '{0}'".FormatWith(versionLine));
              return;
            }

            HandleNewVersionInfo(latestVersion);

            RegistryHelper.SetLastUpdateCheckDate();
          }
        }
      }
      catch (Exception ex)
      {
        //Just to be sure that everything is OK.
        try
        {
          Tracer.PrivateOutput("[Updater]Unable to fetch new version info: " + ex.ToString());
        }
        catch
        {
        }
      }
    }

    private static void HandleNewVersionInfo(Version latestVersion)
    {
      var fileVersion = GetCurrentVersion();

      if (latestVersion > fileVersion)
      {
        bool success = RegistryHelper.StoreLatestVersion(latestVersion);
        if (!success)
        {
          Tracer.PrivateOutput("[Updater]Unable to store latest known value in registry.");
        }
      }
    }

    private static bool? IsLatestVersion(Version currentVersion)
    {
      Version storedRecentVersion = RegistryHelper.GetLatestKnownVersion();
      if (storedRecentVersion == null)
      {
        return null;
      }
      return currentVersion >= storedRecentVersion;
    }

    #endregion
  }
}