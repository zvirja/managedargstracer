﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

using log4net;
using log4net.Appender;
using log4net.Core;
using log4net.Layout;
using log4net.Repository.Hierarchy;

using ManagedArgsTracer.Helpers;

#endregion

namespace ManagedArgsTracer.Diagnostics
{
  public static class Log
  {
    #region Static Fields

    public static string DateTimeSuffixForLogName;

    #endregion

    #region Constructors and Destructors

    static Log()
    {
      DateTimeSuffixForLogName = DateTime.Now.ToString("yyyy-MM-dd-HHmmss");

      Hierarchy hierarchy = (Hierarchy)LogManager.GetRepository();

      PatternLayout patternLayout = new PatternLayout();
      patternLayout.ConversionPattern = "%4t %d{ABSOLUTE} %-5p %m%n";
      patternLayout.ActivateOptions();

      FileAppender appender = new FileAppender();
      appender.AppendToFile = false;
      appender.File = GetLogFileName();
      appender.Layout = patternLayout;
      appender.ActivateOptions();
      hierarchy.Root.AddAppender(appender);

      hierarchy.Root.Level = Level.Info;
      hierarchy.Configured = true;

      InstallGlobalExceptionsHook();
    }

    #endregion

    #region Public Methods and Operators

    public static void Debug(string message, Type type)
    {
      Assert.ArgumentNotNull(message, "message");
      ILog logger = LogManager.GetLogger(type ?? typeof(Log));
      if (logger != null)
      {
        logger.Debug(message);
      }
    }

    public static void Debug(string message, object contextObject)
    {
      Debug(message, contextObject != null ? contextObject.GetType() : null);
    }

    public static void Error(string message, Exception exception, Type ownerType)
    {
      Assert.ArgumentNotNull(message, "message");
      Assert.ArgumentNotNull(ownerType, "ownerType");
      if (string.IsNullOrEmpty(message))
      {
        message = "Exception with no description";
      }
      if (ownerType == null)
      {
        ownerType = MethodBase.GetCurrentMethod().DeclaringType;
      }
      ILog logger = LogManager.GetLogger(ownerType);
      if (logger != null)
      {
        if (exception != null)
        {
          logger.Error(message, exception);
        }
        else
        {
          logger.Error(message);
        }
      }
    }

    public static void Error(string message, Exception exception, object contextObject)
    {
      Error(message, exception, contextObject != null ? contextObject.GetType() : null);
    }

    public static void Info(string message, Type ownerType)
    {
      Assert.ArgumentNotNull(message, "message");
      Assert.ArgumentNotNull(ownerType, "ownerType");
      ILog logger = LogManager.GetLogger(ownerType);
      if (logger != null)
      {
        logger.Info(message);
      }
    }

    public static void Info(string message, object contextObject)
    {
      Info(message, contextObject != null ? contextObject.GetType() : null);
    }

    public static void Warn(string message, Type ownerType, Exception exception = null)
    {
      if (string.IsNullOrEmpty(message))
      {
        message = "Exception with no description";
      }
      if (ownerType == null)
      {
        ownerType = MethodBase.GetCurrentMethod().DeclaringType;
      }
      ILog logger = LogManager.GetLogger(ownerType);
      if (logger != null)
      {
        if (exception != null)
        {
          logger.Warn(message, exception);
        }
        else
        {
          logger.Warn(message);
        }
      }
    }

    public static void Warn(string message, object contextObject, Exception exception = null)
    {
      Warn(message, contextObject != null ? contextObject.GetType() : null, exception);
    }

    #endregion

    #region Methods

    private static string GetLogFileName()
    {
      return "PrivateSessionLog_{0}.log".FormatWith(DateTimeSuffixForLogName);
    }

    private static void InstallGlobalExceptionsHook()
    {
      AppDomain.CurrentDomain.UnhandledException += (sender, args) => Log.Error("Unhandled exception at AppDomain level", args.ExceptionObject as Exception, typeof(Log));
    }

    #endregion
  }
}