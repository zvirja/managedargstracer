﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using log4net;
using log4net.Appender;
using log4net.Layout;
using log4net.Repository.Hierarchy;

using ManagedArgsTracer.Helpers;

#endregion

namespace ManagedArgsTracer.Diagnostics
{
  public static class PublicLog
  {
    #region Constants

    private const string LoggerName = "PublicLog";

    #endregion

    #region Constructors and Destructors

    static PublicLog()
    {
      Hierarchy hierarchy = (Hierarchy)LogManager.GetRepository();
      var patternLayout = new PatternLayout("%date{ABSOLUTE} %m%n");
      patternLayout.ActivateOptions();

      var appender = new FileAppender();
      appender.AppendToFile = false;
      appender.File = GetPublicLogFileName();
      appender.Layout = patternLayout;
      appender.ActivateOptions();

      var logger = (Logger)hierarchy.GetLogger(LoggerName);
      logger.Additivity = false;
      logger.Level = hierarchy.LevelMap["ALL"];

      logger.AddAppender(appender);
    }

    #endregion

    #region Public Methods and Operators

    public static void Info(string message)
    {
      if (message.IsNullOrEmpty())
      {
        message = "<null or empty message>";
      }
      ILog logger = LogManager.GetLogger(LoggerName);
      logger.Info(message);
    }

    #endregion

    #region Methods

    private static string GetPublicLogFileName()
    {
      return "SessionLog_{0}.log".FormatWith(Log.DateTimeSuffixForLogName);
    }

    #endregion
  }
}