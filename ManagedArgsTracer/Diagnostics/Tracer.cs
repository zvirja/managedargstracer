﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ManagedArgsTracer.Helpers;
using ManagedArgsTracer.Session;

#endregion

namespace ManagedArgsTracer.Diagnostics
{
  public static class Tracer
  {
    #region Constants

    public const string ErrorDuringInitializationErrorMsg = "Error during the initialization. See the logs for more details.";

    public const string UnexpectedErrorWrongWorkReportMessage =
      "Unexpected error happened during the execution. The application might behave wrongly. It's recommended to send log file to the developer.";

    #endregion

    #region Public Methods and Operators

    public static void PrivateOutput(string message)
    {
      Log.Info("PT: " + message, typeof(Tracer));
      if (GlobalSessionVars.NoisyOutput)
      {
        PublicOutputInternal(message, false);
      }
    }

    public static void PublicOutput(string message)
    {
      PublicOutputInternal(message, true);
    }

    public static void ReportUnexpectedState(string message = null, bool log = false)
    {
      PublicOutput(UnexpectedErrorWrongWorkReportMessage);
      if (message.IsNullOrEmpty())
      {
        return;
      }
      PrivateOutput(message);
      if (log)
      {
        Log.Error(message, null, typeof(Tracer));
      }
    }

    #endregion

    #region Methods

    private static void PublicOutputInternal(string message, bool addToPrivateLog)
    {
      if (addToPrivateLog)
      {
        Log.Info("T: " + message, typeof(Tracer));
      }
      PublicLog.Info(message);
      Console.WriteLine(DateTime.Now.ToString("HH:mm:ss.ffff") + " " + message);
      Console.WriteLine();
    }

    #endregion
  }
}