﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Diagnostics.Runtime.Interop;

#endregion

namespace ManagedArgsTracer.Session
{
  public interface INativeDebugInterfaces
  {
    #region Public Properties

    IDebugAdvanced DebugAdvanced { get; set; }

    IDebugClient DebugClient { get; set; }

    IDebugControl DebugControl { get; set; }

    IDebugControl2 DebugControl2 { get; set; }

    IDebugDataSpaces DebugDataSpaces { get; set; }

    IDebugSymbols3 DebugSymbols3 { get; set; }

    IDebugSystemObjects DebugSystemObjects { get; set; }

    #endregion
  }
}