﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using JetBrains.Annotations;

using ManagedArgsTracer.Diagnostics;

#endregion

namespace ManagedArgsTracer.Session
{
  [Flags]
  internal enum SessionFlags : uint
  {
    NONE = 0,

    /// <summary>
    /// Suppress stack traces for all the method.
    /// </summary>
    NO_STACK_TRACE = 0x1,

    /// <summary>
    /// Lite output. Disables the verbose mode and displays only critical messages.
    /// </summary>
    LITE_OUTPUT = 0x2,

    NOISY = 0x4
  }

  internal class SessionFlagsManager
  {
    #region Static Fields

    public static Dictionary<string, SessionFlags> KnownFlags;

    #endregion

    #region Constructors and Destructors

    static SessionFlagsManager()
    {
      KnownFlags = new Dictionary<string, SessionFlags>
                     {
                       { "nostack", SessionFlags.NO_STACK_TRACE },
                       { "lite", SessionFlags.LITE_OUTPUT },
                       { "noisy", SessionFlags.NOISY }
                     };
    }

    #endregion

    #region Public Methods and Operators

    /// <summary>
    /// Tries to resolve known flag. If unable to resolve, GlobalSessionFlags.NONE is returned.
    /// </summary>
    public static SessionFlags GetFlagByName([NotNull] string flagName)
    {
      Assert.ArgumentNotNullOrEmpty(flagName, "flagName");
      SessionFlags flag;
      if (KnownFlags.TryGetValue(flagName, out flag))
      {
        return flag;
      }
      return SessionFlags.NONE;
    }

    #endregion
  }
}