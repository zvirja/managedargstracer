﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ManagedArgsTracer.Runtime;
using ManagedArgsTracer.Runtime.Wrappers;

using Microsoft.Diagnostics.Runtime.Interop;

#endregion

namespace ManagedArgsTracer.Session
{
  internal static class GlobalSessionVars
  {
    #region Static Fields

    public static bool NoisyOutput;

    private static SessionFlags currentFlags;

    #endregion

    #region Public Properties

    public static ClrRuntimeHelper ClrRuntimeHelper { get; set; }

    public static SessionFlags CurrentFlags
    {
      get
      {
        return currentFlags;
      }
      set
      {
        currentFlags = value;
        NoisyOutput = (value & SessionFlags.NOISY) == SessionFlags.NOISY;
      }
    }

    public static IDebugControl DebugControl { get; set; }

    public static XClrDataProcessWrapper XClrDataProcessWrapper { get; set; }

    #endregion
  }
}