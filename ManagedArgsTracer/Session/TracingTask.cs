﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ManagedArgsTracer.Breakpoints.Definition;
using ManagedArgsTracer.Common;
using ManagedArgsTracer.Helpers;

#endregion

namespace ManagedArgsTracer.Session
{
  internal class TracingTask
  {
    #region Constants

    public const char GlobalSessionFlagChar = '-';

    #endregion

    #region Constructors and Destructors

    public TracingTask(string inputArgs)
    {
      this.InputString = new CommandLineString(inputArgs);
    }

    #endregion

    #region Public Properties

    public List<BreakpointDefinition> BreakpointDefinitions { get; protected set; }

    public int PidToAttach { get; protected set; }

    public string ProcessNameToAttach { get; protected set; }

    public SessionFlags SessionFlags { get; set; }

    #endregion

    #region Properties

    private CommandLineString InputString { get; set; }

    #endregion

    #region Public Methods and Operators

    public void Parse()
    {
      this.BreakpointDefinitions = BreakpointDefinitionsParser.ParseBreakpointDefinitions(this.InputString);
      this.FetchGlobalSessionFlags();
      this.ParsePidOrProcessName();
      this.InputString.NavigateToNextNonSpaceChar(false);
      if (this.InputString.CurrentCharPos < this.InputString.CharPosLimit)
      {
        this.InputString.ThrowInvalidOperationExceptionDuringParsing("End of line is expected.");
      }
    }

    #endregion

    #region Methods

    private void FetchGlobalSessionFlags()
    {
      this.InputString.NavigateToNextNonSpaceChar();
      while (this.InputString.CurrentChar == GlobalSessionFlagChar)
      {
        //Because current char is -
        this.InputString.CurrentCharPos++;

        //Only -flagName is currently supported
        StringBuilder flagName = this.InputString.CollectCharsTillStopChar(' ');
        if (flagName.Length == 0)
        {
          this.InputString.ThrowInvalidOperationExceptionDuringParsing("Flag name could not be empty.");
        }
        string flagNameStr = flagName.ToString();
        SessionFlags flag = SessionFlagsManager.GetFlagByName(flagNameStr);
        if (flag == SessionFlags.NONE)
        {
          this.InputString.ThrowInvalidOperationExceptionDuringParsing("Flag '{0}' is unknown session flag.".FormatWith(flagNameStr));
        }

        //Save flag, commit success to easily troubleshoot failure reason if error.
        this.SessionFlags |= flag;
        this.InputString.DebugCommitSuccess();

        this.InputString.NavigateToNextNonSpaceChar();
      }
    }

    private void ParsePidOrProcessName()
    {
      this.InputString.NavigateToNextNonSpaceChar();
      string pidOrProcessName = this.InputString.CollectCharsTillStopChar(' ', char.MinValue, char.MinValue, null, false).ToString();
      if (pidOrProcessName.IsNullOrEmpty())
      {
        this.InputString.ThrowInvalidOperationExceptionDuringParsing("Either process name or id should be specified.");
      }

      int pidToAttach;
      if (int.TryParse(pidOrProcessName, out pidToAttach))
      {
        this.PidToAttach = pidToAttach;
      }
      else
      {
        this.ProcessNameToAttach = pidOrProcessName;
      }
      this.InputString.DebugCommitSuccess();
    }

    #endregion
  }
}