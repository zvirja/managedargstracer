﻿#region

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

using ManagedArgsTracer.Breakpoints;
using ManagedArgsTracer.Diagnostics;
using ManagedArgsTracer.Helpers;
using ManagedArgsTracer.Interop.Clr;
using ManagedArgsTracer.Interop.Native;
using ManagedArgsTracer.Runtime;
using ManagedArgsTracer.Runtime.Wrappers;

using Microsoft.Diagnostics.Runtime;
using Microsoft.Diagnostics.Runtime.Interop;

#endregion

namespace ManagedArgsTracer.Session
{
  internal class SessionManager : IDisposable
  {
    #region Static Fields

    public static SessionManager Instance;

    #endregion

    #region Public Properties

    public BreakpointsManager BreakpointsManager { get; set; }

    public ClrExceptionNotificationTranslator ClrNotificationsTranslator { get; set; }

    public ClrRuntimeHelper ClrRuntimeHelper { get; set; }

    public ClrInfo ClrVersion { get; set; }

    public DacLibWrapper DacLibWrapper { get; set; }

    public string DacLocation { get; set; }

    public DataTarget DataTarget { get; set; }

    public RawDebuggerEventsSource DebuggerEventsSource { get; set; }

    public NativeDebugInterfaces NativeDebugInterfaces { get; set; }

    public NotificationsSource NotificationsSource { get; set; }

    public ClrRuntime Runtime { get; set; }

    public bool TargetApplicationExited { get; set; }

    public Process TargetProcessInfo { get; set; }

    public TracingTask Task { get; set; }

    public XClrDataProcessWrapper XCLRDataProcessWrapper { get; set; }


    #endregion

    #region Public Methods and Operators

    public void Dispose()
    {
      this.Dispose(true);
      GC.SuppressFinalize(this);
    }

    public bool StartSession(TracingTask taskToTrace)
    {
      this.Task = taskToTrace;
      return this.Initialize();
    }

    #endregion

    #region Methods

    protected virtual void Dispose(bool managed)
    {
      try
      {
        if (this.TargetApplicationExited)
        {
          Tracer.PrivateOutput("Target application exited. Breakpoints cleanup skipped.");
        } else if (this.BreakpointsManager != null)
        {
          this.BreakpointsManager.CleanupBreakpoints();
          Tracer.PrivateOutput("Breakpoints were cleared.");
        }
      }
      catch (Exception ex)
      {
        Log.Error("Exception during breakpoint clean.", ex, this);
        Tracer.ReportUnexpectedState("Unable to perform breakpoins cleanup.");
      }

      if (this.DataTarget != null)
      {
        this.DataTarget.Dispose();
      }
    }

    protected bool Initialize()
    {
      try
      {
        if (!this.InitializeProcessInfo())
        {
          return false;
        }
        if (!this.InitializeClrMD())
        {
          return false;
        }
        if (!this.InitializeCustomClr())
        {
          return false;
        }
        if (!this.InitializeNativeDebugger())
        {
          return false;
        }
        if (!this.InitializeOtherInstances())
        {
          return false;
        }
        if (!this.InitializeGlobalVars())
        {
          return false;
        }
        Instance = this;
        return true;
      }
      catch (Exception ex)
      {
        Log.Error("Unhandled exception in SessionManager Initialize method.", ex, this);
        Tracer.PublicOutput("Unexpected exception during initialization. Message: '{0}'. Please report session log file to developer.".FormatWith(ex.Message));
        return false;
      }
    }

    private Process GetProcessInfo()
    {
      if (this.Task.PidToAttach != 0)
      {
        try
        {
          Process process = Process.GetProcessById(this.Task.PidToAttach);
          return process;
        }
        catch (Exception ex)
        {
          Log.Error("Unable to get process by id {0}".FormatWith(this.Task.PidToAttach), ex, this);
          Tracer.PublicOutput("Unable to attach to proces with ID {0}. Message: '{1}'".FormatWith(this.Task.PidToAttach, ex.Message));
          return null;
        }
      }
      else
      {
        Process[] processes = Process.GetProcessesByName(this.Task.ProcessNameToAttach);
        if (processes.Length == 0 && this.Task.ProcessNameToAttach.EndsWith(".exe", StringComparison.OrdinalIgnoreCase))
        {
          processes = Process.GetProcessesByName(this.Task.ProcessNameToAttach.Substring(0, this.Task.ProcessNameToAttach.Length - 4));
        }
        if (processes.Length == 0)
        {
          Tracer.PublicOutput("No process with name '{0}' was found.".FormatWith(this.Task.ProcessNameToAttach));
          return null;
        }
        if (processes.Length > 1)
        {
          Tracer.PublicOutput(
            "More than one process with name '{1}' exist. Specify process by ID rather than by name.{0}Processes with following IDs were found:{0}{2}".FormatWith(
              Environment.NewLine,
              this.Task.ProcessNameToAttach,
              string.Join(" | ", processes.Select(proc => proc.Id.ToString(CultureInfo.InvariantCulture)).ToArray())));
          return null;
        }
        return processes.First();
      }
    }

    private bool InitializeClrMD()
    {
      this.DataTarget = DataTarget.AttachToProcess(this.TargetProcessInfo.Id, 10000, AttachFlag.Invasive);
      if (this.DataTarget.ClrVersions.Count == 0)
      {
        Tracer.PublicOutput("The process you specified is not a valid .NET process. Only .NET processes are supported.");
        return false;
      }
      if (this.DataTarget.ClrVersions.Count > 1)
      {
        Tracer.PublicOutput("Target process hosts more than one CLR runtimes. Such processes are not currently supported by the application.");
        return false;
      }
      this.ClrVersion = this.DataTarget.ClrVersions.First();
      this.DacLocation = this.ClrVersion.TryGetDacLocation();
      if (this.DacLocation.IsNullOrEmpty())
      {
        Tracer.PublicOutput("Unable to resolve MSCORDACWKS location. Please report to developers about this issue.");
        return false;
      }

      try
      {
        this.Runtime = this.DataTarget.CreateRuntime(this.DacLocation);
      }
      catch (Exception ex)
      {
        Log.Error("Unable to create runtime", ex, this);
        Tracer.PublicOutput("Failure during the initialization. Ensure that bitness of current and target applications match.");
        return false;
      }

      this.NativeDebugInterfaces = new NativeDebugInterfaces(this.DataTarget.DebuggerInterface);
      this.ClrRuntimeHelper = new ClrRuntimeHelper(this.Runtime);

      return true;
    }

    private bool InitializeCustomClr()
    {
      try
      {
        this.DacLibWrapper = new DacLibWrapper(this.DacLocation);
        Guid idOfXClrDataProcess = new Guid("5c552ab6-fc09-4cb3-8e36-22fa03c798b7");
        IXCLRDataProcess rawXClrDataProcess;
        int responseCode = this.DacLibWrapper.ClrDataCreateInstanceMethod(ref idOfXClrDataProcess, new CustomClrDataTargetImpl(this), out rawXClrDataProcess);
        if (responseCode != 0)
        {
          Log.Error("Unable to get IXClrDataProcess. Response code: {0}".FormatWith(responseCode), null, this);
          Tracer.PublicOutput(Tracer.ErrorDuringInitializationErrorMsg);
          return false;
        }
        this.XCLRDataProcessWrapper = new XClrDataProcessWrapper(rawXClrDataProcess);
        this.ClrNotificationsTranslator = new ClrExceptionNotificationTranslator(this.XCLRDataProcessWrapper);
        return true;
      }
      catch (Exception ex)
      {
        Log.Error("Unable to create DacLibWrapper.", ex, this);
        Tracer.PublicOutput(Tracer.ErrorDuringInitializationErrorMsg);
        return false;
      }
    }

    private bool InitializeGlobalVars()
    {
      GlobalSessionVars.XClrDataProcessWrapper = this.XCLRDataProcessWrapper;
      GlobalSessionVars.ClrRuntimeHelper = this.ClrRuntimeHelper;
      GlobalSessionVars.DebugControl = this.NativeDebugInterfaces.DebugControl;

      return true;
    }

    private bool InitializeNativeDebugger()
    {
      this.DebuggerEventsSource = new RawDebuggerEventsSource();
      IntPtr iUnknownPtr = Marshal.GetComInterfaceForObject(this.DebuggerEventsSource, typeof(IDebugEventCallbacks));
      int responseCode;
      if ((responseCode = this.NativeDebugInterfaces.DebugClient.SetEventCallbacks(iUnknownPtr)) != 0)
      {
        Tracer.PublicOutput(Tracer.ErrorDuringInitializationErrorMsg);
        Log.Error("Unable to set callback for native debug client. Response code: {0}".FormatWith(responseCode.ToString("X")), null, this);
        return false;
      }

      var codesToDisable = new uint[]
      {
        0xC0000420, //STATUS_ASSERTION_FAILURE
        0xC0000005, //STATUS_ACCESS_VIOLATION
        0x80000002, //STATUS_DATATYPE_MISALIGNMENT
        0xC0000094, //STATUS_INTEGER_DIVIDE_BY_ZERO
       /* 0xC000008E, //STATUS_FLOAT_DIVIDE_BY_ZERO*/
        0x80000001, //STATUS_GUARD_PAGE_VIOLATION
        0xC0000095, //STATUS_INTEGER_OVERFLOW
        0xC0000006, //STATUS_IN_PAGE_ERROR
        0xC000001C, //STATUS_INVALID_SYSTEM_SERVICE
        0xC000001E, //STATUS_INVALID_LOCK_SEQUENCE
        0xC0000409, //STATUS_STACK_BUFFER_OVERRUN
        0xC00000FD, //STATUS_STACK_OVERFLOW
        0x80000007, //STATUS_WAKE_SYSTEM_DEBUGGER
        0xCFFFFFFF, //STATUS_APPLICATION_HANG
        0xC0000008, //STATUS_INVALID_HANDLE
        0x80000003, //STATUS_BREAKPOINT
        0xE06D7363, //STATUS_CPP_EH_EXCEPTION
        0xE0434f4D, //STATUS_CLR_EXCEPTION
        0x40010008, //DBG_CONTROL_BREAK
        0x40010005, //DBG_CONTROL_C
        0x40010009, //DBG_COMMAND_EXCEPTION
        0xC000001D,//STATUS_ILLEGAL_INSTRUCTION
        0xC0000037,//STATUS_PORT_DISCONNECTED
        0x80000004,//STATUS_SINGLE_STEP
        0xC0000421, //STATUS_VERIFIER_STOP

      };

      return this.SetIgnoreForSpecificExceptions(codesToDisable);
    }

    private bool SetIgnoreForSpecificExceptions(uint[] exceptionCodes)
    {
      var filtersToAdjust = new List<DEBUG_EXCEPTION_FILTER_PARAMETERS>();
      foreach (uint exceptionCode in exceptionCodes)
      {
        var parameter = new DEBUG_EXCEPTION_FILTER_PARAMETERS[1];
        int responseCode = this.NativeDebugInterfaces.DebugControl.GetExceptionFilterParameters(1, new uint[]{exceptionCode}, 0, parameter);
        if (responseCode != 0)
        {
          Tracer.PublicOutput(Tracer.ErrorDuringInitializationErrorMsg);
          Tracer.PrivateOutput("Unable to get exception filter parameters for native {0} exception. Response code: {1}".FormatWith(exceptionCode.ToString("X"), responseCode.ToString("X")));
        }
        else if (parameter[0].ExecutionOption == DEBUG_FILTER_EXEC_OPTION.BREAK)
        {
          parameter[0].ExecutionOption = DEBUG_FILTER_EXEC_OPTION.IGNORE;
          filtersToAdjust.Add(parameter[0]);
        }
      }

      if (filtersToAdjust.Count > 0)
      {
        DEBUG_EXCEPTION_FILTER_PARAMETERS[] paramsArray = filtersToAdjust.ToArray();
        int responseCode = this.NativeDebugInterfaces.DebugControl.SetExceptionFilterParameters((uint)paramsArray.Length, paramsArray);
        if (responseCode != 0)
        {
          //Tracer.PublicOutput(Tracer.ErrorDuringInitializationErrorMsg);
          Log.Error("Unable to set exception filter parameters for native debug client. Response code: {0}".FormatWith(responseCode), null, this);
          return false;
        }
        Tracer.PrivateOutput("Filter parameters were adjusted for following codes: " + string.Join(",",paramsArray.Select(p => p.ExceptionCode.ToString("X"))));
      }

      return true;
    }

    private bool InitializeOtherInstances()
    {
      this.NotificationsSource = new NotificationsSource(this.DebuggerEventsSource, this.ClrNotificationsTranslator);
      this.BreakpointsManager = new BreakpointsManager(this);

      this.NotificationsSource.TargetProcessExit += this.NotificationsSourceOnTargetProcessExit;
      return true;
    }

    private bool InitializeProcessInfo()
    {
      this.TargetProcessInfo = this.GetProcessInfo();
      if (this.TargetProcessInfo == null)
      {
        return false;
      }
      return this.ValidateProcessInfoBitness(this.TargetProcessInfo);
    }

    private void NotificationsSourceOnTargetProcessExit(object sender, EventArgs eventArgs)
    {
      Tracer.PublicOutput("!!! The target application exited.");
      this.TargetApplicationExited = true;
    }

    private bool ValidateProcessInfoBitness(Process targetProcessInfo)
    {
      bool isWowProcess;
      if (!NativeMethods.IsWow64Process(targetProcessInfo.Handle, out isWowProcess))
      {
        int errCode = Marshal.GetLastWin32Error();
        Tracer.PublicOutput(Tracer.ErrorDuringInitializationErrorMsg);
        Tracer.PrivateOutput("Unable to check target process bitness. Error code: {0}".FormatWith(errCode));
        return false;
      }

      //if target and current process bitnesses mismatch
      if (IntPtr.Size == 4 ^ isWowProcess)
      {
        Tracer.PublicOutput("Target process is not an {0} process. Only {0} are supported by current version.".FormatWith(IntPtr.Size == 4 ? "X86" : "X64"));
        return false;
      }

      return true;
    }

    #endregion
  }
}