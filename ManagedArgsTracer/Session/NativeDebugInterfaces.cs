﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using JetBrains.Annotations;

using ManagedArgsTracer.Diagnostics;

using Microsoft.Diagnostics.Runtime.Interop;

#endregion

namespace ManagedArgsTracer.Session
{
  public class NativeDebugInterfaces : INativeDebugInterfaces
  {
    #region Constructors and Destructors

    public NativeDebugInterfaces([NotNull] IDebugClient debugClient)
    {
      Assert.ArgumentNotNull(debugClient, "debugClient");
      this.DebugClient = debugClient;
      this.DebugControl = (IDebugControl)debugClient;
      this.DebugControl2 = (IDebugControl2)debugClient;
      this.DebugSystemObjects = (IDebugSystemObjects)debugClient;
      this.DebugDataSpaces = (IDebugDataSpaces)debugClient;
      this.DebugAdvanced = (IDebugAdvanced)debugClient;
      this.DebugSymbols3 = (IDebugSymbols3)debugClient;
    }

    #endregion

    #region Public Properties

    public IDebugAdvanced DebugAdvanced { get; set; }

    public IDebugClient DebugClient { get; set; }

    public IDebugControl DebugControl { get; set; }

    public IDebugControl2 DebugControl2 { get; set; }

    public IDebugDataSpaces DebugDataSpaces { get; set; }

    public IDebugSymbols3 DebugSymbols3 { get; set; }

    public IDebugSystemObjects DebugSystemObjects { get; set; }

    #endregion
  }
}