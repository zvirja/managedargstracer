﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

#endregion

namespace ManagedArgsTracer.Interop.Native
{
  internal static class NativeMethods
  {
    #region Enums

    [Flags]
    public enum LoadLibraryFlags : uint
    {
      NoFlags = 0U,

      DontResolveDllReferences = 1U,

      LoadIgnoreCodeAuthzLevel = 16U,

      LoadLibraryAsDatafile = 2U,

      LoadLibraryAsDatafileExclusive = 64U,

      LoadLibraryAsImageResource = 32U,

      LoadWithAlteredSearchPath = 8U,
    }

    #endregion

    #region Public Methods and Operators

    [DllImport("kernel32.dll", SetLastError = true)]
    public static extern IntPtr GetProcAddress(IntPtr hModule, [MarshalAs(UnmanagedType.LPStr)] string procedureName);

    [DllImport("kernel32.dll", SetLastError = true)]
    public static extern IntPtr LoadLibraryEx(string fileName, IntPtr hFile, LoadLibraryFlags dwFlags);

    [DllImport("kernel32.dll", SetLastError = true, ExactSpelling = true)]
    public static extern IntPtr VirtualAllocEx(IntPtr hProcess, IntPtr lpAddress, IntPtr dwSize, uint flAllocationType, uint flProtect);

    [DllImport("kernel32.dll", SetLastError = true, ExactSpelling = true)]
    public static extern bool VirtualFreeEx(IntPtr hProcess, IntPtr lpAddress, IntPtr dwSize, int dwFreeType);

    [DllImport("kernel32.dll", SetLastError = true, CallingConvention = CallingConvention.Winapi)]
    [return: MarshalAs(UnmanagedType.Bool)]
    public static extern bool IsWow64Process([In] IntPtr processHandle, [Out] [MarshalAs(UnmanagedType.Bool)] out bool wow64Process);

    #endregion

    #region Methods

    [DllImport("kernel32.dll", SetLastError = true)]
    private static extern bool FreeLibrary(IntPtr hModule);

    #endregion
  }
}