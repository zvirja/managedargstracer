﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;

using Microsoft.Diagnostics.Runtime.Interop;

#endregion

namespace ManagedArgsTracer.Interop.Clr
{
  [Flags]
  internal enum DataOtherNotifyFlag : uint
  {
    NONE = 0x00000000,

    CLRDATA_NOTIFY_ON_MODULE_LOAD = 0x00000001,

    CLRDATA_NOTIFY_ON_MODULE_UNLOAD = 0x00000002,

    CLRDATA_NOTIFY_ON_EXCEPTION = 0x00000004
  }

  [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
  [Guid("5c552ab6-fc09-4cb3-8e36-22fa03c798b7")]
  [ComImport]
  internal interface IXCLRDataProcess
  {
    /*
    * Flush any cached data for this process. All ICLR* interfaces obtained
    * for this process will become invalid with this call.
    */
    /*     HRESULT Flush(); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int Flush();

    /*
     * Begin enumeration of tasks.
     * Returns S_FALSE if the enumeration is empty.
     */
    /*     HRESULT StartEnumTasks([out] CLRDATA_ENUM* handle); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int StartEnumTasks_dummy();

    /*
     * Get the next entry in the enumeration.
     * Returns S_FALSE if there isn't a next entry.
     */
    /*     HRESULT EnumTask([in, out] CLRDATA_ENUM* handle,
                         [out] IXCLRDataTask** task); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int EnumTask_dummy();

    /*
     * Release the enumerator.
     */
    /*     HRESULT EndEnumTasks([in] CLRDATA_ENUM handle); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int EndEnumTasks_dummy();

    /*
     * Get the managed task running on the given OS thread ID
     */
    /*     HRESULT GetTaskByOSThreadID([in] ULONG32 osThreadID,
                                    [out] IXCLRDataTask** task); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetTaskByOSThreadID(uint osThreadID, [MarshalAs(UnmanagedType.Interface)] [Out] out IXCLRDataTask task);

    /*
     * Get the managed task corresponding to the given task ID.
     */
    /*     HRESULT GetTaskByUniqueID([in] ULONG64 taskID,
                                  [out] IXCLRDataTask** task); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetTaskByUniqueID_dummy();

    /*
     * Get state flags, defined in CLRDataProcessFlag.
     */
    /*     HRESULT GetFlags([out] ULONG32* flags); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetFlags_dummy();

    /*
     * Determine whether the given interface represents
     * the same target state.
     */
    /*     HRESULT IsSameObject([in] IXCLRDataProcess* process); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int IsSameObject_dummy();

    /*
     * Get the managed object representing the process.
     */
    /*     HRESULT GetManagedObject([out] IXCLRDataValue** value); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetManagedObject_dummy();

    /*
     * Mark the process so that it attempts to reach the
     * desired execution state the next time it executes.
     */
    /*     HRESULT GetDesiredExecutionState([out] ULONG32* state); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetDesiredExecutionState_dummy();

    /*     HRESULT SetDesiredExecutionState([in] ULONG32 state); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int SetDesiredExecutionState_dummy();

    /*
     * Return an indicator of the type of data referred
     * to by the given address.
     */
    /*     HRESULT GetAddressType([in] CLRDATA_ADDRESS address,
                               [out] CLRDataAddressType* type); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetAddressType_dummy();

    /*
     * Get a name for the given address if
     * the address refers to non-managed-method information.
     * Method names can be retrieved by using GetMethodInstanceByAddress
     * and GetName on the method instance.
     * 
     * Returns S_FALSE if the buffer is not large enough for the name,
     * and sets nameLen to be the buffer length needed.
     */
    /*     HRESULT GetRuntimeNameByAddress([in] CLRDATA_ADDRESS address,
                                        [in] ULONG32 flags,
                                        [in] ULONG32 bufLen,
                                        [out] ULONG32 *nameLen,
                                        [out, size_is(bufLen)] WCHAR nameBuf[],
                                        [out] CLRDATA_ADDRESS* displacement); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetRuntimeNameByAddress(ulong address, uint flags, uint bufLen, out uint realLen, [MarshalAs(UnmanagedType.LPWStr)] [Out] StringBuilder nameBuf, out ulong displacement);

    /*
     * App domain enumeration.
     */
    /*     HRESULT StartEnumAppDomains([out] CLRDATA_ENUM* handle); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int StartEnumAppDomains_dummy();

    /*     HRESULT EnumAppDomain([in, out] CLRDATA_ENUM* handle,
                              [out] IXCLRDataAppDomain** appDomain); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int EnumAppDomain_dummy();

    /*     HRESULT EndEnumAppDomains([in] CLRDATA_ENUM handle); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int EndEnumAppDomains_dummy();

    /*
     * Find an app domain by its unique ID.
     */
    /*     HRESULT GetAppDomainByUniqueID([in] ULONG64 id,
                                       [out] IXCLRDataAppDomain** appDomain); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetAppDomainByUniqueID_dummy();

    /*
     * Assembly enumeration.
     */
    /*     HRESULT StartEnumAssemblies([out] CLRDATA_ENUM* handle); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int StartEnumAssemblies_dummy();

    /*     HRESULT EnumAssembly([in, out] CLRDATA_ENUM* handle,
                             [out] IXCLRDataAssembly **assembly); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int EnumAssembly_dummy();

    /*     HRESULT EndEnumAssemblies([in] CLRDATA_ENUM handle); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int EndEnumAssemblies_dummy();

    /*
     * Module enumeration.
     */
    /*     HRESULT StartEnumModules([out] CLRDATA_ENUM* handle); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int StartEnumModules(out ulong handle);

    /*     HRESULT EnumModule([in, out] CLRDATA_ENUM* handle,
                           [out] IXCLRDataModule **mod); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int EnumModule(ref ulong handle, [MarshalAs(UnmanagedType.Interface)] [Out] out IXCLRDataModule module);

    /*     HRESULT EndEnumModules([in] CLRDATA_ENUM handle); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int EndEnumModules(ulong handle);

    /*
     * Look up a module by address.
     */
    /*     HRESULT GetModuleByAddress([in] CLRDATA_ADDRESS address,
                                   [out] IXCLRDataModule** mod); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetModuleByAddress([In] ulong address, [Out] [MarshalAs(UnmanagedType.Interface)] out IXCLRDataModule module);

    /*
     * Look up method instances by native code address.
     */
    /*     HRESULT StartEnumMethodInstancesByAddress([in] CLRDATA_ADDRESS address,
                                                  [in] IXCLRDataAppDomain* appDomain,
                                                  [out] CLRDATA_ENUM* handle); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int StartEnumMethodInstancesByAddress(ulong address, [MarshalAs(UnmanagedType.Interface)] [In] object appDomain, out ulong handle);

    /*     HRESULT EnumMethodInstanceByAddress([in] CLRDATA_ENUM* handle,
                                            [out] IXCLRDataMethodInstance** method); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int EnumMethodInstanceByAddress(ref ulong handle, [MarshalAs(UnmanagedType.Interface)] out object method);

    /*     HRESULT EndEnumMethodInstancesByAddress([in] CLRDATA_ENUM handle); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int EndEnumMethodInstancesByAddress(ulong handle);

    /*
     * Look up the name and value of a piece of data by its address.
     */
    /*     HRESULT GetDataByAddress([in] CLRDATA_ADDRESS address,
                                 [in] ULONG32 flags,
                                 [in] IXCLRDataAppDomain* appDomain,
                                 [in] IXCLRDataTask* tlsTask,
                                 [in] ULONG32 bufLen,
                                 [out] ULONG32 *nameLen,
                                 [out, size_is(bufLen)] WCHAR nameBuf[],
                                 [out] IXCLRDataValue** value,
                                 [out] CLRDATA_ADDRESS* displacement); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetDataByAddress_dummy();

    /*
     * Get managed state, if any, for the given system exception.
     * OBSOLETE, DO NOT USE.
     */
    /*     HRESULT GetExceptionStateByExceptionRecord([in] EXCEPTION_RECORD64* record,
                                                   [out] IXCLRDataExceptionState **exState); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetExceptionStateByExceptionRecord_dummy();

    /*
     * Translate a system exception record into
     * a particular kind of notification if possible.
     */
    /*     HRESULT TranslateExceptionRecordToNotification([in] EXCEPTION_RECORD64* record,
                                                       [in] IXCLRDataExceptionNotification* notify); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int TranslateExceptionRecordToNotification(ref EXCEPTION_RECORD64 record, [MarshalAs(UnmanagedType.Interface)] IXCLRDataExceptionNotification notify);

    /*     HRESULT Request([in] ULONG32 reqCode,
                        [in] ULONG32 inBufferSize,
                        [in, size_is(inBufferSize)] BYTE* inBuffer,
                        [in] ULONG32 outBufferSize,
                        [out, size_is(outBufferSize)] BYTE* outBuffer); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int Request(
      uint reqCode,
      uint inBufferSize,
      [MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 1)] [In] byte[] inBuffer,
      uint outBufferSize,
      [MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 3)] [Out] byte[] outBuffer);

    /*
     * Create a simple value based on the given
     * type and address information.
     */
    /*     HRESULT CreateMemoryValue([in] IXCLRDataAppDomain* appDomain,
                                  [in] IXCLRDataTask* tlsTask,
                                  [in] IXCLRDataTypeInstance* type,
                                  [in] CLRDATA_ADDRESS addr,
                                  [out] IXCLRDataValue** value); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int CreateMemoryValue_dummy();

    /*
     * Update all existing notifications for a module.
     * If module is NULL all modules are affected.
     */
    /*     HRESULT SetAllTypeNotifications(IXCLRDataModule* mod,
                                        ULONG32 flags); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int SetAllTypeNotifications_notImpl();

    /*     HRESULT SetAllCodeNotifications(IXCLRDataModule* mod,
                                        ULONG32 flags); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int SetAllCodeNotifications_notImpl();

    /*
     * Request notification when a type is
     * loaded or unloaded.
     * If mods is NULL singleMod is used as
     * the module for all tokens.
     * If flags is NULL singleFlags is used as
     * the flags for all tokens.
     */
    /*     HRESULT GetTypeNotifications([in] ULONG32 numTokens,
                                     [in, size_is(numTokens)]
                                     IXCLRDataModule* mods[],
                                     [in] IXCLRDataModule* singleMod,
                                     [in, size_is(numTokens)] mdTypeDef tokens[],
                                     [out, size_is(numTokens)] ULONG32 flags[]); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetTypeNotifications_notImplemented();

    /*     HRESULT SetTypeNotifications([in] ULONG32 numTokens,
                                     [in, size_is(numTokens)]
                                     IXCLRDataModule* mods[],
                                     [in] IXCLRDataModule* singleMod,
                                     [in, size_is(numTokens)] mdTypeDef tokens[],
                                     [in, size_is(numTokens)] ULONG32 flags[],
                                     [in] ULONG32 singleFlags); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int SetTypeNotifications_notImplemented();

    /*
     * Request notification when code is generated or
     * discarded for a method.
     * If mods is NULL singleMod is used as
     * the module for all tokens.
     * If flags is NULL singleFlags is used as
     * the flags for all tokens.
     */
    /*     HRESULT GetCodeNotifications([in] ULONG32 numTokens,
                                     [in, size_is(numTokens)]
                                     IXCLRDataModule* mods[],
                                     [in] IXCLRDataModule* singleMod,
                                     [in, size_is(numTokens)] mdMethodDef tokens[],
                                     [out, size_is(numTokens)] ULONG32 flags[]); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetCodeNotifications_dummy();

    /*     HRESULT SetCodeNotifications([in] ULONG32 numTokens,
                                     [in, size_is(numTokens)]
                                     IXCLRDataModule* mods[],
                                     [in] IXCLRDataModule* singleMod,
                                     [in, size_is(numTokens)] mdMethodDef tokens[],
                                     [in, size_is(numTokens)] ULONG32 flags[],
                                     [in] ULONG32 singleFlags); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int SetCodeNotifications_dummy();

    /*
     * Control notifications other than code and
     * type notifications.
     */
    /*     HRESULT GetOtherNotificationFlags([out] ULONG32* flags); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetOtherNotificationFlags([MarshalAs(UnmanagedType.U4)] out DataOtherNotifyFlag flags);

    /*     HRESULT SetOtherNotificationFlags([in] ULONG32 flags); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int SetOtherNotificationFlags([MarshalAs(UnmanagedType.U4)] DataOtherNotifyFlag flags);

    /*
     * Look up method definitions by IL code address.
     */
    /*     HRESULT StartEnumMethodDefinitionsByAddress([in] CLRDATA_ADDRESS address,
                                                    [out] CLRDATA_ENUM* handle); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int StartEnumMethodDefinitionsByAddress_dummy();

    /*     HRESULT EnumMethodDefinitionByAddress([in] CLRDATA_ENUM* handle,
                                              [out] IXCLRDataMethodDefinition** method); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int EnumMethodDefinitionByAddress_dummy();

    /*     HRESULT EndEnumMethodDefinitionsByAddress([in] CLRDATA_ENUM handle); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int EndEnumMethodDefinitionsByAddress_dummy();

    /*
     * Given an address which is a CLR stub
     * (and potentially state from a previous follow)
     * determine the next execution address at which
     * to check whether the stub has been exited.
     * OBSOLETE: Use FollowStub2.
     */
    /*     HRESULT FollowStub([in] ULONG32 inFlags,
                           [in] CLRDATA_ADDRESS inAddr,
                           [in] CLRDATA_FOLLOW_STUB_BUFFER* inBuffer,
                           [out] CLRDATA_ADDRESS* outAddr,
                           [out] CLRDATA_FOLLOW_STUB_BUFFER* outBuffer,
                           [out] ULONG32* outFlags); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int FollowStub_dummy();

    /* Requires revision 7. */
    /*     HRESULT FollowStub2([in] IXCLRDataTask* task,
                            [in] ULONG32 inFlags,
                            [in] CLRDATA_ADDRESS inAddr,
                            [in] CLRDATA_FOLLOW_STUB_BUFFER* inBuffer,
                            [out] CLRDATA_ADDRESS* outAddr,
                            [out] CLRDATA_FOLLOW_STUB_BUFFER* outBuffer,
                            [out] ULONG32* outFlags); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int FollowStub2_dummy();
  }
}