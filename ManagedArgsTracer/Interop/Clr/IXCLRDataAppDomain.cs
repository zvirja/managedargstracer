﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;

#endregion

namespace ManagedArgsTracer.Interop.Clr
{
  [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
  [Guid("7CA04601-C702-4670-A63C-FA44F7DA7BD5")]
  [ComImport]
  internal interface IXCLRDataAppDomain
  {
    /*
     * Get the process that contains this app domain.
     */
    /*     HRESULT GetProcess([out] IXCLRDataProcess** process); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetProcess_dummy();

    /*
     * Get the app domain's name.
     */
    /*     HRESULT GetName([in] ULONG32 bufLen,
                        [out] ULONG32 *nameLen,
                        [out, size_is(bufLen)] WCHAR name[]); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetName_dummy();

    /* 
     * Get a unique, stable identifier for this object.
     */
    /*     HRESULT GetUniqueID([out] ULONG64* id); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetUniqueID(out ulong id);

    /*
     * Get state flags, defined in CLRDataAppDomainFlag.
     */
    /*     HRESULT GetFlags([out] ULONG32* flags); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetFlags_dummy();

    /*
     * Determine whether the given interface represents
     * the same target state.
     */
    /*     HRESULT IsSameObject([in] IXCLRDataAppDomain* appDomain); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int IsSameObject_dummy();

    /*
     * Get the managed object representing the app domain.
     */
    /*     HRESULT GetManagedObject([out] IXCLRDataValue** value); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetManagedObject_dummy();

    /*     HRESULT Request([in] ULONG32 reqCode,
                        [in] ULONG32 inBufferSize,
                        [in, size_is(inBufferSize)] BYTE* inBuffer,
                        [in] ULONG32 outBufferSize,
                        [out, size_is(outBufferSize)] BYTE* outBuffer); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int Request_dummy();
  }
}