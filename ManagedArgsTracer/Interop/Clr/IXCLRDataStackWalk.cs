﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;

using ManagedArgsTracer.Interop.Clr;

#endregion

namespace ManagedArgsTracer.Interop.Clr
{
  internal enum CLRDataDetailedFrameType : uint
  {
    CLRDATA_DETFRAME_UNRECOGNIZED = 0,

    CLRDATA_DETFRAME_UNKNOWN_STUB = CLRDATA_DETFRAME_UNRECOGNIZED + 1,

    CLRDATA_DETFRAME_CLASS_INIT = CLRDATA_DETFRAME_UNKNOWN_STUB + 1,

    CLRDATA_DETFRAME_EXCEPTION_FILTER = CLRDATA_DETFRAME_CLASS_INIT + 1,

    CLRDATA_DETFRAME_SECURITY = CLRDATA_DETFRAME_EXCEPTION_FILTER + 1,

    CLRDATA_DETFRAME_CONTEXT_POLICY = CLRDATA_DETFRAME_SECURITY + 1,

    CLRDATA_DETFRAME_INTERCEPTION = CLRDATA_DETFRAME_CONTEXT_POLICY + 1,

    CLRDATA_DETFRAME_PROCESS_START = CLRDATA_DETFRAME_INTERCEPTION + 1,

    CLRDATA_DETFRAME_THREAD_START = CLRDATA_DETFRAME_PROCESS_START + 1,

    CLRDATA_DETFRAME_TRANSITION_TO_MANAGED = CLRDATA_DETFRAME_THREAD_START + 1,

    CLRDATA_DETFRAME_TRANSITION_TO_UNMANAGED = CLRDATA_DETFRAME_TRANSITION_TO_MANAGED + 1,

    CLRDATA_DETFRAME_COM_INTEROP_STUB = CLRDATA_DETFRAME_TRANSITION_TO_UNMANAGED + 1,

    CLRDATA_DETFRAME_DEBUGGER_EVAL = CLRDATA_DETFRAME_COM_INTEROP_STUB + 1,

    CLRDATA_DETFRAME_CONTEXT_SWITCH = CLRDATA_DETFRAME_DEBUGGER_EVAL + 1,

    CLRDATA_DETFRAME_FUNC_EVAL = CLRDATA_DETFRAME_CONTEXT_SWITCH + 1,

    CLRDATA_DETFRAME_FINALLY = CLRDATA_DETFRAME_FUNC_EVAL + 1
  };
}

[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
[Guid("E59D8D22-ADA7-49a2-89B5-A415AFCFC95F")]
[ComImport]
internal interface IXCLRDataStackWalk
{
  /* 
     * Get the current context of this stack walk.
     * This is the original context with any unwinding
     * applied to it.  As unwinding may only restore
     * a subset of the registers, such as only non-volatile
     * registers, the context may not exactly match the
     * register state at the time of the actual call.
     */
  /*     HRESULT GetContext([in] ULONG32 contextFlags,
                           [in] ULONG32 contextBufSize,
                           [out] ULONG32* contextSize,
                           [out, size_is(contextBufSize)] BYTE contextBuf[]); */

  [MethodImpl(MethodImplOptions.PreserveSig)]
  int GetContext_dummy();

  /* 
     * Change the current context of this stack walk, allowing the
     * debugger to move it to an arbitrary context. Does not actually
     * alter the current context of the thread whose stack is being walked.
     * OBSOLETE: Use SetContext2.
     */
  /*     HRESULT SetContext([in] ULONG32 contextSize,
                           [in, size_is(contextSize)] BYTE context[]); */

  [MethodImpl(MethodImplOptions.PreserveSig)]
  int SetContext_dummy();

  /*
     * Attempt to advance the stack walk to the next frame that
     * matches the stack walk's filter. If the current frame type is
     * CLRDATA_UNRECOGNIZED_FRAME, Next() will be unable to
     * advance. (The debugger will have to walk the unrecognized frame
     * itself, reset the walk's context, and try again.)
     *
     * Upon creation, the stack walk is positioned "before" the first
     * frame on the stack.  Debuggers must call Next() to advance to
     * the first frame before any other functions will work. The
     * function will output S_FALSE when there are no more frames that
     * meet its filter criteria.
     */
  /*     HRESULT Next(); */

  [MethodImpl(MethodImplOptions.PreserveSig)]
  int Next();

  /*
     * Return the number of bytes skipped by the last call to Next().
     * If Next() moved to the very next frame, outputs 0.
     *
     * Note that calling GetStackSizeSkipped() after any function other
     * than Next() has no meaning.
     */
  /*     HRESULT GetStackSizeSkipped([out] ULONG64* stackSizeSkipped); */

  [MethodImpl(MethodImplOptions.PreserveSig)]
  int GetStackSizeSkipped(out ulong stackSizeSkipped);

  /* 
     * Return information about the type of the current frame
     */
  /*     HRESULT GetFrameType([out] CLRDataSimpleFrameType* simpleType,
                             [out] CLRDataDetailedFrameType* detailedType); */

  [MethodImpl(MethodImplOptions.PreserveSig)]
  int GetFrameType(out CLRDataSimpleFrameType simpleType, out CLRDataDetailedFrameType detailedType);

  /*
     * Return the current frame, if it is recognized.
     */
  /*     HRESULT GetFrame([out] IXCLRDataFrame** frame); */

  [MethodImpl(MethodImplOptions.PreserveSig)]
  int GetFrame([MarshalAs(UnmanagedType.Interface)] [Out] out IXCLRDataFrame frame);

  /*     HRESULT Request([in] ULONG32 reqCode,
                        [in] ULONG32 inBufferSize,
                        [in, size_is(inBufferSize)] BYTE* inBuffer,
                        [in] ULONG32 outBufferSize,
                        [out, size_is(outBufferSize)] BYTE* outBuffer); */

  [MethodImpl(MethodImplOptions.PreserveSig)]
  int Request_dummy();

  /* 
     * Change the current context of this stack walk, allowing the
     * debugger to move it to an arbitrary context. Does not actually
     * alter the current context of the thread whose stack is being walked.
     */
  /*     HRESULT SetContext2([in] ULONG32 flags,
                            [in] ULONG32 contextSize,
                            [in, size_is(contextSize)] BYTE context[]); */

  [MethodImpl(MethodImplOptions.PreserveSig)]
  int SetContext2_dummy();
}