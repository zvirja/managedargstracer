﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;

#endregion

namespace ManagedArgsTracer.Interop.Clr
{
  [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
  [Guid("31201a94-4337-49b7-aef7-0c755054091f")]
  [ComImport]
  internal interface IXCLRDataExceptionNotification2
  {
    /*
     * The given app domain was loaded or unloaded.
     */
    /*     HRESULT OnAppDomainLoaded([in] IXCLRDataAppDomain* domain); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int OnAppDomainLoaded_dummy();

    /*     HRESULT OnAppDomainUnloaded([in] IXCLRDataAppDomain* domain); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int OnAppDomainUnloaded_dummy();

    /*
     * A managed exception has been raised.
     */
    /*     HRESULT OnException([in] IXCLRDataExceptionState* exception); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int OnException_dummy();
  }
}