﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;

using Microsoft.Diagnostics.Runtime.Interop;

#endregion

namespace ManagedArgsTracer.Interop.Clr
{
  [Guid("6d05fae3-189c-4630-a6dc-1c251e1c01ab")]
  [ComImport]
  [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
  internal interface IClrDataTarget2 : IClrDataTarget
  {
    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetMachineType(out IMAGE_FILE_MACHINE machineType);

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetPointerSize(out uint pointerSize);

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetImageBase([MarshalAs(UnmanagedType.LPWStr)] [In] string imagePath, out ulong baseAddress);

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int ReadVirtual(ulong address, [MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 2)] [Out] byte[] buffer, uint bytesRequested, out uint bytesRead);

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int WriteVirtual(ulong address, [MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 2)] [In] byte[] buffer, uint bytesRequested, out uint bytesWritten);

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetTLSValue(uint threadID, uint index, out ulong value);

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int SetTLSValue(uint threadID, uint index, ulong value);

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetCurrentThreadID(out uint threadID);

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetThreadContext(uint threadID, uint contextFlags, uint contextSize, IntPtr context);

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int SetThreadContext(uint threadID, uint contextSize, IntPtr context);

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int Request(uint reqCode, uint inBufferSize, IntPtr inBuffer, IntPtr outBufferSize, out IntPtr outBuffer);

    /* 
         HRESULT AllocVirtual([in] CLRDATA_ADDRESS addr,
                         [in] ULONG32 size,
                         [in] ULONG32 typeFlags,
                         [in] ULONG32 protectFlags,
                         [out] CLRDATA_ADDRESS* virt)    */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int AllocVirtual(ulong address, uint size, uint typeFlags, uint protectFlags, out ulong ptrToVirtual);

    /*HRESULT FreeVirtual([in] CLRDATA_ADDRESS addr,
                            [in] ULONG32 size,
                            [in] ULONG32 typeFlags);*/

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int FreeVirtual(ulong address, uint size, int typeFlags);
  }
}