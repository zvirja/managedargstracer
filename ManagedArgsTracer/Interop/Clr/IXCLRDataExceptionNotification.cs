﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;

#endregion

namespace ManagedArgsTracer.Interop.Clr
{
  [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
  [Guid("2D95A079-42A1-4837-818F-0B97D7048E0E")]
  [ComImport]
  internal interface IXCLRDataExceptionNotification
  {
    /*
     * New code was generated or discarded for a method.
     */
    /*     HRESULT OnCodeGenerated([in] IXCLRDataMethodInstance* method); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int OnCodeGenerated([MarshalAs(UnmanagedType.Interface)] IXCLRDataMethodInstance method);

    /*     HRESULT OnCodeDiscarded([in] IXCLRDataMethodInstance* method); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int OnCodeDiscarded([MarshalAs(UnmanagedType.Interface)] IXCLRDataMethodInstance method);

    /*
     * The process or task reached the desired execution state.
     */
    /*     HRESULT OnProcessExecution([in] ULONG32 state); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int OnProcessExecution(uint state);

    /*     HRESULT OnTaskExecution([in] IXCLRDataTask* task,
                                [in] ULONG32 state); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int OnTaskExecution([MarshalAs(UnmanagedType.Interface)] IXCLRDataTask task, uint state);

    /*
     * The given module was loaded or unloaded.
     */
    /*     HRESULT OnModuleLoaded([in] IXCLRDataModule* mod); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int OnModuleLoaded([MarshalAs(UnmanagedType.Interface)] IXCLRDataModule module);

    /*     HRESULT OnModuleUnloaded([in] IXCLRDataModule* mod); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int OnModuleUnloaded([MarshalAs(UnmanagedType.Interface)] IXCLRDataModule module);

    /*
     * The given type was loaded or unloaded.
     */
    /*     HRESULT OnTypeLoaded([in] IXCLRDataTypeInstance* typeInst); */

    /// <summary>
    /// NOT IMPLEMENTED
    /// </summary>
    [MethodImpl(MethodImplOptions.PreserveSig)]
    int OnTypeLoaded([MarshalAs(UnmanagedType.Interface)] IXCLRDataTypeInstance typeInstance);

    /*     HRESULT OnTypeUnloaded([in] IXCLRDataTypeInstance* typeInst); */

    /// <summary>
    /// NOT IMPLEMENTED
    /// </summary>
    [MethodImpl(MethodImplOptions.PreserveSig)]
    int OnTypeUnloaded([MarshalAs(UnmanagedType.Interface)] IXCLRDataTypeInstance typeInstance);
  }
}