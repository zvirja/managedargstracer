﻿#region

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

using ManagedArgsTracer.Diagnostics;
using ManagedArgsTracer.Helpers;
using ManagedArgsTracer.Interop.Native;
using ManagedArgsTracer.Session;

using Microsoft.Diagnostics.Runtime.Interop;

#endregion

namespace ManagedArgsTracer.Interop.Clr
{
  internal class CustomClrDataTargetImpl : IClrDataTarget2
  {
    #region Constructors and Destructors

    public CustomClrDataTargetImpl(SessionManager sessionManager)
    {
      this.SessionManagerRef = sessionManager;
    }

    #endregion

    #region Public Properties

    public SessionManager SessionManagerRef { get; set; }

    #endregion

    #region Public Methods and Operators

    public int AllocVirtual(ulong address, uint size, uint typeFlags, uint protectFlags, out ulong ptrToVirtual)
    {
      ulong procHandle;
      this.SessionManagerRef.NativeDebugInterfaces.DebugSystemObjects.GetCurrentProcessHandle(out procHandle);

      IntPtr result = NativeMethods.VirtualAllocEx((IntPtr)procHandle, (IntPtr)address, new IntPtr(size), typeFlags, protectFlags);
      int lastWin32Error = Marshal.GetLastWin32Error();
      ptrToVirtual = (ulong)result.ToInt64();

      return result != IntPtr.Zero ? 0 : lastWin32Error;
    }

    public int FreeVirtual(ulong address, uint size, int typeFlags)
    {
      ulong procHandle;
      this.SessionManagerRef.NativeDebugInterfaces.DebugSystemObjects.GetCurrentProcessHandle(out procHandle);
      var result = NativeMethods.VirtualFreeEx((IntPtr)procHandle, (IntPtr)address, new IntPtr(size), typeFlags);
      return result ? 0 : Marshal.GetLastWin32Error();
    }

    public int GetCurrentThreadID(out uint threadID)
    {
      uint currentThreadID;
      this.SessionManagerRef.NativeDebugInterfaces.DebugSystemObjects.GetCurrentThreadId(out currentThreadID);
      threadID = currentThreadID;
      return 0;
    }

    public int GetImageBase(string imagePath, out ulong baseAddress)
    {
      imagePath = Path.GetFileNameWithoutExtension(imagePath);
      imagePath += "!";
      ulong baseAddr = 0;
      IDebugSymbols3 debugSymbols3 = this.SessionManagerRef.NativeDebugInterfaces.DebugSymbols3;
      if (debugSymbols3 != null)
      {
        debugSymbols3.GetSymbolModule(imagePath, out baseAddr);
      }
      baseAddress = baseAddr;
      return debugSymbols3 != null ? 0 : -1;
    }

    public int GetMachineType(out IMAGE_FILE_MACHINE machineType)
    {
      IMAGE_FILE_MACHINE procType;
      int result = this.SessionManagerRef.NativeDebugInterfaces.DebugControl.GetExecutingProcessorType(out procType);
      if (result != 0)
      {
        Log.Error("Unable to get valid machine type. Response code: {0}".FormatWith(result), null, this);
        machineType = IMAGE_FILE_MACHINE.UNKNOWN;
        return -1;
      }
      else
      {
        machineType = procType;
        return 0;
      }
    }

    public int GetPointerSize(out uint pointerSize)
    {
      int isPointer64Bit = this.SessionManagerRef.NativeDebugInterfaces.DebugControl.IsPointer64Bit();
      //If x64 - returns S_OK - 0
      pointerSize = isPointer64Bit == 0 ? 8u : 4u;
      return 0;
    }

    public int GetTLSValue(uint threadID, uint index, out ulong value)
    {
      value = 0L;
      return 0;
    }

    public int GetThreadContext(uint threadID, uint contextFlags, uint contextSize, IntPtr context)
    {
      uint engineThreadId;
      this.SessionManagerRef.NativeDebugInterfaces.DebugSystemObjects.GetThreadIdBySystemId(threadID, out engineThreadId);
      this.SessionManagerRef.NativeDebugInterfaces.DebugSystemObjects.SetCurrentThreadId(engineThreadId);
      this.SessionManagerRef.NativeDebugInterfaces.DebugAdvanced.GetThreadContext(context, contextSize);
      return 0;
    }

    public int ReadVirtual(ulong address, byte[] buffer, uint bytesRequested, out uint bytesRead)
    {
      if (buffer.Length < bytesRequested)
      {
        bytesRequested = (uint)buffer.Length;
      }
      uint actualBytesRead = 0U;
      int result = this.SessionManagerRef.NativeDebugInterfaces.DebugDataSpaces.ReadVirtual(address, buffer, bytesRequested, out actualBytesRead);
      bytesRead = actualBytesRead;
      return result;
    }

    public int Request(uint reqCode, uint inBufferSize, IntPtr inBuffer, IntPtr outBufferSize, out IntPtr outBuffer)
    {
      outBuffer = IntPtr.Zero;
      this.LogUnexpectedMethodCall(System.Reflection.MethodBase.GetCurrentMethod().Name);
      return -1;
    }

    public int SetTLSValue(uint threadID, uint index, ulong value)
    {
      this.LogUnexpectedMethodCall(System.Reflection.MethodBase.GetCurrentMethod().Name);
      return -1;
    }

    public int SetThreadContext(uint threadID, uint contextSize, IntPtr context)
    {
      this.LogUnexpectedMethodCall(System.Reflection.MethodBase.GetCurrentMethod().Name);
      return -1;
    }

    public int WriteVirtual(ulong address, byte[] buffer, uint bytesRequested, out uint bytesWritten)
    {
      uint actualWritten;
      var result = this.SessionManagerRef.NativeDebugInterfaces.DebugDataSpaces.WriteVirtual(address, buffer, bytesRequested, out actualWritten);
      bytesWritten = actualWritten;
      return result;
    }

    #endregion

    #region Methods

    private void LogUnexpectedMethodCall(string methodName)
    {
      Log.Error("Unexpected call of method {0} in ClrDataTargetImpl".FormatWith(methodName), null, this);
      Tracer.PublicOutput("Unexpected behavior. Application might not work as expected. Please provide developer with log file.");
    }

    #endregion
  }
}