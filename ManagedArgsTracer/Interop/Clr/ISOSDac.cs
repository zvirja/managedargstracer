﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;

#endregion

namespace ManagedArgsTracer.Interop.Clr
{
  [Guid("436f00f2-b42a-4b9f-870c-e73db66ae930")]
  [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
  [ComImport]
  internal interface ISOSDac
  {
    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetThreadStoreData_dummy(out object data);

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetAppDomainStoreData_dummy(out object data);

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetAppDomainList(uint count, [MarshalAs(UnmanagedType.LPArray)] [Out] ulong[] values, out uint pNeeded);

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetAppDomainData_dummy(ulong addr, out object data);

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetAppDomainName(ulong addr, uint count, [Out] StringBuilder lpFilename, out uint pNeeded);

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetDomainFromContext(ulong context, out ulong domain);

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetAssemblyList(ulong appDomain, int count, [MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 1)] [Out] ulong[] values, out int pNeeded);

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetAssemblyData_dummy(ulong baseDomainPtr, ulong assembly, out object data);

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetAssemblyName(ulong assembly, uint count, [Out] StringBuilder name, out uint pNeeded);

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetModule(ulong addr, [MarshalAs(UnmanagedType.IUnknown)] out object module);

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetModuleData_dummy(ulong moduleAddr, out object data);

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int TraverseModuleMap_dummy(int mmt, ulong moduleAddr, [MarshalAs(UnmanagedType.FunctionPtr)] [In] object pCallback, IntPtr token);

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetAssemblyModuleList(ulong assembly, uint count, [MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 1)] [Out] ulong[] modules, out uint pNeeded);

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetILForModule(ulong moduleAddr, uint rva, out ulong il);

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetThreadData_dummy(ulong thread, out object data);

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetThreadFromThinlockID(uint thinLockId, out ulong pThread);

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetStackLimits(ulong threadPtr, out ulong lower, out ulong upper, out ulong fp);

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetMethodDescData_dummy(ulong methodDesc, ulong ip, out object data, uint cRevertedRejitVersions, object[] rgRevertedRejitData, out ulong pcNeededRevertedRejitData);

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetMethodDescPtrFromIP(ulong ip, out ulong ppMD);

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetMethodDescName(ulong methodDesc, uint count, [Out] StringBuilder name, out uint pNeeded);

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetMethodDescPtrFromFrame(ulong frameAddr, out ulong ppMD);

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetMethodDescFromToken(ulong moduleAddr, uint token, out ulong methodDesc);

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetMethodDescTransparencyData_do_not_use();

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetCodeHeaderData_dummy(ulong ip, out object data);

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetJitManagerList_dummy(uint count, [MarshalAs(UnmanagedType.LPArray)] [Out] object[] jitManagers, out uint pNeeded);

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetJitHelperFunctionName(ulong ip, uint count, char name, out uint pNeeded);

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetJumpThunkTarget_do_not_use(uint ctx, out ulong targetIP, out ulong targetMD);

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetThreadpoolData_dummy(out object data);

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetWorkRequestData_dummy(ulong addrWorkRequest, out object data);

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetHillClimbingLogEntry_do_not_use();

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetObjectData_dummy(ulong objAddr, out object data);

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetObjectStringData(ulong obj, uint count, [Out] StringBuilder stringData, out uint pNeeded);

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetObjectClassName(ulong obj, uint count, [Out] StringBuilder className, out uint pNeeded);

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetMethodTableName(ulong mt, uint count, [Out] StringBuilder mtName, out uint pNeeded);

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetMethodTableData_dummy(ulong mt, out object data);

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetMethodTableSlot(ulong mt, uint slot, out ulong value);

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetMethodTableFieldData_dummy(ulong mt, out object data);

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetMethodTableTransparencyData_do_not_use();

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetMethodTableForEEClass(ulong eeClass, out ulong value);

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetFieldDescData_dummy(ulong fieldDesc, out object data);

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetFrameName(ulong vtable, uint count, [Out] StringBuilder frameName, out uint pNeeded);

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetPEFileBase(ulong addr, out ulong baseAddr);

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetPEFileName(ulong addr, uint count, [Out] StringBuilder ptr, out uint pNeeded);

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetGCHeapData_dummy(out object data);

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetGCHeapList(uint count, [MarshalAs(UnmanagedType.LPArray)] [Out] ulong[] heaps, out uint pNeeded);

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetGCHeapDetails_dummy(ulong heap, out object details);

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetGCHeapStaticData_dummy(out object data);

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetHeapSegmentData_dummy(ulong seg, out object data);

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetOOMData_do_not_use();

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetOOMStaticData_do_not_use();

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetHeapAnalyzeData_do_not_use();

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetHeapAnalyzeStaticData_do_not_use();

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetDomainLocalModuleData_do_not_use();

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetDomainLocalModuleDataFromAppDomain_dummy(ulong appDomainAddr, int moduleID, out object data);

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetDomainLocalModuleDataFromModule_dummy(ulong moduleAddr, out object data);

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetThreadLocalModuleData_dummy(ulong thread, uint index, out object data);

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetSyncBlockData_dummy(uint number, out object data);

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetSyncBlockCleanupData_do_not_use();

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetHandleEnum([MarshalAs(UnmanagedType.IUnknown)] out object ppHandleEnum);

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetHandleEnumForTypes([In] uint[] types, uint count, [MarshalAs(UnmanagedType.IUnknown)] out object ppHandleEnum);

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetHandleEnumForGC(uint gen, [MarshalAs(UnmanagedType.IUnknown)] out object ppHandleEnum);

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int TraverseEHInfo_do_not_use();

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetNestedExceptionData(ulong exception, out ulong exceptionObject, out ulong nextNestedException);

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetStressLogAddress(out ulong stressLog);

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int TraverseLoaderHeap(ulong loaderHeapAddr, IntPtr pCallback);

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetCodeHeapList_dummy(ulong jitManager, uint count, [MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 1)] [Out] object[] codeHeaps, out uint pNeeded);

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int TraverseVirtCallStubHeap(ulong pAppDomain, uint heaptype, IntPtr pCallback);

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetUsefulGlobals_dummy(out object data);

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetClrWatsonBuckets(ulong thread, out IntPtr pGenericModeBlock);

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetTLSIndex(out uint pIndex);

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetDacModuleHandle(out IntPtr phModule);

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetRCWData_dummy(ulong addr, out object data);

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetRCWInterfaces__dummy(ulong rcw, uint count, [MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 1)] [Out] object[] interfaces, out uint pNeeded);

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetCCWData_dummy(ulong ccw, out object data);

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetCCWInterfaces_dummy(ulong ccw, uint count, [MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 1)] [Out] object[] interfaces, out uint pNeeded);

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int TraverseRCWCleanupList_do_not_use();

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetStackReferences(uint osThreadID, [MarshalAs(UnmanagedType.IUnknown)] out object ppEnum);

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetRegisterName(int regName, uint count, [Out] StringBuilder buffer, out uint pNeeded);

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetThreadAllocData_dummy(ulong thread, ref object data);

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetHeapAllocData_dummy(uint count, [MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 1)] [Out] object[] data, out uint pNeeded);

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetFailedAssemblyList(ulong appDomain, int count, ulong[] values, out uint pNeeded);

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetPrivateBinPaths(ulong appDomain, int count, [Out] StringBuilder paths, out uint pNeeded);

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetAssemblyLocation(ulong assembly, int count, [Out] StringBuilder location, out uint pNeeded);

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetAppDomainConfigFile(ulong appDomain, int count, [Out] StringBuilder configFile, out uint pNeeded);

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetApplicationBase(ulong appDomain, int count, [Out] StringBuilder appBase, out uint pNeeded);

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetFailedAssemblyData(ulong assembly, out uint pContext, out int pResult);

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetFailedAssemblyLocation(ulong assesmbly, uint count, [Out] StringBuilder location, out uint pNeeded);

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetFailedAssemblyDisplayName(ulong assembly, uint count, [Out] StringBuilder name, out uint pNeeded);
  }
}