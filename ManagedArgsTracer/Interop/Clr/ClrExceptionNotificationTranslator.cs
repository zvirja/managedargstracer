﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ManagedArgsTracer.Runtime.Wrappers;

using Microsoft.Diagnostics.Runtime.Interop;

#endregion

namespace ManagedArgsTracer.Interop.Clr
{
  internal class ClrExceptionNotificationTranslator
  {
    #region Constants

    public const uint CLRDATA_NOTIFY_EXCEPTION = 0xe0444143;

    #endregion

    #region Constructors and Destructors

    public ClrExceptionNotificationTranslator(XClrDataProcessWrapper dataProcess)
    {
      this.DataProcess = dataProcess;
    }

    #endregion

    #region Properties

    private XClrDataProcessWrapper DataProcess { get; set; }

    #endregion

    #region Public Methods and Operators

    public void TranslateExceptionNotification(ref EXCEPTION_RECORD64 exceptionRecord, IXCLRDataExceptionNotification notificationReceiver)
    {
      if (exceptionRecord.ExceptionCode != CLRDATA_NOTIFY_EXCEPTION)
      {
        return;
      }
      this.DataProcess.TranslateExceptionRecordToNotification(ref exceptionRecord, notificationReceiver);
    }

    #endregion
  }
}