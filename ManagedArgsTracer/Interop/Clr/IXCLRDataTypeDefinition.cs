﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;

#endregion

namespace ManagedArgsTracer.Interop.Clr
{
  [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
  [Guid("4675666C-C275-45b8-9F6C-AB165D5C1E09")]
  [ComImport]
  internal interface IXCLRDataTypeDefinition
  {
    /*
     * Get the module this type is part of.
     */
    /*     HRESULT GetModule([out] IXCLRDataModule **mod); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetModule([MarshalAs(UnmanagedType.Interface)] [Out] out IXCLRDataModule module);

    /*
     * Enumerate the methods for this type.
     */
    /*     HRESULT StartEnumMethodDefinitions([out] CLRDATA_ENUM* handle); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int StartEnumMethodDefinitions(out ulong handle);

    /*     HRESULT EnumMethodDefinition([in, out] CLRDATA_ENUM* handle,
                                     [out] IXCLRDataMethodDefinition **methodDefinition); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int EnumMethodDefinition(ref ulong handle, [MarshalAs(UnmanagedType.Interface)] [Out] out IXCLRDataMethodDefinition methodDefinition);

    /*     HRESULT EndEnumMethodDefinitions([in] CLRDATA_ENUM handle); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int EndEnumMethodDefinitions(ulong handle);

    /*
     * Look up methods by name.
     */
    /*     HRESULT StartEnumMethodDefinitionsByName([in] LPCWSTR name,
                                                 [in] ULONG32 flags,
                                                 [out] CLRDATA_ENUM* handle); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int StartEnumMethodDefinitionsByName([MarshalAs(UnmanagedType.LPWStr)] [Out] string name, uint flags, out ulong handle);

    /*     HRESULT EnumMethodDefinitionByName([in,out] CLRDATA_ENUM* handle,
                                           [out] IXCLRDataMethodDefinition** method); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int EnumMethodDefinitionByName(ref ulong handle, [MarshalAs(UnmanagedType.Interface)] [Out] out IXCLRDataMethodDefinition methodDef);

    /*     HRESULT EndEnumMethodDefinitionsByName([in] CLRDATA_ENUM handle); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int EndEnumMethodDefinitionsByName(ulong handle);

    /*
     * Get a method definition by metadata token.
     */
    /*     HRESULT GetMethodDefinitionByToken([in] mdMethodDef token,
                                           [out] IXCLRDataMethodDefinition** methodDefinition); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetMethodDefinitionByToken_dummy();

    /*
     * Enumerate instances of this definition.
     */
    /*     HRESULT StartEnumInstances([in] IXCLRDataAppDomain* appDomain,
                                   [out] CLRDATA_ENUM* handle); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int StartEnumInstances([MarshalAs(UnmanagedType.Interface)] IXCLRDataAppDomain appDomain, out ulong handle);

    /*     HRESULT EnumInstance([in, out] CLRDATA_ENUM* handle,
                             [out] IXCLRDataTypeInstance **instance); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int EnumInstance(ref ulong handle, [MarshalAs(UnmanagedType.Interface)] [Out] out IXCLRDataTypeInstance instance);

    /*     HRESULT EndEnumInstances([in] CLRDATA_ENUM handle); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int EndEnumInstances(ulong handle);

    /*
     * Get the namespace-qualified name for this type definition.
     */
    /*     HRESULT GetName([in] ULONG32 flags,
                        [in] ULONG32 bufLen,
                        [out] ULONG32 *nameLen,
                        [out, size_is(bufLen)] WCHAR nameBuf[]); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetName(uint flags, uint bufLen, out uint realLen, [MarshalAs(UnmanagedType.LPWStr)] StringBuilder nameBuf);

    /*
     * Get the metadata token and scope.
     */
    /*     HRESULT GetTokenAndScope([out] mdTypeDef* token,
                                 [out] IXCLRDataModule **mod); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetTokenAndScope([Out] out uint token, [MarshalAs(UnmanagedType.Interface)] [Out] out IXCLRDataModule module);

    /*
     * Get standard element type.
     */
    /*     HRESULT GetCorElementType([out] CorElementType* type); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetCorElementType_dummy();

    /*
     * Get state flags, defined in CLRDataTypeFlag.
     */
    /*     HRESULT GetFlags([out] ULONG32* flags); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetFlags_dummy();

    /*
     * Determine whether the given interface represents
     * the same target state.
     */
    /*     HRESULT IsSameObject([in] IXCLRDataTypeDefinition* type); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int IsSameObject([MarshalAs(UnmanagedType.Interface)] IXCLRDataTypeDefinition type);

    /*     HRESULT Request([in] ULONG32 reqCode,
                        [in] ULONG32 inBufferSize,
                        [in, size_is(inBufferSize)] BYTE* inBuffer,
                        [in] ULONG32 outBufferSize,
                        [out, size_is(outBufferSize)] BYTE* outBuffer); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int Request_dummy();

    /*
     * If this is an array type, return the number
     * of dimensions in the array.
     */
    /*     HRESULT GetArrayRank([out] ULONG32* rank); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetArrayRank_dummy();

    /*
     * Get the base type of this type, if any.
     */
    /*     HRESULT GetBase([out] IXCLRDataTypeDefinition** base); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetBase([MarshalAs(UnmanagedType.Interface)] [Out] out IXCLRDataTypeDefinition baseType);

    /*
     * Get the number of fields in the type.
     */
    /*     HRESULT GetNumFields([in] ULONG32 flags,
                             [out] ULONG32* numFields); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetNumFields_dummy();

    /*
     * Enumerate the fields for this type.
     * OBSOLETE: Use EnumField2.
     */
    /*     HRESULT StartEnumFields([in] ULONG32 flags,
                                [out] CLRDATA_ENUM* handle); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int StartEnumFields(uint flags, out ulong handle);

    /*     HRESULT EnumField([in, out] CLRDATA_ENUM* handle,
                          [in] ULONG32 nameBufLen,
                          [out] ULONG32* nameLen,
                          [out, size_is(nameBufLen)] WCHAR nameBuf[],
                          [out] IXCLRDataTypeDefinition** type,
                          [out] ULONG32* flags,
                          [out] mdFieldDef* token); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int EnumField_dummy();

    /*     HRESULT EndEnumFields([in] CLRDATA_ENUM handle); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int EndEnumFields(ulong handle);

    /*     HRESULT StartEnumFieldsByName([in] LPCWSTR name,
                                      [in] ULONG32 nameFlags,
                                      [in] ULONG32 fieldFlags,
                                      [out] CLRDATA_ENUM* handle); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int StartEnumFieldsByName_dummy();

    /*     HRESULT EnumFieldByName([in, out] CLRDATA_ENUM* handle,
                                [out] IXCLRDataTypeDefinition** type,
                                [out] ULONG32* flags,
                                [out] mdFieldDef* token); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int EnumFieldByName_dummy();

    /*     HRESULT EndEnumFieldsByName([in] CLRDATA_ENUM handle); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int EndEnumFieldsByName_dummy();

    /*
     * Look up information for a specific field by token.
     */
    /*     HRESULT GetFieldByToken([in] mdFieldDef token,
                                [in] ULONG32 nameBufLen,
                                [out] ULONG32* nameLen,
                                [out, size_is(nameBufLen)] WCHAR nameBuf[],
                                [out] IXCLRDataTypeDefinition** type,
                                [out] ULONG32* flags); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetFieldByToken_dummy();

    /*
     * Request notification when the given type is
     * loaded or unloaded.
     */
    /*     HRESULT GetTypeNotification([out] ULONG32* flags); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetTypeNotification_dummy();

    /*     HRESULT SetTypeNotification([in] ULONG32 flags); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int SetTypeNotification_dummy();

    /*     HRESULT EnumField2([in, out] CLRDATA_ENUM* handle,
                           [in] ULONG32 nameBufLen,
                           [out] ULONG32* nameLen,
                           [out, size_is(nameBufLen)] WCHAR nameBuf[],
                           [out] IXCLRDataTypeDefinition** type,
                           [out] ULONG32* flags,
                           [out] IXCLRDataModule** tokenScope,
                           [out] mdFieldDef* token); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int EnumField2_dummy();

    /*     HRESULT EnumFieldByName2([in, out] CLRDATA_ENUM* handle,
                                 [out] IXCLRDataTypeDefinition** type,
                                 [out] ULONG32* flags,
                                 [out] IXCLRDataModule** tokenScope,
                                 [out] mdFieldDef* token); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int EnumFieldByName2_dummy();

    /*     HRESULT GetFieldByToken2([in] IXCLRDataModule* tokenScope,
                                 [in] mdFieldDef token,
                                 [in] ULONG32 nameBufLen,
                                 [out] ULONG32* nameLen,
                                 [out, size_is(nameBufLen)] WCHAR nameBuf[],
                                 [out] IXCLRDataTypeDefinition** type,
                                 [out] ULONG32* flags); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetFieldByToken2_dummy();
  }
}