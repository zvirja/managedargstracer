﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;

#endregion

namespace ManagedArgsTracer.Interop.Clr
{
  internal enum CLRDataMethodFlag : uint
  {
    CLRDATA_METHOD_DEFAULT = 0x00000000,

    // Method has a 'this' pointer.
    CLRDATA_METHOD_HAS_THIS = 0x00000001,
  };

  [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
  [Guid("ECD73800-22CA-4b0d-AB55-E9BA7E6318A5")]
  [ComImport]
  internal interface IXCLRDataMethodInstance
  {
    /*
  /*
     * Get the type instance for this method.
     */
    /* HRESULT GetTypeInstance([out] IXCLRDataTypeInstance **typeInstance);*/

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetTypeInstance([MarshalAs(UnmanagedType.Interface)] [Out] out IXCLRDataTypeInstance typeInstance);

    /*
     * Get the definition that matches this instance.
     */
    /*HRESULT GetDefinition([out] IXCLRDataMethodDefinition **methodDefinition);*/

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetDefinition([MarshalAs(UnmanagedType.Interface)] [Out] out IXCLRDataMethodDefinition methodDefinition);

    /*
     * Get the metadata token and scope.
     */
    /* HRESULT GetTokenAndScope([out] mdMethodDef* token,
                             [out] IXCLRDataModule **mod);*/

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetTokenAndScope(out uint token, [MarshalAs(UnmanagedType.Interface)] out IXCLRDataModule module);

    /*
     * Get the fully qualified name for this method instance.
     */
    /*HRESULT GetName([in] ULONG32 flags,
                    [in] ULONG32 bufLen,
                    [out] ULONG32 *nameLen,
                    [out, size_is(bufLen)] WCHAR nameBuf[]);*/

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetName(uint flags, uint buffLen, out uint realLen, [MarshalAs(UnmanagedType.LPWStr)] [Out] StringBuilder nameBuff);

    /*
     * Get state flags, defined in CLRDataMethodFlag.
     */
    /* HRESULT GetFlags([out] ULONG32* flags);*/

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetFlags(out CLRDataMethodFlag flags);

    /*
     * Determine whether the given interface represents
     * the same target state.
     */
    /* HRESULT IsSameObject([in] IXCLRDataMethodInstance* method);*/

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int IsSameObject_dummy();

    /*
     * Get the EnC version of this instance.
     */
    /*HRESULT GetEnCVersion([out] ULONG32* version);*/

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetEnCVersion_dummy();

    /*
     * Enumerate this method's parameterization.
     */
    /*   HRESULT GetNumTypeArguments([out] ULONG32* numTypeArgs);*/

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetNumTypeArguments_NotImpl(out uint numTypeArgs);

    /* HRESULT GetTypeArgumentByIndex([in] ULONG32 index,
                                   [out] IXCLRDataTypeInstance** typeArg);*/

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetTypeArgumentByIndex_NotImpl(uint index, [MarshalAs(UnmanagedType.Interface)] [Out] out IXCLRDataTypeInstance typeArg);

    /*
     * Access the IL <-> address mapping information.
     */
    /*HRESULT GetILOffsetsByAddress([in] CLRDATA_ADDRESS address,
                                  [in] ULONG32 offsetsLen,
                                  [out] ULONG32 *offsetsNeeded,
                                  [out, size_is(offsetsLen)] 
                                  ULONG32 ilOffsets[]);*/

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetILOffsetsByAddress_dummy();

    /* HRESULT GetAddressRangesByILOffset([in] ULONG32 ilOffset,
                                       [in] ULONG32 rangesLen,
                                       [out] ULONG32 *rangesNeeded,
                                       [out, size_is(rangesLen)] 
                                       CLRDATA_ADDRESS_RANGE addressRanges[]);*/

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetAddressRangesByILOffset_dummy();

    /* HRESULT GetILAddressMap([in] ULONG32 mapLen,
                            [out] ULONG32 *mapNeeded,
                            [out, size_is(mapLen)]
                            CLRDATA_IL_ADDRESS_MAP maps[]);*/

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetILAddressMap_dummy();

    /*
     * Get the native code regions associated with this method.
     */
    /* HRESULT StartEnumExtents([out] CLRDATA_ENUM* handle);*/

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int StartEnumExtents_dummy();

    /*  HRESULT EnumExtent([in, out] CLRDATA_ENUM* handle,
                       [out] CLRDATA_ADDRESS_RANGE* extent);*/

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int EnumExtent_dummy();

    /*HRESULT EndEnumExtents([in] CLRDATA_ENUM handle);*/

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int EndEnumExtents_dummy();

    /* HRESULT Request([in] ULONG32 reqCode,
                    [in] ULONG32 inBufferSize,
                    [in, size_is(inBufferSize)] BYTE* inBuffer,
                    [in] ULONG32 outBufferSize,
                    [out, size_is(outBufferSize)] BYTE* outBuffer);*/

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int Request_dummy();

    /*
     * Gets the most representative start address of
     * the native code for this method.
     * A method may have multiple entry points, so this
     * address is not guaranteed to be hit by all entries.
     * Requires revision 1.
     */
    /*HRESULT GetRepresentativeEntryAddress([out] CLRDATA_ADDRESS* addr);*/

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetRepresentativeEntryAddress(out ulong address);
  }
}