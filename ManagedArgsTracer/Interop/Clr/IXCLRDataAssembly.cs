﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;

#endregion

namespace ManagedArgsTracer.Interop.Clr
{
  [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
  [Guid("2FA17588-43C2-46ab-9B51-C8F01E39C9AC")]
  [ComImport]
  internal interface IXCLRDataAssembly
  {
    /*
    * Enumerate modules in the assembly.
    */
    /*     HRESULT StartEnumModules([out] CLRDATA_ENUM* handle); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int StartEnumModules_dummy();

    /*     HRESULT EnumModule([in, out] CLRDATA_ENUM* handle,
                           [out] IXCLRDataModule **mod); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int EnumModule_dummy();

    /*     HRESULT EndEnumModules([in] CLRDATA_ENUM handle); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int EndEnumModules_dummy();

    /*
     * Get the assembly's base name.
     */
    /*     HRESULT GetName([in] ULONG32 bufLen,
                        [out] ULONG32 *nameLen,
                        [out, size_is(bufLen)] WCHAR name[]); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetName_dummy();

    /*
     * Get the full path and filename for the assembly,
     * if there is one.
     */
    /*     HRESULT GetFileName([in] ULONG32 bufLen,
                            [out] ULONG32 *nameLen,
                            [out, size_is(bufLen)] WCHAR name[]); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetFileName_dummy();

    /*
     * Get state flags, defined in CLRDataAssemblyFlag.
     */
    /*     HRESULT GetFlags([out] ULONG32* flags); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetFlags_dummy();

    /*
     * Determine whether the given interface represents
     * the same target state.
     */
    /*     HRESULT IsSameObject([in] IXCLRDataAssembly* assembly); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int IsSameObject_dummy();

    /*     HRESULT Request([in] ULONG32 reqCode,
                        [in] ULONG32 inBufferSize,
                        [in, size_is(inBufferSize)] BYTE* inBuffer,
                        [in] ULONG32 outBufferSize,
                        [out, size_is(outBufferSize)] BYTE* outBuffer); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int Request_dummy();

    /*
     * Enumerate the app domains using this assembly.
     */
    /*     HRESULT StartEnumAppDomains([out] CLRDATA_ENUM* handle); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int StartEnumAppDomains_dummy();

    /*     HRESULT EnumAppDomain([in, out] CLRDATA_ENUM* handle,
                              [out] IXCLRDataAppDomain** appDomain); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int EnumAppDomain_dummy();

    /*     HRESULT EndEnumAppDomains([in] CLRDATA_ENUM handle); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int EndEnumAppDomains_dummy();

    /*
     * Get the metadata display name for the assembly.
     * Requires revision 2.
     */
    /*     HRESULT GetDisplayName([in] ULONG32 bufLen,
                               [out] ULONG32 *nameLen,
                               [out, size_is(bufLen)] WCHAR name[]); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetDisplayName_dummy();
  }
}