﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;

#endregion

namespace ManagedArgsTracer.Interop.Clr
{
  [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
  [Guid("96EC93C7-1000-4e93-8991-98D8766E6666")]
  [ComImport]
  internal interface IXCLRDataValue
  {
    /*
     * Get state flags, defined in CLRDataValueFlag.
     */
    /*     HRESULT GetFlags([out] ULONG32* flags); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetFlags_dummy();

    /*
     * Get the address of the object.
     * Fails unless the object is a single contiguous
     * piece of data in memory.
     * OBSOLETE: Use GetLocation instead.
     */
    /*     HRESULT GetAddress([out] CLRDATA_ADDRESS* address); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetAddress_dummy();

    /*
     * Return the size (in bytes) of the object.
     */
    /*     HRESULT GetSize([out] ULONG64* size); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetSize_dummy();

    /*
     * Copy between an object and a buffer.
     * Returns S_FALSE if the buffer was not at least as large
     * as the object.
     */
    /*     HRESULT GetBytes([in] ULONG32 bufLen,
                         [out] ULONG32 *dataSize,
                         [out, size_is(bufLen)] BYTE buffer[]); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetBytes_dummy();

    /*     HRESULT SetBytes([in] ULONG32 bufLen,
                         [out] ULONG32 *dataSize,
                         [in, size_is(bufLen)] BYTE buffer[]); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int SetBytes_dummy();

    /*
     * Get the type of the object
     */
    /*     HRESULT GetType([out] IXCLRDataTypeInstance **typeInstance); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetType_dummy();

    /* 
     * Get the number of fields in the object.
     * OBSOLETE: Use GetNumFields2.
     */
    /*     HRESULT GetNumFields([out] ULONG32 *numFields); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetNumFields_dummy();

    /* 
     * Gets one field of the object.
     *
     * Because field ordering is not fixed, can also return name
     * information and/or the metadata token, if the caller passes in
     * appropriate values.
     * OBSOLETE: Use EnumField.
     */
    /*     HRESULT GetFieldByIndex([in] ULONG32 index,
                                [out] IXCLRDataValue **field,
                                [in] ULONG32 bufLen,
                                [out] ULONG32 *nameLen,
                                [out, size_is(bufLen)] WCHAR nameBuf[],
                                [out] mdFieldDef* token); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetFieldByIndex_dummy();

    /*     HRESULT Request([in] ULONG32 reqCode,
                        [in] ULONG32 inBufferSize,
                        [in, size_is(inBufferSize)] BYTE* inBuffer,
                        [in] ULONG32 outBufferSize,
                        [out, size_is(outBufferSize)] BYTE* outBuffer); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int Request_dummy();

    /*
     * Get the number of fields in the value.
     * If a type is passed in only fields defined
     * by that type are enumerated.
     */
    /*     HRESULT GetNumFields2([in] ULONG32 flags,
                              [in] IXCLRDataTypeInstance* fromType,
                              [out] ULONG32* numFields); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetNumFields2_dummy();

    /*
     * Enumerate the fields for this value.
     * If a type is passed in only fields defined
     * by that type are enumerated.
     */
    /*     HRESULT StartEnumFields([in] ULONG32 flags,
                                [in] IXCLRDataTypeInstance* fromType,
                                [out] CLRDATA_ENUM* handle); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int StartEnumFields_dummy();

    /*     HRESULT EnumField([in, out] CLRDATA_ENUM* handle,
                          [out] IXCLRDataValue** field,
                          [in] ULONG32 nameBufLen,
                          [out] ULONG32* nameLen,
                          [out, size_is(nameBufLen)] WCHAR nameBuf[],
                          [out] mdFieldDef* token); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int EnumField_dummy();

    /*     HRESULT EndEnumFields([in] CLRDATA_ENUM handle); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int EndEnumFields_dummy();

    /*     HRESULT StartEnumFieldsByName([in] LPCWSTR name,
                                      [in] ULONG32 nameFlags,
                                      [in] ULONG32 fieldFlags,
                                      [in] IXCLRDataTypeInstance* fromType,
                                      [out] CLRDATA_ENUM* handle); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int StartEnumFieldsByName_dummy();

    /*     HRESULT EnumFieldByName([in, out] CLRDATA_ENUM* handle,
                                [out] IXCLRDataValue** field,
                                [out] mdFieldDef* token); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int EnumFieldByName_dummy();

    /*     HRESULT EndEnumFieldsByName([in] CLRDATA_ENUM handle); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int EndEnumFieldsByName_dummy();

    /*
     * Retrieve a field by field metadata token.
     */
    /*     HRESULT GetFieldByToken([in] mdFieldDef token,
                                [out] IXCLRDataValue **field,
                                [in] ULONG32 bufLen,
                                [out] ULONG32 *nameLen,
                                [out, size_is(bufLen)] WCHAR nameBuf[]); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetFieldByToken_dummy();

    /*
     * Get the value implicitly associated with this value.
     * For pointers or reference values this is the value
     *   pointed/referred to.
     * For boxed values this is the contained value.
     * For other values there is no associated value.
     */
    /*     HRESULT GetAssociatedValue([out] IXCLRDataValue** assocValue); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetAssociatedValue_dummy();

    /*
     * Get the type implicitly associated with this value.
     * For pointers or reference types this is the type
     *   pointed/referred to.
     * For boxed values this is the type of the contained value.
     * For arrays this is the element type.
     * For other values there is no associated type.
     */
    /*     HRESULT GetAssociatedType([out] IXCLRDataTypeInstance** assocType); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetAssociatedType_dummy();

    /*
     * String methods that only work for string data values.
     */

    /* 
     * Return the length and contents of the string.
     */
    /*     HRESULT GetString([in] ULONG32 bufLen,
                          [out] ULONG32 *strLen,
                          [out, size_is(bufLen)] WCHAR str[]); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetString_dummy();

    /*
     * Array methods that only work for array data values.
     */

    /*
     * Return the definition of the array.
     */
    /*     HRESULT GetArrayProperties([out] ULONG32 *rank,
                                   [out] ULONG32 *totalElements,
                                   [in] ULONG32 numDim,
                                   [out, size_is(numDim)] ULONG32 dims[],
                                   [in] ULONG32 numBases,
                                   [out, size_is(numBases)] LONG32 bases[]); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetArrayProperties_dummy();

    /*
     * Return a value representing the given element in the array.
     */
    /*     HRESULT GetArrayElement([in] ULONG32 numInd,
                                [in, size_is(numInd)] LONG32 indices[],
                                [out] IXCLRDataValue **value); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetArrayElement_dummy();

    /*     HRESULT EnumField2([in, out] CLRDATA_ENUM* handle,
                           [out] IXCLRDataValue** field,
                           [in] ULONG32 nameBufLen,
                           [out] ULONG32* nameLen,
                           [out, size_is(nameBufLen)] WCHAR nameBuf[],
                           [out] IXCLRDataModule** tokenScope,
                           [out] mdFieldDef* token); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int EnumField2_dummy();

    /*     HRESULT EnumFieldByName2([in, out] CLRDATA_ENUM* handle,
                                 [out] IXCLRDataValue** field,
                                 [out] IXCLRDataModule** tokenScope,
                                 [out] mdFieldDef* token); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int EnumFieldByName2_dummy();

    /*     HRESULT GetFieldByToken2([in] IXCLRDataModule* tokenScope,
                                 [in] mdFieldDef token,
                                 [out] IXCLRDataValue **field,
                                 [in] ULONG32 bufLen,
                                 [out] ULONG32 *nameLen,
                                 [out, size_is(bufLen)] WCHAR nameBuf[]); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetFieldByToken2_dummy();

    /*
     * Returns the locations the value's
     * data is spread across.
     * Placeholder values, such as values for variables
     * which are dead, may not have any locations.
     * Memory locations return the memory address in arg.
     * Register locations do not return an indication
     * of which register.
     * Requires revision 3.
     */
    /*     HRESULT GetNumLocations([out] ULONG32* numLocs); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetNumLocations_dummy();

    /*     HRESULT GetLocationByIndex([in] ULONG32 loc,
                                   [out] ULONG32* flags,
                                   [out] CLRDATA_ADDRESS* arg); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetLocationByIndex_dummy();
  }
}