﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;

#endregion

namespace ManagedArgsTracer.Interop.Clr
{
  [Flags]
  internal enum CLRDATA_METHNOTIFY : uint
  {
    NONE = 0,

    GENERATED = 1,

    DISCARDED = 2
  }

  [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
  [Guid("AAF60008-FB2C-420b-8FB1-42D244A54A97")]
  [ComImport]
  internal interface IXCLRDataMethodDefinition
  {
    /*
      /*
     * Get the type this method is part of.
     */
    /*HRESULT GetTypeDefinition([out] IXCLRDataTypeDefinition **typeDefinition);*/

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetTypeDefinition([MarshalAs(UnmanagedType.Interface)] [Out] out IXCLRDataTypeDefinition typeDefinition);

    /*
     * Enumerate instances of this definition.
     */
    /*    HRESULT StartEnumInstances([in] IXCLRDataAppDomain* appDomain,
                               [out] CLRDATA_ENUM* handle);*/

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int StartEnumInstances([MarshalAs(UnmanagedType.Interface)] object appDomain, out ulong handle);

    /*HRESULT EnumInstance([in, out] CLRDATA_ENUM* handle,
                         [out] IXCLRDataMethodInstance **instance);*/

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int EnumInstance(ref ulong handle, [MarshalAs(UnmanagedType.Interface)] out IXCLRDataMethodInstance instance);

    /*HRESULT EndEnumInstances([in] CLRDATA_ENUM handle);*/

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int EndEnumInstances(ulong handle);

    /*
     * Get the method's name.
     */
    /*  HRESULT GetName([in] ULONG32 flags,
                    [in] ULONG32 bufLen,
                    [out] ULONG32 *nameLen,
                    [out, size_is(bufLen)] WCHAR name[]);*/

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetName(uint flags, uint buffLen, out uint nameLen, [MarshalAs(UnmanagedType.LPWStr)] StringBuilder name);

    /*
     * Get the metadata token and scope.
     */
    /*HRESULT GetTokenAndScope([out] mdMethodDef* token,
                             [out] IXCLRDataModule **mod);*/

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetTokenAndScope(out uint token, [MarshalAs(UnmanagedType.Interface)] out IXCLRDataModule module);

    /*
     * Get state flags, defined in CLRDataMethodFlag.
     */
    /*  HRESULT GetFlags([out] ULONG32* flags);*/

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetFlags(out CLRDataMethodFlag flags);

    /*
     * Determine whether the given interface represents
     * the same target state.
     */
    /*HRESULT IsSameObject([in] IXCLRDataMethodDefinition* method);*/

    [MethodImpl(MethodImplOptions.PreserveSig)]
    void IsSameObject_dummy();

    /*
     * Get the latest EnC version of this method.
     */
    /* HRESULT GetLatestEnCVersion([out] ULONG32* version);*/

    [MethodImpl(MethodImplOptions.PreserveSig)]
    void GetLatestEnCVersion_dummy();

    /*
     * Get the IL code regions associated with this method.
     */
    /*HRESULT StartEnumExtents([out] CLRDATA_ENUM* handle);*/

    [MethodImpl(MethodImplOptions.PreserveSig)]
    void StartEnumExtents_dummy();

    /*  HRESULT EnumExtent([in, out] CLRDATA_ENUM* handle,
                       [out] CLRDATA_METHDEF_EXTENT* extent);*/

    [MethodImpl(MethodImplOptions.PreserveSig)]
    void EnumExtent_dummy();

    /*HRESULT EndEnumExtents([in] CLRDATA_ENUM handle);*/

    [MethodImpl(MethodImplOptions.PreserveSig)]
    void EndEnumExtents_dummy();

    /*
     * Request notification when code is generated or
     * discarded for the method.
     */
    /*HRESULT GetCodeNotification([out] ULONG32* flags);*/

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetCodeNotification(out CLRDATA_METHNOTIFY flags);

    /* HRESULT SetCodeNotification([in] ULONG32 flags);*/

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int SetCodeNotification(CLRDATA_METHNOTIFY flags);

    /*HRESULT Request([in] ULONG32 reqCode,
                    [in] ULONG32 inBufferSize,
                    [in, size_is(inBufferSize)] BYTE* inBuffer,
                    [in] ULONG32 outBufferSize,
                    [out, size_is(outBufferSize)] BYTE* outBuffer);*/

    [MethodImpl(MethodImplOptions.PreserveSig)]
    void Request_dummy();

    /*
     * Gets the most representative start address of
     * the native code for this method.
     * A method may have multiple entry points, so this
     * address is not guaranteed to be hit by all entries.
     * Requires revision 1.
     */
    /* HRESULT GetRepresentativeEntryAddress([out] CLRDATA_ADDRESS* addr);*/

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetRepresentativeEntryAddress(out ulong address);

    /*  HRESULT HasClassOrMethodInstantiation([out] BOOL* bGeneric);*/

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int HasClassOrMethodInstantiation(out bool generic);
  }
}