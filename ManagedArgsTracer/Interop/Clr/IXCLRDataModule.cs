﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;

#endregion

namespace ManagedArgsTracer.Interop.Clr
{
  [Guid("88E32849-0A0A-4cb0-9022-7CD2E9E139E2")]
  [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
  [ComImport]
  internal interface IXCLRDataModule
  {
    /*
       * Enumerate assemblies this module is part of.
       * Module-to-assembly is an enumeration as a
       * shared module might be part of more than one assembly.
       */
    /*     HRESULT StartEnumAssemblies([out] CLRDATA_ENUM* handle); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int StartEnumAssemblies_dummy();

    /*     HRESULT EnumAssembly([in, out] CLRDATA_ENUM* handle,
                             [out] IXCLRDataAssembly **assembly); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int EnumAssembly_dummy();

    /*     HRESULT EndEnumAssemblies([in] CLRDATA_ENUM handle); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int EndEnumAssemblies_dummy();

    /*
     * Enumerate types in this module.
     */
    /*     HRESULT StartEnumTypeDefinitions([out] CLRDATA_ENUM* handle); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int StartEnumTypeDefinitions(out ulong handle);

    /*     HRESULT EnumTypeDefinition([in, out] CLRDATA_ENUM* handle,
                                   [out] IXCLRDataTypeDefinition **typeDefinition); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int EnumTypeDefinition(ref ulong handle, [MarshalAs(UnmanagedType.Interface)] [Out] out IXCLRDataTypeDefinition typeDefinition);

    /*     HRESULT EndEnumTypeDefinitions([in] CLRDATA_ENUM handle); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int EndEnumTypeDefinitions(ulong handle);

    /*     HRESULT StartEnumTypeInstances([in] IXCLRDataAppDomain* appDomain,
                                       [out] CLRDATA_ENUM* handle); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int StartEnumTypeInstances([MarshalAs(UnmanagedType.Interface)] IXCLRDataAppDomain appDomain, out ulong handle);

    /*     HRESULT EnumTypeInstance([in, out] CLRDATA_ENUM* handle,
                                 [out] IXCLRDataTypeInstance **typeInstance); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int EnumTypeInstance(ref ulong handle, [MarshalAs(UnmanagedType.Interface)] [Out] out IXCLRDataTypeInstance typeInstance);

    /*     HRESULT EndEnumTypeInstances([in] CLRDATA_ENUM handle); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int EndEnumTypeInstances(ulong handle);

    /*
     * Look up types by name.
     */
    /*     HRESULT StartEnumTypeDefinitionsByName([in] LPCWSTR name,
                                               [in] ULONG32 flags,
                                               [out] CLRDATA_ENUM* handle); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int StartEnumTypeDefinitionsByName([MarshalAs(UnmanagedType.LPWStr)] string name, uint flags, out ulong handle);

    /*     HRESULT EnumTypeDefinitionByName([in,out] CLRDATA_ENUM* handle,
                                         [out] IXCLRDataTypeDefinition** type); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int EnumTypeDefinitionByName(ref ulong handle, [MarshalAs(UnmanagedType.Interface)] [Out] out IXCLRDataTypeDefinition typeDefinition);

    /*     HRESULT EndEnumTypeDefinitionsByName([in] CLRDATA_ENUM handle); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int EndEnumTypeDefinitionsByName(ulong handle);

    /*     HRESULT StartEnumTypeInstancesByName([in] LPCWSTR name,
                                             [in] ULONG32 flags,
                                             [in] IXCLRDataAppDomain* appDomain,
                                             [out] CLRDATA_ENUM* handle); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int StartEnumTypeInstancesByName([MarshalAs(UnmanagedType.LPWStr)] string name, uint flags, [MarshalAs(UnmanagedType.Interface)] IXCLRDataAppDomain appDomain, out ulong handle);

    /*     HRESULT EnumTypeInstanceByName([in,out] CLRDATA_ENUM* handle,
                                       [out] IXCLRDataTypeInstance** type); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int EnumTypeInstanceByName(ref ulong handle, [MarshalAs(UnmanagedType.Interface)] [Out] out IXCLRDataTypeInstance typeInstance);

    /*     HRESULT EndEnumTypeInstancesByName([in] CLRDATA_ENUM handle); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int EndEnumTypeInstancesByName(ulong handle);

    /*
     * Get a type definition by metadata token.
     */
    /*     HRESULT GetTypeDefinitionByToken([in] mdTypeDef token,
                                         [out] IXCLRDataTypeDefinition** typeDefinition); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetTypeDefinitionByToken_dummy();

    /*
     * Look up methods by name.
     */
    /*     HRESULT StartEnumMethodDefinitionsByName([in] LPCWSTR name,
                                                 [in] ULONG32 flags,
                                                 [out] CLRDATA_ENUM* handle); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int StartEnumMethodDefinitionsByName_dummy();

    /*     HRESULT EnumMethodDefinitionByName([in,out] CLRDATA_ENUM* handle,
                                           [out] IXCLRDataMethodDefinition** method); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int EnumMethodDefinitionByName_dummy();

    /*     HRESULT EndEnumMethodDefinitionsByName([in] CLRDATA_ENUM handle); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int EndEnumMethodDefinitionsByName_dummy();

    /*     HRESULT StartEnumMethodInstancesByName([in] LPCWSTR name,
                                               [in] ULONG32 flags,
                                               [in] IXCLRDataAppDomain* appDomain,
                                               [out] CLRDATA_ENUM* handle); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int StartEnumMethodInstancesByName_dummy();

    /*     HRESULT EnumMethodInstanceByName([in,out] CLRDATA_ENUM* handle,
                                         [out] IXCLRDataMethodInstance** method); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int EnumMethodInstanceByName_dummy();

    /*     HRESULT EndEnumMethodInstancesByName([in] CLRDATA_ENUM handle); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int EndEnumMethodInstancesByName_dummy();

    /*
     * Get a method definition by metadata token.
     */
    /*     HRESULT GetMethodDefinitionByToken([in] mdMethodDef token,
                                           [out] IXCLRDataMethodDefinition** methodDefinition); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetMethodDefinitionByToken_dummy();

    /*
     * Look up pieces of data by name.
     */
    /*     HRESULT StartEnumDataByName([in] LPCWSTR name,
                                    [in] ULONG32 flags,
                                    [in] IXCLRDataAppDomain* appDomain,
                                    [in] IXCLRDataTask* tlsTask,
                                    [out] CLRDATA_ENUM* handle); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int StartEnumDataByName_dummy();

    /*     HRESULT EnumDataByName([in,out] CLRDATA_ENUM* handle,
                               [out] IXCLRDataValue** value); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int EnumDataByName_dummy();

    /*     HRESULT EndEnumDataByName([in] CLRDATA_ENUM handle); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int EndEnumDataByName_dummy();

    /*
     * Get the module's base name.
     */
    /*     HRESULT GetName([in] ULONG32 bufLen,
                        [out] ULONG32 *nameLen,
                        [out, size_is(bufLen)] WCHAR name[]); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetName(uint bufferLen, out uint realLen, [MarshalAs(UnmanagedType.LPWStr)] StringBuilder name);

    /*
     * Get the full path and filename for the module,
     * if there is one.
     */
    /*     HRESULT GetFileName([in] ULONG32 bufLen,
                            [out] ULONG32 *nameLen,
                            [out, size_is(bufLen)] WCHAR name[]); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetFileName(uint bufLen, out uint realLen, [MarshalAs(UnmanagedType.LPWStr)] StringBuilder buf);

    /*
     * Get state flags, defined in CLRDataModuleFlag.
     */
    /*     HRESULT GetFlags([out] ULONG32* flags); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetFlags_dummy();

    /*
     * Determine whether the given interface represents
     * the same target state.
     */
    /*     HRESULT IsSameObject([in] IXCLRDataModule* mod); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int IsSameObject_dummy();

    /*
     * Get the memory regions associated with this module.
     */
    /*     HRESULT StartEnumExtents([out] CLRDATA_ENUM* handle); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int StartEnumExtents_dummy();

    /*     HRESULT EnumExtent([in, out] CLRDATA_ENUM* handle,
                           [out] CLRDATA_MODULE_EXTENT* extent); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int EnumExtent_dummy();

    /*     HRESULT EndEnumExtents([in] CLRDATA_ENUM handle); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int EndEnumExtents_dummy();

    /*     HRESULT Request([in] ULONG32 reqCode,
                        [in] ULONG32 inBufferSize,
                        [in, size_is(inBufferSize)] BYTE* inBuffer,
                        [in] ULONG32 outBufferSize,
                        [out, size_is(outBufferSize)] BYTE* outBuffer); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int Request(
      uint reqCode,
      uint inBufferSize,
      [MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 1)] [In] byte[] inBuffer,
      uint outBufferSize,
      [MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 3)] [Out] byte[] outBuffer);

    /*
     * Enumerate the app domains using this module.
     */
    /*     HRESULT StartEnumAppDomains([out] CLRDATA_ENUM* handle); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int StartEnumAppDomains_dummy();

    /*     HRESULT EnumAppDomain([in, out] CLRDATA_ENUM* handle,
                              [out] IXCLRDataAppDomain** appDomain); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int EnumAppDomain_dummy();

    /*     HRESULT EndEnumAppDomains([in] CLRDATA_ENUM handle); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int EndEnumAppDomains_dummy();

    /*
     * Get the module's version ID.
     * Requires revision 3.
     */
    /*     HRESULT GetVersionId([out] GUID* vid); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetVersionId_dummy();
  }
}