﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;

#endregion

namespace ManagedArgsTracer.Interop.Clr
{
  [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
  [Guid("4D078D91-9CB3-4b0d-97AC-28C8A5A82597")]
  [ComImport]
  internal interface IXCLRDataTypeInstance
  {
    /*
     * Enumerate method instances within this type.
     */
    /*     HRESULT StartEnumMethodInstances([out] CLRDATA_ENUM* handle); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int StartEnumMethodInstances(out ulong handle);

    /*     HRESULT EnumMethodInstance([in, out] CLRDATA_ENUM* handle,
                                   [out] IXCLRDataMethodInstance** methodInstance); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int EnumMethodInstance(ref ulong handle, [MarshalAs(UnmanagedType.Interface)] [Out] out IXCLRDataMethodInstance methodInstance);

    /*     HRESULT EndEnumMethodInstances([in] CLRDATA_ENUM handle); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int EndEnumMethodInstances(ulong handle);

    /*
     * Look up method instances by name.
     */
    /*     HRESULT StartEnumMethodInstancesByName([in] LPCWSTR name,
                                               [in] ULONG32 flags,
                                               [out] CLRDATA_ENUM* handle); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int StartEnumMethodInstancesByName([MarshalAs(UnmanagedType.LPWStr)] string name, uint flags, out ulong handle);

    /*     HRESULT EnumMethodInstanceByName([in,out] CLRDATA_ENUM* handle,
                                         [out] IXCLRDataMethodInstance** method); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int EnumMethodInstanceByName(ref ulong handle, [MarshalAs(UnmanagedType.Interface)] [Out] out IXCLRDataMethodInstance methodInstance);

    /*     HRESULT EndEnumMethodInstancesByName([in] CLRDATA_ENUM handle); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int EndEnumMethodInstancesByName(ulong handle);

    /*
     * Get the number of static fields in the type.
     * OBSOLETE: Use GetNumStaticFields2.
     */
    /*     HRESULT GetNumStaticFields([out] ULONG32* numFields); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetNumStaticFields_dummy();

    /*
     * Get one static field of the type.
     *
     * Because static field ordering is not fixed, can also return name
     * information and/or the metadata token, if the caller passes
     * in appropriate values.
     * OBSOLETE: Use EnumStaticField.
     */
    /*     HRESULT GetStaticFieldByIndex([in] ULONG32 index,
                                      [in] IXCLRDataTask* tlsTask,
                                      [out] IXCLRDataValue **field,
                                      [in] ULONG32 bufLen,
                                      [out] ULONG32 *nameLen,
                                      [out, size_is(bufLen)] WCHAR nameBuf[],
                                      [out] mdFieldDef* token); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetStaticFieldByIndex_dummy();

    /*
     * Look up fields by name.
     * OBSOLETE: Use EnumStaticFieldByName2.
     */
    /*     HRESULT StartEnumStaticFieldsByName([in] LPCWSTR name,
                                            [in] ULONG32 flags,
                                            [in] IXCLRDataTask* tlsTask,
                                            [out] CLRDATA_ENUM* handle); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int StartEnumStaticFieldsByName_dummy();

    /*     HRESULT EnumStaticFieldByName([in,out] CLRDATA_ENUM* handle,
                                      [out] IXCLRDataValue** value); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int EnumStaticFieldByName_dummy();

    /*     HRESULT EndEnumStaticFieldsByName([in] CLRDATA_ENUM handle); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int EndEnumStaticFieldsByName_dummy();

    /*
     * Enumerate this type's parameterization.
     */
    /*     HRESULT GetNumTypeArguments([out] ULONG32* numTypeArgs); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetNumTypeArguments_dummy();

    /*     HRESULT GetTypeArgumentByIndex([in] ULONG32 index,
                                       [out] IXCLRDataTypeInstance** typeArg); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetTypeArgumentByIndex_dummy();

    /*
     * Get the fully qualified name for this type instance.
     */
    /*     HRESULT GetName([in] ULONG32 flags,
                        [in] ULONG32 bufLen,
                        [out] ULONG32 *nameLen,
                        [out, size_is(bufLen)] WCHAR nameBuf[]); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetName(uint flags, uint buffLen, out uint realLen, [MarshalAs(UnmanagedType.LPWStr)] [Out] StringBuilder nameBuff);

    /*
     * Get the module for this type instance.
     */
    /*     HRESULT GetModule([out] IXCLRDataModule **mod); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetModule_dummy();

    /*
     * Get the definition matching this instance.
     */
    /*     HRESULT GetDefinition([out] IXCLRDataTypeDefinition **typeDefinition); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetDefinition([MarshalAs(UnmanagedType.Interface)] [Out] out IXCLRDataTypeDefinition typeDefinition);

    /*
     * Get state flags, defined in CLRDataTypeFlag.
     */
    /*     HRESULT GetFlags([out] ULONG32* flags); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetFlags_dummy();

    /*
     * Determine whether the given interface represents
     * the same target state.
     */
    /*     HRESULT IsSameObject([in] IXCLRDataTypeInstance* type); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int IsSameObject_dummy();

    /*     HRESULT Request([in] ULONG32 reqCode,
                        [in] ULONG32 inBufferSize,
                        [in, size_is(inBufferSize)] BYTE* inBuffer,
                        [in] ULONG32 outBufferSize,
                        [out, size_is(outBufferSize)] BYTE* outBuffer); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int Request_dummy();

    /*
     * Get the number of static fields in the type.
     */
    /*     HRESULT GetNumStaticFields2([in] ULONG32 flags,
                                    [out] ULONG32* numFields); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetNumStaticFields2_dummy();

    /*
     * Enumerate values for the static fields of this type.
     */
    /*     HRESULT StartEnumStaticFields([in] ULONG32 flags,
                                      [in] IXCLRDataTask* tlsTask,
                                      [out] CLRDATA_ENUM* handle); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int StartEnumStaticFields_dummy();

    /*     HRESULT EnumStaticField([in,out] CLRDATA_ENUM* handle,
                                [out] IXCLRDataValue** value); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int EnumStaticField_dummy();

    /*     HRESULT EndEnumStaticFields([in] CLRDATA_ENUM handle); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int EndEnumStaticFields_dummy();

    /*     HRESULT StartEnumStaticFieldsByName2([in] LPCWSTR name,
                                             [in] ULONG32 nameFlags,
                                             [in] ULONG32 fieldFlags,
                                             [in] IXCLRDataTask* tlsTask,
                                             [out] CLRDATA_ENUM* handle); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int StartEnumStaticFieldsByName2_dummy();

    /*     HRESULT EnumStaticFieldByName2([in,out] CLRDATA_ENUM* handle,
                                       [out] IXCLRDataValue** value); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int EnumStaticFieldByName2_dummy();

    /*     HRESULT EndEnumStaticFieldsByName2([in] CLRDATA_ENUM handle); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int EndEnumStaticFieldsByName2_dummy();

    /*
     * Retrieve a static field by field metadata token.
     */
    /*     HRESULT GetStaticFieldByToken([in] mdFieldDef token,
                                      [in] IXCLRDataTask* tlsTask,
                                      [out] IXCLRDataValue **field,
                                      [in] ULONG32 bufLen,
                                      [out] ULONG32 *nameLen,
                                      [out, size_is(bufLen)] WCHAR nameBuf[]); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetStaticFieldByToken_dummy();

    /*
     * Get the base type of this type, if any.
     */
    /*     HRESULT GetBase([out] IXCLRDataTypeInstance** base); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetBase([MarshalAs(UnmanagedType.Interface)] [Out] out IXCLRDataTypeInstance baseInst);

    /*     HRESULT EnumStaticField2([in,out] CLRDATA_ENUM* handle,
                                 [out] IXCLRDataValue** value,
                                 [in] ULONG32 bufLen,
                                 [out] ULONG32 *nameLen,
                                 [out, size_is(bufLen)] WCHAR nameBuf[],
                                 [out] IXCLRDataModule** tokenScope,
                                 [out] mdFieldDef* token); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int EnumStaticField2_dummy();

    /*     HRESULT EnumStaticFieldByName3([in,out] CLRDATA_ENUM* handle,
                                       [out] IXCLRDataValue** value,
                                       [out] IXCLRDataModule** tokenScope,
                                       [out] mdFieldDef* token); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int EnumStaticFieldByName3_dummy();

    /*     HRESULT GetStaticFieldByToken2([in] IXCLRDataModule* tokenScope,
                                       [in] mdFieldDef token,
                                       [in] IXCLRDataTask* tlsTask,
                                       [out] IXCLRDataValue **field,
                                       [in] ULONG32 bufLen,
                                       [out] ULONG32 *nameLen,
                                       [out, size_is(bufLen)] WCHAR nameBuf[]); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetStaticFieldByToken2_dummy();
  }
}