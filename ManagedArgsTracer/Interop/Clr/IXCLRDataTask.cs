﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;

#endregion

namespace ManagedArgsTracer.Interop.Clr
{
  [Flags]
  internal enum CLRDataSimpleFrameType : uint
  {
    CLRDATA_SIMPFRAME_UNRECOGNIZED = 0x1,

    CLRDATA_SIMPFRAME_MANAGED_METHOD = 0x2,

    CLRDATA_SIMPFRAME_RUNTIME_MANAGED_CODE = 0x4,

    CLRDATA_SIMPFRAME_RUNTIME_UNMANAGED_CODE = 0x8,

    SIMPFRAME_ALL = CLRDATA_SIMPFRAME_UNRECOGNIZED | CLRDATA_SIMPFRAME_MANAGED_METHOD | CLRDATA_SIMPFRAME_RUNTIME_MANAGED_CODE | CLRDATA_SIMPFRAME_RUNTIME_UNMANAGED_CODE
  }

  [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
  [Guid("A5B0BEEA-EC62-4618-8012-A24FFC23934C")]
  [ComImport]
  internal interface IXCLRDataTask
  {
    /* 
     * Get the process for this task. 
     */
    /*     HRESULT GetProcess([out] IXCLRDataProcess** process); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetProcess_dummy();

    /*
     * Get the application domain that the task is
     * currently running in.  This can change over time.
     */
    /*     HRESULT GetCurrentAppDomain([out] IXCLRDataAppDomain **appDomain); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetCurrentAppDomain([MarshalAs(UnmanagedType.Interface)] [Out] out IXCLRDataAppDomain appDomain);

    /* 
     * Get a unique, stable identifier for this task.
     */
    /*     HRESULT GetUniqueID([out] ULONG64* id); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetUniqueID_dummy();

    /*
     * Get state flags, defined in CLRDataTaskFlag.
     */
    /*     HRESULT GetFlags([out] ULONG32* flags); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetFlags_dummy();

    /*
     * Determine whether the given interface represents
     * the same target state.
     */
    /*     HRESULT IsSameObject([in] IXCLRDataTask* task); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int IsSameObject_dummy();

    /*
     * Get the managed object representing the task.
     */
    /*     HRESULT GetManagedObject([out] IXCLRDataValue** value); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetManagedObject_dummy();

    /*
     * Mark the task so that it attempts to reach the
     * given execution state the next time it executes.
     */
    /*     HRESULT GetDesiredExecutionState([out] ULONG32* state); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetDesiredExecutionState_dummy();

    /*     HRESULT SetDesiredExecutionState([in] ULONG32 state); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int SetDesiredExecutionState_dummy();

    /*
     * Create a stack walker to walk this task's stack. The
     * flags parameter takes a bitfield of values from the
     * CLRDataSimpleFrameType enum.
     */
    /*     HRESULT CreateStackWalk([in] ULONG32 flags,
                                [out] IXCLRDataStackWalk** stackWalk); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int CreateStackWalk(CLRDataSimpleFrameType flags, [MarshalAs(UnmanagedType.Interface)] [Out] out IXCLRDataStackWalk stackWalk);

    /* 
     * Get the current OS thread ID for this task. If this task is on a fiber,
     * the ID may change over time. 
     */
    /*     HRESULT GetOSThreadID([out] ULONG32* id); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetOSThreadID_dummy();

    /* 
     * Get the current context for this task, controlled by the given flags.
     * Returns S_FALSE if the size is not large enough.
     */
    /*     HRESULT GetContext([in] ULONG32 contextFlags,
                           [in] ULONG32 contextBufSize,
                           [out] ULONG32* contextSize,
                           [out, size_is(contextBufSize)] BYTE contextBuf[]); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetContext_dummy();

    /* 
     * Destructively set the current context for this task.
     */
    /*     HRESULT SetContext([in] ULONG32 contextSize,
                           [in, size_is(contextSize)] BYTE context[]); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int SetContext_dummy();

    /*
     * Get the current exception state for the
     * task, if any.  This may be the first element
     * in a list of exception states if there are
     * nested exceptions.
     */
    /*     HRESULT GetCurrentExceptionState([out] IXCLRDataExceptionState **exception); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetCurrentExceptionState_dummy();

    /*     HRESULT Request([in] ULONG32 reqCode,
                        [in] ULONG32 inBufferSize,
                        [in, size_is(inBufferSize)] BYTE* inBuffer,
                        [in] ULONG32 outBufferSize,
                        [out, size_is(outBufferSize)] BYTE* outBuffer); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int Request_dummy();

    /*
     * Get the task's name if it has one.
     * Requires revision 1.
     */
    /*     HRESULT GetName([in] ULONG32 bufLen,
                        [out] ULONG32 *nameLen,
                        [out, size_is(bufLen)] WCHAR name[]); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetName_dummy();

    /*
     * Get the last exception state for the
     * task, if any.  If an exception is currently
     * being processed the last exception state may
     * be the same as the current exception state.
     * Requires revision 2.
     */
    /*     HRESULT GetLastExceptionState([out] IXCLRDataExceptionState **exception); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetLastExceptionState_dummy();
  }
}