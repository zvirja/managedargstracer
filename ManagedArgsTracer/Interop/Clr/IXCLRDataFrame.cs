﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;

#endregion

namespace ManagedArgsTracer.Interop.Clr
{
  [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
  [Guid("271498C2-4085-4766-BC3A-7F8ED188A173")]
  [ComImport]
  internal interface IXCLRDataFrame
  {
    /* 
     * Return information about the type of this frame.
     */
    /*     HRESULT GetFrameType([out] CLRDataSimpleFrameType* simpleType,
                             [out] CLRDataDetailedFrameType* detailedType); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetFrameType(out CLRDataSimpleFrameType simpleType, out CLRDataDetailedFrameType detailedType);

    /* 
     * Get the stack walk context as of this frame.
     * This is the original context with any unwinding
     * applied to it.  As unwinding may only restore
     * a subset of the registers, such as only non-volatile
     * registers, the context may not exactly match the
     * register state at the time of the actual call.
     */
    /*     HRESULT GetContext([in] ULONG32 contextFlags,
                           [in] ULONG32 contextBufSize,
                           [out] ULONG32* contextSize,
                           [out, size_is(contextBufSize)] BYTE contextBuf[]); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetContext_dummy();

    /*
     * Return the app domain of this frame
     */
    /*     HRESULT GetAppDomain([out] IXCLRDataAppDomain** appDomain); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetAppDomain_dummy();

    /*
     * Return the number of arguments on the stack.
     */
    /*     HRESULT GetNumArguments([out] ULONG32* numArgs); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetNumArguments_dummy();

    /*
     * Return an argument by (0-based) index.
     * The name parameter is filled in if name information is availble.
     */
    /*     HRESULT GetArgumentByIndex([in] ULONG32 index,
                                   [out] IXCLRDataValue** arg,
                                   [in] ULONG32 bufLen,
                                   [out] ULONG32 *nameLen,
                                   [out, size_is(bufLen)] WCHAR name[]); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetArgumentByIndex_dummy();

    /*
     * Return the number of local variables on the stack.
     */
    /*     HRESULT GetNumLocalVariables([out] ULONG32* numLocals); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetNumLocalVariables_dummy();

    /*
     * Return a local variable by (0-based) index.
     * The name parameter is filled in if name information is availble.
     */
    /*     HRESULT GetLocalVariableByIndex([in] ULONG32 index,
                                        [out] IXCLRDataValue** localVariable,
                                        [in] ULONG32 bufLen,
                                        [out] ULONG32 *nameLen,
                                        [out, size_is(bufLen)] WCHAR name[]); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetLocalVariableByIndex_dummy();

    /*
     * Get a name for the frame's current instruction pointer.
     * This is either a method's name or a runtime code name.
     *
     * Returns S_FALSE if the buffer is not large enough for the name,
     * and sets nameLen to be the buffer length needed.
     */
    /*     HRESULT GetCodeName([in] ULONG32 flags,
                            [in] ULONG32 bufLen,
                            [out] ULONG32 *nameLen,
                            [out, size_is(bufLen)] WCHAR nameBuf[]); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetCodeName(uint flags, uint bufLen, out uint nameLen, [MarshalAs(UnmanagedType.LPWStr)] [Out] StringBuilder nameBuf);

    /*
     * Gets the method instance corresponding to this frame.
     */
    /*     HRESULT GetMethodInstance([out] IXCLRDataMethodInstance** method); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetMethodInstance_dummy();

    /*     HRESULT Request([in] ULONG32 reqCode,
                        [in] ULONG32 inBufferSize,
                        [in, size_is(inBufferSize)] BYTE* inBuffer,
                        [in] ULONG32 outBufferSize,
                        [out, size_is(outBufferSize)] BYTE* outBuffer); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int Request_dummy();

    /*
     * Enumerate the full parameterization of the frame's
     * type and method.
     */
    /*     HRESULT GetNumTypeArguments([out] ULONG32* numTypeArgs); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetNumTypeArguments_dummy();

    /*     HRESULT GetTypeArgumentByIndex([in] ULONG32 index,
                                       [out] IXCLRDataTypeInstance** typeArg); */

    [MethodImpl(MethodImplOptions.PreserveSig)]
    int GetTypeArgumentByIndex_dummy();
  }
}