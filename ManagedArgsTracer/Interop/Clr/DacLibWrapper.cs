﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

using ManagedArgsTracer.Helpers;
using ManagedArgsTracer.Interop.Native;

#endregion

namespace ManagedArgsTracer.Interop.Clr
{
  [UnmanagedFunctionPointer(CallingConvention.StdCall)]
  internal delegate int CreateDacInstance(
    [ComAliasName("REFIID")] [In] ref Guid riid,
    [MarshalAs(UnmanagedType.Interface)] [In] IClrDataTarget2 data,
    [MarshalAs(UnmanagedType.Interface)] out IXCLRDataProcess ppObj);

  internal class DacLibWrapper
  {
    #region Constructors and Destructors

    public DacLibWrapper(string dacPath)
    {
      this.DacLibraryPath = dacPath;
      this.LibraryHandle = NativeMethods.LoadLibraryEx(this.DacLibraryPath, IntPtr.Zero, NativeMethods.LoadLibraryFlags.NoFlags);
      int lastWin32Error = Marshal.GetLastWin32Error();
      if (this.LibraryHandle.ToInt64() == 0L)
      {
        throw new InvalidOperationException("Unable to load MSCORDACWKS libary from path {0}. Error code: {1}.".FormatWith(dacPath, lastWin32Error));
      }
      this.CreateClrDataHandle = NativeMethods.GetProcAddress(this.LibraryHandle, "CLRDataCreateInstance");
      lastWin32Error = Marshal.GetLastWin32Error();
      if (this.CreateClrDataHandle.ToInt64() == 0L)
      {
        throw new InvalidOperationException("Unable to get exported CLRDataCreateInstance function from library. Error code: {0}.".FormatWith(lastWin32Error));
      }
      this.ClrDataCreateInstanceMethod = (CreateDacInstance)Marshal.GetDelegateForFunctionPointer(this.CreateClrDataHandle, typeof(CreateDacInstance));
    }

    #endregion

    #region Public Properties

    public CreateDacInstance ClrDataCreateInstanceMethod { get; protected set; }

    public string DacLibraryPath { get; set; }

    #endregion

    #region Properties

    private IntPtr CreateClrDataHandle { get; set; }

    private IntPtr LibraryHandle { get; set; }

    #endregion
  }
}