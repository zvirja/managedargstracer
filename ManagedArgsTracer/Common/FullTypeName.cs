﻿#region

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

using ManagedArgsTracer.Helpers;

#endregion

namespace ManagedArgsTracer.Common
{
  [DebuggerDisplay("{ModuleName}!{TypeName}")]
  public struct FullTypeName
  {
    #region Static Fields

    public static FullTypeName EmptyType = new FullTypeName(null, null);

    #endregion

    #region Constructors and Destructors

    public FullTypeName(string typeName, string moduleName)
      : this()
    {
      this.TypeName = typeName;
      this.ModuleName = moduleName;
    }

    public FullTypeName(string typeName)
      : this(typeName, null)
    {
    }

    #endregion

    #region Public Properties

    public bool IsEmpty
    {
      get
      {
        return this.ModuleName == null && this.TypeName == null;
      }
    }

    public string ModuleName { get; private set; }

    public string TypeName { get; private set; }

    #endregion

    #region Public Methods and Operators

    public bool PartiallyMatch(FullTypeName another, bool caseSensitive = false, bool allowToCompareLastPartOnly = false)
    {
      bool result = this.TypeName.EqualsWithCase(another.TypeName, caseSensitive);
      if (this.ModuleName == null || another.ModuleName == null)
      {
        //Allows to compare last pieces only. So for System.String it will be enough to pass String only.
        if (!result && allowToCompareLastPartOnly)
        {
          int lastPosOfDot = this.TypeName.LastIndexOf('.');
          if (lastPosOfDot > -1)
          {
            string lastPiece = this.TypeName.Substring(lastPosOfDot + 1).Trim();
            result = another.TypeName.EqualsWithCase(lastPiece, caseSensitive);
          }
        }
        return result;
      }
      else
      {
        return result && this.ModuleName.EqualsWithCase(another.ModuleName, caseSensitive);
      }
    }

    public override string ToString()
    {
      if (this.IsEmpty)
      {
        return "-";
      }
      return this.ModuleName.IsNullOrEmpty() ? this.TypeName : this.ModuleName + "!" + this.TypeName;
    }

    #endregion
  }
}