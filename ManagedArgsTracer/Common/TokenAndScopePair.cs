﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ManagedArgsTracer.Diagnostics;
using ManagedArgsTracer.Helpers;
using ManagedArgsTracer.Runtime.Wrappers;

#endregion

namespace ManagedArgsTracer.Common
{
  internal class TokenAndScopePair
  {
    #region Fields

    private string m_stringRepresentation;

    #endregion

    #region Constructors and Destructors

    public TokenAndScopePair(uint token, XClrDataModuleWrapper module)
    {
      this.Token = token;
      this.Module = module;
    }

    #endregion

    #region Public Properties

    public bool IsValid
    {
      get
      {
        return this.Token != 0U && this.Module != null;
      }
    }

    public XClrDataModuleWrapper Module { get; set; }

    public uint Token { get; set; }

    #endregion

    #region Public Methods and Operators

    public override string ToString()
    {
      if (this.m_stringRepresentation != null)
      {
        return this.m_stringRepresentation;
      }

      //Token is broken, return something dummy
      if (!this.IsValid)
      {
        Tracer.ReportUnexpectedState("Token is broken.");
        return Guid.NewGuid().ToString();
      }

      string modPart = this.Module.FilePath;
      if (modPart.IsNullOrEmpty())
      {
        modPart = this.Module.Name;
      }

      this.m_stringRepresentation = modPart + "_" + this.Token;
      return this.m_stringRepresentation;
    }

    #endregion
  }
}