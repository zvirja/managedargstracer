﻿#region

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

using ManagedArgsTracer.Helpers;

#endregion

namespace ManagedArgsTracer.Common
{
  [DebuggerDisplay("({VarName},{Type})")]
  internal struct FullVariableName
  {
    #region Static Fields

    public static FullVariableName EmptyName = new FullVariableName(null, FullTypeName.EmptyType);

    #endregion

    #region Constructors and Destructors

    public FullVariableName(string varName, FullTypeName type)
      : this()
    {
      this.VarName = varName;
      this.Type = type;
    }

    public FullVariableName(string varName)
      : this(varName, FullTypeName.EmptyType)
    {
    }

    #endregion

    #region Public Properties

    public bool IsEmpty
    {
      get
      {
        return this.VarName == null && this.Type.IsEmpty;
      }
    }

    public FullTypeName Type { get; private set; }

    public string VarName { get; private set; }

    #endregion

    #region Public Methods and Operators

    public bool PartiallyMatch(FullVariableName another, bool caseSensitive = false)
    {
      bool result = this.VarName.EqualsWithCase(another.VarName, caseSensitive);
      if (this.Type.IsEmpty)
      {
        return result;
      }
      else
      {
        return result && this.Type.PartiallyMatch(another.Type, caseSensitive);
      }
    }

    #endregion
  }
}