﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#endregion

namespace ManagedArgsTracer.Common
{
  [Serializable]
  internal class InvalidCommandLineInputException : Exception
  {
    #region Constructors and Destructors

    public InvalidCommandLineInputException(string message)
      : base(message)
    {
    }

    #endregion
  }
}