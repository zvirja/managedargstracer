﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ManagedArgsTracer.Helpers;

#endregion

namespace ManagedArgsTracer.Common
{
  internal class CommandLineString
  {
    #region Constructors and Destructors

    public CommandLineString(string rawCommandLine)
    {
      this.RawString = rawCommandLine;
      this.RawChars = this.RawString.ToCharArray();
      this.CurrentCharPos = 0;
      this.DebugLastCommittedSuccessPos = 0;
    }

    #endregion

    #region Public Properties

    public int CharPosLimit
    {
      get
      {
        return this.RawChars.Length;
      }
    }

    public char CurrentChar
    {
      get
      {
        return this.RawChars[this.CurrentCharPos];
      }
    }

    public int CurrentCharPos { get; set; }

    public char[] RawChars { get; protected set; }

    public string RawString { get; protected set; }

    #endregion

    #region Properties

    protected int DebugLastCommittedSuccessPos { get; set; }

    #endregion

    #region Public Methods and Operators

    public void AssertNotEnd()
    {
      if (this.CurrentCharPos >= this.CharPosLimit)
      {
        this.ThrowInvalidOperationExceptionDuringParsing("Unexpected end of input line.");
      }
    }

    public virtual StringBuilder CollectCharsTillNonLetterOrDigit(StringBuilder buffer = null, bool assertNotEnd = true)
    {
      var sbResult = buffer ?? new StringBuilder();
      while (this.CurrentCharPos < this.CharPosLimit && (char.IsLetter(this.CurrentChar) || char.IsDigit(this.CurrentChar)))
      {
        sbResult.Append(this.CurrentChar);
        this.CurrentCharPos++;
      }

      if (assertNotEnd)
      {
        this.AssertNotEnd();
      }
      return sbResult;
    }

    public virtual StringBuilder CollectCharsTillStopChar(HashSet<char> stopChars, StringBuilder buffer = null, bool assertNotEnd = true)
    {
      var sbResult = buffer ?? new StringBuilder();
      while (this.CurrentCharPos < this.CharPosLimit && !stopChars.Contains(this.CurrentChar))
      {
        sbResult.Append(this.CurrentChar);
        this.CurrentCharPos++;
      }
      if (assertNotEnd)
      {
        this.AssertNotEnd();
      }
      return sbResult;
    }

    public virtual StringBuilder CollectCharsTillStopChar(
      char stopChar1,
      char stopChar2 = char.MinValue,
      char stopChar3 = char.MinValue,
      StringBuilder buffer = null,
      bool assertNotEnd = true)
    {
      var sbResult = buffer ?? new StringBuilder();
      while (this.CurrentCharPos < this.CharPosLimit && CharNotEqualAny(this.CurrentChar, stopChar1, stopChar2, stopChar3))
      {
        sbResult.Append(this.CurrentChar);
        this.CurrentCharPos++;
      }
      if (assertNotEnd)
      {
        this.AssertNotEnd();
      }
      return sbResult;
    }

    public void DebugCommitSuccess()
    {
      this.DebugLastCommittedSuccessPos = this.CurrentCharPos;
    }

    public virtual void NavigateToNextNonSpaceChar(bool assertNotEnd = true)
    {
      while (this.CurrentCharPos < this.CharPosLimit && this.CurrentChar == ' ')
      {
        this.CurrentCharPos++;
      }
      if (assertNotEnd)
      {
        this.AssertNotEnd();
      }
    }

    public virtual void SkipTillStopChar(char stopChar1, char stopChar2 = char.MinValue, char stopChar3 = char.MinValue, bool assertNotEnd = true)
    {
      while (this.CurrentCharPos < this.CharPosLimit && CharNotEqualAny(this.CurrentChar, stopChar1, stopChar2, stopChar3))
      {
        this.CurrentCharPos++;
      }
      if (assertNotEnd)
      {
        this.AssertNotEnd();
      }
    }

    public virtual void ThrowInvalidOperationExceptionDuringParsing(string message = null)
    {
      throw new InvalidCommandLineInputException(
        "{1}{0}The successfully processed part of line: '{2}'{0}Refer to the application help (e.g. /?) to get insight into allowed syntax.".
          FormatWith(Environment.NewLine, message ?? "Unexpected state during input value parsing.", this.RawString.Substring(0, this.DebugLastCommittedSuccessPos)));
    }

    #endregion

    #region Methods

    protected static bool CharNotEqualAny(char charToCompare, char etalon1, char etalon2, char etalon3)
    {
      return charToCompare != etalon1 && (etalon2 == char.MinValue || charToCompare != etalon2)
             && (etalon3 == char.MinValue || charToCompare != etalon3);
    }

    #endregion
  }
}