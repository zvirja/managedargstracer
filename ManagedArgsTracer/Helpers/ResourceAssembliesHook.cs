﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

using ManagedArgsTracer.Properties;

#endregion

namespace ManagedArgsTracer.Helpers
{
  internal static class ResourceAssembliesHook
  {
    #region Static Fields

    private static Assembly ClrMDAsssembly;

    private static Assembly Log4NetAssembly;

    #endregion

    #region Public Methods and Operators

    public static void InitializeHook()
    {
      Log4NetAssembly = Assembly.Load(Resources.log4net);
      ClrMDAsssembly = Assembly.Load(Resources.Microsoft_Diagnostics_Runtime);
      AppDomain.CurrentDomain.AssemblyResolve += CurrentDomainOnAssemblyResolve;
    }

    #endregion

    #region Methods

    private static Assembly CurrentDomainOnAssemblyResolve(object sender, ResolveEventArgs args)
    {
      if (args.Name.StartsWith("log4net", StringComparison.OrdinalIgnoreCase))
      {
        return Log4NetAssembly;
      }
      if (args.Name.StartsWith("Microsoft.Diagnostics.Runtime", StringComparison.OrdinalIgnoreCase))
      {
        return ClrMDAsssembly;
      }

      return null;
    }

    #endregion
  }
}