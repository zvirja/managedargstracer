﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ManagedArgsTracer.Breakpoints.Definition;
using ManagedArgsTracer.Breakpoints.Tracing;

#endregion

namespace ManagedArgsTracer.Helpers
{
  internal static class TracingHelper
  {
    #region Public Methods and Operators

    public static void AppendCurrentChainValueToBuffer(
      StringBuilder result,
      ArgumentValueResolvingContext tracingContext,
      string currentArgName,
      string value,
      bool isFinal,
      bool forceDisplay)
    {
      bool traceIntermediate = (tracingContext.RuntimeBreakpoint.Flags & MethodTracingFlag.CHAIN_NO_INTERMEDIATE) != MethodTracingFlag.CHAIN_NO_INTERMEDIATE || forceDisplay;

      if (result.Length > 0 && (isFinal || traceIntermediate))
      {
        result.Append('.');
      }

      if (isFinal)
      {
        result.AppendFormat("{0}: {1}", currentArgName, value);
        return;
      }

      if (!traceIntermediate)
      {
        return;
      }

      if ((tracingContext.RuntimeBreakpoint.Flags & MethodTracingFlag.CHAIN_INTERMEDIATE_VALUES) == MethodTracingFlag.CHAIN_INTERMEDIATE_VALUES || forceDisplay)
      {
        result.AppendFormat("{0}({1})", currentArgName, value);
      }
      else
      {
        result.Append(currentArgName);
      }
    }

    #endregion
  }
}