﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#endregion

namespace ManagedArgsTracer.Helpers
{
  internal static class StringHelper
  {
    #region Public Methods and Operators

    public static bool EqualsWithCase(this string str, string stringToCompare, bool caseSensitive)
    {
      if (str == null)
      {
        return stringToCompare == null;
      }
      return str.Equals(stringToCompare, GetComparison(caseSensitive));
    }

    public static string FormatWith(this string format, params object[] @params)
    {
      return string.Format(format, @params);
    }

    public static StringComparison GetComparison(bool caseSensitive)
    {
      return caseSensitive ? StringComparison.Ordinal : StringComparison.OrdinalIgnoreCase;
    }

    public static string GetSubStrTillChar(this string str, char stopChar)
    {
      if (str.IsNullOrEmpty())
      {
        return str;
      }
      int indexOfStopChar = str.IndexOf(stopChar);
      if (indexOfStopChar < 0)
      {
        return str;
      }
      else
      {
        return str.Substring(0, indexOfStopChar);
      }
    }

    public static string GetValueOrDefault(this string str, string defaultValue)
    {
      return str.NotNullNotEmpty() ? str : defaultValue;
    }

    public static bool IsNullOrEmpty(this string str)
    {
      return string.IsNullOrEmpty(str);
    }

    public static string JoinToString(this IEnumerable<string> input, string separator)
    {
      return string.Join(separator, input);
    }

    public static string JoinToString(this string[] input, string separator)
    {
      return string.Join(separator, input);
    }

    public static bool NotNullNotEmpty(this string str)
    {
      return !string.IsNullOrEmpty(str);
    }

    public static TOut NullOr<TIn, TOut>(this TIn input, Func<TIn, TOut> valueReader)
      where TIn : class
      where TOut : class
    {
      if (input == null)
      {
        return null;
      }
      return valueReader(input);
    }

    public static string ToHexString(this ulong value)
    {
      string formatPattern = IntPtr.Size == 4 ? "0x{0:x8}" : "0x{0:x16}";
      return string.Format(formatPattern, value);
    }

    #endregion
  }
}