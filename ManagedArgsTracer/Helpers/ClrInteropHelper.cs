﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ManagedArgsTracer.Common;
using ManagedArgsTracer.Diagnostics;
using ManagedArgsTracer.Interop.Clr;
using ManagedArgsTracer.Runtime.Wrappers;

#endregion

namespace ManagedArgsTracer.Helpers
{
  internal static class ClrInteropHelper
  {
    #region Delegates

    public delegate int EndEnumMethod(ulong handle);

    public delegate int EnumMethod<TValue>(ref ulong handle, out TValue instance);

    public delegate int GetNameDelegate(uint bufferLen, out uint realLen, StringBuilder name);

    public delegate int GetTokenAndScopeDelegate(out uint token, out IXCLRDataModule module);

    public delegate int GetValueDelegate<TValue>(out TValue value);

    public delegate int StartEnumMethod(out ulong handle);

    #endregion

    #region Public Methods and Operators

    public static List<TResult> EnumerateInstances<TResult>(StartEnumMethod startEnum, EnumMethod<TResult> enumEnum, EndEnumMethod endEnum)
    {
      ulong handle;
      if (startEnum(out handle) != 0)
      {
        return null;
      }
      var result = new List<TResult>();
      TResult currentInstance;
      while (enumEnum(ref handle, out currentInstance) == 0)
      {
        result.Add(currentInstance);
      }
      endEnum(handle);
      return result;
    }

    public static List<TResult> EnumerateInstancesCustomSelector<TResult, TRawInstance>(
      StartEnumMethod startEnum,
      EnumMethod<TRawInstance> enumEnum,
      EndEnumMethod endEnum,
      Func<TRawInstance, TResult> valueGetter)
    {
      ulong handle;
      int retCode;
      if ((retCode = startEnum(out handle)) != 0)
      {
        return new List<TResult>();
      }
      var result = new List<TResult>();
      TRawInstance currentInstance;
      while (enumEnum(ref handle, out currentInstance) == 0)
      {
        result.Add(valueGetter(currentInstance));
      }
      endEnum(handle);
      return result;
    }

    public static TValue GetInstanceCustomHandler<TValue, TRawValue>(GetValueDelegate<TRawValue> valueSource, Func<TRawValue, bool, TValue> valueFetcher)
    {
      TRawValue resultInstance;
      int resultCode = valueSource(out resultInstance);
      return valueFetcher(resultInstance, resultCode == 0);
    }

    public static TValue GetInstanceCustomOrNull<TValue, TRawValue>(GetValueDelegate<TRawValue> valueSource, Func<TRawValue, TValue> valueConverter)
    {
      TRawValue resultInstance;
      int resultCode = valueSource(out resultInstance);
      return resultCode == 0 ? valueConverter(resultInstance) : default(TValue);
    }

    public static TValue GetInstanceOrNull<TValue>(GetValueDelegate<TValue> valueSource)
    {
      TValue resultInstance;
      int resultCode = valueSource(out resultInstance);
      return resultCode == 0 ? resultInstance : default(TValue);
    }

    public static string GetName(GetNameDelegate getName)
    {
      const int fixedLen = 1000;
      var nameSb = new StringBuilder(fixedLen);
      uint realLen;
      int retCode;

      if ((retCode = getName(fixedLen, out realLen, nameSb)) == 0)
      {
        string resultString = nameSb.ToString();

        //Try to trim value if possible
        if (realLen != 0 && resultString.Length >= realLen - 1)
        {
          return resultString.Substring(0, (int)realLen - 1);
        }
        return resultString;
      }
      return null;
    }

    public static bool GetTokenAndScope(GetTokenAndScopeDelegate tokenAndScopeSource, out uint token, out XClrDataModuleWrapper module)
    {
      IXCLRDataModule nModule;
      int result = tokenAndScopeSource(out token, out nModule);
      if (result != 0)
      {
        module = null;
        return false;
      }
      module = new XClrDataModuleWrapper(nModule);
      return true;
    }

    public static TokenAndScopePair GetTokenAndScope(GetTokenAndScopeDelegate tokenAndScopeSource)
    {
      XClrDataModuleWrapper modWrapper;
      uint token;
      if (!GetTokenAndScope(tokenAndScopeSource, out token, out modWrapper))
      {
        Tracer.ReportUnexpectedState("Unable to get unique identifier based on token and scope.");
        //Generate some unique value to never match it.
        return new TokenAndScopePair(0, null);
      }
      return new TokenAndScopePair(token, modWrapper);
    }

    #endregion
  }
}