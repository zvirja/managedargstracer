﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

#endregion

namespace ManagedArgsTracer.Helpers
{
  [CLSCompliant(true)]
  internal class ReflectionHelper
  {
    #region Public Methods and Operators

    public static T GetDelegateForCall<T>(object obj, string methodName, BindingFlags flags) where T : class
    {
      return (T)(object)Delegate.CreateDelegate(typeof(T), obj, obj.GetType().GetMethod(methodName, flags));
    }

    public static void SetValueForValueType<T>(FieldInfo field, ref T item, object value) where T : struct
    {
      field.SetValueDirect(__makeref(item), value);
    }

    #endregion
  }
}