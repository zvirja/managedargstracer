﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#endregion

namespace ManagedArgsTracer.Breakpoints.Definition
{
  public abstract class MethodArg
  {
    #region Fields

    private string friendlyAlias;

    #endregion

    #region Public Properties

    public virtual string FriendlyAlias
    {
      get
      {
        return this.friendlyAlias;
      }
      set
      {
        this.friendlyAlias = value;
      }
    }

    public abstract bool IsPlaceholder { get; }

    #endregion

    #region Public Methods and Operators

    /// <summary>
    /// Is used to get short verbal info about restriction. For instance: AnyType or OfType 'System.String', OfSimpleType Int32
    /// </summary>
    public abstract string GetShortInfoAboutRestrictionPart();

    /// <summary>
    /// Is used to get short verbal info about what to trace. For instance: NoTrace, arg, arg.SubFld
    /// </summary>
    /// <returns></returns>
    public abstract string GetShortInfoAboutTracingPart();

    public abstract bool Match(string fullTypeName);

    #endregion
  }
}