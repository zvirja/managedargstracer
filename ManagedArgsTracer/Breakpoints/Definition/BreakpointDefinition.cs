﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ManagedArgsTracer.Helpers;

#endregion

namespace ManagedArgsTracer.Breakpoints.Definition
{
  internal class BreakpointDefinition
  {
    #region Constructors and Destructors

    public BreakpointDefinition()
    {
      this.MethodArgs = new List<MethodArg>();
    }

    #endregion

    #region Public Properties

    public bool ExpectedInstanceMethod
    {
      get
      {
        return this.ThisArg != null;
      }
    }

    public MethodTracingFlag Flags { get; set; }

    public List<MethodArg> MethodArgs { get; set; }

    public string MethodName { get; set; }

    public string ModuleName { get; set; }

    public string RawDefinition { get; set; }

    public MethodArg ThisArg { get; set; }

    public string[] TypeNames { get; set; }

    #endregion

    #region Public Methods and Operators

    public string GetDefinitionInfo()
    {
      var result = new StringBuilder();
      result.AppendFormat("Module: {0}", this.ModuleName ?? "<ANY>");
      result.AppendLine();

      if (this.TypeNames.Length > 1)
      {
        result.AppendFormat("Type names: {0}", this.TypeNames.JoinToString(","));
      }
      else
      {
        result.AppendFormat("Type name: {0}", this.TypeNames.FirstOrDefault());
      }
      result.AppendLine();

      result.AppendFormat("Method: {0}(", this.MethodName);
      //Here append arguments info
      bool isFirstArgInfo = true;
      foreach (MethodArg methodArg in this.MethodArgs)
      {
        string shortArgumentInfo;
        if (methodArg.FriendlyAlias == null)
        {
          shortArgumentInfo = "<{0},{1}>".FormatWith(methodArg.GetShortInfoAboutRestrictionPart(), methodArg.GetShortInfoAboutTracingPart());
        }
        else
        {
          shortArgumentInfo = "<\"{0}\":{1},{2}>".FormatWith(methodArg.FriendlyAlias, methodArg.GetShortInfoAboutRestrictionPart(), methodArg.GetShortInfoAboutTracingPart());
        }

        if (!isFirstArgInfo)
        {
          result.Append(",");
        }
        else
        {
          isFirstArgInfo = false;
        }
        result.Append(shortArgumentInfo);
      }
      result.AppendLine(")");

      //Trace info whether static or Instance
      if (this.ThisArg != null)
      {
        result.Append("Instance method only. ");
        if (!this.ThisArg.IsPlaceholder)
        {
          result.AppendFormat("This tracing info: {0}", this.ThisArg.GetShortInfoAboutTracingPart());
        }
      }
      else
      {
        result.Append("Static or instance method.");
      }
      result.AppendLine();

      if (this.Flags != MethodTracingFlag.NONE)
      {
        result.Append("Flags: " + this.Flags);
      }

      return result.ToString();
    }

    #endregion
  }
}