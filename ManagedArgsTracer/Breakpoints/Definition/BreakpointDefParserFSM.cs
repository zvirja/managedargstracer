﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ManagedArgsTracer.Breakpoints.Definition.Placeholders;
using ManagedArgsTracer.Breakpoints.Definition.Tracing;
using ManagedArgsTracer.Breakpoints.Tracing.KnownTypes;
using ManagedArgsTracer.Common;
using ManagedArgsTracer.Helpers;

#endregion

namespace ManagedArgsTracer.Breakpoints.Definition
{
  internal class BreakpointDefParserFSM
  {
    #region Constants

    public const char GroupCloseChar = '}';

    public const char GroupOpenChar = '{';

    public const char MethodFlagChar = '/';

    public const char PlaceholderChar = '*';

    public const char TracingChar = '#';

    #endregion

    #region Constructors and Destructors

    public BreakpointDefParserFSM(CommandLineString inputString)
    {
      this.InputString = inputString;
      this.ResultDefinition = new BreakpointDefinition();
    }

    #endregion

    #region Public Properties

    public CommandLineString InputString { get; set; }

    #endregion

    #region Properties

    protected char CurrentChar
    {
      get
      {
        return this.InputString.CurrentChar;
      }
    }

    protected int CurrentCharPos
    {
      get
      {
        return this.InputString.CurrentCharPos;
      }
      set
      {
        this.InputString.CurrentCharPos = value;
      }
    }

    protected BreakpointDefinition ResultDefinition { get; set; }

    #endregion

    #region Public Methods and Operators

    public static bool ContainsMoreBreakpointDef(CommandLineString commandLineString)
    {
      return commandLineString.RawString.IndexOf('(', commandLineString.CurrentCharPos) > -1;
    }

    public virtual BreakpointDefinition ParseDefinition()
    {
      //Skip all spaces, be ready to start.
      this.NavigateToNextNonSpaceChar();

      //At this position definition begins
      int methodDefRawStartInd = this.CurrentCharPos;

      this.DebugCommitSuccess();
      //We before method name
      this.FetchMethodName();
      this.DebugCommitSuccess();
      this.NavigateToNextNonSpaceChar();
      //We on opening parenthesis
      this.FetchMethodArgs();
      this.DebugCommitSuccess();
      this.NavigateToNextNonSpaceChar();

      int methodDefRawLen = this.CurrentCharPos - methodDefRawStartInd;

      //We are on flag, some other definition or process name.
      this.FetchMethodFlags();

      this.ResultDefinition.RawDefinition = this.InputString.RawString.Substring(methodDefRawStartInd, methodDefRawLen).Trim();

      return this.ResultDefinition;
    }

    #endregion

    #region Methods

    protected virtual void AppendMethodArg(MethodArg arg)
    {
      this.ResultDefinition.MethodArgs.Add(arg);
    }

    protected virtual void CollectFullTypeName(out string moduleName, out string typeName, HashSet<char> limitChars)
    {
      StringBuilder moduleOrTypeName = this.InputString.CollectCharsTillStopChar(
        new HashSet<char>(limitChars)
          {
            '!',
            ' '
          });
      if (this.CurrentChar == '!')
      {
        moduleName = moduleOrTypeName.ToString();
        this.CurrentCharPos++;
        //Let's collect type name
        moduleOrTypeName = this.InputString.CollectCharsTillStopChar(
          new HashSet<char>(limitChars)
            {
              ' '
            });
        typeName = moduleOrTypeName.ToString();
      }
      else
      {
        moduleName = null;
        typeName = moduleOrTypeName.ToString();
      }
    }

    protected void DebugCommitSuccess()
    {
      this.InputString.DebugCommitSuccess();
    }

    protected virtual void FetchMethodArg()
    {
      //here we should decide whether argument is placeholder or for tracing.
      int originalPos = this.CurrentCharPos;
      string nextArgument = this.InputString.CollectCharsTillStopChar(',', ')').ToString().Trim();

      bool phCharPresent = nextArgument.IndexOf(PlaceholderChar) > -1;
      bool traceCharPresent = nextArgument.IndexOf(TracingChar) > -1;

      //Special case to mark that this is instance type. Note, "this" might be continued by chain: func(#this.Names.First)
      if (!phCharPresent && !traceCharPresent && nextArgument.EqualsWithCase("this", true))
      {
        this.AppendThisArg(new PlaceholderMethodArg());
        return;
      }

      //Restore position. Fetchers should work from begin. We restore it only if not append "this" mark.
      this.CurrentCharPos = originalPos;

      if (!(phCharPresent ^ traceCharPresent))
      {
        this.InputString.ThrowInvalidOperationExceptionDuringParsing(
          "Argument definition should contain either placeholder({0}) or tracing({1}) character.".FormatWith(PlaceholderChar, TracingChar));
        return;
      }

      //Placeholder char present - this is placeholder definition.
      if (phCharPresent)
      {
        this.FetchPlaceholderMethodArg();
      }
      else
      {
        this.FetchTracingMethodArg();
      }
    }

    protected void NavigateToNextNonSpaceChar()
    {
      this.InputString.NavigateToNextNonSpaceChar();
    }

    private void AppendThisArg(MethodArg thisArg)
    {
      if (this.ResultDefinition.ThisArg != null || this.ResultDefinition.MethodArgs.Count > 0)
      {
        this.InputString.ThrowInvalidOperationExceptionDuringParsing("Method arguments contain more that one 'this' arg or 'this' is not the first arg.");
      }
      this.ResultDefinition.ThisArg = thisArg;
    }

    private string CollectArgumentAlias()
    {
      this.NavigateToNextNonSpaceChar();

      if (!char.IsLetter(this.CurrentChar))
      {
        return null;
      }

      StringBuilder alias = this.InputString.CollectCharsTillNonLetterOrDigit();
      return alias.ToString().Trim();
    }

    private void FetchMethodArgs()
    {
      if (this.CurrentChar != '(')
      {
        this.InputString.ThrowInvalidOperationExceptionDuringParsing("It's expected to get open parenthesis here.");
      }
      //Skip that parenthesis
      this.CurrentCharPos++;
      this.DebugCommitSuccess();

      this.NavigateToNextNonSpaceChar();
      while (this.CurrentChar != ')')
      {
        this.FetchMethodArg();
        this.DebugCommitSuccess();
        //recenty fetched arg. Next char comma or closing parenthesis (or stupid spaces)
        this.NavigateToNextNonSpaceChar();
        if (this.CurrentChar == ',')
        {
          //We have more args to fetch. Skip that comma
          this.CurrentCharPos++;
        }
        this.NavigateToNextNonSpaceChar();
      }
      //Skip ) char.
      this.CurrentCharPos++;
    }

    private void FetchMethodFlag()
    {
      //We are on / char.
      this.CurrentCharPos++;
      var flagName = this.InputString.CollectCharsTillStopChar(' ');
      if (flagName.Length == 0)
      {
        this.InputString.ThrowInvalidOperationExceptionDuringParsing("Flag name could not be empty.");
        return;
      }

      string flagNameStr = flagName.ToString();
      MethodTracingFlag flag = MethodTracingFlagsManager.GetFlagByName(flagNameStr);
      if (flag == MethodTracingFlag.NONE)
      {
        this.InputString.ThrowInvalidOperationExceptionDuringParsing("Flag '{0}' doesn't seem to be a known method flag name.".FormatWith(flagNameStr));
      }

      //Save flag
      this.ResultDefinition.Flags |= flag;
    }

    private void FetchMethodFlags()
    {
      while (this.CurrentChar == MethodFlagChar)
      {
        this.FetchMethodFlag();
        this.DebugCommitSuccess();
        this.NavigateToNextNonSpaceChar();
      }
    }

    private void FetchMethodName()
    {
      string moduleName;
      string typeAndMethodName;
      this.CollectFullTypeName(out moduleName, out typeAndMethodName, new HashSet<char>() { '(' });
      this.ResultDefinition.ModuleName = moduleName;

      typeAndMethodName = typeAndMethodName.Trim();

      //Method name = Type.Name.MethodName
      int typeAndMethodDelimiterInd = typeAndMethodName.LastIndexOf('.');
      if (typeAndMethodDelimiterInd == -1)
      {
        this.InputString.ThrowInvalidOperationExceptionDuringParsing("Method name is wrong - no dots.");
        return;
      }

      if (typeAndMethodDelimiterInd == 0)
      {
        this.InputString.ThrowInvalidOperationExceptionDuringParsing("Type name is wrong - cannot start with dot.");
        return;
      }

      //to properly handle Type..ctor
      if (typeAndMethodName[typeAndMethodDelimiterInd - 1] == '.')
      {
        typeAndMethodDelimiterInd = typeAndMethodDelimiterInd - 1;
      }

      string typeNameCandidate = typeAndMethodName.Substring(0, typeAndMethodDelimiterInd);

      //Type name could be composite
      if (typeNameCandidate[0] == GroupOpenChar)
      {
        if (typeNameCandidate[typeNameCandidate.Length - 1] != GroupCloseChar)
        {
          this.InputString.ThrowInvalidOperationExceptionDuringParsing("Group with type names is wrong - no closing brace.");
          return;
        }

        if (typeNameCandidate.Length < 3)
        {
          this.InputString.ThrowInvalidOperationExceptionDuringParsing("Group with type names is wrong - no types inside.");
          return;
        }

        this.ResultDefinition.TypeNames = typeNameCandidate.Substring(1, typeNameCandidate.Length - 2)
          .Split(",".ToCharArray())
          .Select(val => val.Trim())
          .ToArray();
      }
      else
      {
        this.ResultDefinition.TypeNames = new string[] { typeNameCandidate };
      }

      this.ResultDefinition.MethodName = typeAndMethodName.Substring(typeAndMethodDelimiterInd + 1);

      this.DebugCommitSuccess();
    }

    /* There are only following forms of placeholders:
     * func(*) - any type
     * func(*alias) - any type, alias
     * func(type*) - type restrictive.
     * func(type*alias) - type restrictive, with alias
     * We should handle two cases - when type is simple type and user type. User type allows '!' to be present, but doesn't require.
     * 
     */

    private void FetchPlaceholderMethodArg()
    {
      //We are at the beginning. This is either restrictive or non-restrictive placeholder.

      this.NavigateToNextNonSpaceChar();

      MethodArg resultArg = null;

      do
      {
        //That means that preceding type is omitted.
        if (this.CurrentChar == PlaceholderChar)
        {
          resultArg = new PlaceholderMethodArg();
          this.CurrentCharPos++;
          break;
        }

        string moduleName;
        string typeName;
        this.CollectFullTypeName(
          out moduleName,
          out typeName,
          new HashSet<char>()
            {
              ',',
              ')',
              PlaceholderChar
            });

        this.InputString.NavigateToNextNonSpaceChar();

        if (this.CurrentChar != PlaceholderChar)
        {
          this.InputString.ThrowInvalidOperationExceptionDuringParsing("Placeholder char ({0}) is expected.".FormatWith(PlaceholderChar));
          return;
        }
        this.CurrentCharPos++;

        //For case when this is a known type. For instance: func(int*) or func(double*)

        IValueReader valueReader = KnownTypesManager.ResolveValueReader(typeName);
        if (valueReader != null)
        {
          resultArg = new KnownTypePlaceholderMethodArg(valueReader);
          break;
        }

        //This is a type, that wasn't recognized as a known. For instance: func(Custom.TypeAA*).
        resultArg = new TypeRestrictivePlaceholderMethodArg(typeName, moduleName);
      }
      while (false);

      string alias = this.CollectArgumentAlias();
      if (alias.NotNullNotEmpty())
      {
        resultArg.FriendlyAlias = alias.Trim();
      }

      this.AppendMethodArg(resultArg);
    }

    /*
     * Values of these argumens will be traced. The following syntaxes are supported.
     * Suppose that # is tracing char.
     * 
     * func(#) - Trace: Simple type. Retriction: NONE.
     * func(type#) - Trace: Simple type. Restriction by type name.
     * func(#alias) - Trace: Simple type. Retriction: NONE. Specify friendly alias.
     * func(type#alias) - Trace: Simple type. Restriction by type name. Specify friendly alias.
     * func(#ch.ain) - Trace: Dereferenced chain. Restriction: NONE.
     * func(##ch.ain) - Trace: Dereferenced chain. Restriction: NONE. Type is value type.
     * func(type#ch.ain) - Trace: Dereferenced chain. Restriction by type name.
     * func(type##ch.ain) - Trace: Dereferenced chain. Restriction by type name. Type is value type.
     * func(#this) - Trace this. Only if current type is simple. Mark that current method is instance.
     * func(#this.Subchain) - Trace chain of this. Mark that current method is instance.
     * func(##this.Subchain) - Trace chain of this. Mark that current method is instance and type is struct.
     */

    private void FetchTracingMethodArg()
    {
      this.NavigateToNextNonSpaceChar();
      string moduleName;
      string typeName;
      this.CollectFullTypeName(
        out moduleName,
        out typeName,
        new HashSet<char>
          {
            '#'
          });

      bool typeNameIsSpecified = false;
      FullTypeName fullType = new FullTypeName();
      if (typeName.NotNullNotEmpty())
      {
        fullType = moduleName.IsNullOrEmpty() ? new FullTypeName(typeName) : new FullTypeName(typeName, moduleName);
        typeNameIsSpecified = true;
      }

      //Type name was collected (or wasn't). Current char should be tracing char.
      this.NavigateToNextNonSpaceChar();

      if (this.CurrentChar != TracingChar)
      {
        this.InputString.ThrowInvalidOperationExceptionDuringParsing("Unable to parse argument.");
        return;
      }

      this.CurrentCharPos++;

      //If duplicated tracing char - this is struct.
      bool isValueType = false;
      if (this.CurrentChar == TracingChar)
      {
        isValueType = true;
        this.CurrentCharPos++;
      }

      this.NavigateToNextNonSpaceChar();

      //Collect non-grouping arg

      if (this.CurrentChar != GroupOpenChar)
      {
        string argChain = this.InputString.CollectCharsTillStopChar(',', ')', ' ').ToString().Trim();

        bool isThis;
        MethodArg resultArg = this.ProcessMethodArgChain(argChain, isValueType, typeNameIsSpecified, fullType, out isThis);
        if (isThis)
        {
          this.AppendThisArg(resultArg);
        }
        else
        {
          this.AppendMethodArg(resultArg);
        }
        return;
      }

      //Skip {
      this.CurrentCharPos++;

      string argChains = this.InputString.CollectCharsTillStopChar(GroupCloseChar).ToString().Trim();

      //Skip }
      this.CurrentCharPos++;

      //Split subchains by ',' and later process each one individually
      string[] nonParsedSubChains = argChains.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

      bool isThisDef = false;
      var parsedArgs = new List<MethodArg>();
      foreach (string nonParsedSubChain in nonParsedSubChains)
      {
        bool isThisLocal;
        MethodArg arg = this.ProcessMethodArgChain(nonParsedSubChain.Trim(), isValueType, typeNameIsSpecified, fullType, out isThisLocal);
        parsedArgs.Add(arg);
        if (isThisLocal)
        {
          isThisDef = true;
        }
      }

      if (parsedArgs.Count == 0)
      {
        this.InputString.ThrowInvalidOperationExceptionDuringParsing("Group argument definition should contain at least one argument definition.");
        return;
      }

      MethodArg resultGroupDefinition = typeNameIsSpecified ? new GroupRestrictiveTracingArg(parsedArgs, fullType) : new GroupAnyTracingArg(parsedArgs);
      if (isThisDef)
      {
        this.AppendThisArg(resultGroupDefinition);
      }
      else
      {
        this.AppendMethodArg(resultGroupDefinition);
      }
    }

    /*
     * Method to process arguments chain.
     * */

    private MethodArg ProcessMethodArgChain(string chain, bool isValueType, bool isTypeSpecified, FullTypeName typeName, out bool isThis)
    {
      isThis = false;

      /* func(#this) - Trace this. Only if current type is simple. Mark that current method is instance. */
      if (chain.EqualsWithCase("this", true))
      {
        isThis = true;
        if (isTypeSpecified)
        {
          this.InputString.ThrowInvalidOperationExceptionDuringParsing("'this' argument cannot be specified with type.");
          return null;
        }
        return new KnownAnyTracingArg();
      }

      /* func(#) - Trace: Simple type. Retriction: NONE.
       * func(type#) - Trace: Simple type. Restriction by type name. 
       * func(#alias) - Trace: Simple type. Restriction: NONE. Specify friendly alias.
       * func(type#alias) - Trace: Simple type. Restriction by type name. Specify friendly alias. */
      if (chain.IsNullOrEmpty() || chain.IndexOf('.') < 0)
      {
        MethodArg resultArg = null;

        if (isTypeSpecified)
        {
          IValueReader knownReader = KnownTypesManager.ResolveValueReader(typeName.TypeName);
          //Invalid known type. Is not allowed, because non-known type are unknown for tracing.
          if (knownReader == null)
          {
            this.InputString.ThrowInvalidOperationExceptionDuringParsing("The '{0}' type is not a valid known type name.".FormatWith(typeName.TypeName));
            return null;
          }
          resultArg = new KnownRestrictiveTracingArg(knownReader);
        }
        else
        {
          resultArg = new KnownAnyTracingArg();
        }

        string trimmedArgsChain = chain.Trim();
        if (trimmedArgsChain.NotNullNotEmpty())
        {
          resultArg.FriendlyAlias = trimmedArgsChain;
        }

        return resultArg;
      }

      /* func(#this.Subchain) - Trace chain of this. Mark that current method is instance.
       * func(##this.Subchain) - Trace chain of this. Mark taht current method is instance and current class is struct*/
      if (chain.StartsWith("this.", StringComparison.Ordinal))
      {
        isThis = true;
        if (isTypeSpecified)
        {
          this.InputString.ThrowInvalidOperationExceptionDuringParsing("'this.Subchain..' argument cannot be specified with type.");
          return null;
        }
        return new ChainedAnyTracingArg(chain, isValueType);
      }

      /* func(#chain) - Trace: Dereferenced chain. Restriction: NONE.
       * func(type#chain) - Trace: Dereferenced chain. Restriction by type name.
       * func(type##chain) - Trace: Dereferenced chain. Restriction by type name. Type is value type. */
      if (isTypeSpecified)
      {
        return new ChainedRestrictiveTracingArg(chain, typeName, isValueType);
      }
      return new ChainedAnyTracingArg(chain, isValueType);
    }

    #endregion
  }
}