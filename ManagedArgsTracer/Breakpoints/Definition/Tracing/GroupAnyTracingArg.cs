﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using JetBrains.Annotations;

using ManagedArgsTracer.Diagnostics;
using ManagedArgsTracer.Helpers;

#endregion

namespace ManagedArgsTracer.Breakpoints.Definition.Tracing
{
  internal class GroupAnyTracingArg : MethodArg
  {
    #region Constructors and Destructors

    public GroupAnyTracingArg([NotNull] List<MethodArg> innerArgs)
    {
      Assert.ArgumentNotNull(innerArgs, "innerArgs");
      Assert.IsTrue(innerArgs.Count > 0, "Collection of innerArgs should contain at least one argument");
      Assert.IsTrue(innerArgs.All(arg => !arg.IsPlaceholder), "Collection of innerArgs should not contain placeholders");
      this.ArgsToTrace = innerArgs;
    }

    #endregion

    #region Public Properties

    public List<MethodArg> ArgsToTrace { get; private set; }

    public override string FriendlyAlias
    {
      get
      {
        return this.ArgsToTrace[0].FriendlyAlias;
      }
      set
      {
        //dummy
      }
    }

    public override bool IsPlaceholder
    {
      get
      {
        return false;
      }
    }

    #endregion

    #region Public Methods and Operators

    public override string GetShortInfoAboutRestrictionPart()
    {
      return "Any";
    }

    public override string GetShortInfoAboutTracingPart()
    {
      return "{{{0}}}".FormatWith(this.ArgsToTrace.Select(arg => arg.GetShortInfoAboutTracingPart()).JoinToString(","));
    }

    public override bool Match(string fullTypeName)
    {
      return true;
    }

    #endregion
  }
}