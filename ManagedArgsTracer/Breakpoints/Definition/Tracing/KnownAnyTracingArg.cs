﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ManagedArgsTracer.Breakpoints.Tracing.KnownTypes;

#endregion

namespace ManagedArgsTracer.Breakpoints.Definition.Tracing
{
  internal class KnownAnyTracingArg : TracingMethodArg
  {
    #region Public Methods and Operators

    public override string GetShortInfoAboutRestrictionPart()
    {
      return "AnyKnown";
    }

    public override string GetShortInfoAboutTracingPart()
    {
      return "TraceThisKnown";
    }

    public override bool Match(string fullTypeName)
    {
      //If type name could be resolved as simple, than it's match.
      return KnownTypesManager.ResolveValueReader(fullTypeName) != null;
    }

    #endregion
  }
}