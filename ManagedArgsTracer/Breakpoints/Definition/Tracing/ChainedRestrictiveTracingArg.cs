﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using JetBrains.Annotations;

using ManagedArgsTracer.Common;
using ManagedArgsTracer.Helpers;

#endregion

namespace ManagedArgsTracer.Breakpoints.Definition.Tracing
{
  internal class ChainedRestrictiveTracingArg : ChainedAnyTracingArg
  {
    #region Constructors and Destructors

    public ChainedRestrictiveTracingArg([NotNull] string chain, FullTypeName argType, bool isValueType)
      : base(chain, isValueType)
    {
      this.ArgType = argType;
    }

    #endregion

    #region Public Properties

    public FullTypeName ArgType { get; set; }

    #endregion

    #region Public Methods and Operators

    public override string GetShortInfoAboutRestrictionPart()
    {
      return "OfType '{0}'".FormatWith(this.ArgType);
    }

    public override bool Match(string fullTypeName)
    {
      return this.ArgType.TypeName.EqualsWithCase(fullTypeName, true);
    }

    #endregion
  }
}