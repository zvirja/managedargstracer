﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using JetBrains.Annotations;

using ManagedArgsTracer.Breakpoints.Tracing.KnownTypes;
using ManagedArgsTracer.Diagnostics;
using ManagedArgsTracer.Helpers;

#endregion

namespace ManagedArgsTracer.Breakpoints.Definition.Tracing
{
  internal class KnownRestrictiveTracingArg : KnownAnyTracingArg
  {
    #region Constructors and Destructors

    public KnownRestrictiveTracingArg([NotNull] IValueReader valueReader)
    {
      Assert.ArgumentNotNull(valueReader, "valueReader");
      this.ValueReader = valueReader;
    }

    #endregion

    #region Public Properties

    public IValueReader ValueReader { get; set; }

    #endregion

    #region Public Methods and Operators

    public override string GetShortInfoAboutRestrictionPart()
    {
      return "OfKnownType {0}".FormatWith(this.ValueReader.MainName);
    }

    public override bool Match(string fullTypeName)
    {
      return this.ValueReader.KnowHowToReadType(fullTypeName);
    }

    #endregion
  }
}