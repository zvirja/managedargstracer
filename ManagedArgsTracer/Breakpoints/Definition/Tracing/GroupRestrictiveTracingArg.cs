﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using JetBrains.Annotations;

using ManagedArgsTracer.Common;
using ManagedArgsTracer.Helpers;

#endregion

namespace ManagedArgsTracer.Breakpoints.Definition.Tracing
{
  internal class GroupRestrictiveTracingArg : GroupAnyTracingArg
  {
    #region Constructors and Destructors

    public GroupRestrictiveTracingArg([NotNull] List<MethodArg> innerArgs, FullTypeName argType)
      : base(innerArgs)
    {
      this.ArgType = argType;
    }

    #endregion

    #region Public Properties

    public FullTypeName ArgType { get; set; }

    #endregion

    #region Public Methods and Operators

    public override string GetShortInfoAboutRestrictionPart()
    {
      return "OfType '{0}'".FormatWith(this.ArgType);
    }

    public override bool Match(string fullTypeName)
    {
      return this.ArgType.TypeName.EqualsWithCase(fullTypeName, true);
    }

    #endregion
  }
}