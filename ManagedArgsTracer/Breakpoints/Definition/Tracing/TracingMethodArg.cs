﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#endregion

namespace ManagedArgsTracer.Breakpoints.Definition.Tracing
{
  internal abstract class TracingMethodArg : MethodArg
  {
    #region Public Properties

    public override bool IsPlaceholder
    {
      get
      {
        return false;
      }
    }

    #endregion
  }
}