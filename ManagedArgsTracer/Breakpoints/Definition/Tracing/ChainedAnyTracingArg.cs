﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using JetBrains.Annotations;

using ManagedArgsTracer.Diagnostics;

#endregion

namespace ManagedArgsTracer.Breakpoints.Definition.Tracing
{
  internal class ChainedAnyTracingArg : TracingMethodArg
  {
    #region Constructors and Destructors

    public ChainedAnyTracingArg([NotNull] string chain, bool isValueType)
    {
      Assert.ArgumentNotNullOrEmpty(chain, "chain");
      this.IsValueType = isValueType;
      this.RawChain = chain;
      this.Chain = chain.Split(".".ToCharArray());
    }

    #endregion

    #region Public Properties

    public string[] Chain { get; private set; }

    public override string FriendlyAlias
    {
      get
      {
        return this.Chain[0];
      }
      set
      {
        //dummy
      }
    }

    public bool IsValueType { get; set; }

    public string RawChain { get; protected set; }

    #endregion

    #region Public Methods and Operators

    public override string GetShortInfoAboutRestrictionPart()
    {
      return "Any";
    }

    public override string GetShortInfoAboutTracingPart()
    {
      return "Trace " + (this.IsValueType ? "*" : string.Empty) + this.RawChain;
    }

    public override bool Match(string fullTypeName)
    {
      return true;
    }

    #endregion
  }
}