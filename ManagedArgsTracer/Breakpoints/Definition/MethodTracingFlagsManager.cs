﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using JetBrains.Annotations;

using ManagedArgsTracer.Diagnostics;
using ManagedArgsTracer.Helpers;

#endregion

namespace ManagedArgsTracer.Breakpoints.Definition
{
  [Flags]
  internal enum MethodTracingFlag : uint
  {
    NONE = 0,

    NO_STACK_TRACE = 0x1,

    NO_SHORT_STRUCTURES_FIX = 0x2,

    NO_THIS_TYPE = 0x4,

    FORCE_THIS_TYPE = 0x8,

    TRACE_NATIVE_FRAMES = 0x10,

    USE_DAC_STACKWALK = 0x20,

    TRACE_FRAMES_WITH_ADDRESSES = 0x40 | TRACE_NATIVE_FRAMES,

    DISALLOW_INHERITED = 0x80,

    USE_NATIVE_STACKWALK = 0x100,

    USE_CLRMD_STACKWALK = 0x200,

    USE_ALL_STACKWALKS = 0x400,

    CHAIN_INTERMEDIATE_VALUES = 0x800,

    CHAIN_NO_INTERMEDIATE = 0x1000,

    COUNT_METHOD_HITS_ONLY = 0x2000,

    COUNT_METHOD_HITS_DISPLAY_HIT = 0x4000 | COUNT_METHOD_HITS_ONLY
  }

  internal class MethodTracingFlagsManager
  {
    #region Static Fields

    public static Dictionary<string[], MethodTracingFlag> KnownFlags;

    #endregion

    #region Constructors and Destructors

    static MethodTracingFlagsManager()
    {
      KnownFlags = new Dictionary<string[], MethodTracingFlag>
                     {
                       {
                         new[]
                           {
                             "nostack", "nos"
                           },
                         MethodTracingFlag.NO_STACK_TRACE
                       },
                       {
                         new[]
                           {
                             "nostructfix", "nosf"
                           },
                         MethodTracingFlag.NO_SHORT_STRUCTURES_FIX
                       },
                       {
                         new[]
                           {
                             "nothistype", "nott"
                           },
                         MethodTracingFlag.NO_THIS_TYPE
                       },
                       {
                         new[]
                           {
                             "forcethistype", "ftt"
                           },
                         MethodTracingFlag.FORCE_THIS_TYPE
                       },
                       {
                         new[]
                           {
                             "nativeframes", "nf"
                           },
                         MethodTracingFlag.TRACE_NATIVE_FRAMES
                       },
                       {
                         new[]
                           {
                             "dacstack", "ds"
                           },
                         MethodTracingFlag.USE_DAC_STACKWALK
                       },
                       {
                         new[]
                           {
                             "addressstack", "as"
                           },
                         MethodTracingFlag.TRACE_FRAMES_WITH_ADDRESSES
                       },
                       {
                         new[]
                           {
                             "noinherited", "noi"
                           },
                         MethodTracingFlag.DISALLOW_INHERITED
                       },
                       {
                         new[]
                           {
                             "clrmdstack", "cs"
                           },
                         MethodTracingFlag.USE_CLRMD_STACKWALK
                       },
                       {
                         new[]
                           {
                             "nativestack", "ns"
                           },
                         MethodTracingFlag.USE_NATIVE_STACKWALK
                       },
                       {
                         new[]
                           {
                             "allstacks", "asw"
                           },
                         MethodTracingFlag.USE_ALL_STACKWALKS
                       },
                       {
                         new[]
                           {
                             "chainmediatevalues", "cmv"
                           },
                         MethodTracingFlag.CHAIN_INTERMEDIATE_VALUES
                       },
                       {
                         new[]
                           {
                             "chainnomediate", "cnm"
                           },
                         MethodTracingFlag.CHAIN_NO_INTERMEDIATE
                       },
                       {
                         new[]
                           {
                             "counthits", "ch"
                           },
                         MethodTracingFlag.COUNT_METHOD_HITS_ONLY
                       },
                       {
                         new[]
                           {
                             "countdisplayhits", "cdh"
                           },
                         MethodTracingFlag.COUNT_METHOD_HITS_DISPLAY_HIT
                       },
                     };

#if DEBUG
      ValidateKnownFlagsForDuplicates();
#endif
    }

    #endregion

    #region Public Methods and Operators

    /// <summary>
    /// Tries to resolve known flag. If unable to resolve, MethodTracingFlag.NONE is returned.
    /// </summary>
    public static MethodTracingFlag GetFlagByName([NotNull] string flagName)
    {
      Assert.ArgumentNotNullOrEmpty(flagName, "flagName");
      foreach (KeyValuePair<string[], MethodTracingFlag> flagCandidate in KnownFlags)
      {
        if (flagCandidate.Key.Any(fl => fl.EqualsWithCase(flagName, false)))
        {
          return flagCandidate.Value;
        }
      }
      return MethodTracingFlag.NONE;
    }

    public static bool IsFlagCompatibleWithExisting(MethodTracingFlag currentFlagSet, MethodTracingFlag newFlag)
    {
      return true;
    }

    #endregion

    #region Methods

    private static void ValidateKnownFlagsForDuplicates()
    {
      var present = new HashSet<string>();
      foreach (KeyValuePair<string[], MethodTracingFlag> knownFlag in KnownFlags)
      {
        foreach (string key in knownFlag.Key)
        {
          Assert.IsTrue(!present.Contains(key), "Duplicated key detected");
          present.Add(key);
        }
      }
    }

    #endregion
  }
}