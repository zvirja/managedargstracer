﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ManagedArgsTracer.Common;
using ManagedArgsTracer.Helpers;

#endregion

namespace ManagedArgsTracer.Breakpoints.Definition
{
  internal static class BreakpointDefinitionsParser
  {
    #region Public Methods and Operators

    public static List<BreakpointDefinition> ParseBreakpointDefinitions(CommandLineString commandLineString)
    {
      SanityCheckInputArgs(commandLineString);

      var result = new List<BreakpointDefinition>();
      while (BreakpointDefParserFSM.ContainsMoreBreakpointDef(commandLineString))
      {
        var breakpointParser = new BreakpointDefParserFSM(commandLineString);
        BreakpointDefinition oneMoreBreakpointDef = breakpointParser.ParseDefinition();
        result.Add(oneMoreBreakpointDef);
      }

      if (result.Count == 0)
      {
        commandLineString.ThrowInvalidOperationExceptionDuringParsing("No breakpoint definitions were found. Likely, arguments format is wrong.");
      }

      return result;
    }

    #endregion

    #region Methods

    /// <summary>
    /// Stupid check, but who knows probably sometimes it will help to catch errors.
    /// </summary>
    private static void SanityCheckInputArgs(CommandLineString inputString)
    {
      var rawInputString = inputString.RawString;
      if (rawInputString.IsNullOrEmpty())
      {
        inputString.ThrowInvalidOperationExceptionDuringParsing("Arguments are empty.");
      }
      if (rawInputString.Trim().IsNullOrEmpty())
      {
        inputString.ThrowInvalidOperationExceptionDuringParsing("Arguments are empty.");
      }
      if (rawInputString.IndexOf("(", StringComparison.OrdinalIgnoreCase) < 0)
      {
        inputString.ThrowInvalidOperationExceptionDuringParsing("Arguments don't contain any '(' char. Doesn't seem to be valid.");
      }
      if (rawInputString.IndexOf(")", StringComparison.OrdinalIgnoreCase) < 0)
      {
        inputString.ThrowInvalidOperationExceptionDuringParsing("Arguments don't contain any ')' char. Doesn't seem to be valid.");
      }
      if (rawInputString.Trim().Split(" ".ToCharArray(), StringSplitOptions.RemoveEmptyEntries).Length < 2)
      {
        inputString.ThrowInvalidOperationExceptionDuringParsing("Splitted by space args contain less than 2 parts. It's impossible for valid arguments.");
      }
    }

    #endregion
  }
}