﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using JetBrains.Annotations;

using ManagedArgsTracer.Breakpoints.Tracing.KnownTypes;
using ManagedArgsTracer.Diagnostics;

#endregion

namespace ManagedArgsTracer.Breakpoints.Definition.Placeholders
{
  internal class KnownTypePlaceholderMethodArg : PlaceholderMethodArg
  {
    #region Constructors and Destructors

    public KnownTypePlaceholderMethodArg([NotNull] IValueReader valueReader)
    {
      Assert.ArgumentNotNull(valueReader, "valueReader");
      this.ValueReader = valueReader;
    }

    #endregion

    #region Properties

    private IValueReader ValueReader { get; set; }

    #endregion

    #region Public Methods and Operators

    public override bool Match(string fullTypeName)
    {
      return this.ValueReader.KnowHowToReadType(fullTypeName);
    }

    #endregion
  }
}