﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#endregion

namespace ManagedArgsTracer.Breakpoints.Definition.Placeholders
{
  internal class PlaceholderMethodArg : MethodArg
  {
    #region Public Properties

    public override bool IsPlaceholder
    {
      get
      {
        return true;
      }
    }

    #endregion

    #region Public Methods and Operators

    public override string GetShortInfoAboutRestrictionPart()
    {
      return "Any";
    }

    public override string GetShortInfoAboutTracingPart()
    {
      return "NoTrace";
    }

    public override bool Match(string fullTypeName)
    {
      return true;
    }

    #endregion
  }
}