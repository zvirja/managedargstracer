﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using JetBrains.Annotations;

using ManagedArgsTracer.Common;
using ManagedArgsTracer.Diagnostics;
using ManagedArgsTracer.Helpers;

#endregion

namespace ManagedArgsTracer.Breakpoints.Definition.Placeholders
{
  internal class TypeRestrictivePlaceholderMethodArg : PlaceholderMethodArg
  {
    #region Constructors and Destructors

    public TypeRestrictivePlaceholderMethodArg([NotNull] string variableTypeName, string variableModule = null)
    {
      Assert.ArgumentNotNullOrEmpty(variableTypeName, "variableTypeName");
      this.TypeName = new FullTypeName(variableTypeName, variableModule);
    }

    #endregion

    #region Public Properties

    public FullTypeName TypeName { get; protected set; }

    #endregion

    #region Public Methods and Operators

    public override string GetShortInfoAboutRestrictionPart()
    {
      return "OfType '{0}'".FormatWith(this.TypeName);
    }

    public override bool Match(string fullTypeName)
    {
      return this.TypeName.TypeName.EqualsWithCase(fullTypeName, true);
    }

    #endregion
  }
}