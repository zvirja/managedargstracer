﻿#region

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

using ManagedArgsTracer.Breakpoints.Definition;
using ManagedArgsTracer.Common;
using ManagedArgsTracer.Diagnostics;
using ManagedArgsTracer.Helpers;
using ManagedArgsTracer.Runtime;
using ManagedArgsTracer.Runtime.Wrappers;
using ManagedArgsTracer.Session;

using Microsoft.Diagnostics.Runtime;
using Microsoft.Diagnostics.Runtime.Interop;

#endregion

namespace ManagedArgsTracer.Breakpoints
{
  internal class BreakpointsManager
  {
    #region Constants

    private const uint DEBUG_ANY_ID = uint.MaxValue;

    #endregion

    #region Constructors and Destructors

    public BreakpointsManager(SessionManager sessionManager)
    {
      this.SessionManager = sessionManager;
      this.FastBreakpointsLookupTable = new Dictionary<uint, RuntimeBreakpoint>();
      this.AlreadyBreakpoinedAdresses = new HashSet<ulong>();
      this.ActiveBreakpoints = new List<IDebugBreakpoint>();
      this.InitializeAllBreakpoints();

      this.SessionManager.NotificationsSource.MethodJitted += this.OnDeferredMethodJitted;
      this.SessionManager.NotificationsSource.ModuleLoaded += this.OnDeferredModuleLoaded;
      this.SessionManager.NotificationsSource.BreakpointHit += this.OnBreakpointHit;
    }

    #endregion

    #region Public Properties

    public List<RuntimeBreakpoint> Active { get; set; }

    public List<RuntimeBreakpoint> AllBreakpoints { get; set; }

    public bool AllModulesResolved
    {
      get
      {
        return this.PendingQueue.All(bp => bp.ModuleIsLoaded);
      }
    }

    public Dictionary<uint, RuntimeBreakpoint> FastBreakpointsLookupTable { get; set; }

    public List<RuntimeBreakpoint> Invalid { get; set; }

    public List<RuntimeBreakpoint> PendingQueue { get; set; }

    #endregion

    #region Properties

    protected SessionManager SessionManager { get; set; }

    /// <summary>
    /// Holds all the set breakpoints to cleanup them on exit.
    /// </summary>
    private List<IDebugBreakpoint> ActiveBreakpoints { get; set; } 

    /// <summary>
    /// Holds collection of all the addresses for which breakpoint has been set.
    /// </summary>
    private HashSet<ulong> AlreadyBreakpoinedAdresses { get; set; }

    #endregion

    #region Public Methods and Operators

    /// <summary>
    /// Removes all the set breakpoits
    /// </summary>
    public void CleanupBreakpoints()
    {
      IDebugControl debugControl = this.SessionManager.NativeDebugInterfaces.DebugControl;

      foreach (IDebugBreakpoint breakpoint in this.ActiveBreakpoints)
      {
        try
        {
          int retCode = debugControl.RemoveBreakpoint(breakpoint);
          if (retCode != 0)
          {
            Tracer.ReportUnexpectedState("Unable to remove breakpoint. Ret code: " + retCode.ToString("X"));
          }
        }
        catch (Exception ex)
        {
          Log.Error("Unable to remove breakpoint.", ex, this);
          Tracer.ReportUnexpectedState("Unable to remove breakpoint.");
        }
      }
    }

    public void PrintStatistics()
    {
      var buffer = new StringBuilder();
      buffer.AppendLine();
      buffer.AppendLine("-----");
      buffer.AppendLine("METHOD HITS STATISTICS");
      buffer.AppendLine("-----");

      bool hitInfoIsPresent = false;
      foreach (RuntimeBreakpoint runtimeBreakpoint in this.AllBreakpoints)
      {
        if (runtimeBreakpoint.ArgumentsTracer.PrintHitStatistics(buffer))
        {
          hitInfoIsPresent = true;
        }
      }

      if (!hitInfoIsPresent)
      {
        buffer.AppendLine("NO METHODS HAS BEEN BREAKPOINED DURING THE SESSION.");
      }

      Tracer.PublicOutput(buffer.ToString());
    }

    public void ResolveBreakpoints()
    {
      this.ResolveAllPossibleBreakpoints();
      if (this.PendingQueue.Count == 0)
      {
        Tracer.PublicOutput("All method definitions were bound to classes.");
      }
      else if (this.AllModulesResolved)
      {
        Tracer.PublicOutput("All method definitions were bound to modules.");
      }
      else
      {
        this.SessionManager.NotificationsSource.EnableModuleLoadNotifications(this.SessionManager.XCLRDataProcessWrapper);
        Tracer.PrivateOutput("ModuleLoaded notifications were turned on.");
        Tracer.PublicOutput("There are method definitions for which module was not found. Will be looking for upcoming module loads to find a match.");
      }
    }

    #endregion

    #region Methods

    private static string GetTypeMessageDependingOnNumOfTypes(IEnumerable<string> types, string patternIfOne, string pattertIfMore)
    {
      if (types.Count() > 1)
      {
        return pattertIfMore.FormatWith(types.JoinToString(","));
      }
      return patternIfOne.FormatWith(types.FirstOrDefault());
    }

    private void AppendMethodDefinitionsForType(
      List<InheritanceMember<XClrDataMethodDefinitionWraper>> resultBuffer,
      RuntimeBreakpoint runtimeBreakpoint,
      XClrDataTypeDefinitionWrapper typeDefinition)
    {
      runtimeBreakpoint.ModulesProbingHistory.Add(typeDefinition.Name);

      IEnumerable<InheritanceMember<XClrDataMethodDefinitionWraper>> methodDefs = typeDefinition.FindMethodDefinitionByName(runtimeBreakpoint.Definition.MethodName)
        .Select(md => new InheritanceMember<XClrDataMethodDefinitionWraper>(md, 0));

      resultBuffer.AddRange(methodDefs);

      //Check whether inheritance is allowed
      if ((runtimeBreakpoint.Flags & MethodTracingFlag.DISALLOW_INHERITED) == MethodTracingFlag.DISALLOW_INHERITED)
      {
        return;
      }

      XClrDataTypeInstanceWrapper typeInstance = typeDefinition.Module.GetTypeInstancesByName(null, typeDefinition.Name).FirstOrDefault(inst => inst.Definition.IsSameObject(typeDefinition));

      if (typeInstance == null)
      {
        Tracer.PublicOutput("The type '{0}' has not been loaded yet. Lookup for the method definitions in the parent types will be skipped.".FormatWith(typeDefinition.Name));
        return;
      }

      //Check whether type is already loaded. If MT is 0 - is not loaded. We should not continue if type is not loaded.
      /* uint token;
        XClrDataModuleWrapper module;

        if (!typeDefinition.GetTokenAndScope(out token, out module))
        {
          Tracer.ReportUnexpectedState("Unable to get token and scope for the {0} type.".FormatWith(typeDefinition.Name));
          return;
        }

        if (this.SessionManager.XCLRDataProcessWrapper.GetMethodTableForType(module.ModuleAddress, token) == 0UL)
        {
          Tracer.PublicOutput("The type '{0}' has not been loaded yet. Lookup for the method definitions in the parent types will be skipped.".FormatWith(typeDefinition.Name));
          return;
        }*/

      XClrDataTypeInstanceWrapper baseType = typeInstance.BaseType;
      if (baseType == null || typeDefinition.BaseType == null)
      {
        return;
      }

      this.ContinueAppendMethodDefinitionsForType(resultBuffer, runtimeBreakpoint, baseType, 1);
    }

    private void ContinueAppendMethodDefinitionsForType(
      List<InheritanceMember<XClrDataMethodDefinitionWraper>> resultBuffer,
      RuntimeBreakpoint runtimeBreakpoint,
      XClrDataTypeInstanceWrapper typeInstance,
      int nestingLevel)
    {
      if (nestingLevel > 1000)
      {
        Tracer.PrivateOutput("Ubnormal inheritance chain detected (1000 chains)! Stack Overflow was prevented!");
        return;
      }

      runtimeBreakpoint.ModulesProbingHistory.Add(typeInstance.Definition.Name);

      IEnumerable<InheritanceMember<XClrDataMethodDefinitionWraper>> methodDefs = typeInstance.FindMethodsByName(runtimeBreakpoint.Definition.MethodName)
        .Select(md => new InheritanceMember<XClrDataMethodDefinitionWraper>(md.Definition, nestingLevel));

      resultBuffer.AddRange(methodDefs);

      XClrDataTypeInstanceWrapper baseType = typeInstance.BaseType;
      //It happens that null is never returned for typeInstance.BaseType.
      if (baseType == null || typeInstance.Definition.BaseType == null)
      {
        return;
      }

      this.ContinueAppendMethodDefinitionsForType(resultBuffer, runtimeBreakpoint, baseType, nestingLevel + 1);
    }

    private void DisableJitNotificationForMethod(XClrDataMethodDefinitionWraper methodDef)
    {
      Tracer.PrivateOutput("Jit notifications for the method '{0}' turned off. Generic: {1}.".FormatWith(methodDef.FullName, methodDef.Generic));
      this.SessionManager.NotificationsSource.DisableJitNotifications(methodDef);
    }

    private void EnableJitNotificationForMethod(XClrDataMethodDefinitionWraper methodDef)
    {
      Tracer.PrivateOutput("Jit notifications for the method '{0}' (uniqId: '{2}') turned on. Generic: {1}.".FormatWith(methodDef.FullName, methodDef.Generic, methodDef.TokenAndScope));
      this.SessionManager.NotificationsSource.EnableJitNotifications(methodDef);
    }

    private void InitializeAllBreakpoints()
    {
      this.AllBreakpoints =
        this.SessionManager.Task.BreakpointDefinitions.Select(
          bpDef => new RuntimeBreakpoint(bpDef, this.SessionManager.NativeDebugInterfaces, this.SessionManager.ClrRuntimeHelper, this.SessionManager.XCLRDataProcessWrapper))
          .ToList();
      this.PendingQueue = new List<RuntimeBreakpoint>(this.AllBreakpoints);
      this.Active = new List<RuntimeBreakpoint>();
      this.Invalid = new List<RuntimeBreakpoint>();
    }

    private bool IsBreakpointValidForMethod(RuntimeBreakpoint breakpoint, XClrDataMethodInstanceWraper methodInstance)
    {
      //If user specified "this" argument, likely it expects that method is non static. It might cause issues during tracing.
      if (!methodInstance.IsInstance && breakpoint.Definition.ExpectedInstanceMethod)
      {
        return false;
      }

      List<MethodArg> expectedArgs = breakpoint.Definition.MethodArgs;

      int numArguments = methodInstance.NumArguments;
      if (expectedArgs.Count != numArguments)
      {
        return false;
      }
      if (expectedArgs.Count == 0)
      {
        return true;
      }
      List<FullTypeName> arguments = methodInstance.Arguments;
      if (arguments == null)
      {
        Tracer.PrivateOutput("Unable to get arguments for '{0}' method.".FormatWith(methodInstance.NameWithArgs));
        return false;
      }
      for (int i = 0; i < expectedArgs.Count; i++)
      {
        MethodArg argDefinition = expectedArgs[i];
        FullTypeName actualArg = arguments[i];
        if (!argDefinition.Match(actualArg.TypeName))
        {
          return false;
        }
      }
      return true;
    }

    private void MoveBreakpointToInvalid(RuntimeBreakpoint breakpoint, string invalidReason)
    {
      if (breakpoint.Invalid)
      {
        Tracer.PrivateOutput(
          "Attempt to move invalid definition '{0}' to invalid queue for one more time. It might happen if a few method instances per definions has been found.".FormatWith(
            breakpoint.Definition.RawDefinition));
        return;
      }
      breakpoint.Invalid = true;
      breakpoint.InvalidReason = invalidReason;
      this.PendingQueue.Remove(breakpoint);
      this.Invalid.Add(breakpoint);

      Tracer.PublicOutput("Method definition '{0}' appears to be invalid. Reason: {1}".FormatWith(breakpoint.Definition.RawDefinition, invalidReason));
    }

    private void OnBreakpointHit(object sender, NativeBreakpointTuple breakpoint)
    {
      RuntimeBreakpoint runtimeBreakpoint;
      if (!this.FastBreakpointsLookupTable.TryGetValue(breakpoint.BreakpointId, out runtimeBreakpoint))
      {
        Tracer.ReportUnexpectedState("Unable to find breakpoint with ID " + breakpoint.BreakpointId);
        return;
      }

      runtimeBreakpoint.ArgumentsTracer.HandleBreakpoint(breakpoint);
    }

    private void OnDeferredMethodJitted(object sender, XClrDataMethodInstanceWraper methodInstance)
    {
      Tracer.PublicOutput("Method '{0}' has been JITTED.".FormatWith(methodInstance.NameWithArgs));

      string instanceUniqID = methodInstance.TokenAndScope.ToString();
      List<RuntimeBreakpoint> suitableBreakpoints =
        this.PendingQueue.Where(rtBp => rtBp.WaitingForJitCount > 0 && rtBp.ClrMethodDefinitionUniqIds.Any(mdDef => mdDef.Value.EqualsWithCase(instanceUniqID, false)))
          .ToList();
      if (!Assert.IsTrueSoft(suitableBreakpoints.Count > 0, "Unable to find breakpoint corresponding to JIT notification."))
      {
        return;
      }

      foreach (RuntimeBreakpoint runtimeBreakpoint in suitableBreakpoints)
      {
        runtimeBreakpoint.WaitingForJitCount--;
        this.ProcessMethodInstanceInternal(runtimeBreakpoint, methodInstance);
      }

      //Should later check whether it will not cause issues for Generic
      //this.DisableJitNotificationForMethod(methodInstance.Definition);
    }

    private void OnDeferredModuleLoaded(object sender, XClrDataModuleWrapper moduleWrapper)
    {
      Tracer.PrivateOutput("Module '{0}' was loaded.".FormatWith(moduleWrapper.Name));

      this.ProcessClrModule(moduleWrapper);
      if (this.AllModulesResolved)
      {
        this.SessionManager.NotificationsSource.DisableModuleLoadNotifications(this.SessionManager.XCLRDataProcessWrapper);
        Tracer.PrivateOutput("ModuleLoaded notifications were turned off.");
        Tracer.PublicOutput("Finally all the method definitions were bound to modules.");
      }
      else
      {
        Tracer.PrivateOutput("Number method definitions that still wait for their module: {0}".FormatWith(this.PendingQueue.Count(rtBp => !rtBp.ModuleIsLoaded)));
      }
    }

    private void ProcessBreakpointWithResolvedTypes(RuntimeBreakpoint runtimeBreakpoint, List<XClrDataTypeDefinitionWrapper> typeDefinitions)
    {
      //All method definitions. Could contain duplicates.
      var duplicatedMethodDefs = new List<InheritanceMember<XClrDataMethodDefinitionWraper>>();
      foreach (XClrDataTypeDefinitionWrapper typeDefinition in typeDefinitions)
      {
        this.AppendMethodDefinitionsForType(duplicatedMethodDefs, runtimeBreakpoint, typeDefinition);
      }

      var nonDuplicatedDefs = new List<InheritanceMember<XClrDataMethodDefinitionWraper>>();
      var alreadyAddedDefs = new HashSet<string>();

      foreach (InheritanceMember<XClrDataMethodDefinitionWraper> inheritanceMember in duplicatedMethodDefs)
      {
        string key = inheritanceMember.Value.TokenAndScope.ToString();
        if (!alreadyAddedDefs.Contains(key))
        {
          nonDuplicatedDefs.Add(inheritanceMember);
          alreadyAddedDefs.Add(key);
        }
      }

      if (nonDuplicatedDefs.Count == 0)
      {
        //Move to invalid if not virtual resolve or base type not found.
        //Warn if DISALLOW_INHERITED flag is activated.
        this.MoveBreakpointToInvalid(
          runtimeBreakpoint,
          "{0} contain the '{1}' method.{2}".FormatWith(
            GetTypeMessageDependingOnNumOfTypes(runtimeBreakpoint.Definition.TypeNames, "The type '{0}' doesn't", "The types '{0}' don't"),
            runtimeBreakpoint.Definition.MethodName,
            (runtimeBreakpoint.Flags & MethodTracingFlag.DISALLOW_INHERITED) == MethodTracingFlag.DISALLOW_INHERITED
              ? " Note, the DISALLOW_INHERITED flag is enabled, that might be a reason of the missed method."
              : string.Empty));
        return;
      }

      runtimeBreakpoint.ClrMethodDefinitionUniqIds.AddRange(nonDuplicatedDefs.Select(inhMem => new InheritanceMember<string>(inhMem.Value.TokenAndScope.ToString(), inhMem.NestingLevel)));
      runtimeBreakpoint.PossibleSuitableMethodDefs += nonDuplicatedDefs.Count;

      this.ProcessMethodDefinitions(runtimeBreakpoint, nonDuplicatedDefs);
    }

    private void ProcessCandidatesExpliticlySpecifiedModule(IEnumerable<RuntimeBreakpoint> explicitlySpecified, XClrDataModuleWrapper module)
    {
      foreach (RuntimeBreakpoint runtimeBreakpoint in explicitlySpecified)
      {
        runtimeBreakpoint.ModuleIsLoaded = true;
        string[] bpTypeNames = runtimeBreakpoint.Definition.TypeNames;

        List<XClrDataTypeDefinitionWrapper> suitableTypes = bpTypeNames
          .SelectMany(
            bpTypeName => module
                            .GetTypeDefinitionsByName(bpTypeName)
                            //Double sure that they match. It seems that CLR has less restrictions for suitable types.
                            .Where(typeDef => typeDef.Name.EqualsWithCase(bpTypeName, true))).ToList();

        if (suitableTypes.Count == 0)
        {
          this.MoveBreakpointToInvalid(
            runtimeBreakpoint,
            "The '{0}' module doesn't contain {1}".FormatWith(
              module.Name,
              GetTypeMessageDependingOnNumOfTypes(bpTypeNames, "{0} type.", "any of '{0}' types.")));
          continue;
        }

        Tracer.PrivateOutput("Module dependent method definition '{0}' was bound to the '{1}' module.".FormatWith(runtimeBreakpoint.Definition.RawDefinition, module.Name));
        this.ProcessBreakpointWithResolvedTypes(runtimeBreakpoint, suitableTypes);
      }
    }

    private void ProcessCandidatesWithAnyModule(IEnumerable<RuntimeBreakpoint> candidates, XClrDataModuleWrapper module)
    {
      foreach (RuntimeBreakpoint candidate in candidates)
      {
        string[] bpTypeNames = candidate.Definition.TypeNames;

        List<XClrDataTypeDefinitionWrapper> suitableTypes = bpTypeNames
          .SelectMany(
            bpTypeName => module
                            .GetTypeDefinitionsByName(bpTypeName)
                            //Double sure that they match. It seems that CLR has less restrictions for suitable types.
                            .Where(typeDef => typeDef.Name.EqualsWithCase(bpTypeName, true))).ToList();

        if (suitableTypes.Count == 0)
        {
          continue;
        }
        candidate.ModuleIsLoaded = true;
        Tracer.PrivateOutput("Module independent method definition '{0}' was bound to the '{1}' module.".FormatWith(candidate.Definition.RawDefinition, module.Name));

        this.ProcessBreakpointWithResolvedTypes(candidate, suitableTypes);
      }
    }

    private void ProcessClrModule(XClrDataModuleWrapper module)
    {
      IEnumerable<RuntimeBreakpoint> allCandidates = this.PendingQueue.Where(rtBp => !rtBp.ModuleIsLoaded).ToArray();

      RuntimeBreakpoint[] explicitlySpecified = allCandidates.Where(rtBp => rtBp.Definition.ModuleName.EqualsWithCase(module.Name, false)).ToArray();
      Tracer.PrivateOutput(
        "Module '{0}' is being currently processed. Num of definitions explicitly specified this module: {1}.".FormatWith(module.Name, explicitlySpecified.Length));
      this.ProcessCandidatesExpliticlySpecifiedModule(explicitlySpecified, module);

      IEnumerable<RuntimeBreakpoint> candidatesWithoutModuleRestr = allCandidates.Where(rtBp => rtBp.Definition.ModuleName.IsNullOrEmpty()).ToArray();
      this.ProcessCandidatesWithAnyModule(candidatesWithoutModuleRestr, module);
    }

    private void ProcessMethodDefinitions(RuntimeBreakpoint runtimeBreakpoint, List<InheritanceMember<XClrDataMethodDefinitionWraper>> methodDefWrapper)
    {
      Dictionary<string, int> ignoredMethods = new Dictionary<string, int>();
      var nonGenericDefs = new List<InheritanceMember<XClrDataMethodDefinitionWraper>>();

      foreach (InheritanceMember<XClrDataMethodDefinitionWraper> methodDefInh in methodDefWrapper)
      {
        XClrDataMethodDefinitionWraper methodDef = methodDefInh.Value;

        if (methodDefInh.Value.Generic)
        {
          string methodName = methodDefInh.Value.FullName;
          if (ignoredMethods.ContainsKey(methodName))
          {
            ignoredMethods[methodName]++;
          }
          else
          {
            ignoredMethods.Add(methodName, 1);
          }
          continue;
        }
        runtimeBreakpoint.OverloadCandidates.Add(methodDef.TokenAndScope.ToString());
        nonGenericDefs.Add(methodDefInh);
      }

      if (ignoredMethods.Count > 0)
      {
        Tracer.PublicOutput(
          "Generic methods are not currently supported, so some overloads were skipped:{0}{1}".FormatWith(
            Environment.NewLine,
            ignoredMethods.Select(kv => "{0}: {1} overloads".FormatWith(kv.Key, kv.Value)).JoinToString(Environment.NewLine)));
      }

      //Two cycles to fill OverloadCandidates with all suitable candidates before each candidate processing.

      foreach (InheritanceMember<XClrDataMethodDefinitionWraper> methodDefInh in nonGenericDefs)
      {
        XClrDataMethodDefinitionWraper methodDef = methodDefInh.Value;

        List<XClrDataMethodInstanceWraper> methodInstances = methodDef.GetInstances();
        //Method wasn't compiled yet
        if (methodInstances.Count == 0)
        {
          Tracer.PublicOutput("Method '{0}' has not been JITTED yet. Wait for JIT notification.".FormatWith(methodDef.FullName));
          this.EnableJitNotificationForMethod(methodDef);
          runtimeBreakpoint.WaitingForJitCount++;
        }
        else
        {
          foreach (XClrDataMethodInstanceWraper mInstance in methodInstances)
          {
            this.ProcessMethodInstanceInternal(runtimeBreakpoint, mInstance);
          }
        }
      }
    }

    private void ProcessMethodInstanceInternal(RuntimeBreakpoint breakpoint, XClrDataMethodInstanceWraper methodInstance)
    {
      //That candidate is no longer suitable.
      breakpoint.OverloadCandidates.Remove(methodInstance.TokenAndScope.ToString());

      if (!this.IsBreakpointValidForMethod(breakpoint, methodInstance))
      {
        breakpoint.MethodsProbingHistory.Add(methodInstance.NameWithArgs);
        Tracer.PrivateOutput(
          "Probed method with signature '{0}', but it does't match expected signature for the definition '{1}'.".FormatWith(
            methodInstance.NameWithArgs,
            breakpoint.Definition.RawDefinition));
      }
      else
      {
        breakpoint.SuitableOverloadWasFound = true;

        string errorMessage;
        bool successfullySet = this.SetDebuggerBreakpoint(methodInstance, breakpoint, out errorMessage);
        if (successfullySet)
        {
          breakpoint.NumOfSuccessfullySetBreakpoints++;

          InheritanceMember<string> relatedDefinitionUniqId =
            breakpoint.ClrMethodDefinitionUniqIds.FirstOrDefault(def => def.Value.EqualsWithCase(methodInstance.TokenAndScope.ToString(), false));
          if (relatedDefinitionUniqId.IsFilled && relatedDefinitionUniqId.NestingLevel > 0)
          {
            breakpoint.Flags |= MethodTracingFlag.FORCE_THIS_TYPE;
          }
        }
        else
        {
          Tracer.PublicOutput("Unable to set breakpoint for the '{0}' method: {1}".FormatWith(methodInstance.NameWithArgs, errorMessage));
        }
      }

      //We probed all the overloads we have. It's time to move bp to either active or invalid.
      if (breakpoint.OverloadCandidates.Count == 0)
      {
        if (breakpoint.SuitableOverloadWasFound)
        {
          if (breakpoint.NumOfSuccessfullySetBreakpoints > 0)
          {
            this.PendingQueue.Remove(breakpoint);
            this.Active.Add(breakpoint);
          }
          else
          {
            this.MoveBreakpointToInvalid(
              breakpoint,
              "{0} suitable overloads for the '{1}' method, however tool was unable to set any native breakpoint.".FormatWith(
                GetTypeMessageDependingOnNumOfTypes(breakpoint.Definition.TypeNames, "The type '{0}' contains", "The types '{0}' contain"),
                breakpoint.Definition.MethodName));
          }
        }
        else
        {
          this.MoveBreakpointToInvalid(
            breakpoint,
            "Unable to find suitable overload for the '{1}' method.{0}PROBED TYPES:{0}{2}{0}{0}PROBED METHOD OVERLOADS:{0}{3}".FormatWith(
              Environment.NewLine,
              breakpoint.Definition.MethodName,
              breakpoint.ModulesProbingHistory.JoinToString(Environment.NewLine),
              breakpoint.MethodsProbingHistory.JoinToString(Environment.NewLine)));
        }
      }
    }

    private void ResolveAllPossibleBreakpoints()
    {
      List<XClrDataModuleWrapper> allModules = this.SessionManager.XCLRDataProcessWrapper.GetModules();
      Tracer.PrivateOutput("Initial method definitions resolve. Extracted {0} modules.".FormatWith(allModules.Count));
      foreach (XClrDataModuleWrapper module in allModules)
      {
        //No sense to continue, if all breakpoints were resolved.
        if (this.AllModulesResolved)
        {
          break;
        }

        this.ProcessClrModule(module);
      }
    }

    private ulong ResolveNativeAddressForMethod(XClrDataMethodInstanceWraper methodInstance, RuntimeBreakpoint breakpoint)
    {
      ulong codeAddress = methodInstance.GetRepresentativeEntryAddress();

      if (codeAddress != 0UL && codeAddress != ulong.MaxValue)
      {
        return codeAddress;
      }

      //To refresh cache.
      this.SessionManager.Runtime.Flush();

      codeAddress = this.ResolveNativeAddressUsingClrReflection(methodInstance, breakpoint);

      if (codeAddress != 0UL && codeAddress != ulong.MaxValue)
      {
        return codeAddress;
      }

      //Unable to get code address using the DAC layer directly.

      //This operation is extremelly slow, but I pray we do that only in rare cases.
      Tracer.PrivateOutput("Native address resolve thorough the ClrMD happened.");
      var sw = Stopwatch.StartNew();

      try
      {
        //Try to obtain module name from method instance
        string moduleNameToRestrict = methodInstance.TypeInstance.NullOr(ti => ti.Definition.NullOr(td => td.Module.NullOr(m => m.Name)));
        if (moduleNameToRestrict.IsNullOrEmpty())
        {
          moduleNameToRestrict = breakpoint.Definition.ModuleName;
        }

        string typeName = methodInstance.TypeName;

        if (typeName.IsNullOrEmpty())
        {
          Tracer.ReportUnexpectedState("Unable to get type name for the {0} method instance.".FormatWith(methodInstance.NameWithArgs));
          return 0UL;
        }

        foreach (ClrModule clrModule in this.SessionManager.Runtime.EnumerateModules())
        {
          //If module name is specified, process module only if file and name matches expected.
          if (moduleNameToRestrict.NotNullNotEmpty())
          {
            if (!clrModule.IsFile || !Path.GetFileNameWithoutExtension(clrModule.FileName).EqualsWithCase(moduleNameToRestrict, false))
            {
              continue;
            }
          }

          ClrType hostType = clrModule.GetTypeByName(typeName);
          if (hostType != null)
          {
            ClrMethod method = hostType.Methods.FirstOrDefault(meth => meth.GetFullSignature().EqualsWithCase(methodInstance.NameWithArgs, false));
            if (method != null)
            {
              codeAddress = method.NativeCode;
              break;
            }
          }
        }
      }
      finally
      {
        sw.Stop();
        Tracer.PrivateOutput("Native address resolve thorough the ClrMD took {0} msec.".FormatWith(sw.ElapsedMilliseconds));
      }

      return codeAddress;
    }

    private ulong ResolveNativeAddressUsingClrReflection(XClrDataMethodInstanceWraper methodInstance, RuntimeBreakpoint breakpoint)
    {
      Stopwatch sw = Stopwatch.StartNew();
      try
      {
        XClrDataTypeDefinitionWrapper holdingType = methodInstance.TypeInstance.NullOr(ti => ti.Definition);
        if (holdingType == null)
        {
          return 0UL;
        }

        TokenAndScopePair tokenAndScope = holdingType.TokenAndScope;
        if (!tokenAndScope.IsValid)
        {
          return 0UL;
        }

        TokenAndScopePair methodTokenAndStope = methodInstance.TokenAndScope;
        if (!methodTokenAndStope.IsValid)
        {
          return 0UL;
        }

        ulong mt = this.SessionManager.XCLRDataProcessWrapper.GetMethodTableForType(tokenAndScope.Module.ModuleAddress, tokenAndScope.Token);
        ClrRuntimeHelper clrRuntimeHelper = this.SessionManager.ClrRuntimeHelper;
        if (mt == 0UL)
        {
          var moduleMTs = clrRuntimeHelper.GetMethodTableList(tokenAndScope.Module.ModuleAddress);
          mt = moduleMTs.FirstOrDefault(modMt => clrRuntimeHelper.GetMetadataToken(modMt) == tokenAndScope.Token);
        }

        if (mt == 0UL)
        {
          return 0UL;
        }

        var methodDescs = clrRuntimeHelper.GetMethodDescList(mt);

        uint etalonToken = methodTokenAndStope.Token;
        Tuple<uint, ulong> appropriateTuple = methodDescs.Select(clrRuntimeHelper.GetMethodDescData).FirstOrDefault(data => data.Item1 == etalonToken);

        if (appropriateTuple == null)
        {
          return 0UL;
        }

        return appropriateTuple.Item2;
      }
      finally
      {
        sw.Stop();
        Tracer.PrivateOutput("Native address resolve thorough the ClrMD reflection happened. Operation took {0} msec.".FormatWith(sw.ElapsedMilliseconds));
      }
    }

    private bool SetDebuggerBreakpoint(XClrDataMethodInstanceWraper methodInstance, RuntimeBreakpoint breakpoint, out string errorMessage)
    {
      errorMessage = null;

      ulong codeAddress = this.ResolveNativeAddressForMethod(methodInstance, breakpoint);

      if (codeAddress == ulong.MaxValue)
      {
        errorMessage = "Unable to resolve JITTED code address for the '{0}' method.".FormatWith(methodInstance.NameWithArgs);
        return false;
      }

      //Don't set breakpoint if it was already set.
      if (this.AlreadyBreakpoinedAdresses.Contains(codeAddress))
      {
        return true;
      }

      int resultCode = 0;
      int tier = 0;
      do
      {
        IDebugBreakpoint nativeBreakpoint;
        resultCode = this.SessionManager.NativeDebugInterfaces.DebugControl.AddBreakpoint(DEBUG_BREAKPOINT_TYPE.CODE, DEBUG_ANY_ID, out nativeBreakpoint);
        if (resultCode != 0)
        {
          break;
        }

        tier = 1;
        resultCode = nativeBreakpoint.SetOffset(codeAddress);
        if (resultCode != 0)
        {
          break;
        }

        tier = 2;
        uint nativeBreakpointId;
        resultCode = nativeBreakpoint.GetId(out nativeBreakpointId);
        if (resultCode != 0)
        {
          break;
        }

        //To later cleanup it.
        this.ActiveBreakpoints.Add(nativeBreakpoint);

        tier = 3;
        resultCode = nativeBreakpoint.SetFlags(DEBUG_BREAKPOINT_FLAG.ENABLED);
        if (resultCode != 0)
        {
          break;
        }

        tier = 4;
        breakpoint.ArgumentsTracer.AddBreakpointInfo(nativeBreakpointId, methodInstance);
        this.FastBreakpointsLookupTable[nativeBreakpointId] = breakpoint;
        this.AlreadyBreakpoinedAdresses.Add(codeAddress);

        Tracer.PublicOutput("Native breakpoint with ID {0} was set for the '{1}' method.".FormatWith(nativeBreakpointId, methodInstance.NameWithArgs));
      }
      while (false);

      if (resultCode != 0)
      {
        errorMessage = "Unable to create native Debug breakpoint (err code: {0}, tier: {1}).".FormatWith(resultCode, tier);
        return false;
      }
      return true;
    }

    #endregion
  }
}