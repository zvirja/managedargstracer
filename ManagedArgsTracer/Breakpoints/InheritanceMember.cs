#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#endregion

namespace ManagedArgsTracer.Breakpoints
{
  internal struct InheritanceMember<TValue>
  {
    #region Constructors and Destructors

    public InheritanceMember(TValue value, int nestingLevel)
      : this()
    {
      this.Value = value;
      this.NestingLevel = nestingLevel;
      this.IsFilled = true;
    }

    #endregion

    #region Public Properties

    public bool IsFilled { get; private set; }

    public int NestingLevel { get; set; }

    public TValue Value { get; private set; }

    #endregion
  }
}