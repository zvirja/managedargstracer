﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using JetBrains.Annotations;

using ManagedArgsTracer.Breakpoints.Definition;
using ManagedArgsTracer.Breakpoints.Tracing;
using ManagedArgsTracer.Diagnostics;
using ManagedArgsTracer.Runtime;
using ManagedArgsTracer.Runtime.Wrappers;
using ManagedArgsTracer.Session;

#endregion

namespace ManagedArgsTracer.Breakpoints
{
  internal class RuntimeBreakpoint
  {
    #region Constructors and Destructors

    public RuntimeBreakpoint(
      [NotNull] BreakpointDefinition definition,
      [NotNull] NativeDebugInterfaces debugInterfaces,
      [NotNull] ClrRuntimeHelper clrRuntimeHelper,
      [NotNull] XClrDataProcessWrapper xclrProcess)
    {
      Assert.ArgumentNotNull(clrRuntimeHelper, "clrRuntimeHelper");
      Assert.ArgumentNotNull(definition, "definition");
      Assert.ArgumentNotNull(debugInterfaces, "debugInterfaces");
      Assert.ArgumentNotNull(xclrProcess, "xclrProcess");
      this.Definition = definition;
      this.ArgumentsTracer = new BreakpointArgumentsTracer(this, debugInterfaces, clrRuntimeHelper, xclrProcess);
      this.OverloadCandidates = new HashSet<string>();
      this.MethodsProbingHistory = new List<string>();
      this.ModulesProbingHistory = new List<string>();
      this.ClrMethodDefinitionUniqIds = new List<InheritanceMember<string>>();
      this.Flags = this.Definition.Flags;
    }

    #endregion

    #region Public Properties

    public BreakpointArgumentsTracer ArgumentsTracer { get; set; }

    /// <summary>
    /// Holds collection of all MethodDefinitions IDs that were found for the current definition.
    /// </summary>
    public List<InheritanceMember<string>> ClrMethodDefinitionUniqIds { get; set; }

    public BreakpointDefinition Definition { get; set; }

    public MethodTracingFlag Flags { get; set; }

    public bool Invalid { get; set; }

    public string InvalidReason { get; set; }

    public List<string> MethodsProbingHistory { get; set; }

    public bool ModuleIsLoaded { get; set; }

    public List<string> ModulesProbingHistory { get; set; }

    public int NumOfSuccessfullySetBreakpoints { get; set; }

    /// <summary>
    /// This struct holds tokens of all the method overloads that might be suitable
    /// </summary>
    public HashSet<string> OverloadCandidates { get; set; }

    public int PossibleSuitableMethodDefs { get; set; }

    public bool SuitableOverloadWasFound { get; set; }

    public bool TraceThisType
    {
      get
      {
        if ((this.Flags & MethodTracingFlag.NO_THIS_TYPE) == MethodTracingFlag.NO_THIS_TYPE)
        {
          return false;
        }
        return (this.Flags & MethodTracingFlag.FORCE_THIS_TYPE) == MethodTracingFlag.FORCE_THIS_TYPE;
      }
    }

    public int WaitingForJitCount { get; set; }

    #endregion
  }
}