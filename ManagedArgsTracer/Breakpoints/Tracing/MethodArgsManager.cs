﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ManagedArgsTracer.Session;

#endregion

namespace ManagedArgsTracer.Breakpoints.Tracing
{
  internal class MethodArgsManager
  {
    #region Static Fields

    private static MethodRawArgumentsFetcher RawArgumentsFetcher;

    #endregion

    #region Constructors and Destructors

    static MethodArgsManager()
    {
      RawArgumentsFetcher = IntPtr.Size == 4 ? (MethodRawArgumentsFetcher)new X86MethodRawArgumentsFetcher() : (MethodRawArgumentsFetcher)new X64MethodRawArgumentsFetcher();
    }

    #endregion

    #region Public Methods and Operators

    public static List<AddressedArgumentValue> GetRawMethodArgs(NativeDebugInterfaces debugInterfaces, int numberOfArguments)
    {
      return RawArgumentsFetcher.FetchArguments(debugInterfaces, numberOfArguments);
    }

    #endregion
  }
}