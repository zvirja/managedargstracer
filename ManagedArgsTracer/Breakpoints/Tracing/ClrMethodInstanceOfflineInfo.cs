﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ManagedArgsTracer.Common;
using ManagedArgsTracer.Runtime.Wrappers;

#endregion

namespace ManagedArgsTracer.Breakpoints.Tracing
{
  internal class ClrMethodInstanceOfflineInfo
  {
    #region Constructors and Destructors

    public ClrMethodInstanceOfflineInfo(XClrDataMethodInstanceWraper methodInstance)
    {
      this.IsInstance = methodInstance.IsInstance;
      this.Arguments = methodInstance.Arguments;
      this.NameWithArgs = methodInstance.NameWithArgs;
      this.ClrTypeName = methodInstance.TypeName;
      this.HitCount = 0;
    }

    #endregion

    #region Public Properties

    public List<FullTypeName> Arguments { get; private set; }

    public string ClrTypeName { get; private set; }

    public ulong HitCount { get; set; }

    public bool IsInstance { get; private set; }

    public string NameWithArgs { get; private set; }

    #endregion
  }
}