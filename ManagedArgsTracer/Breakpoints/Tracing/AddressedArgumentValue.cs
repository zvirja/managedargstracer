﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#endregion

namespace ManagedArgsTracer.Breakpoints.Tracing
{
  public struct AddressedArgumentValue
  {
    #region Constructors and Destructors

    public AddressedArgumentValue(ulong value)
      : this(value, 0UL)
    {
    }

    public AddressedArgumentValue(ulong value, ulong valueAddress)
      : this()
    {
      this.Value = value;
      this.ValueAddress = valueAddress;
    }

    #endregion

    #region Public Properties

    public ulong Value { get; private set; }

    public ulong ValueAddress { get; private set; }

    #endregion
  }
}