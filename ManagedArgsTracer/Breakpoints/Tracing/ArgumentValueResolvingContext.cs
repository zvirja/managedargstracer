﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using JetBrains.Annotations;

using ManagedArgsTracer.Diagnostics;
using ManagedArgsTracer.Helpers;
using ManagedArgsTracer.Runtime;
using ManagedArgsTracer.Runtime.Wrappers;
using ManagedArgsTracer.Session;

using Microsoft.Diagnostics.Runtime;

#endregion

namespace ManagedArgsTracer.Breakpoints.Tracing
{
  internal class ArgumentValueResolvingContext : IArgumentValueResolvingContext, ICleverFlusher
  {
    #region Constructors and Destructors

    public ArgumentValueResolvingContext(
      [NotNull] NativeDebugInterfaces debugInterfaces,
      [NotNull] XClrDataProcessWrapper xClrProcess,
      [NotNull] ClrRuntime clrRuntime,
      [NotNull] ClrRuntimeHelper clrRuntimeHelper,
      [NotNull] XClrDataTaskWrapper currentDataTask,
      uint osThreadId,
      [NotNull] RuntimeBreakpoint runtimeBreakpoint,
      [NotNull] ClrMethodInstanceOfflineInfo methodInstanceOfflineInfo)
    {
      Assert.ArgumentNotNull(debugInterfaces, "debugInterfaces");
      Assert.ArgumentNotNull(xClrProcess, "xClrProcess");
      Assert.ArgumentNotNull(clrRuntimeHelper, "clrRuntimeHelper");
      Assert.ArgumentNotNull(clrRuntime, "clrRuntime");
      Assert.ArgumentNotNull(currentDataTask, "currentDataTask");
      Assert.ArgumentNotNull(runtimeBreakpoint, "runtimeBreakpoint");
      Assert.ArgumentNotNull(methodInstanceOfflineInfo, "clrMethodInstance");
      this.DebugInterfaces = debugInterfaces;
      this.XClrProcess = xClrProcess;
      this.ClrRuntime = clrRuntime;
      this.ClrRuntimeHelper = clrRuntimeHelper;
      this.CurrentDataTask = currentDataTask;
      this.NativeOsThreadId = osThreadId;
      this.RuntimeBreakpoint = runtimeBreakpoint;
      this.MethodInstanceOfflineInfo = methodInstanceOfflineInfo;
    }

    #endregion

    #region Public Properties

    public ulong AppDomainId
    {
      get
      {
        XClrDataAppDomainWrapper appDomain = this.CurrentDataTask.AppDomain;
        if (appDomain == null)
        {
          Tracer.ReportUnexpectedState("Unable to get AppDomain for thread with ID {0}".FormatWith(this.NativeOsThreadId));
          return 0;
        }
        return appDomain.ID;
      }
    }

    public ICleverFlusher CleverFlusher
    {
      get
      {
        return this;
      }
    }

    public ClrRuntime ClrRuntime { get; set; }

    public ClrRuntimeHelper ClrRuntimeHelper { get; set; }

    public XClrDataTaskWrapper CurrentDataTask { get; set; }

    public NativeDebugInterfaces DebugInterfaces { get; set; }

    public ClrMethodInstanceOfflineInfo MethodInstanceOfflineInfo { get; set; }

    public uint NativeOsThreadId { get; set; }

    public RuntimeBreakpoint RuntimeBreakpoint { get; set; }

    public IClrRuntimeHelper RuntimeHelper
    {
      get
      {
        return this.ClrRuntimeHelper;
      }
    }

    /// <summary>
    /// Is used to specify whether runtime was already flushed during this tracing interation. Is used to not flush runtime more than once during the same breakpoint handling.
    /// </summary>
    public bool RuntimeWasAlreadyFlushed { get; set; }

    public XClrDataProcessWrapper XClrProcess { get; set; }

    #endregion

    #region Public Methods and Operators

    /// <summary>
    /// Flushes ClrRuntime if it wasn't already flushed.
    /// </summary>
    /// <returns>True if ClrRuntime was actually flushed.</returns>
    public bool FlushClrRuntime()
    {
      if (this.RuntimeWasAlreadyFlushed)
      {
        return false;
      }
      this.ClrRuntime.Flush();
      this.RuntimeWasAlreadyFlushed = true;
      return true;
    }

    #endregion
  }
}