﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ManagedArgsTracer.Breakpoints.Definition;
using ManagedArgsTracer.Breakpoints.Definition.Tracing;
using ManagedArgsTracer.Breakpoints.Tracing.KnownTypes;
using ManagedArgsTracer.Common;
using ManagedArgsTracer.Helpers;

#endregion

namespace ManagedArgsTracer.Breakpoints.Tracing.Tracers
{
  internal class KnownAnyTracer : ArgumentTracer
  {
    #region Public Methods and Operators

    public override ArgumentTracer Clone()
    {
      return new KnownAnyTracer();
    }

    public override bool HandlesMethodArgumentDefinition(MethodArg argDefinition)
    {
      return argDefinition is KnownAnyTracingArg;
    }

    public override bool ReadArgumentValue(
      AddressedArgumentValue rawArgumentValue,
      FullTypeName definitionArgumentTypeName,
      ArgumentValueResolvingContext context,
      out string valueOrErrorExplanation)
    {
      IValueReader knownReader = KnownTypesManager.ResolveValueReader(definitionArgumentTypeName.TypeName);

      if (knownReader == null)
      {
        valueOrErrorExplanation = "Unable to resolve '{0}' argument type as a known type.".FormatWith(definitionArgumentTypeName.TypeName);
        return false;
      }
      return knownReader.ReadDirectArgumentValue(rawArgumentValue, context.DebugInterfaces, context, out valueOrErrorExplanation);
    }

    #endregion
  }
}