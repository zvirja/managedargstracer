﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ManagedArgsTracer.Breakpoints.Definition;
using ManagedArgsTracer.Breakpoints.Definition.Tracing;
using ManagedArgsTracer.Breakpoints.Tracing.Tracers.ChainedTracing;
using ManagedArgsTracer.Common;
using ManagedArgsTracer.Helpers;

using Microsoft.Diagnostics.Runtime;

#endregion

namespace ManagedArgsTracer.Breakpoints.Tracing.Tracers
{
  internal class ChainedAnyTracer : ArgumentTracer
  {
    #region Properties

    protected ChainedAnyTracingArg ArgDefinition { get; set; }

    protected bool ArgumentWasTracedAtLeastOnce { get; set; }

    protected string[] ParsedCachedChain
    {
      get
      {
        return this.ArgDefinition.Chain;
      }
    }

    #endregion

    #region Public Methods and Operators

    public override ArgumentTracer Clone()
    {
      return new ChainedAnyTracer();
    }

    public override bool HandlesMethodArgumentDefinition(MethodArg argDefinition)
    {
      return argDefinition is ChainedAnyTracingArg;
    }

    public override void Initialize(MethodArg argDefinition, ArgumentTracerResolver argumentTracerResolver)
    {
      base.Initialize(argDefinition, argumentTracerResolver);
      this.ArgDefinition = (ChainedAnyTracingArg)argDefinition;
    }

    public override bool ReadArgumentValue(
      AddressedArgumentValue rawArgumentValue,
      FullTypeName definitionArgumentTypeName,
      ArgumentValueResolvingContext context,
      out string valueOrErrorExplanation)
    {
      var resultArgumentValue = new StringBuilder();
      //<null> term is suitable for reference types only.
      if (rawArgumentValue.Value == 0UL && !this.ArgDefinition.IsValueType)
      {
        TracingHelper.AppendCurrentChainValueToBuffer(resultArgumentValue, context, this.ParsedCachedChain[0], "<null>", false, true);
        valueOrErrorExplanation = resultArgumentValue.ToString();
        return true;
      }

      ClrType clrType = this.GetArgumentTypeCached(rawArgumentValue.Value, definitionArgumentTypeName, context);
      if (clrType == null)
      {
        valueOrErrorExplanation = "Unable to resolve argument's Clr type.";
        return false;
      }

      this.TraceArgumentValue(resultArgumentValue, clrType, rawArgumentValue, context);
      valueOrErrorExplanation = resultArgumentValue.ToString();
      return true;
    }

    #endregion

    #region Methods

    protected virtual ClrType GetArgumentTypeCached(ulong rawArgumentValue, FullTypeName definitionArgumentTypeName, ArgumentValueResolvingContext context)
    {
      //If this is the first time the method is called, flush CLR.
      if (!this.ArgumentWasTracedAtLeastOnce)
      {
        if (!this.ArgumentWasTracedAtLeastOnce)
        {
          context.FlushClrRuntime();
        }
        this.ArgumentWasTracedAtLeastOnce = true;
      }

      //ref type
      if (!this.ArgDefinition.IsValueType)
      {
        ClrType resolvedType = this.GetTypeForRef(rawArgumentValue, definitionArgumentTypeName, context);
        if (resolvedType == null && context.FlushClrRuntime())
        {
          resolvedType = this.GetTypeForRef(rawArgumentValue, definitionArgumentTypeName, context);
        }

        return resolvedType;
      }

      return this.GetTypeForValueType(rawArgumentValue, definitionArgumentTypeName, context);
    }

    protected virtual ClrType GetTypeForRef(ulong rawArgumentValue, FullTypeName definitionArgumentTypeName, ArgumentValueResolvingContext context)
    {
      return context.ClrRuntime.GetHeap().GetObjectType(rawArgumentValue);
    }

    protected virtual ClrType GetTypeForValueType(ulong rawArgumentValue, FullTypeName definitionArgumentTypeName, ArgumentValueResolvingContext context)
    {
      return context.ClrRuntimeHelper.ClrTypeCache.FindTypeByNameAppDomainSpecific(
        definitionArgumentTypeName.TypeName,
        context.AppDomainId,
        false,
        context.CleverFlusher,
        definitionArgumentTypeName.ModuleName);
    }

    /// <summary>
    /// Trace argument value. If type is reference, value is for sure not null. Could be 0x000.. for value types.
    /// </summary>
    protected void TraceArgumentValue(StringBuilder resultArgumentValue, ClrType clrType, AddressedArgumentValue rawArgumentValue, ArgumentValueResolvingContext context)
    {
      using (var chainTracer = new FieldsChainValueTracer(resultArgumentValue, clrType, rawArgumentValue, context, this.ParsedCachedChain))
      {
        chainTracer.AppendArgumentValueToBuffer();
      }
    }

    /// <summary>
    /// Is used to apply known name decorations. For instance, is used to transform autoproperty name to raw name.
    /// </summary>
    private string GetDecoratedFieldName(string currentFldName)
    {
      if (currentFldName.Length > 1 && currentFldName[0] == '^')
      {
        return "<{0}>k__BackingField".FormatWith(currentFldName.Substring(1));
      }

      return currentFldName;
    }

    #endregion
  }
}