﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ManagedArgsTracer.Breakpoints.Definition;
using ManagedArgsTracer.Common;
using ManagedArgsTracer.Diagnostics;

#endregion

namespace ManagedArgsTracer.Breakpoints.Tracing.Tracers
{
  internal abstract class ArgumentTracer
  {
    #region Public Methods and Operators

    public abstract ArgumentTracer Clone();

    public abstract bool HandlesMethodArgumentDefinition(MethodArg argDefinition);

    public virtual void Initialize(MethodArg argDefinition, ArgumentTracerResolver argumentTracerResolver)
    {
      Assert.IsTrue(this.HandlesMethodArgumentDefinition(argDefinition), "Current tracer cannot handle ");
    }

    public abstract bool ReadArgumentValue(
      AddressedArgumentValue rawArgumentValue,
      FullTypeName definitionArgumentTypeName,
      ArgumentValueResolvingContext context,
      out string valueOrErrorExplanation);

    #endregion
  }
}