﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ManagedArgsTracer.Breakpoints.Definition;
using ManagedArgsTracer.Breakpoints.Definition.Tracing;
using ManagedArgsTracer.Common;

#endregion

namespace ManagedArgsTracer.Breakpoints.Tracing.Tracers
{
  internal class GroupTracer : ArgumentTracer
  {
    #region Properties

    private GroupAnyTracingArg ArgDefinition { get; set; }

    private ArgumentTracerResolver ArgumentTracerResolver { get; set; }

    #endregion

    #region Public Methods and Operators

    public override ArgumentTracer Clone()
    {
      return new GroupTracer();
    }

    public override bool HandlesMethodArgumentDefinition(MethodArg argDefinition)
    {
      return argDefinition is GroupAnyTracingArg;
    }

    public override void Initialize(MethodArg argDefinition, ArgumentTracerResolver argumentTracerResolver)
    {
      base.Initialize(argDefinition, argumentTracerResolver);
      this.ArgDefinition = (GroupAnyTracingArg)argDefinition;
      this.ArgumentTracerResolver = argumentTracerResolver;
    }

    public override bool ReadArgumentValue(
      AddressedArgumentValue rawArgumentValue,
      FullTypeName definitionArgumentTypeName,
      ArgumentValueResolvingContext context,
      out string valueOrErrorExplanation)
    {
      var result = new StringBuilder();
      bool isFirstResolve = true;
      foreach (MethodArg argToTrace in this.ArgDefinition.ArgsToTrace)
      {
        ArgumentTracer tracer = this.ArgumentTracerResolver(argToTrace, context);
        if (isFirstResolve)
        {
          isFirstResolve = false;
        }
        else
        {
          result.Append(" ^^^ ");
        }
        string value = BreakpointArgumentsTracer.ReadArgumentValue(argToTrace, rawArgumentValue, definitionArgumentTypeName, context, tracer);
        result.Append(value);
      }

      valueOrErrorExplanation = result.ToString();

      return true;
    }

    #endregion
  }
}