﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ManagedArgsTracer.Breakpoints.Tracing.KnownTypes;
using ManagedArgsTracer.Diagnostics;
using ManagedArgsTracer.Helpers;

using Microsoft.Diagnostics.Runtime;

#endregion

namespace ManagedArgsTracer.Breakpoints.Tracing.Tracers.ChainedTracing
{
  internal class FinalChainValueReader : FieldValueReader
  {
    #region Public Methods and Operators

    public override bool TraceCurrentFieldReadNext(
      ClrInstanceField field,
      ulong currentChainAddress,
      bool isInterior,
      StringBuilder outputBuffer,
      ArgumentValueResolvingContext context,
      string undecoratedFieldName,
      out ulong nextChainAddress,
      out bool nextIsInterior,
      out ClrType currentChainType)
    {
      //Return dummy. That is the last chain.
      nextChainAddress = 0UL;
      nextIsInterior = false;
      currentChainType = null;

      //Trace string.
      if (field.ElementType == ClrElementType.String)
      {
        string stringValue = null;
        object rawString = field.GetFieldValue(currentChainAddress, isInterior);
        stringValue = rawString as string;

        //Something goes wrong here.
        if (rawString != null && stringValue == null)
        {
          TracingHelper.AppendCurrentChainValueToBuffer(outputBuffer, context, undecoratedFieldName, "<ERROR DURING STRING READ>", true, true);
          Tracer.PrivateOutput(
            "Unable to read string from field. Expected type of object is string while returned object is of the '{0}' type.".FormatWith(rawString.GetType().Name));
          return false;
        }

        TracingHelper.AppendCurrentChainValueToBuffer(outputBuffer, context, undecoratedFieldName, stringValue ?? "<null>", true, true);
        return false;
      }

      //Trace primitive types.
      if (field.IsPrimitive())
      {
        object primitiveValue = field.GetFieldValue(currentChainAddress, isInterior);
        if (primitiveValue == null)
        {
          TracingHelper.AppendCurrentChainValueToBuffer(outputBuffer, context, undecoratedFieldName, "<ERROR DURING PRIMITIVE READ>", true, true);
          Tracer.PrivateOutput("Unable to read primitive value. Returned value is null. Element type: {0}".FormatWith(field.ElementType));
          return false;
        }

        TracingHelper.AppendCurrentChainValueToBuffer(outputBuffer, context, undecoratedFieldName, primitiveValue.ToString(), true, true);
        return false;
      }

      //We are here and that means that field is non primitive.
      ClrType fieldType = field.Type;
      if (fieldType == null)
      {
        TracingHelper.AppendCurrentChainValueToBuffer(outputBuffer, context, undecoratedFieldName, "<ERROR DURING VALUE READ>", true, true);
        Tracer.PrivateOutput("Unable to get ClrType for field. Element type: {0}, field name: {1}".FormatWith(field.ElementType, field.Name));
        return false;
      }

      IValueReader knownFieldReader = this.ResolveKnownTypeReader(fieldType, field);
      if (knownFieldReader == null)
      {
        TracingHelper.AppendCurrentChainValueToBuffer(outputBuffer, context, undecoratedFieldName, "<UNABLE TO TRACE NON PRIMITIVE>", true, true);
        Tracer.PrivateOutput("Unable to get known reader for field. Element type: '{0}', Field type: '{1}'".FormatWith(field.ElementType, field.Type.Name));
        return false;
      }

      string fieldValue;
      if (!knownFieldReader.ReadFieldValue(field, currentChainAddress, isInterior, out fieldValue))
      {
        TracingHelper.AppendCurrentChainValueToBuffer(outputBuffer, context, undecoratedFieldName, "<ERROR DURING VALUE READ>", true, true);
        Tracer.PrivateOutput(
          "Unable to read field value using the known reader. Element type: '{0}', Field type: '{1}', FieldValue: '{2}', Reader type: '{3}'".FormatWith(
            field.ElementType,
            field.Type.Name,
            field.GetFieldValue(currentChainAddress, isInterior) ?? "<null>",
            knownFieldReader.GetType().Name));
        return false;
      }

      TracingHelper.AppendCurrentChainValueToBuffer(outputBuffer, context, undecoratedFieldName, fieldValue ?? "<null>", true, true);
      return false;
    }

    #endregion

    #region Methods

    private IValueReader ResolveKnownTypeReader(ClrType fieldType, ClrInstanceField field)
    {
      return KnownTypesManager.ResolveValueReader(fieldType.Name);
    }

    #endregion
  }
}