﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ManagedArgsTracer.Diagnostics;
using ManagedArgsTracer.Helpers;

using Microsoft.Diagnostics.Runtime;

#endregion

namespace ManagedArgsTracer.Breakpoints.Tracing.Tracers.ChainedTracing
{
  internal class ReferenceChainValueReader : FieldValueReader
  {
    #region Public Methods and Operators

    public override bool TraceCurrentFieldReadNext(
      ClrInstanceField field,
      ulong currentChainAddress,
      bool isInterior,
      StringBuilder outputBuffer,
      ArgumentValueResolvingContext context,
      string undecoratedFieldName,
      out ulong nextChainAddress,
      out bool nextIsInterior,
      out ClrType currentChainType)
    {
      nextIsInterior = false;
      nextChainAddress = 0UL;
      currentChainType = null;

      ulong fieldValAddress = field.GetFieldAddress(currentChainAddress, isInterior);
      byte[] bufferForRef = new byte[IntPtr.Size];

      uint bytesRead;
      int retCode;
      if ((retCode = context.DebugInterfaces.DebugDataSpaces.ReadVirtual(fieldValAddress, bufferForRef, (uint)bufferForRef.Length, out bytesRead)) != 0
          || bytesRead != bufferForRef.Length)
      {
        TracingHelper.AppendCurrentChainValueToBuffer(outputBuffer, context, undecoratedFieldName, "<ERROR DURING REF VALUE READ>", false, true);
        Tracer.PrivateOutput("Unable to read reference. Address of reference: {0}, retCode: {1}, bytesRead: {2}".FormatWith(fieldValAddress.ToHexString(), retCode, bytesRead));
        return false;
      }

      //For x64
      if (bufferForRef.Length == 8)
      {
        nextChainAddress = BitConverter.ToUInt64(bufferForRef, 0);
      }
      else
      {
        nextChainAddress = BitConverter.ToUInt32(bufferForRef, 0);
      }

      bool refIsNull = nextChainAddress == 0UL;

      string stringAddVal = refIsNull ? "<null>" : nextChainAddress.ToHexString();
      TracingHelper.AppendCurrentChainValueToBuffer(outputBuffer, context, undecoratedFieldName, stringAddVal, false, refIsNull);

      //If reference is not null, resolve type of the current object.
      if (!refIsNull)
      {
        currentChainType = context.ClrRuntime.GetHeap().GetObjectType(nextChainAddress);
      }

      //Continue only if reference is not null.
      return !refIsNull;
    }

    #endregion
  }
}