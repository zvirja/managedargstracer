﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ManagedArgsTracer.Breakpoints.Definition;
using ManagedArgsTracer.Diagnostics;
using ManagedArgsTracer.Helpers;
using ManagedArgsTracer.Interop.Native;

using Microsoft.Diagnostics.Runtime;

#endregion

namespace ManagedArgsTracer.Breakpoints.Tracing.Tracers.ChainedTracing
{
  internal class EmbeddedStructArgumentFixer
  {
    #region Constructors and Destructors

    public EmbeddedStructArgumentFixer(ClrType type, AddressedArgumentValue originalValue, ArgumentValueResolvingContext context)
    {
      this.Type = type;
      this.OriginalValue = originalValue;
      this.Context = context;
      this.ArgSize = CountStructureSize(type);
    }

    #endregion

    #region Public Properties

    public uint ArgSize { get; private set; }

    public bool FixIsApplied
    {
      get
      {
        return this.CleanupRequired;
      }
    }

    #endregion

    #region Properties

    private IntPtr AllocatedRegion { get; set; }

    private bool CleanupRequired { get; set; }

    private ArgumentValueResolvingContext Context { get; set; }

    private AddressedArgumentValue OriginalValue { get; set; }

    private IntPtr ProcHandle { get; set; }

    private ClrType Type { get; set; }

    #endregion

    #region Public Methods and Operators

    public void CleanUp()
    {
      if (this.CleanupRequired)
      {
        NativeMethods.VirtualFreeEx(this.ProcHandle, this.AllocatedRegion, IntPtr.Zero, 0x8000);
      }
    }

    public ulong? GetFixedArgumentValue()
    {
      //Fix isn't even required.
      if (!this.FixRequired())
      {
        return this.OriginalValue.Value;
      }

      //This value is likely located on stack and we can simply return its address.
      if (this.OriginalValue.ValueAddress != 0UL)
      {
        return this.OriginalValue.ValueAddress;
      }

      //Allocate memory in target process and write value there.
      ulong procHandle;
      if (this.Context.DebugInterfaces.DebugSystemObjects.GetCurrentProcessHandle(out procHandle) != 0)
      {
        return null;
      }
      this.ProcHandle = new IntPtr((long)procHandle);

      this.AllocatedRegion = this.AllocateMemInTargetProcess(this.ProcHandle, this.ArgSize);
      if (this.AllocatedRegion == IntPtr.Zero)
      {
        return null;
      }

      this.CleanupRequired = true;

      byte[] bufferWithValue = BitConverter.GetBytes(this.OriginalValue.Value);
      uint bytesWritten;
      if (
        this.Context.DebugInterfaces.DebugDataSpaces.WriteVirtual((ulong)this.AllocatedRegion.ToInt64(), bufferWithValue, this.ArgSize, out bytesWritten) != 0 || bytesWritten !=
        this.ArgSize)
      {
        return null;
      }

      return (ulong)this.AllocatedRegion.ToInt64();
    }

    #endregion

    #region Methods

    internal static uint CountStructureSize(ClrType structureType)
    {
      /* I haven't found any way how to find real size of struct. 
      * Of course, it's equal or larger than sum of the individual field sizes, however padding could be applied so hard to judge for sure.
      * I decided to find a field with largers offset and add size of the field itself. Everybody should proud of that idea :) */

      ClrInstanceField fieldWithLargestOffset = GetFieldWithLargestOffset(structureType.Fields);
      if (fieldWithLargestOffset == null)
      {
        return 0;
      }
      if (fieldWithLargestOffset.ElementType != ClrElementType.Struct)
      {
        return (uint)(fieldWithLargestOffset.Offset + fieldWithLargestOffset.Size);
      }

      //If last field is a struct, operation should be applied recursively.
      ClrType typeOfField = fieldWithLargestOffset.Type;
      //That should be not null for each time, however author of ClrMD asked to check for null.
      if (typeOfField != null)
      {
        return (uint)fieldWithLargestOffset.Offset + CountStructureSize(typeOfField);
      }
      Tracer.ReportUnexpectedState("Unable to get Type for the {0} field. Output might be wrong.".FormatWith(fieldWithLargestOffset.Name));
      return (uint)(fieldWithLargestOffset.Offset + fieldWithLargestOffset.Size);
    }

    private static ClrInstanceField GetFieldWithLargestOffset(IList<ClrInstanceField> fields)
    {
      int currentOffset = -1;
      ClrInstanceField currentField = null;
      foreach (ClrInstanceField field in fields)
      {
        if (field.Offset > currentOffset)
        {
          currentField = field;
          currentOffset = field.Offset;
        }
      }
      return currentField;
    }

    private IntPtr AllocateMemInTargetProcess(IntPtr procHandle, uint argSize)
    {
      return NativeMethods.VirtualAllocEx(procHandle, IntPtr.Zero, new IntPtr(argSize), 0x00001000 | 0x00002000, 0x04);
    }

    private bool FixRequired()
    {
      //Fix is disabled by flag.
      if ((this.Context.RuntimeBreakpoint.Flags & MethodTracingFlag.NO_SHORT_STRUCTURES_FIX) == MethodTracingFlag.NO_SHORT_STRUCTURES_FIX)
      {
        return false;
      }

      //ArgSize 0 means that struct has no fields. No sense to apply fix in that case.
      return this.ArgSize <= IntPtr.Size && this.ArgSize != 0;
    }

    #endregion
  }
}