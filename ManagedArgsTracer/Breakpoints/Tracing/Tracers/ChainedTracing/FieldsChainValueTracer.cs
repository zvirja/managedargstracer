﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using JetBrains.Annotations;

using ManagedArgsTracer.Diagnostics;
using ManagedArgsTracer.Helpers;

using Microsoft.Diagnostics.Runtime;

#endregion

namespace ManagedArgsTracer.Breakpoints.Tracing.Tracers.ChainedTracing
{
  internal class FieldsChainValueTracer : IDisposable
  {
    #region Constructors and Destructors

    public FieldsChainValueTracer(
      [NotNull] StringBuilder outputBuffer,
      [NotNull] ClrType argumentType,
      AddressedArgumentValue rawArgumentValue,
      [NotNull] ArgumentValueResolvingContext context,
      [NotNull] string[] fieldsChain
      )
    {
      Assert.ArgumentNotNull(outputBuffer, "outputBuffer");
      Assert.ArgumentNotNull(argumentType, "argumentType");
      Assert.ArgumentNotNull(context, "context");
      Assert.ArgumentNotNull(fieldsChain, "fieldsChain");
      this.OutputBuffer = outputBuffer;
      this.ArgumentType = argumentType;
      this.RawArgumentValue = rawArgumentValue;
      this.Context = context;
      this.FieldsChain = fieldsChain;
    }

    #endregion

    #region Properties

    private ClrType ArgumentType { get; set; }

    private ArgumentValueResolvingContext Context { get; set; }

    private string[] FieldsChain { get; set; }

    private StringBuilder OutputBuffer { get; set; }

    private AddressedArgumentValue RawArgumentValue { get; set; }

    private EmbeddedStructArgumentFixer StructArgumentFixer { get; set; }

    #endregion

    #region Public Methods and Operators

    public void AppendArgumentValueToBuffer()
    {
      ulong? argumentValue = this.TraceAndGetInitialArgumentValue();
      if (argumentValue == null)
      {
        return;
      }
      var iterator = new FieldsChainTracingIterator(this.OutputBuffer, argumentValue.Value, this.ArgumentType, this.Context, this.FieldsChain, 1);
      while (iterator.TraceCurrentChainValue())
      {
        //Dummy. Just move iterator next until end.
      }
    }

    public void Dispose()
    {
      this.Dispose(true);
      GC.SuppressFinalize(this);
    }

    #endregion

    #region Methods

    protected virtual void Dispose(bool managed)
    {
      if (this.StructArgumentFixer != null)
      {
        this.StructArgumentFixer.CleanUp();
      }
    }

    private ulong? TraceAndGetFixedStructValue()
    {
      this.StructArgumentFixer = new EmbeddedStructArgumentFixer(this.ArgumentType, this.RawArgumentValue, this.Context);
      ulong? fixedValue = this.StructArgumentFixer.GetFixedArgumentValue();
      if (fixedValue == null)
      {
        TracingHelper.AppendCurrentChainValueToBuffer(this.OutputBuffer, this.Context, this.FieldsChain[0], "<UNABLE TO RESOLVE VALUE FOR SMALL STRUCT>", false, true);
        Tracer.PrivateOutput("Unable to fix value for the '{0}' structure. Calculated struct size: {1}.".FormatWith(this.ArgumentType.Name, this.StructArgumentFixer.ArgSize));
        return null;
      }
      TracingHelper.AppendCurrentChainValueToBuffer(
        this.OutputBuffer,
        this.Context,
        (this.StructArgumentFixer.FixIsApplied ? "**" : "*") + this.FieldsChain[0],
        this.RawArgumentValue.Value.ToHexString(),
        false,
        false);
      return fixedValue;
    }

    private ulong? TraceAndGetInitialArgumentValue()
    {
      if (this.ArgumentType.ElementType == ClrElementType.Struct)
      {
        return this.TraceAndGetFixedStructValue();
      }
      TracingHelper.AppendCurrentChainValueToBuffer(this.OutputBuffer, this.Context, this.FieldsChain[0], this.RawArgumentValue.Value.ToHexString(), false, false);
      return this.RawArgumentValue.Value;
    }

    #endregion
  }
}