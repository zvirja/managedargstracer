﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ManagedArgsTracer.Helpers;

using Microsoft.Diagnostics.Runtime;

#endregion

namespace ManagedArgsTracer.Breakpoints.Tracing.Tracers.ChainedTracing
{
  internal class StructChainValueReader : FieldValueReader
  {
    #region Public Methods and Operators

    public override bool TraceCurrentFieldReadNext(
      ClrInstanceField field,
      ulong currentChainAddress,
      bool isInterior,
      StringBuilder outputBuffer,
      ArgumentValueResolvingContext context,
      string undecoratedFieldName,
      out ulong nextChainAddress,
      out bool nextIsInterior,
      out ClrType currentChainType)
    {
      nextChainAddress = field.GetFieldAddress(currentChainAddress, isInterior);
      nextIsInterior = true;
      currentChainType = field.Type;

      TracingHelper.AppendCurrentChainValueToBuffer(outputBuffer, context, "*" + undecoratedFieldName, nextChainAddress.ToHexString(), false, false);

      return true;
    }

    #endregion
  }
}