﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Diagnostics.Runtime;

#endregion

namespace ManagedArgsTracer.Breakpoints.Tracing.Tracers.ChainedTracing
{
  internal abstract class FieldValueReader
  {
    #region Public Methods and Operators

    public abstract bool TraceCurrentFieldReadNext(
      ClrInstanceField field,
      ulong currentChainAddress,
      bool isInterior,
      StringBuilder outputBuffer,
      ArgumentValueResolvingContext context,
      string undecoratedFieldName,
      out ulong nextChainAddress,
      out bool nextIsInterior,
      out ClrType currentChainType);

    #endregion
  }
}