﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using JetBrains.Annotations;

using ManagedArgsTracer.Diagnostics;
using ManagedArgsTracer.Helpers;

using Microsoft.Diagnostics.Runtime;

#endregion

namespace ManagedArgsTracer.Breakpoints.Tracing.Tracers.ChainedTracing
{
  internal class FieldsChainTracingIterator
  {
    #region Static Fields

    private static FieldValueReader FinalChainReader = new FinalChainValueReader();

    private static FieldValueReader ReferenceChainReader = new ReferenceChainValueReader();

    private static FieldValueReader StructChainReader = new StructChainValueReader();

    #endregion

    #region Fields

    private bool CurrentIsInterior;

    private ClrType CurrentObjectType;

    private ulong CurrentObjectValue;

    #endregion

    #region Constructors and Destructors

    public FieldsChainTracingIterator(
      [NotNull] StringBuilder outputBuffer,
      ulong initialArgumentsValue,
      [NotNull] ClrType argType,
      [NotNull] ArgumentValueResolvingContext context,
      [NotNull] string[] fields,
      int firstFieldIndex)
    {
      Assert.ArgumentNotNull(outputBuffer, "outputBuffer");
      Assert.ArgumentNotNull(argType, "argType");
      Assert.ArgumentNotNull(context, "context");
      Assert.ArgumentNotNull(fields, "fields");

      this.OutputBuffer = outputBuffer;
      this.CurrentObjectValue = initialArgumentsValue;
      this.CurrentObjectType = argType;
      this.CurrentIsInterior = argType.ElementType == ClrElementType.Struct;
      this.Context = context;
      this.AllFieldsChain = fields;
      this.CurrentFieldChainIndex = firstFieldIndex - 1;
    }

    #endregion

    #region Properties

    private string[] AllFieldsChain { get; set; }

    private ArgumentValueResolvingContext Context { get; set; }

    private int CurrentFieldChainIndex { get; set; }

    private bool IsLastIteration
    {
      get
      {
        return this.CurrentFieldChainIndex + 1 >= this.AllFieldsChain.Length;
      }
    }

    private StringBuilder OutputBuffer { get; set; }

    #endregion

    #region Public Methods and Operators

    /// <summary>
    /// This method traces current value and goes ahead.
    /// </summary>
    /// <returns></returns>
    public bool TraceCurrentChainValue()
    {
      this.CurrentFieldChainIndex++;
      //Should never reach that point, but handle that properly if reach.
      if (this.CurrentFieldChainIndex >= this.AllFieldsChain.Length)
      {
        return false;
      }

      string fieldName = this.AllFieldsChain[this.CurrentFieldChainIndex];
      string decoratedFieldName = this.DecorateFieldName(fieldName);

      //Resolve ClrInstanceField
      ClrInstanceField field = this.CurrentObjectType.GetFieldByName(decoratedFieldName);
      if (field == null)
      {
        this.TraceErrorDuringFieldFetch("'{0}' FIELD WAS NOT FOUND".FormatWith(fieldName));
        Tracer.PrivateOutput("Field '{0}' decorated to '{1}' was not found in type '{2}'.".FormatWith(fieldName, decoratedFieldName, this.CurrentObjectType.Name));
        return false;
      }

      //The primitive value appears at the middle of the chain. That is not acceptable.
      if (field.IsPrimitive() && !this.IsLastIteration)
      {
        this.TraceErrorDuringFieldFetch("NON-PRIMITIVE FIELD EXPECTED.CURRENT FIELD TYPE: {0}".FormatWith(field.ElementType.ToString()));
        return false;
      }

      FieldValueReader fieldValueReader = this.ResolveFieldValueReader(field, fieldName, this.IsLastIteration);

      //Resolve chain context for the next iteration.
      bool readerContinueChain = fieldValueReader.TraceCurrentFieldReadNext(
        field,
        this.CurrentObjectValue,
        this.CurrentIsInterior,
        this.OutputBuffer,
        this.Context,
        fieldName,
        out this.CurrentObjectValue,
        out this.CurrentIsInterior,
        out this.CurrentObjectType);

      //It may happen that type wasn't resolved. In that case stop processing with error. 
      if (readerContinueChain && this.CurrentObjectType == null)
      {
        this.OutputBuffer.Append(" <TYPE OF NEXT WASN'T RESOLVED. UNABLE NO FINISH CHAIN.>");
        return false;
      }

      return readerContinueChain;
    }

    #endregion

    #region Methods

    private string DecorateFieldName(string fieldName)
    {
      /* Currently only AutoProperites are decorated. $fieldName is decorated to <fieldName>k__BackingField
       * 
       */

      if (fieldName.Length > 1 && fieldName[0] == '$')
      {
        return "<{0}>k__BackingField".FormatWith(fieldName.Substring(1));
      }
      return fieldName;
    }

    private FieldValueReader ResolveFieldValueReader(ClrInstanceField field, string undecoratedFieldName, bool isLastIteration)
    {
      if (isLastIteration)
      {
        return FinalChainReader;
      }
      if (field.IsObjectReference())
      {
        return ReferenceChainReader;
      }
      return StructChainReader;
    }

    private void TraceErrorDuringFieldFetch(string errorMessage)
    {
      this.OutputBuffer.AppendFormat(".<{0}>", errorMessage);
    }

    #endregion
  }
}