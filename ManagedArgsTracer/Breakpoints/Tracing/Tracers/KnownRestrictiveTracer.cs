﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ManagedArgsTracer.Breakpoints.Definition;
using ManagedArgsTracer.Breakpoints.Definition.Tracing;
using ManagedArgsTracer.Common;

#endregion

namespace ManagedArgsTracer.Breakpoints.Tracing.Tracers
{
  internal class KnownRestrictiveTracer : ArgumentTracer
  {
    #region Public Properties

    public KnownRestrictiveTracingArg ArgDefinition { get; set; }

    #endregion

    #region Public Methods and Operators

    public override ArgumentTracer Clone()
    {
      return new KnownRestrictiveTracer();
    }

    public override bool HandlesMethodArgumentDefinition(MethodArg argDefinition)
    {
      return argDefinition is KnownRestrictiveTracingArg;
    }

    public override void Initialize(MethodArg argDefinition, ArgumentTracerResolver argumentTracerResolver)
    {
      base.Initialize(argDefinition, argumentTracerResolver);
      this.ArgDefinition = (KnownRestrictiveTracingArg)argDefinition;
    }

    public override bool ReadArgumentValue(
      AddressedArgumentValue rawArgumentValue,
      FullTypeName definitionArgumentTypeName,
      ArgumentValueResolvingContext context,
      out string valueOrErrorExplanation)
    {
      return this.ArgDefinition.ValueReader.ReadDirectArgumentValue(rawArgumentValue, context.DebugInterfaces, context, out valueOrErrorExplanation);
    }

    #endregion
  }
}