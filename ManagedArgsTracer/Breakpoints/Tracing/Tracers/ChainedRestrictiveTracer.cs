#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ManagedArgsTracer.Breakpoints.Definition;
using ManagedArgsTracer.Breakpoints.Definition.Tracing;
using ManagedArgsTracer.Common;

using Microsoft.Diagnostics.Runtime;

#endregion

namespace ManagedArgsTracer.Breakpoints.Tracing.Tracers
{
  internal class ChainedRestrictiveTracer : ChainedAnyTracer
  {
    #region Public Properties

    public new ChainedRestrictiveTracingArg ArgDefinition { get; set; }

    #endregion

    #region Public Methods and Operators

    public override ArgumentTracer Clone()
    {
      return new ChainedRestrictiveTracer();
    }

    public override bool HandlesMethodArgumentDefinition(MethodArg argDefinition)
    {
      return argDefinition is ChainedRestrictiveTracingArg;
    }

    public override void Initialize(MethodArg argDefinition, ArgumentTracerResolver argumentTracerResolver)
    {
      base.Initialize(argDefinition, argumentTracerResolver);
      this.ArgDefinition = (ChainedRestrictiveTracingArg)argDefinition;
    }

    #endregion

    #region Methods

    protected override ClrType GetArgumentTypeCached(ulong rawArgumentValue, FullTypeName definitionArgumentTypeName, ArgumentValueResolvingContext context)
    {
      return base.GetArgumentTypeCached(rawArgumentValue, this.ArgDefinition.ArgType, context);
    }

    #endregion
  }
}