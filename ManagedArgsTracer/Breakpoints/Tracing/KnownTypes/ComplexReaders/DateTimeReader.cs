﻿#region

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;

using ManagedArgsTracer.Breakpoints.Tracing.Tracers.ChainedTracing;
using ManagedArgsTracer.Helpers;
using ManagedArgsTracer.Session;

using Microsoft.Diagnostics.Runtime;

#endregion

namespace ManagedArgsTracer.Breakpoints.Tracing.KnownTypes.ComplexReaders
{
  public class DateTimeReader : IValueReader
  {
    #region Static Fields

    private static FieldInfo DateDataFldInfo;

    #endregion

    #region Constructors and Destructors

    static DateTimeReader()
    {
      DateDataFldInfo = typeof(DateTime).GetField("dateData", BindingFlags.Instance | BindingFlags.NonPublic);
    }

    public DateTimeReader()
    {
      this.MainName = "System.DateTime";
    }

    #endregion

    #region Public Properties

    public string MainName { get; private set; }

    #endregion

    #region Public Methods and Operators

    public bool KnowHowToReadType(string typeName)
    {
      return typeName.EqualsWithCase(this.MainName, true);
    }

    public bool ReadDirectArgumentValue(AddressedArgumentValue rawArgumentValue, INativeDebugInterfaces debuginterfaces, IArgumentValueResolvingContext context, out string fieldValue)
    {
      fieldValue = null;
      ClrType dateTimeType = context.RuntimeHelper.ClrTypeCache.FindTypeByName("System.DateTime");
      if (dateTimeType == null)
      {
        return false;
      }

      //Ensure that size of DateTime structure is equal IntPtr.Size (so it works for X64 only :)) In this case value could be fetched directly.
      uint size = EmbeddedStructArgumentFixer.CountStructureSize(dateTimeType);
      if (size > IntPtr.Size)
      {
        return this.ReadDateTimeByStructAddress(dateTimeType, rawArgumentValue.Value, out fieldValue);
      }

      fieldValue = this.GetDateTimeValueFromDateData(rawArgumentValue.Value);
      return true;
    }

    public bool ReadFieldValue(ClrInstanceField field, ulong objectAddress, bool isInterior, out string fieldValue)
    {
      /*
       * ulong dateData - That is the field that actually holds the data.
       * */

      ulong dateTimeStructAddr = field.GetFieldAddress(objectAddress, isInterior);
      ClrType dateTimeType = field.Type;

      return this.ReadDateTimeByStructAddress(dateTimeType, dateTimeStructAddr, out fieldValue);
    }

    #endregion

    #region Methods

    private string GetDateTimeValueFromDateData(ulong dateData)
    {
      var dateTime = new DateTime();
      ReflectionHelper.SetValueForValueType(DateDataFldInfo, ref dateTime, dateData);

      return dateTime.ToString("O", CultureInfo.InvariantCulture);
    }

    private bool ReadDateTimeByStructAddress(ClrType dateTimeType, ulong structAddr, out string fieldValue)
    {
      ClrInstanceField dateDataFld = dateTimeType.GetFieldByName("dateData");
      if (dateDataFld == null)
      {
        fieldValue = null;
        return false;
      }

      var dateData = (ulong)dateDataFld.GetFieldValue(structAddr, true);

      fieldValue = this.GetDateTimeValueFromDateData(dateData);
      return true;
    }

    #endregion
  }
}