#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ManagedArgsTracer.Helpers;
using ManagedArgsTracer.Session;

using Microsoft.Diagnostics.Runtime;

#endregion

namespace ManagedArgsTracer.Breakpoints.Tracing.KnownTypes.ComplexReaders
{
  public class GuidReader : IValueReader
  {
    #region Constructors and Destructors

    public GuidReader()
    {
      this.MainName = "System.Guid";
    }

    #endregion

    #region Public Properties

    public string MainName { get; private set; }

    #endregion

    #region Public Methods and Operators

    public bool KnowHowToReadType(string typeName)
    {
      return typeName.EqualsWithCase(this.MainName, true);
    }

    public bool ReadDirectArgumentValue(AddressedArgumentValue rawArgumentValue, INativeDebugInterfaces debuginterfaces, IArgumentValueResolvingContext context, out string fieldValue)
    {
      ClrType type = context.RuntimeHelper.ClrTypeCache.FindTypeByName("System.Guid");
      if (type == null)
      {
        fieldValue = null;
        return false;
      }

      return this.ReadGuid(rawArgumentValue.Value, type, out fieldValue);
    }

    public bool ReadFieldValue(ClrInstanceField field, ulong objectAddress, bool isInterior, out string fieldValue)
    {
      ulong guidStructAddr = field.GetFieldAddress(objectAddress, isInterior);

      ClrType guidType = field.Type;
      if (guidType == null)
      {
        fieldValue = null;
        return false;
      }
      return this.ReadGuid(guidStructAddr, guidType, out fieldValue);
    }

    #endregion

    #region Methods

    private static TValue ReadComponent<TValue>(string fieldName, ClrType guidType, ulong guidStructAddr, ref bool errorIsPresent)
    {
      if (errorIsPresent)
      {
        return default (TValue);
      }

      ClrInstanceField field = guidType.GetFieldByName(fieldName);
      if (field == null)
      {
        errorIsPresent = true;
        return default(TValue);
      }

      return (TValue)field.GetFieldValue(guidStructAddr, true);
    }

    private bool ReadGuid(ulong guidStructAddr, ClrType guidType, out string result)
    {
      /*
       * GUID HOLDS THE FOLLOWING:
      private int _a;
      private short _b;
      private short _c;
      private byte _d;
      private byte _e;
      private byte _f;
      private byte _g;
      private byte _h;
      private byte _i;
      private byte _j;
      private byte _k;
       * 
       */
      bool errorPresent = false;

      var a = ReadComponent<int>("_a", guidType, guidStructAddr, ref errorPresent);
      var b = ReadComponent<short>("_b", guidType, guidStructAddr, ref errorPresent);
      var c = ReadComponent<short>("_c", guidType, guidStructAddr, ref errorPresent);
      var d = ReadComponent<byte>("_d", guidType, guidStructAddr, ref errorPresent);
      var e = ReadComponent<byte>("_e", guidType, guidStructAddr, ref errorPresent);
      var f = ReadComponent<byte>("_f", guidType, guidStructAddr, ref errorPresent);
      var g = ReadComponent<byte>("_h", guidType, guidStructAddr, ref errorPresent);
      var h = ReadComponent<byte>("_h", guidType, guidStructAddr, ref errorPresent);
      var i = ReadComponent<byte>("_i", guidType, guidStructAddr, ref errorPresent);
      var j = ReadComponent<byte>("_j", guidType, guidStructAddr, ref errorPresent);
      var k = ReadComponent<byte>("_k", guidType, guidStructAddr, ref errorPresent);

      if (errorPresent)
      {
        result = null;
        return false;
      }

      var guid = new Guid(a, b, c, d, e, f, g, h, i, j, k);
      result = guid.ToString("B");
      return true;
    }

    #endregion
  }
}