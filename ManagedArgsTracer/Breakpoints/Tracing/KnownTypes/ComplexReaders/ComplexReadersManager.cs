﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#endregion

namespace ManagedArgsTracer.Breakpoints.Tracing.KnownTypes.ComplexReaders
{
  internal class ComplexReadersManager
  {
    #region Static Fields

    private static readonly List<IValueReader> KnownReaders;

    #endregion

    #region Constructors and Destructors

    static ComplexReadersManager()
    {
      KnownReaders = new List<IValueReader>
                       {
                         new GuidReader(),
                         new DateTimeReader()
                       };
    }

    #endregion

    #region Public Methods and Operators

    public static IValueReader FindMatchingReader(string typeName)
    {
      return KnownReaders.FirstOrDefault(reader => reader.KnowHowToReadType(typeName));
    }

    #endregion
  }
}