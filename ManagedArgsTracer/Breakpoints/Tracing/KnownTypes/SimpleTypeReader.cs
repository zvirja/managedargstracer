﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ManagedArgsTracer.Breakpoints.Tracing.KnownTypes.SimpleTypes;
using ManagedArgsTracer.Diagnostics;
using ManagedArgsTracer.Helpers;
using ManagedArgsTracer.Session;

using Microsoft.Diagnostics.Runtime;

#endregion

namespace ManagedArgsTracer.Breakpoints.Tracing.KnownTypes
{
  internal class SimpleTypeReader : IValueReader
  {
    #region Constructors and Destructors

    public SimpleTypeReader(SimpleTypeDescriptor typeDescriptor)
    {
      this.TypeDescriptor = typeDescriptor;
    }

    #endregion

    #region Public Properties

    public string MainName
    {
      get
      {
        return this.TypeDescriptor.MainName;
      }
    }

    #endregion

    #region Properties

    private SimpleTypeDescriptor TypeDescriptor { get; set; }

    #endregion

    #region Public Methods and Operators

    public bool KnowHowToReadType(string typeName)
    {
      return this.TypeDescriptor.MainName.EqualsWithCase(typeName, true) || this.TypeDescriptor.Aliases.Any(alias => alias.EqualsWithCase(typeName, true));
    }

    public bool ReadDirectArgumentValue(AddressedArgumentValue rawArgumentValue, INativeDebugInterfaces debuginterfaces, IArgumentValueResolvingContext context, out string fieldValue)
    {
      fieldValue = this.TypeDescriptor.ValueReader(rawArgumentValue.Value, debuginterfaces, context.RuntimeHelper.Runtime);
      return true;
    }

    public bool ReadFieldValue(ClrInstanceField field, ulong objectAddress, bool isInterior, out string fieldValue)
    {
      Tracer.ReportUnexpectedState("SimpleTypeReader was invoked for {0} type.".FormatWith(this.TypeDescriptor.MainName));
      fieldValue = null;
      return false;
    }

    #endregion
  }
}