﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ManagedArgsTracer.Session;

using Microsoft.Diagnostics.Runtime;

#endregion

namespace ManagedArgsTracer.Breakpoints.Tracing.KnownTypes
{
  public interface IValueReader
  {
    #region Public Properties

    string MainName { get; }

    #endregion

    #region Public Methods and Operators

    bool KnowHowToReadType(string typeName);

    bool ReadDirectArgumentValue(AddressedArgumentValue rawArgumentValue, INativeDebugInterfaces debuginterfaces, IArgumentValueResolvingContext context, out string fieldValue);

    bool ReadFieldValue(ClrInstanceField field, ulong objectAddress, bool isInterior, out string fieldValue);

    #endregion
  }
}