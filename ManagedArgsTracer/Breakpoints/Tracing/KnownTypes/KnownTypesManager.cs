﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ManagedArgsTracer.Breakpoints.Tracing.KnownTypes.ComplexReaders;
using ManagedArgsTracer.Breakpoints.Tracing.KnownTypes.SimpleTypes;

#endregion

namespace ManagedArgsTracer.Breakpoints.Tracing.KnownTypes
{
  internal static class KnownTypesManager
  {
    #region Public Methods and Operators

    public static IValueReader ResolveValueReader(string typeNameOrAlias)
    {
      SimpleTypeDescriptor simpleDescriptor = SimpleTypesManager.ResolveSimpleTypeByNameOrAlias(typeNameOrAlias);
      if (simpleDescriptor != null)
      {
        return new SimpleTypeReader(simpleDescriptor);
      }

      return ComplexReadersManager.FindMatchingReader(typeNameOrAlias);
    }

    #endregion
  }
}