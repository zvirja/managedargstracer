﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ManagedArgsTracer.Session;

using Microsoft.Diagnostics.Runtime;

#endregion

namespace ManagedArgsTracer.Breakpoints.Tracing.KnownTypes.SimpleTypes
{
  internal delegate string EntityValueReader(ulong rawArgumentValue, INativeDebugInterfaces debugInterfaces, ClrRuntime runtime);

  internal class SimpleTypeDescriptor
  {
    #region Fields

    public string[] Aliases;

    public string MainName;

    public EntityValueReader ValueReader;

    #endregion

    #region Constructors and Destructors

    public SimpleTypeDescriptor(string mainName, string[] aliases, EntityValueReader valueReader)
    {
      this.MainName = mainName;
      this.Aliases = aliases;
      this.ValueReader = valueReader;
    }

    #endregion
  }
}