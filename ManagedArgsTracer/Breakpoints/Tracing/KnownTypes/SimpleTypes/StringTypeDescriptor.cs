﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ManagedArgsTracer.Diagnostics;
using ManagedArgsTracer.Helpers;
using ManagedArgsTracer.Session;

using Microsoft.Diagnostics.Runtime;

#endregion

namespace ManagedArgsTracer.Breakpoints.Tracing.KnownTypes.SimpleTypes
{
  internal class StringTypeDescriptor : SimpleTypeDescriptor
  {
    #region Constructors and Destructors

    public StringTypeDescriptor(string mainName, string[] aliases)
      : base(mainName, aliases, ReadStringValue)
    {
    }

    #endregion

    #region Methods

    private static string ReadStringValue(ulong rawArgumentValue, INativeDebugInterfaces debuginterfaces, ClrRuntime runtime)
    {
      if (rawArgumentValue == 0UL)
      {
        return "<null>";
      }
      ClrType clrType = runtime.GetHeap().GetObjectType(rawArgumentValue);
      const string errorDuringArgTypeResolvingVal = "<ERROR DURING VAR TYPE RESOLVING>";
      if (clrType == null)
      {
        Tracer.PrivateOutput("Unable to get ClrType for string argument (is null).");
        return errorDuringArgTypeResolvingVal;
      }
      if (clrType.ElementType != ClrElementType.String)
      {
        Tracer.PrivateOutput("Unable to properly resolve type for string. Expected element type is string, but it was resolved as {0}".FormatWith(clrType.ElementType));
        return errorDuringArgTypeResolvingVal;
      }
      var stringValue = (string)clrType.GetValue(rawArgumentValue);
      return stringValue;
    }

    #endregion
  }
}