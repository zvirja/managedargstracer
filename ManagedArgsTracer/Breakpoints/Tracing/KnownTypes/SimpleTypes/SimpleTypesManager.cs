﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using JetBrains.Annotations;

using ManagedArgsTracer.Diagnostics;
using ManagedArgsTracer.Helpers;
using ManagedArgsTracer.Session;

using Microsoft.Diagnostics.Runtime;

#endregion

namespace ManagedArgsTracer.Breakpoints.Tracing.KnownTypes.SimpleTypes
{
  internal static class SimpleTypesManager
  {
    //Delegate for proper method of type converter.

    #region Constructors and Destructors

    static SimpleTypesManager()
    {
      SimpleTypeDescriptors = new List<SimpleTypeDescriptor>
                                {
                                  MakeSimpleTypeDescriptor("SByte", new[] { "sbyte", "System.SByte" }, 1, (bytes, index) => (sbyte)bytes[index]),
                                  MakeSimpleTypeDescriptor("Byte", new[] { "byte", "System.Byte" }, 1, (bytes, index) => bytes[index]),
                                  MakeSimpleTypeDescriptor("Int16", new[] { "short", "System.Int16" }, 2, BitConverter.ToInt16),
                                  MakeSimpleTypeDescriptor("UInt16", new[] { "ushort", "System.UInt16" }, 2, BitConverter.ToUInt16),
                                  MakeSimpleTypeDescriptor("Int32", new[] { "int", "System.Int32" }, 4, BitConverter.ToInt32),
                                  MakeSimpleTypeDescriptor("UInt32", new[] { "uint", "System.UInt32" }, 4, BitConverter.ToUInt32),
                                  MakeSimpleTypeDescriptor("Int64", new[] { "long", "System.Int64" }, 8, BitConverter.ToInt64),
                                  MakeSimpleTypeDescriptor("UInt64", new[] { "ulong", "System.UInt64" }, 8, BitConverter.ToUInt64),
                                  MakeSimpleTypeDescriptor("Char", new[] { "char", "System.Char" }, 2, (bytes, index) => (char)BitConverter.ToUInt16(bytes, index)),
                                  MakeSimpleTypeDescriptor("Single", new[] { "float", "System.Single" }, 4, BitConverter.ToSingle),
                                  MakeSimpleTypeDescriptor("Double", new[] { "double", "System.Double" }, 4, BitConverter.ToSingle),
                                  MakeSimpleTypeDescriptor("Boolean", new[] { "bool", "System.Boolean" }, 1, (bytes, index) => bytes[index] != 0),
                                  new StringTypeDescriptor("System.String", new[] { "string" })
                                };
    }

    #endregion

    #region Delegates

    private delegate TValue ValueConverter<TValue>(byte[] bytes, int startIndex);

    #endregion

    #region Public Properties

    public static List<SimpleTypeDescriptor> SimpleTypeDescriptors { get; private set; }

    #endregion

    #region Public Methods and Operators

    /// <summary>
    /// Tries to resolve simple type descriptor. If unable to resolve - null is returned.
    /// </summary>
    public static SimpleTypeDescriptor ResolveSimpleTypeByNameOrAlias([NotNull] string nameOrAlias)
    {
      Assert.ArgumentNotNullOrEmpty(nameOrAlias, "nameOrAlias");
      foreach (SimpleTypeDescriptor typeDescriptor in SimpleTypeDescriptors)
      {
        if (typeDescriptor.MainName.EqualsWithCase(nameOrAlias, true) || typeDescriptor.Aliases.Any(alias => alias.EqualsWithCase(nameOrAlias, true)))
        {
          return typeDescriptor;
        }
      }
      return null;
    }

    #endregion

    #region Methods

    private static SimpleTypeDescriptor MakeSimpleTypeDescriptor<TNative>(
      string mainName,
      string[] knownAliases,
      int nativeBytes,
      ValueConverter<TNative> toNativeConverter)
    {
      //Fall to arc independed.
      if (nativeBytes <= 4)
      {
        return MakeSimpleTypeDescriptorWithoutDereference(mainName, knownAliases, nativeBytes, toNativeConverter);
      }

      Assert.IsTrue(nativeBytes <= 8, "Native types should be less or equal 8 bytes.");

      //If current target is X64 (and client is X64 too), we can use arc independent type (i.e. to special fetching).
      if (IntPtr.Size == 8)
      {
        return MakeSimpleTypeDescriptorWithoutDereference(mainName, knownAliases, nativeBytes, toNativeConverter);
      }

      return new SimpleTypeDescriptor(
        mainName,
        knownAliases,
        delegate(ulong value, INativeDebugInterfaces interfaces, ClrRuntime runtime)
          {
            //We are here only if actual type consumes more than IntPtr value size. In this case we need additional dereference
            var buffer = new byte[nativeBytes];
            uint bytesRead;
            if (interfaces.DebugDataSpaces.ReadVirtual(value, buffer, (uint)nativeBytes, out bytesRead) != 0 || bytesRead != nativeBytes)
            {
              Tracer.PrivateOutput("Unable to fetch simple value for {0} type".FormatWith(mainName));
              return "<UNABLE TO FETCH>";
            }

            TNative nativeValue = toNativeConverter(buffer, 0);
            return nativeValue.ToString();
          });
    }

    /// <summary>
    /// Make simple type descriptor for without dereferencing the value.
    /// </summary>
    private static SimpleTypeDescriptor MakeSimpleTypeDescriptorWithoutDereference<TNative>(
      string mainName,
      string[] knownAliases,
      int nativeBytes,
      ValueConverter<TNative> toNativeConverter)
    {
      return new SimpleTypeDescriptor(
        mainName,
        knownAliases,
        delegate(ulong value, INativeDebugInterfaces interfaces, ClrRuntime runtime)
          {
            byte[] valueAsBytes = BitConverter.GetBytes(value);
            int indexToStartRead = 0;
            if (valueAsBytes.Length != nativeBytes)
            {
              //From MSDN (http://msdn.microsoft.com/en-us/library/system.bitconverter(v=vs.110).aspx). 
              //If value is  1,234,567,890 (0x499602D2), for Little Endian: D2-02-96-49, Big Endian: 49-96-02-D2
              //So for little endian we don't need to navigate, for big endian - navigate to position.
              if (!BitConverter.IsLittleEndian)
              {
                indexToStartRead = valueAsBytes.Length - nativeBytes;
              }
            }

            TNative nativeValue = toNativeConverter(valueAsBytes, indexToStartRead);
            return nativeValue.ToString();
          });
    }

    #endregion
  }
}