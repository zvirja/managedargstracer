﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ManagedArgsTracer.Runtime;

#endregion

namespace ManagedArgsTracer.Breakpoints.Tracing
{
  public interface IArgumentValueResolvingContext
  {
    #region Public Properties

    ulong AppDomainId { get; }

    ICleverFlusher CleverFlusher { get; }

    IClrRuntimeHelper RuntimeHelper { get; }

    #endregion
  }
}