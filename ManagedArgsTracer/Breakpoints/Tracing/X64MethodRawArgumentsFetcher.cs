﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ManagedArgsTracer.Session;

#endregion

namespace ManagedArgsTracer.Breakpoints.Tracing
{
  internal class X64MethodRawArgumentsFetcher : MethodRawArgumentsFetcher
  {
    #region Public Methods and Operators

    public override List<AddressedArgumentValue> FetchArguments(NativeDebugInterfaces debugInterfaces, int numberOfArgs)
    {
      ArchitectureSpecificContexts.X64THREADCONTEXT? threadContext = this.GetThreadContext<ArchitectureSpecificContexts.X64THREADCONTEXT>(debugInterfaces);
      if (!threadContext.HasValue)
      {
        return null;
      }

      var result = new List<AddressedArgumentValue>();
      if (numberOfArgs == 0)
      {
        return result;
      }

      //First arg in RCX
      result.Add(new AddressedArgumentValue(threadContext.Value.Rcx));

      numberOfArgs--;
      if (numberOfArgs == 0)
      {
        return result;
      }

      //Second arg in RDX
      result.Add(new AddressedArgumentValue(threadContext.Value.Rdx));
      numberOfArgs--;
      if (numberOfArgs == 0)
      {
        return result;
      }

      //Third arg in R8
      result.Add(new AddressedArgumentValue(threadContext.Value.R8));
      numberOfArgs--;
      if (numberOfArgs == 0)
      {
        return result;
      }

      //Fourth arg in R9
      result.Add(new AddressedArgumentValue(threadContext.Value.R9));
      numberOfArgs--;
      if (numberOfArgs == 0)
      {
        return result;
      }

      //The rest part of arguments should be fetched from stack.
      ulong startLocation = threadContext.Value.Rsp;
      //Home space is 4 QWORDs, so we should add 0x20 + 0x8 to skip return address.
      startLocation += 0x28;
      //At this point startLocation points to the next argument.

      uint bytesToRead = 8U * (uint)numberOfArgs;

      var buffer = new byte[bytesToRead];
      uint bytesRead;
      if (debugInterfaces.DebugDataSpaces.ReadVirtual(startLocation, buffer, bytesToRead, out bytesRead) != 0 || bytesRead != bytesToRead)
      {
        return null;
      }

      for (int i = 0; i < numberOfArgs; i++)
      {
        ulong nextArg = BitConverter.ToUInt64(buffer, i * 8);
        result.Add(new AddressedArgumentValue(nextArg, startLocation + ((uint)i * 8U)));
      }

      return result;
    }

    #endregion
  }
}