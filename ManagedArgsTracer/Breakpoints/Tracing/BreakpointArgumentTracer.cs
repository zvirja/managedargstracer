﻿#region

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;

using JetBrains.Annotations;

using ManagedArgsTracer.Breakpoints.Definition;
using ManagedArgsTracer.Breakpoints.Tracing.Tracers;
using ManagedArgsTracer.Common;
using ManagedArgsTracer.Diagnostics;
using ManagedArgsTracer.Helpers;
using ManagedArgsTracer.Interop.Clr;
using ManagedArgsTracer.Runtime;
using ManagedArgsTracer.Runtime.Wrappers;
using ManagedArgsTracer.Session;

using Microsoft.Diagnostics.Runtime;

#endregion

namespace ManagedArgsTracer.Breakpoints.Tracing
{
  internal delegate ArgumentTracer ArgumentTracerResolver(MethodArg argumentDefinition, ArgumentValueResolvingContext context);

  internal class BreakpointArgumentsTracer
  {
    #region Constants

    private const string EndOfTraceDecoration = "=========================";

    private const string UnableToFetchFallback = "<UNABLE TO FETCH>";

    #endregion

    #region Static Fields

    private static List<ArgumentTracer> KnownTracerPrototypes;

    #endregion

    #region Constructors and Destructors

    static BreakpointArgumentsTracer()
    {
      KnownTracerPrototypes = new List<ArgumentTracer>
                                {
                                  //The order does matter. Usually non-restrictive (unknown) can handle restrictive too, so should check from more specific to general.
                                  new KnownRestrictiveTracer(),
                                  new KnownAnyTracer(),
                                  new ChainedRestrictiveTracer(),
                                  new ChainedAnyTracer(),
                                  new GroupTracer()
                                };
    }

    public BreakpointArgumentsTracer(
      [NotNull] RuntimeBreakpoint runtimeBreakpoint,
      [NotNull] NativeDebugInterfaces nativeDebugInterfaces,
      ClrRuntimeHelper clrRuntimeHelper,
      [NotNull] XClrDataProcessWrapper xclrProcess)
    {
      Assert.ArgumentNotNull(runtimeBreakpoint, "runtimeBreakpoint");
      Assert.ArgumentNotNull(nativeDebugInterfaces, "nativeDebugInterfaces");
      Assert.ArgumentNotNull(xclrProcess, "xclrProcess");

      this.RuntimeBreakpoint = runtimeBreakpoint;
      this.NativeDebugInterfaces = nativeDebugInterfaces;
      this.Runtime = clrRuntimeHelper.Runtime;
      this.ClrRuntimeHelper = clrRuntimeHelper;

      this.XClrProcess = xclrProcess;

      this.ArgumentTracersCache = new Dictionary<MethodArgMethodInstancePair, object>();
      this.BreakpoinedInstanceInfos = new Dictionary<uint, ClrMethodInstanceOfflineInfo>();
    }

    #endregion

    #region Properties

    private Dictionary<MethodArgMethodInstancePair, object> ArgumentTracersCache { get; set; }

    private Dictionary<uint, ClrMethodInstanceOfflineInfo> BreakpoinedInstanceInfos { get; set; }

    private ClrRuntimeHelper ClrRuntimeHelper { get; set; }

    private NativeDebugInterfaces NativeDebugInterfaces { get; set; }

    private ClrRuntime Runtime { get; set; }

    private RuntimeBreakpoint RuntimeBreakpoint { get; set; }

    private XClrDataProcessWrapper XClrProcess { get; set; }

    #endregion

    #region Public Methods and Operators

    public void AddBreakpointInfo(uint breakointId, XClrDataMethodInstanceWraper methodInstance)
    {
      this.BreakpoinedInstanceInfos[breakointId] = new ClrMethodInstanceOfflineInfo(methodInstance);
    }

    public void HandleBreakpoint(NativeBreakpointTuple breakpoint)
    {
      Stopwatch sw = Stopwatch.StartNew();
      try
      {
        Tracer.PrivateOutput("Breakpoint handling beginning +++++++++++++++++++++++++++++++++++++++");

        ClrMethodInstanceOfflineInfo methodInstanceInfo;
        if (!this.BreakpoinedInstanceInfos.TryGetValue(breakpoint.BreakpointId, out methodInstanceInfo))
        {
          Tracer.ReportUnexpectedState("Unable to find method instance in runtime breakpoint using breakpoint ID.");
          return;
        }

        methodInstanceInfo.HitCount++;
        if ((this.RuntimeBreakpoint.Flags & MethodTracingFlag.COUNT_METHOD_HITS_ONLY) == MethodTracingFlag.COUNT_METHOD_HITS_ONLY)
        {
          if ((this.RuntimeBreakpoint.Flags & MethodTracingFlag.COUNT_METHOD_HITS_DISPLAY_HIT) == MethodTracingFlag.COUNT_METHOD_HITS_DISPLAY_HIT)
          {
            Tracer.PublicOutput("CALLED: " + methodInstanceInfo.NameWithArgs);
          }
          return;
        }

        int numberOfArgs = this.RuntimeBreakpoint.Definition.MethodArgs.Count;

        bool methodIsInstance = methodInstanceInfo.IsInstance;
        if (methodIsInstance)
        {
          numberOfArgs += 1;
        }

        List<AddressedArgumentValue> args = MethodArgsManager.GetRawMethodArgs(this.NativeDebugInterfaces, numberOfArgs);

        if (args == null)
        {
          Tracer.ReportUnexpectedState("Unable to fetch raw arguments from thread context.", true);
          return;
        }
        if (args.Count != numberOfArgs)
        {
          Tracer.ReportUnexpectedState("Number of fetched arguments doesn't correspond expected ({0} instead of {1}).".FormatWith(args.Count, numberOfArgs), true);
          return;
        }
        uint currentOsThreadId;
        if (this.NativeDebugInterfaces.DebugSystemObjects.GetCurrentThreadSystemId(out currentOsThreadId) != 0)
        {
          Tracer.ReportUnexpectedState("Unable to get system thread ID.", true);
          return;
        }

        if (this.XClrProcess.RawXClrDataProcess.Flush() != 0)
        {
          Tracer.ReportUnexpectedState("Unable to flush XClrDataProcess.", true);
          return;
        }

        XClrDataTaskWrapper dataTask = this.XClrProcess.GetDataTaskByOSThreadID(currentOsThreadId);
        if (dataTask == null)
        {
          Tracer.ReportUnexpectedState("Unable to resolve XClrDataTask for thread with ID {0}".FormatWith(currentOsThreadId));
          return;
        }

        var context = new ArgumentValueResolvingContext(
          this.NativeDebugInterfaces,
          this.XClrProcess,
          this.Runtime,
          this.ClrRuntimeHelper,
          dataTask,
          currentOsThreadId,
          this.RuntimeBreakpoint,
          methodInstanceInfo);

        var buffer = new StringBuilder();
        this.TraceMethodArguments(buffer, args.ToArray(), context);
        this.AppendStackTrace(buffer, context);
        buffer.Append(EndOfTraceDecoration);
        Tracer.PublicOutput(buffer.ToString());
      }
      finally
      {
        sw.Stop();
        Tracer.PrivateOutput("Breakpoint was handled. Operation took: {0} ms. +++++++++++++++++++++++++++++++++++++++".FormatWith(sw.ElapsedMilliseconds));
      }
    }

    public bool PrintHitStatistics(StringBuilder buffer)
    {
      bool isResultPresent = false;
      foreach (KeyValuePair<uint, ClrMethodInstanceOfflineInfo> breakpoinedInstanceInfo in this.BreakpoinedInstanceInfos)
      {
        buffer.AppendFormat(
          "{1} :: {2} hits.{0}",
          Environment.NewLine,
          breakpoinedInstanceInfo.Value.NameWithArgs,
          breakpoinedInstanceInfo.Value.HitCount.ToString(CultureInfo.InvariantCulture));
        isResultPresent = true;
      }

      return isResultPresent;
    }

    #endregion

    #region Methods

    internal static string ReadArgumentValue(
      MethodArg methodArg,
      AddressedArgumentValue rawValue,
      FullTypeName definitionArgumentTypeName,
      ArgumentValueResolvingContext context,
      ArgumentTracer tracer)
    {
      if (tracer == null)
      {
        Tracer.PrivateOutput("Unable to resolve tracer for '{0}' argument definition.".FormatWith(methodArg.GetType().Name));
        return "<UNABLE TO RESOLVE HOW VALUE SHOULD BE TRACED>";
      }

      string valueOrError;
      if (tracer.ReadArgumentValue(rawValue, definitionArgumentTypeName, context, out valueOrError))
      {
        return valueOrError;
      }
      else
      {
        return "<ERROR DURING FETCHING: " + valueOrError + ">";
      }
    }

    [MethodImpl(MethodImplOptions.NoInlining)]
    private void AppendStackTrace(StringBuilder buffer, ArgumentValueResolvingContext context)
    {
      //Check whether stack trace was disabled either locally or globally
      if ((this.RuntimeBreakpoint.Flags & MethodTracingFlag.NO_STACK_TRACE) == MethodTracingFlag.NO_STACK_TRACE
          || (GlobalSessionVars.CurrentFlags & SessionFlags.NO_STACK_TRACE) == SessionFlags.NO_STACK_TRACE)
      {
        return;
      }

      Stopwatch sw = Stopwatch.StartNew();
      try
      {
        const string unableToResolveErrorMessage = "<ERROR DURING STACK TRACE RESOLVE>";
        const string delimiter = "---------------------------------------";

        bool wasTracedAtLeastOne = false;

        //Critical mode :)
        var collectAllPossible = (context.RuntimeBreakpoint.Flags & MethodTracingFlag.USE_ALL_STACKWALKS) == MethodTracingFlag.USE_ALL_STACKWALKS;

        if ((context.RuntimeBreakpoint.Flags & MethodTracingFlag.USE_NATIVE_STACKWALK) == MethodTracingFlag.USE_NATIVE_STACKWALK || collectAllPossible)
        {
          wasTracedAtLeastOne = true;

          buffer.AppendLine();
          buffer.AppendLine("Native StackWalk");
          buffer.AppendLine(delimiter);
          buffer.Append(this.CollectStackTraceUsingNativeApi(context) ?? unableToResolveErrorMessage);
          buffer.AppendLine();
        }

        if ((context.RuntimeBreakpoint.Flags & MethodTracingFlag.USE_DAC_STACKWALK) == MethodTracingFlag.USE_DAC_STACKWALK || collectAllPossible)
        {
          wasTracedAtLeastOne = true;

          buffer.AppendLine();
          buffer.AppendLine("DAC StackWalk");
          buffer.AppendLine(delimiter);
          buffer.Append(this.CollectStackTraceUsingClrApi(context) ?? unableToResolveErrorMessage);
          buffer.AppendLine();
        }

        if ((context.RuntimeBreakpoint.Flags & MethodTracingFlag.USE_CLRMD_STACKWALK) == MethodTracingFlag.USE_CLRMD_STACKWALK || collectAllPossible)
        {
          wasTracedAtLeastOne = true;

          buffer.AppendLine();
          buffer.AppendLine("ClrMD StackWalk");
          buffer.AppendLine(delimiter);
          buffer.Append(this.CollectStackTraceUsingClrMDReflection(context) ?? unableToResolveErrorMessage);
          buffer.AppendLine();
        }

        if (wasTracedAtLeastOne)
        {
          return;
        }

        buffer.AppendLine();
        buffer.AppendLine("DAC StackWalk");
        buffer.AppendLine(delimiter);
        buffer.Append(this.CollectStackTraceUsingClrApi(context) ?? unableToResolveErrorMessage);
      }
      finally
      {
        sw.Stop();
        Tracer.PrivateOutput("Stack unwind took {0} ms.".FormatWith(sw.ElapsedMilliseconds));
      }
    }

    private string CollectStackTraceUsingClrApi(ArgumentValueResolvingContext context)
    {
      XClrDataStackWalkWrapper stackWalk = context.CurrentDataTask.CreateStackWalk(CLRDataSimpleFrameType.SIMPFRAME_ALL);
      //Sometimes it doesn't work due to caching.
      if (stackWalk == null)
      {
        Tracer.PrivateOutput("Unable to create stackWalk for thread with ID {0}".FormatWith(context.NativeOsThreadId));
        return null;
      }

      var stackTraceCollector = new StackTraceCollector(stackWalk);
      string stackTrace = stackTraceCollector.GetStackTrace();

      return stackTrace;
    }

    private string CollectStackTraceUsingClrMDReflection(ArgumentValueResolvingContext context)
    {
      context.FlushClrRuntime();
      IList<ClrStackFrame> frames = context.ClrRuntimeHelper.GetStackTrace(context.NativeOsThreadId);
      return frames.Select(frame => frame.ToString()).JoinToString(Environment.NewLine);
    }

    private string CollectStackTraceUsingNativeApi(ArgumentValueResolvingContext context)
    {
      var stackTraceCollector = new NativeStackTraceCollector(context);
      return stackTraceCollector.GetStackTrace();
    }

    private string GetArgumentValue(MethodArg methodArg, AddressedArgumentValue rawValue, FullTypeName definitionArgumentTypeName, ArgumentValueResolvingContext context)
    {
      try
      {
        if (methodArg.IsPlaceholder)
        {
          return "<UNKNOWN>";
        }

        ArgumentTracer tracer = this.ResolveArgumentTracer(methodArg, context);
        return ReadArgumentValue(methodArg, rawValue, definitionArgumentTypeName, context, tracer);
      }
      catch (Exception ex)
      {
        Log.Error("Unable to read argument value.", ex, this);
        return "<FAILURE DURING VALUE RESOLVE>";
      }
    }

    private string GetThisArgumentType(AddressedArgumentValue rawValue, ArgumentValueResolvingContext context)
    {
      if (rawValue.Value == 0ul)
      {
        return "<this IS NULL>";
      }
      ClrType clrType = context.ClrRuntime.GetHeap().GetObjectType(rawValue.Value);

      //Try to re-resolve with flushed runtime.
      if (clrType == null && context.FlushClrRuntime())
      {
        clrType = context.ClrRuntime.GetHeap().GetObjectType(rawValue.Value);
      }

      if (clrType == null)
      {
        Tracer.PrivateOutput("Unable to resolve ClrType for object/struct with address '{0}'".FormatWith(rawValue.Value.ToHexString()));
        return "<UNABLE TO RESOLVE>";
      }

      return clrType.Name;
    }

    private ArgumentTracer ResolveArgumentTracer(MethodArg argumentDefinition, ArgumentValueResolvingContext context)
    {
      //Each breakpoint could have a few method instances, therefore we should gracefully handle that
      var key = new MethodArgMethodInstancePair(argumentDefinition, context.MethodInstanceOfflineInfo);

      object tracerFromCache;
      if (this.ArgumentTracersCache.TryGetValue(key, out tracerFromCache))
      {
        return tracerFromCache as ArgumentTracer;
      }

      ArgumentTracer result = null;
      foreach (ArgumentTracer knownPrototype in KnownTracerPrototypes)
      {
        if (knownPrototype.HandlesMethodArgumentDefinition(argumentDefinition))
        {
          result = knownPrototype.Clone();
          result.Initialize(argumentDefinition, this.ResolveArgumentTracer);
          break;
        }
      }

      //Put dummy object if tracer was not resolved. That is to cache failed attempt to resolve a handler.
      this.ArgumentTracersCache.Add(key, (object)result ?? argumentDefinition);
      return result;
    }

    private void TraceMethodArguments(StringBuilder buffer, AddressedArgumentValue[] rawArgumentValues, ArgumentValueResolvingContext context)
    {
      FullTypeName[] nativeMethodArguments = context.MethodInstanceOfflineInfo.Arguments.ToArray();
      MethodArg[] argumentDefinitions = this.RuntimeBreakpoint.Definition.MethodArgs.ToArray();
      buffer.AppendFormat("[{0}] ", context.NativeOsThreadId);
      buffer.AppendLine("Method has been called. Details:{0}MethodName: {1}{0}--".FormatWith(Environment.NewLine, context.MethodInstanceOfflineInfo.NameWithArgs));
      //For Instance methods we should skip first argument later, because it is "this".
      bool isInstanceMethod = context.MethodInstanceOfflineInfo.IsInstance;
      int rawArgumentIndexCorrection = isInstanceMethod ? 1 : 0;
      //Trace THIS value if need.
      if (this.RuntimeBreakpoint.Definition.ThisArg != null)
      {
        string thisArgumentValue = this.GetArgumentValue(
          this.RuntimeBreakpoint.Definition.ThisArg,
          rawArgumentValues[0],
          new FullTypeName(context.MethodInstanceOfflineInfo.ClrTypeName),
          context);
        if (thisArgumentValue == null)
        {
          thisArgumentValue = UnableToFetchFallback;
        }

        //Trace type of 'this' if method is marked as virtual
        if (isInstanceMethod && this.RuntimeBreakpoint.TraceThisType)
        {
          buffer.AppendLine("[this {0}]:`{1}`".FormatWith(this.GetThisArgumentType(rawArgumentValues[0], context), thisArgumentValue));
        }
        else
        {
          buffer.AppendLine("[this]:`{0}`".FormatWith(thisArgumentValue));
        }
      }

      else if (isInstanceMethod && this.RuntimeBreakpoint.TraceThisType)
      {
        buffer.AppendLine("[this {0}]".FormatWith(this.GetThisArgumentType(rawArgumentValues[0], context)));
      }

      //Trace all arguments.
      for (int i = 0; i < argumentDefinitions.Length; i++)
      {
        AddressedArgumentValue rawArgumentValue = rawArgumentValues[i + rawArgumentIndexCorrection];
        MethodArg currentArgDef = argumentDefinitions[i];
        FullTypeName actualMethodArgType = nativeMethodArguments[i];
        string argumentValue = this.GetArgumentValue(currentArgDef, rawArgumentValue, actualMethodArgType, context);
        if (argumentValue == null)
        {
          argumentValue = UnableToFetchFallback;
        }

        if (currentArgDef.FriendlyAlias == null)
        {
          buffer.AppendLine("arg{0}:`{1}`".FormatWith(i + 1, argumentValue));
        }
        else
        {
          buffer.AppendLine("{0}:`{1}`".FormatWith(currentArgDef.FriendlyAlias, argumentValue));
        }
      }
    }

    #endregion

    private struct MethodArgMethodInstancePair
    {
      #region Fields

      public readonly MethodArg MethodArg;

      public readonly ClrMethodInstanceOfflineInfo MethodOfflineInfo;

      #endregion

      #region Constructors and Destructors

      public MethodArgMethodInstancePair([NotNull] MethodArg methodArg, [NotNull] ClrMethodInstanceOfflineInfo methodOfflineInfo)
      {
        Assert.ArgumentNotNull(methodArg, "methodArg");
        Assert.ArgumentNotNull(methodOfflineInfo, "methodOfflineInfo");
        this.MethodArg = methodArg;
        this.MethodOfflineInfo = methodOfflineInfo;
      }

      #endregion

      #region Public Methods and Operators

      public bool Equals(MethodArgMethodInstancePair other)
      {
        return this.MethodArg.Equals(other.MethodArg) && this.MethodOfflineInfo.Equals(other.MethodOfflineInfo);
      }

      public override bool Equals(object obj)
      {
        if (ReferenceEquals(null, obj))
        {
          return false;
        }
        return obj is MethodArgMethodInstancePair && this.Equals((MethodArgMethodInstancePair)obj);
      }

      public override int GetHashCode()
      {
        unchecked
        {
          return (this.MethodArg.GetHashCode() * 397) ^ this.MethodOfflineInfo.GetHashCode();
        }
      }

      #endregion
    }
  }
}