﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ManagedArgsTracer.Breakpoints.Definition;
using ManagedArgsTracer.Diagnostics;
using ManagedArgsTracer.Helpers;
using ManagedArgsTracer.Session;

using Microsoft.Diagnostics.Runtime.Interop;

#endregion

namespace ManagedArgsTracer.Breakpoints.Tracing
{
  internal class NativeStackTraceCollector
  {
    #region Constructors and Destructors

    public NativeStackTraceCollector(ArgumentValueResolvingContext context)
    {
      this.DebugInterfaces = context.DebugInterfaces;
      this.TracingContext = context;
      this.TraceNativeFrames = (context.RuntimeBreakpoint.Flags & MethodTracingFlag.TRACE_NATIVE_FRAMES) == MethodTracingFlag.TRACE_NATIVE_FRAMES;
      this.TraceAddresses = (context.RuntimeBreakpoint.Flags & MethodTracingFlag.TRACE_FRAMES_WITH_ADDRESSES) == MethodTracingFlag.TRACE_FRAMES_WITH_ADDRESSES;
    }

    #endregion

    #region Properties

    private NativeDebugInterfaces DebugInterfaces { get; set; }

    private bool TraceAddresses { get; set; }

    private bool TraceNativeFrames { get; set; }

    private ArgumentValueResolvingContext TracingContext { get; set; }

    #endregion

    #region Public Methods and Operators

    public string GetStackTrace()
    {
      var result = new StringBuilder();

      DEBUG_STACK_FRAME[] framesBuffer = new DEBUG_STACK_FRAME[1000];
      uint framesFilled;
      int retCode;
      if ((retCode = this.DebugInterfaces.DebugControl.GetStackTrace(0, 0, 0, framesBuffer, framesBuffer.Length, out framesFilled)) != 0)
      {
        Tracer.PrivateOutput("Unable to get StackTrace from DebugControl. Ret code: " + retCode);
        return null;
      }

      if (framesFilled == 0u)
      {
        Tracer.PrivateOutput("Number of returned frames from DebugControl.GetStackTrace is zero.");
        return null;
      }

      bool lastWasUnmanaged = false;
      for (int i = 0; i < framesFilled; i++)
      {
        DEBUG_STACK_FRAME frame = framesBuffer[i];

        string name = this.ResolveNameForAddress(i, frame.InstructionOffset);

        //The only case when name is empty - managed only resolving and frame belongs to unmanaged code.
        if (name.IsNullOrEmpty())
        {
          if (lastWasUnmanaged)
          {
            continue;
          }
          lastWasUnmanaged = true;
          name = "[Unmanaged]";
        }
        else
        {
          lastWasUnmanaged = false;
        }

        if (this.TraceAddresses)
        {
          result.AppendLine("[{0}] {1}".FormatWith(frame.InstructionOffset.ToHexString(), name ?? "<UNKNOWN>"));
        }
        else
        {
          result.AppendLine(name ?? "<UNKNOWN>");
        }
      }

      return result.ToString();
    }

    #endregion

    #region Methods

    private string ResolveNameForAddress(int frameIndex, ulong instructionOffset)
    {
      string methodName = this.TracingContext.XClrProcess.ResolveMethodNameByAddress(instructionOffset);

      // You can ask why I do that.
      // I've noted that sometimes method could not be retrieved for the first frame. Added that hack to cover that.
      if (methodName.IsNullOrEmpty() && frameIndex == 0)
      {
        return this.TracingContext.MethodInstanceOfflineInfo.NameWithArgs;
      }

      if (methodName.NotNullNotEmpty() || !this.TraceNativeFrames)
      {
        return methodName;
      }

      ulong displacement;
      methodName = ClrInteropHelper.GetName(
        (uint len, out uint realLen, StringBuilder builder) =>
        this.DebugInterfaces.DebugSymbols3.GetNameByOffset(instructionOffset, builder, (int)len, out realLen, out displacement));

      return methodName.IsNullOrEmpty() ? "<UNRESOLVED>" : methodName;
    }

    #endregion
  }
}