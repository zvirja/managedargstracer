﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

using ManagedArgsTracer.Session;

#endregion

namespace ManagedArgsTracer.Breakpoints.Tracing
{
  internal abstract class MethodRawArgumentsFetcher
  {
    #region Public Methods and Operators

    public abstract List<AddressedArgumentValue> FetchArguments(NativeDebugInterfaces debugInterfaces, int numberOfArgs);

    #endregion

    #region Methods

    protected TContext? GetThreadContext<TContext>(NativeDebugInterfaces debugInterfaces) where TContext : struct
    {
      int sizeOfContext = Marshal.SizeOf(typeof(TContext));
      IntPtr ptrToBuffer = Marshal.AllocHGlobal(sizeOfContext);
      try
      {
        if (debugInterfaces.DebugAdvanced.GetThreadContext(ptrToBuffer, (uint)sizeOfContext) != 0)
        {
          return null;
        }
        TContext result = (TContext)Marshal.PtrToStructure(ptrToBuffer, typeof(TContext));
        return result;
      }
      finally
      {
        Marshal.FreeHGlobal(ptrToBuffer);
      }
    }

    #endregion
  }
}