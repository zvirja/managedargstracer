﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ManagedArgsTracer.Diagnostics;
using ManagedArgsTracer.Interop.Clr;
using ManagedArgsTracer.Runtime.Wrappers;

#endregion

namespace ManagedArgsTracer.Breakpoints.Tracing
{
  internal class StackTraceCollector
  {
    #region Constructors and Destructors

    public StackTraceCollector(XClrDataStackWalkWrapper stackWalker)
    {
      this.StackWalker = stackWalker;
    }

    #endregion

    #region Properties

    private XClrDataStackWalkWrapper StackWalker { get; set; }

    #endregion

    #region Public Methods and Operators

    /// <summary>
    /// Collects the full stack trace. If error, null is returned. All the errors are logged to the private tracer.
    /// </summary>
    public string GetStackTrace()
    {
      var result = new StringBuilder();

      do
      {
        XClrDataFrameWrapper currentFrame = this.StackWalker.GetCurrentFrame();

        XClrDataStackWalkWrapper.DataFrameType? typeOfCurrentFrame = currentFrame.GetCurrentFrameType();
        if (typeOfCurrentFrame == null)
        {
          Tracer.PrivateOutput("StackTracer. Unable to get type of frame.");
          return null;
        }

        XClrDataStackWalkWrapper.DataFrameType frameType = typeOfCurrentFrame.Value;
        if (frameType.SimpleType == CLRDataSimpleFrameType.CLRDATA_SIMPFRAME_MANAGED_METHOD || frameType.SimpleType == CLRDataSimpleFrameType.CLRDATA_SIMPFRAME_RUNTIME_MANAGED_CODE)
        {
          string codeName = currentFrame.GetCodeName();
          result.AppendLine(codeName ?? "<UNABLE TO GET NAME>");
        }
        else
        {
          result.AppendLine(frameType.SimpleType == CLRDataSimpleFrameType.CLRDATA_SIMPFRAME_RUNTIME_UNMANAGED_CODE ? "[Unmanaged code]" : "[Unrecognized]");
        }
      }
      while (this.StackWalker.Next());
      return result.ToString();
    }

    #endregion
  }
}