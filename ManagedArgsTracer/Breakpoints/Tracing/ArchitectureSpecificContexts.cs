﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

#endregion

namespace ManagedArgsTracer.Breakpoints.Tracing
{
  internal class ArchitectureSpecificContexts
  {
    [StructLayout(LayoutKind.Sequential)]
    internal struct M128A
    {
      public ulong Low;

      public long High;
    }

    /*
     * 
     * typedef struct DECLSPEC_ALIGN(16) _XSAVE_FORMAT {
    WORD   ControlWord;
    WORD   StatusWord;
    BYTE  TagWord;
    BYTE  Reserved1;
    WORD   ErrorOpcode;
    DWORD ErrorOffset;
    WORD   ErrorSelector;
    WORD   Reserved2;
    DWORD DataOffset;
    WORD   DataSelector;
    WORD   Reserved3;
    DWORD MxCsr;
    DWORD MxCsr_Mask;
    M128A FloatRegisters[8];

#if defined(_WIN64)

    M128A XmmRegisters[16];
    BYTE  Reserved4[96];

#else

    M128A XmmRegisters[8];
    BYTE  Reserved4[192];

    //
    // The fields below are not part of XSAVE/XRSTOR format.
    // They are written by the OS which is relying on a fact that
    // neither (FX)SAVE nor (F)XSTOR used this area.
    //

    DWORD   StackControl[7];    // KERNEL_STACK_CONTROL structure actualy
    DWORD   Cr0NpxState;

#endif

} XSAVE_FORMAT
     * 
     * 
     * 
     * */

    /*
     * 
     * 
    //
    // Register parameter home addresses.
    //
    // N.B. These fields are for convience - they could be used to extend the
    //      context record in the future.
    //

    DWORD64 P1Home;
    DWORD64 P2Home;
    DWORD64 P3Home;
    DWORD64 P4Home;
    DWORD64 P5Home;
    DWORD64 P6Home;

    //
    // Control flags.
    //

    DWORD ContextFlags;
    DWORD MxCsr;

    //
    // Segment Registers and processor flags.
    //

    WORD   SegCs;
    WORD   SegDs;
    WORD   SegEs;
    WORD   SegFs;
    WORD   SegGs;
    WORD   SegSs;
    DWORD EFlags;

    //
    // Debug registers
    //

    DWORD64 Dr0;
    DWORD64 Dr1;
    DWORD64 Dr2;
    DWORD64 Dr3;
    DWORD64 Dr6;
    DWORD64 Dr7;

    //
    // Integer registers.
    //

    DWORD64 Rax;
    DWORD64 Rcx;
    DWORD64 Rdx;
    DWORD64 Rbx;
    DWORD64 Rsp;
    DWORD64 Rbp;
    DWORD64 Rsi;
    DWORD64 Rdi;
    DWORD64 R8;
    DWORD64 R9;
    DWORD64 R10;
    DWORD64 R11;
    DWORD64 R12;
    DWORD64 R13;
    DWORD64 R14;
    DWORD64 R15;

    //
    // Program counter.
    //

    DWORD64 Rip;

    //
    // Floating point state.
    //

    union {
        XMM_SAVE_AREA32 FltSave;
        struct {
            M128A Header[2];
            M128A Legacy[8];
            M128A Xmm0;
            M128A Xmm1;
            M128A Xmm2;
            M128A Xmm3;
            M128A Xmm4;
            M128A Xmm5;
            M128A Xmm6;
            M128A Xmm7;
            M128A Xmm8;
            M128A Xmm9;
            M128A Xmm10;
            M128A Xmm11;
            M128A Xmm12;
            M128A Xmm13;
            M128A Xmm14;
            M128A Xmm15;
        } DUMMYSTRUCTNAME;
    } DUMMYUNIONNAME;

    //
    // Vector registers.
    //

    M128A VectorRegister[26];
    DWORD64 VectorControl;

    //
    // Special debug control registers.
    //

    DWORD64 DebugControl;
    DWORD64 LastBranchToRip;
    DWORD64 LastBranchFromRip;
    DWORD64 LastExceptionToRip;
    DWORD64 LastExceptionFromRip;
}
     *     * 
     * */

    [StructLayout(LayoutKind.Sequential)]
    internal struct X64THREADCONTEXT
    {
      //
      // Register parameter home addresses.
      //
      // N.B. These fields are for convience - they could be used to extend the
      //      context record in the future.
      //

      public ulong P1Home;

      public ulong P2Home;

      public ulong P3Home;

      public ulong P4Home;

      public ulong P5Home;

      public ulong P6Home;

      //
      // Control flags.
      //

      public uint ContextFlags;

      public uint MxCsr;

      //
      // Segment Registers and processor flags.
      //

      public ushort SegCs;

      public ushort SegDs;

      public ushort SegEs;

      public ushort SegFs;

      public ushort SegGs;

      public ushort SegSs;

      public uint EFlags;

      //
      // Debug registers
      //

      public ulong Dr0;

      public ulong Dr1;

      public ulong Dr2;

      public ulong Dr3;

      public ulong Dr6;

      public ulong Dr7;

      //
      // Integer registers.
      //

      public ulong Rax;

      public ulong Rcx;

      public ulong Rdx;

      public ulong Rbx;

      public ulong Rsp;

      public ulong Rbp;

      public ulong Rsi;

      public ulong Rdi;

      public ulong R8;

      public ulong R9;

      public ulong R10;

      public ulong R11;

      public ulong R12;

      public ulong R13;

      public ulong R14;

      public ulong R15;

      //
      // Program counter.
      //

      public ulong Rip;

      //
      // Floating point state.
      //

      public XMM_SAVE_AREA32 XMMRegisters;

      // Vector registers.
      //
      [MarshalAs(UnmanagedType.ByValArray, SizeConst = 26)]
      public M128A[] VectorRegister;

      public ulong VectorControl;

      //
      // Special debug control registers.
      //

      public ulong DebugControl;

      public ulong LastBranchToRip;

      public ulong LastBranchFromRip;

      public ulong LastExceptionToRip;

      public ulong LastExceptionFromRip;
    }

    [StructLayout(LayoutKind.Sequential)]
    internal struct X86FLOATING_SAVE_AREA
    {
      public uint ControlWord;

      public uint StatusWord;

      public uint TagWord;

      public uint ErrorOffset;

      public uint ErrorSelector;

      public uint DataOffset;

      public uint DataSelector;

      [MarshalAs(UnmanagedType.ByValArray, SizeConst = 80)]
      public byte[] RegisterArea;

      public uint Cr0NpxState;
    }

    [StructLayout(LayoutKind.Sequential)]
    internal struct X86THREADCONTEXT
    {
      public uint ContextFlags; //set this to an appropriate value 

      // Retrieved by CONTEXT_DEBUG_REGISTERS 
      public uint Dr0;

      public uint Dr1;

      public uint Dr2;

      public uint Dr3;

      public uint Dr6;

      public uint Dr7;

      // Retrieved by CONTEXT_FLOATING_POINT 
      public X86FLOATING_SAVE_AREA FloatSave;

      // Retrieved by CONTEXT_SEGMENTS 
      public uint SegGs;

      public uint SegFs;

      public uint SegEs;

      public uint SegDs;

      // Retrieved by CONTEXT_INTEGER 
      public uint Edi;

      public uint Esi;

      public uint Ebx;

      public uint Edx;

      public uint Ecx;

      public uint Eax;

      // Retrieved by CONTEXT_CONTROL 
      public uint Ebp;

      public uint Eip;

      public uint SegCs;

      public uint EFlags;

      public uint Esp;

      public uint SegSs;

      // Retrieved by CONTEXT_EXTENDED_REGISTERS 
      [MarshalAs(UnmanagedType.ByValArray, SizeConst = 512)]
      public byte[] ExtendedRegisters;
    }

    [StructLayout(LayoutKind.Sequential)]
    internal struct XMM_SAVE_AREA32
    {
      public ushort ControlWord;

      public ushort StatusWord;

      public byte TagWord;

      public byte Reserved1;

      public ushort ErrorOpcode;

      public uint ErrorOffset;

      public ushort ErrorSelector;

      public ushort Reserved2;

      public uint DataOffset;

      public ushort DataSelector;

      public ushort Reserved3;

      public uint MxCsr;

      public uint MxCsr_Mask;

      [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
      public M128A[] FloatRegisters;

      [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
      public M128A[] XmmRegisters;

      [MarshalAs(UnmanagedType.ByValArray, SizeConst = 96)]
      public byte[] Reserved4;
    }
  }
}