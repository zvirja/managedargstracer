﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ManagedArgsTracer.Session;

#endregion

namespace ManagedArgsTracer.Breakpoints.Tracing
{
  internal class X86MethodRawArgumentsFetcher : MethodRawArgumentsFetcher
  {
    #region Public Methods and Operators

    public override List<AddressedArgumentValue> FetchArguments(NativeDebugInterfaces debugInterfaces, int numberOfArgs)
    {
      ArchitectureSpecificContexts.X86THREADCONTEXT? threadContext = this.GetThreadContext<ArchitectureSpecificContexts.X86THREADCONTEXT>(debugInterfaces);
      if (!threadContext.HasValue)
      {
        return null;
      }

      var result = new List<AddressedArgumentValue>();
      if (numberOfArgs == 0)
      {
        return result;
      }

      //First arg in ECX
      result.Add(new AddressedArgumentValue(threadContext.Value.Ecx));
      numberOfArgs--;
      if (numberOfArgs == 0)
      {
        return result;
      }

      //Second arg in EDX
      result.Add(new AddressedArgumentValue(threadContext.Value.Edx));
      numberOfArgs--;
      if (numberOfArgs == 0)
      {
        return result;
      }

      //The rest part of arguments should be fetched from stack.
      ulong startLocation = threadContext.Value.Esp;
      //SKip 8 bytes for return address.
      startLocation += 0x4;
      //At this point startLocation points to the next argument.

      uint bytesToRead = 4U * (uint)numberOfArgs;

      var buffer = new byte[bytesToRead];
      uint bytesRead;
      if (debugInterfaces.DebugDataSpaces.ReadVirtual(startLocation, buffer, bytesToRead, out bytesRead) != 0 || bytesRead != bytesToRead)
      {
        return null;
      }

      //In x86 argumens are pushed from first to last, so we should read them in reverse order.
      for (int i = numberOfArgs - 1; i > -1; i--)
      {
        ulong nextArg = BitConverter.ToUInt32(buffer, i * 4);
        result.Add(new AddressedArgumentValue(nextArg, startLocation + (uint)i * 4u));
      }

      return result;
    }

    #endregion
  }
}