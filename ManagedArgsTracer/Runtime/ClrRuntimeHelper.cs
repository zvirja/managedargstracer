﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

using ManagedArgsTracer.Helpers;

using Microsoft.Diagnostics.Runtime;

#endregion

namespace ManagedArgsTracer.Runtime
{
  internal class ClrRuntimeHelper : IClrRuntimeHelper
  {
    #region Static Fields

    private static readonly BindingFlags DefaultRuntimeMethodFlags = BindingFlags.NonPublic | BindingFlags.Instance;

    #endregion

    #region Constructors and Destructors

    public ClrRuntimeHelper(ClrRuntime runtime)
    {
      this.Runtime = runtime;
      this.Revision = 0;
      this.ClrTypeCache = new ClrTypeCache(this);
      this.Runtime.RuntimeFlushed += this.RuntimeOnRuntimeFlushed;
    }

    #endregion

    #region Delegates

    internal delegate uint GetMetadataTokenDelegate(ulong mt);

    internal delegate object GetMethodDescDataDelegate(ulong md);

    internal delegate IList<ulong> GetMethodDescListDelegate(ulong methodTable);

    internal delegate IList<ulong> GetMethodTableListDelegate(ulong module);

    internal delegate ulong GetModuleForMTDelegate(ulong mt);

    internal delegate string GetNameForMTDelegate(ulong mt);

    internal delegate IList<ClrStackFrame> GetStackTraceDelegate(uint osThreadId);

    #endregion

    #region Public Properties

    public IClrTypeCache ClrTypeCache { get; set; }

    public uint Revision { get; set; }

    public ClrRuntime Runtime { get; set; }

    #endregion

    #region Properties

    private GetMetadataTokenDelegate GetMetadataTokenInvoker { get; set; }

    private GetMethodDescDataDelegate GetMethodDescDataInvoker { get; set; }

    private PropertyInfo GetMethodDescData_MDTokenProp { get; set; }

    private PropertyInfo GetMethodDescData_NativeCodeAddrProp { get; set; }

    private GetMethodDescListDelegate GetMethodDescListInvoker { get; set; }

    private GetMethodTableListDelegate GetMethodTableListInvoker { get; set; }

    private GetModuleForMTDelegate GetModuleForMTInvoker { get; set; }

    private GetNameForMTDelegate GetNameForMTInvoker { get; set; }

    private GetStackTraceDelegate GetStackTraceInvoker { get; set; }

    #endregion

    #region Public Methods and Operators

    public uint GetMetadataToken(ulong mt)
    {
      if (this.GetMetadataTokenInvoker == null)
      {
        this.GetMetadataTokenInvoker = ReflectionHelper.GetDelegateForCall<GetMetadataTokenDelegate>(this.Runtime, "GetMetadataToken", DefaultRuntimeMethodFlags);
      }
      return this.GetMetadataTokenInvoker(mt);
    }

    public Tuple<uint, ulong> GetMethodDescData(ulong md)
    {
      if (this.GetMethodDescDataInvoker == null)
      {
        this.GetMethodDescDataInvoker = ReflectionHelper.GetDelegateForCall<GetMethodDescDataDelegate>(this.Runtime, "GetMethodDescData", DefaultRuntimeMethodFlags);
      }

      object rawResultObj = this.GetMethodDescDataInvoker(md);

      if (this.GetMethodDescData_MDTokenProp == null)
      {
        this.GetMethodDescData_MDTokenProp = rawResultObj.GetType().GetProperty("MDToken");
        this.GetMethodDescData_NativeCodeAddrProp = rawResultObj.GetType().GetProperty("NativeCodeAddr");
      }

      var mdToken = (uint)this.GetMethodDescData_MDTokenProp.GetValue(rawResultObj, null);
      var codeAddr = (ulong)this.GetMethodDescData_NativeCodeAddrProp.GetValue(rawResultObj, null);

      return new Tuple<uint, ulong>(mdToken, codeAddr);
    }

    public IList<ulong> GetMethodDescList(ulong methodTable)
    {
      if (this.GetMethodDescListInvoker == null)
      {
        this.GetMethodDescListInvoker = ReflectionHelper.GetDelegateForCall<GetMethodDescListDelegate>(this.Runtime, "GetMethodDescList", DefaultRuntimeMethodFlags);
      }
      return this.GetMethodDescListInvoker(methodTable);
    }

    public IList<ulong> GetMethodTableList(ulong module)
    {
      if (this.GetMethodTableListInvoker == null)
      {
        this.GetMethodTableListInvoker = ReflectionHelper.GetDelegateForCall<GetMethodTableListDelegate>(this.Runtime, "GetMethodTableList", DefaultRuntimeMethodFlags);
      }
      return this.GetMethodTableListInvoker(module);
    }

    public ulong GetModuleForMT(ulong methodTable)
    {
      if (this.GetModuleForMTInvoker == null)
      {
        this.GetModuleForMTInvoker = ReflectionHelper.GetDelegateForCall<GetModuleForMTDelegate>(this.Runtime, "GetModuleForMT", DefaultRuntimeMethodFlags);
      }
      return this.GetModuleForMTInvoker(methodTable);
    }

    public string GetNameForMT(ulong mt)
    {
      if (this.GetNameForMTInvoker == null)
      {
        this.GetNameForMTInvoker = ReflectionHelper.GetDelegateForCall<GetNameForMTDelegate>(this.Runtime, "GetNameForMT", DefaultRuntimeMethodFlags);
      }
      return this.GetNameForMTInvoker(mt);
    }

    public IList<ClrStackFrame> GetStackTrace(uint osThreadId)
    {
      if (this.GetStackTraceInvoker == null)
      {
        this.GetStackTraceInvoker = ReflectionHelper.GetDelegateForCall<GetStackTraceDelegate>(this.Runtime, "GetStackTrace", DefaultRuntimeMethodFlags);
      }
      return this.GetStackTraceInvoker(osThreadId);
    }

    #endregion

    #region Methods

    private void RuntimeOnRuntimeFlushed(ClrRuntime runtime)
    {
      this.Revision++;
    }

    #endregion
  }
}