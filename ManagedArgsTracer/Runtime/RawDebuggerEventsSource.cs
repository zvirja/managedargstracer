﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ManagedArgsTracer.Diagnostics;
using ManagedArgsTracer.Helpers;
using ManagedArgsTracer.Interop.Clr;
using Microsoft.Diagnostics.Runtime.Interop;

#endregion

namespace ManagedArgsTracer.Runtime
{
  internal class RawDebuggerEventsSource : IDebugEventCallbacks
  {
    #region Delegates

    public delegate void BreakpointHitDelegate(object sender, IDebugBreakpoint breakpoint);

    public delegate void ExceptionHitDelegate(object sender, ref EXCEPTION_RECORD64 Exception, uint FirstChance);

    public delegate void ExitProcessDelegate(object sender, uint exitCode);

    public delegate void SessionStatusChangeDelegate(object sender, DEBUG_SESSION status);

    #endregion

    #region Public Events

    public event BreakpointHitDelegate BreakpointHit;

    public event ExceptionHitDelegate ExceptionHit;

    public event ExitProcessDelegate ExitProcessHit;

    public event SessionStatusChangeDelegate SessionStatusChanged;

    #endregion

    #region Public Methods and Operators

    public int Breakpoint(IDebugBreakpoint breakpoint)
    {
      try
      {
        this.OnBreakpointHit(this, breakpoint);
      }
      catch (Exception ex)
      {
        Log.Error("Unable to handle breakpoint", ex, this);
        Tracer.ReportUnexpectedState();
      }
      return (int)DEBUG_STATUS.GO_HANDLED;
    }

    public int ChangeDebuggeeState(DEBUG_CDS Flags, ulong Argument)
    {
      this.NotifySuspicionMethodCall(System.Reflection.MethodBase.GetCurrentMethod().Name);
      return (int)DEBUG_STATUS.NO_CHANGE;
    }

    public int ChangeEngineState(DEBUG_CES Flags, ulong Argument)
    {
      this.NotifySuspicionMethodCall(System.Reflection.MethodBase.GetCurrentMethod().Name);
      return (int)DEBUG_STATUS.NO_CHANGE;
    }

    public int ChangeSymbolState(DEBUG_CSS Flags, ulong Argument)
    {
      this.NotifySuspicionMethodCall(System.Reflection.MethodBase.GetCurrentMethod().Name);
      return (int)DEBUG_STATUS.NO_CHANGE;
    }

    public int CreateProcess(
      ulong ImageFileHandle,
      ulong Handle,
      ulong BaseOffset,
      uint ModuleSize,
      string ModuleName,
      string ImageName,
      uint CheckSum,
      uint TimeDateStamp,
      ulong InitialThreadHandle,
      ulong ThreadDataOffset,
      ulong StartOffset)
    {
      this.NotifySuspicionMethodCall(System.Reflection.MethodBase.GetCurrentMethod().Name);
      return (int)DEBUG_STATUS.NO_CHANGE;
    }

    public int CreateThread(ulong Handle, ulong DataOffset, ulong StartOffset)
    {
      this.NotifySuspicionMethodCall(System.Reflection.MethodBase.GetCurrentMethod().Name);
      return (int)DEBUG_STATUS.NO_CHANGE;
    }

    public int Exception(ref EXCEPTION_RECORD64 exception, uint firstChance)
    {
      if (exception.ExceptionCode == ClrExceptionNotificationTranslator.CLRDATA_NOTIFY_EXCEPTION)
      {
        try
        {
          this.OnExceptionHit(this, ref exception, firstChance);
        }
        catch (Exception ex)
        {
          Log.Error("Error during processing exception notification.", ex, this);
          Tracer.ReportUnexpectedState();
        }
      }
      return (int)DEBUG_STATUS.NO_CHANGE;
    }

    public int ExitProcess(uint exitCode)
    {
      try
      {
        this.OnExitProcessHit(this, exitCode);
      }
      catch (Exception ex)
      {
        Log.Error("Error during processing exit notification.", ex, this);
        Tracer.ReportUnexpectedState();
      }

      return (int)DEBUG_STATUS.NO_CHANGE;
    }

    public int ExitThread(uint ExitCode)
    {
      this.NotifySuspicionMethodCall(System.Reflection.MethodBase.GetCurrentMethod().Name);
      return (int)DEBUG_STATUS.NO_CHANGE;
    }

    public int GetInterestMask(out DEBUG_EVENT Mask)
    {
      Mask = DEBUG_EVENT.BREAKPOINT | DEBUG_EVENT.SESSION_STATUS | DEBUG_EVENT.EXCEPTION | DEBUG_EVENT.EXIT_PROCESS;
      return 0;
    }

    public int LoadModule(ulong ImageFileHandle, ulong BaseOffset, uint ModuleSize, string ModuleName, string ImageName, uint CheckSum, uint TimeDateStamp)
    {
      this.NotifySuspicionMethodCall(System.Reflection.MethodBase.GetCurrentMethod().Name);
      return (int)DEBUG_STATUS.NO_CHANGE;
    }

    public int SessionStatus(DEBUG_SESSION status)
    {
      try
      {
        this.OnSessionStatusChanged(this, status);
      }
      catch (Exception ex)
      {
        Log.Error("Unable to handle session status change.", ex, this);
        Tracer.ReportUnexpectedState();
      }
      return (int)DEBUG_STATUS.NO_CHANGE;
    }

    public int SystemError(uint Error, uint Level)
    {
      this.NotifySuspicionMethodCall(System.Reflection.MethodBase.GetCurrentMethod().Name);
      return (int)DEBUG_STATUS.NO_CHANGE;
    }

    public int UnloadModule(string ImageBaseName, ulong BaseOffset)
    {
      this.NotifySuspicionMethodCall(System.Reflection.MethodBase.GetCurrentMethod().Name);
      return (int)DEBUG_STATUS.NO_CHANGE;
    }

    #endregion

    #region Methods

    protected virtual void OnBreakpointHit(object source, IDebugBreakpoint breakpoint)
    {
      BreakpointHitDelegate handler = this.BreakpointHit;
      if (handler != null)
      {
        handler(source, breakpoint);
      }
    }

    protected virtual void OnExceptionHit(object source, ref EXCEPTION_RECORD64 exception, uint firstchance)
    {
      ExceptionHitDelegate handler = this.ExceptionHit;
      if (handler != null)
      {
        handler(source, ref exception, firstchance);
      }
    }

    protected virtual void OnExitProcessHit(object source, uint exitcode)
    {
      ExitProcessDelegate handler = this.ExitProcessHit;
      if (handler != null)
      {
        handler(source, exitcode);
      }
    }

    protected virtual void OnSessionStatusChanged(object source, DEBUG_SESSION status)
    {
      SessionStatusChangeDelegate handler = this.SessionStatusChanged;
      if (handler != null)
      {
        handler(source, status);
      }
    }

    private void NotifySuspicionMethodCall(string methodName)
    {
      Tracer.PrivateOutput("Suspicion call of the {0} method (DebuggerEventCallback). It isn't expected that this method is called.".FormatWith(methodName));
    }

    #endregion
  }
}