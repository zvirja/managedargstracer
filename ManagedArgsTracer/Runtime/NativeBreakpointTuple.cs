﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Diagnostics.Runtime.Interop;

#endregion

namespace ManagedArgsTracer.Runtime
{
  internal struct NativeBreakpointTuple
  {
    #region Fields

    public IDebugBreakpoint Breakpoint;

    public uint BreakpointId;

    #endregion

    #region Constructors and Destructors

    public NativeBreakpointTuple(IDebugBreakpoint breakpoint, uint breakpointId)
    {
      this.Breakpoint = breakpoint;
      this.BreakpointId = breakpointId;
    }

    #endregion
  }
}