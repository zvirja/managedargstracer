﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#endregion

namespace ManagedArgsTracer.Runtime
{
  public interface ICleverFlusher
  {
    #region Public Methods and Operators

    bool FlushClrRuntime();

    #endregion
  }
}