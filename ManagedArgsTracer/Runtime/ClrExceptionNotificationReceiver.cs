﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ManagedArgsTracer.Diagnostics;
using ManagedArgsTracer.Interop.Clr;

#endregion

namespace ManagedArgsTracer.Runtime
{
  internal class ClrExceptionNotificationReceiver : IXCLRDataExceptionNotification
  {
    #region Delegates

    public delegate void CodeGenerationDelegate(object sender, IXCLRDataMethodInstance methodInstance);

    public delegate void ModuleLoadingDelegate(object sender, IXCLRDataModule module);

    #endregion

    #region Public Events

    public event CodeGenerationDelegate CodeDiscarded;

    public event CodeGenerationDelegate CodeGenerated;

    public event ModuleLoadingDelegate ModuleLoaded;

    public event ModuleLoadingDelegate ModuleUnloaded;

    #endregion

    #region Public Methods and Operators

    public int OnCodeDiscarded(IXCLRDataMethodInstance method)
    {
      try
      {
        this.OnCodeDiscardedRaise(method);
      }
      catch (Exception ex)
      {
        Log.Error("Unable to notify about discarded code.", ex, this);
        Tracer.ReportUnexpectedState();
      }
      return 0;
    }

    public int OnCodeGenerated(IXCLRDataMethodInstance method)
    {
      try
      {
        this.OnCodeGeneratedRaise(method);
      }
      catch (Exception ex)
      {
        Log.Error("Unable to notify about code compilation.", ex, this);
        Tracer.ReportUnexpectedState();
      }
      return 0;
    }

    public int OnModuleLoaded(IXCLRDataModule module)
    {
      try
      {
        this.OnModuleLoadedRaise(module);
      }
      catch (Exception ex)
      {
        Log.Error("Unable to notify about loaded module.", ex, this);
        Tracer.ReportUnexpectedState();
      }

      return 0;
    }

    public int OnModuleUnloaded(IXCLRDataModule module)
    {
      try
      {
        this.OnModuleUnloadedRaise(module);
      }
      catch (Exception ex)
      {
        Log.Error("Unable to notify about unloaded module.", ex, this);
        Tracer.ReportUnexpectedState();
      }

      return 0;
    }

    public int OnProcessExecution(uint state)
    {
      //Ignore
      return 0;
    }

    public int OnTaskExecution(IXCLRDataTask task, uint state)
    {
      //Ignore
      return 0;
    }

    public int OnTypeLoaded(IXCLRDataTypeInstance typeInstance)
    {
      //No sense to implement, is never called.
      return 0;
    }

    public int OnTypeUnloaded(IXCLRDataTypeInstance typeInstance)
    {
      //No sense to implement, is never called.
      return 0;
    }

    #endregion

    #region Methods

    protected virtual void OnCodeDiscardedRaise(IXCLRDataMethodInstance methodinstance)
    {
      CodeGenerationDelegate handler = this.CodeDiscarded;
      if (handler != null)
      {
        handler(this, methodinstance);
      }
    }

    protected virtual void OnCodeGeneratedRaise(IXCLRDataMethodInstance methodinstance)
    {
      CodeGenerationDelegate handler = this.CodeGenerated;
      if (handler != null)
      {
        handler(this, methodinstance);
      }
    }

    protected virtual void OnModuleLoadedRaise(IXCLRDataModule methodinstance)
    {
      ModuleLoadingDelegate handler = this.ModuleLoaded;
      if (handler != null)
      {
        handler(this, methodinstance);
      }
    }

    protected virtual void OnModuleUnloadedRaise(IXCLRDataModule methodinstance)
    {
      ModuleLoadingDelegate handler = this.ModuleUnloaded;
      if (handler != null)
      {
        handler(this, methodinstance);
      }
    }

    #endregion
  }
}