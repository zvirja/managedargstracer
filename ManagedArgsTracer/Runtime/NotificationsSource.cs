﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using JetBrains.Annotations;

using ManagedArgsTracer.Diagnostics;
using ManagedArgsTracer.Interop.Clr;
using ManagedArgsTracer.Runtime.Wrappers;

using Microsoft.Diagnostics.Runtime.Interop;

#endregion

namespace ManagedArgsTracer.Runtime
{
  internal delegate void ModuleLoadingDelegate(object sender, XClrDataModuleWrapper moduleWrapper);

  internal delegate void MethodJittingDelegate(object sender, XClrDataMethodInstanceWraper methodInstance);

  internal delegate void BreakpointDelegate(object sender, NativeBreakpointTuple breakpoint);

  internal class NotificationsSource
  {
    #region Constructors and Destructors

    public NotificationsSource([NotNull] RawDebuggerEventsSource debuggerEventsSource, [NotNull] ClrExceptionNotificationTranslator clrExceptionNotificationTranslator)
    {
      Assert.ArgumentNotNull(debuggerEventsSource, "debuggerEventsSource");
      Assert.ArgumentNotNull(clrExceptionNotificationTranslator, "clrExceptionNotificationTranslator");
      this.DebuggerEventsSource = debuggerEventsSource;
      this.ClrExceptionNotificationTranslator = clrExceptionNotificationTranslator;
      this.ClrNotificationReceiver = new ClrExceptionNotificationReceiver();

      //Subscribe to clr events to pass them ahead.
      this.ClrNotificationReceiver.ModuleLoaded += (sender, module) => this.OnModuleLoaded(new XClrDataModuleWrapper(module));
      this.ClrNotificationReceiver.ModuleUnloaded += (sender, module) => this.OnModuleUnloaded(new XClrDataModuleWrapper(module));
      this.ClrNotificationReceiver.CodeGenerated += (sender, instance) => this.OnMethodJitted(new XClrDataMethodInstanceWraper(instance));
      this.ClrNotificationReceiver.CodeDiscarded += (sender, instance) => this.OnMethodJitDiscarded(new XClrDataMethodInstanceWraper(instance));

      //Subscribe to native events.
      this.DebuggerEventsSource.ExceptionHit += this.DebuggerEventsSourceOnExceptionHit;
      this.DebuggerEventsSource.BreakpointHit += this.DebuggerEventsSourceOnBreakpointHit;
      this.DebuggerEventsSource.ExitProcessHit += (sender, code) => this.OnTargetProcessExit();
      /*this.DebuggerEventsSource.SessionStatusChanged += this.DebuggerEventsSourceOnSessionStatusChanged;*/
    }

    #endregion

    #region Public Events

    public event BreakpointDelegate BreakpointHit;

    public event MethodJittingDelegate MethodJitDiscarded;

    public event MethodJittingDelegate MethodJitted;

    public event ModuleLoadingDelegate ModuleLoaded;

    public event ModuleLoadingDelegate ModuleUnloaded;

    public event EventHandler TargetProcessExit;

    #endregion

    #region Properties

    private ClrExceptionNotificationTranslator ClrExceptionNotificationTranslator { get; set; }

    private ClrExceptionNotificationReceiver ClrNotificationReceiver { get; set; }

    private RawDebuggerEventsSource DebuggerEventsSource { get; set; }

    #endregion

    #region Public Methods and Operators

    public void DisableJitNotifications(XClrDataMethodDefinitionWraper methodDefinition)
    {
      methodDefinition.CodeNotificationFlag = CLRDATA_METHNOTIFY.NONE;
    }

    public void DisableModuleLoadNotifications(XClrDataProcessWrapper dataProcess)
    {
      dataProcess.OtherNotificationFlag &= ~DataOtherNotifyFlag.CLRDATA_NOTIFY_ON_MODULE_LOAD;
    }

    public void EnableJitNotifications(XClrDataMethodDefinitionWraper methodDefinition)
    {
      methodDefinition.CodeNotificationFlag = CLRDATA_METHNOTIFY.GENERATED;
    }

    public void EnableModuleLoadNotifications(XClrDataProcessWrapper dataProcess)
    {
      dataProcess.OtherNotificationFlag |= DataOtherNotifyFlag.CLRDATA_NOTIFY_ON_MODULE_LOAD;
    }

    #endregion

    #region Methods

    protected virtual void OnBreakpointHit(NativeBreakpointTuple breakpoint)
    {
      BreakpointDelegate handler = this.BreakpointHit;
      if (handler != null)
      {
        handler(this, breakpoint);
      }
    }

    protected virtual void OnMethodJitDiscarded(XClrDataMethodInstanceWraper methodinstance)
    {
      MethodJittingDelegate handler = this.MethodJitDiscarded;
      if (handler != null)
      {
        handler(this, methodinstance);
      }
    }

    protected virtual void OnMethodJitted(XClrDataMethodInstanceWraper methodinstance)
    {
      MethodJittingDelegate handler = this.MethodJitted;
      if (handler != null)
      {
        handler(this, methodinstance);
      }
    }

    protected virtual void OnModuleLoaded(XClrDataModuleWrapper modulewrapper)
    {
      ModuleLoadingDelegate handler = this.ModuleLoaded;
      if (handler != null)
      {
        handler(this, modulewrapper);
      }
    }

    protected virtual void OnModuleUnloaded(XClrDataModuleWrapper modulewrapper)
    {
      ModuleLoadingDelegate handler = this.ModuleUnloaded;
      if (handler != null)
      {
        handler(this, modulewrapper);
      }
    }

    protected virtual void OnTargetProcessExit()
    {
      EventHandler handler = this.TargetProcessExit;
      if (handler != null)
      {
        handler(this, EventArgs.Empty);
      }
    }

    private void DebuggerEventsSourceOnBreakpointHit(object sender, IDebugBreakpoint breakpoint)
    {
      uint breakpointId;
      if (breakpoint.GetId(out breakpointId) != 0)
      {
        Tracer.ReportUnexpectedState("Unable to resolve ID for debugger breakpoint", true);
        return;
      }
      var bpTuple = new NativeBreakpointTuple(breakpoint, breakpointId);
      this.OnBreakpointHit(bpTuple);
    }

    private void DebuggerEventsSourceOnExceptionHit(object sender, ref EXCEPTION_RECORD64 exception, uint firstChance)
    {
      this.ClrExceptionNotificationTranslator.TranslateExceptionNotification(ref exception, this.ClrNotificationReceiver);
    }

    #endregion

    /*    private void DebuggerEventsSourceOnSessionStatusChanged(object sender, DEBUG_SESSION status)
    {
      if (status != DEBUG_SESSION.ACTIVE || status != DEBUG_SESSION.HIBERNATE)
      {
        this.OnTargetProcessExit();
      }
    }*/
  }
}