﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#endregion

namespace ManagedArgsTracer.Runtime.DacRequest
{
  internal enum DacRequestCodes : uint
  {
    DACPRIV_REQUEST_THREAD_STORE_DATA = 0xf0000000,

    DACPRIV_REQUEST_APPDOMAIN_STORE_DATA,

    DACPRIV_REQUEST_APPDOMAIN_LIST,

    DACPRIV_REQUEST_APPDOMAIN_DATA,

    DACPRIV_REQUEST_APPDOMAIN_NAME,

    DACPRIV_REQUEST_APPDOMAIN_APP_BASE,

    DACPRIV_REQUEST_APPDOMAIN_PRIVATE_BIN_PATHS,

    DACPRIV_REQUEST_APPDOMAIN_CONFIG_FILE,

    DACPRIV_REQUEST_ASSEMBLY_LIST,

    DACPRIV_REQUEST_FAILED_ASSEMBLY_LIST,

    DACPRIV_REQUEST_ASSEMBLY_DATA,

    DACPRIV_REQUEST_ASSEMBLY_NAME,

    DACPRIV_REQUEST_ASSEMBLY_DISPLAY_NAME,

    DACPRIV_REQUEST_ASSEMBLY_LOCATION,

    DACPRIV_REQUEST_FAILED_ASSEMBLY_DATA,

    DACPRIV_REQUEST_FAILED_ASSEMBLY_DISPLAY_NAME,

    DACPRIV_REQUEST_FAILED_ASSEMBLY_LOCATION,

    DACPRIV_REQUEST_THREAD_DATA,

    DACPRIV_REQUEST_THREAD_THINLOCK_DATA,

    DACPRIV_REQUEST_CONTEXT_DATA,

    DACPRIV_REQUEST_METHODDESC_DATA,

    DACPRIV_REQUEST_METHODDESC_IP_DATA,

    DACPRIV_REQUEST_METHODDESC_NAME,

    DACPRIV_REQUEST_METHODDESC_FRAME_DATA,

    DACPRIV_REQUEST_CODEHEADER_DATA,

    DACPRIV_REQUEST_THREADPOOL_DATA,

    DACPRIV_REQUEST_WORKREQUEST_DATA,

    DACPRIV_REQUEST_OBJECT_DATA,

    DACPRIV_REQUEST_FRAME_NAME,

    DACPRIV_REQUEST_OBJECT_STRING_DATA,

    DACPRIV_REQUEST_OBJECT_CLASS_NAME,

    DACPRIV_REQUEST_METHODTABLE_NAME,

    DACPRIV_REQUEST_METHODTABLE_DATA,

    DACPRIV_REQUEST_EECLASS_DATA,

    DACPRIV_REQUEST_FIELDDESC_DATA,

    DACPRIV_REQUEST_MANAGEDSTATICADDR,

    DACPRIV_REQUEST_MODULE_DATA,

    DACPRIV_REQUEST_MODULEMAP_TRAVERSE,

    DACPRIV_REQUEST_MODULETOKEN_DATA,

    DACPRIV_REQUEST_PEFILE_DATA,

    DACPRIV_REQUEST_PEFILE_NAME,

    DACPRIV_REQUEST_ASSEMBLYMODULE_LIST,

    DACPRIV_REQUEST_GCHEAP_DATA,

    DACPRIV_REQUEST_GCHEAP_LIST,

    DACPRIV_REQUEST_GCHEAPDETAILS_DATA,

    DACPRIV_REQUEST_GCHEAPDETAILS_STATIC_DATA,

    DACPRIV_REQUEST_HEAPSEGMENT_DATA,

    DACPRIV_REQUEST_UNITTEST_DATA,

    DACPRIV_REQUEST_ISSTUB,

    DACPRIV_REQUEST_DOMAINLOCALMODULE_DATA,

    DACPRIV_REQUEST_DOMAINLOCALMODULEFROMAPPDOMAIN_DATA,

    DACPRIV_REQUEST_DOMAINLOCALMODULE_DATA_FROM_MODULE,

    DACPRIV_REQUEST_SYNCBLOCK_DATA,

    DACPRIV_REQUEST_SYNCBLOCK_CLEANUP_DATA,

    DACPRIV_REQUEST_HANDLETABLE_TRAVERSE,

    DACPRIV_REQUEST_RCWCLEANUP_TRAVERSE,

    DACPRIV_REQUEST_EHINFO_TRAVERSE,

    DACPRIV_REQUEST_STRESSLOG_DATA,

    DACPRIV_REQUEST_JITLIST,

    DACPRIV_REQUEST_JIT_HELPER_FUNCTION_NAME,

    DACPRIV_REQUEST_JUMP_THUNK_TARGET,

    DACPRIV_REQUEST_LOADERHEAP_TRAVERSE,

    DACPRIV_REQUEST_MANAGER_LIST,

    DACPRIV_REQUEST_JITHEAPLIST,

    DACPRIV_REQUEST_CODEHEAP_LIST,

    DACPRIV_REQUEST_METHODTABLE_SLOT,

    DACPRIV_REQUEST_VIRTCALLSTUBHEAP_TRAVERSE,

    DACPRIV_REQUEST_NESTEDEXCEPTION_DATA,

    DACPRIV_REQUEST_USEFULGLOBALS,

    DACPRIV_REQUEST_CLRTLSDATA_INDEX,

    DACPRIV_REQUEST_MODULE_FINDIL
  }
}