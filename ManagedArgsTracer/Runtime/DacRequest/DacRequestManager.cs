﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

#endregion

namespace ManagedArgsTracer.Runtime.DacRequest
{
  internal delegate int RawRequestRunner(uint reqCode, uint inBufferSize, byte[] inBuffer, uint outBufferSize, byte[] outBuffer);

  internal class DacRequestManager
  {
    #region Public Methods and Operators

    public static byte[] GetByteArrayForStruct<T>() where T : struct
    {
      return new byte[Marshal.SizeOf(typeof(T))];
    }

    public static T GetStruct<T>(byte[] bytes) where T : struct
    {
      GCHandle gcHandle = GCHandle.Alloc((object)bytes, GCHandleType.Pinned);
      var result = (T)Marshal.PtrToStructure(gcHandle.AddrOfPinnedObject(), typeof(T));
      gcHandle.Free();
      return result;
    }

    public static bool Request(uint id, byte[] input, byte[] output, RawRequestRunner requestRunner)
    {
      uint inBufferSize = 0U;
      if (input != null)
      {
        inBufferSize = (uint)input.Length;
      }
      uint outBufferSize = 0U;
      if (output != null)
      {
        outBufferSize = (uint)output.Length;
      }
      return requestRunner(id, inBufferSize, input, outBufferSize, output) >= 0;
    }

    public static bool RequestStruct<T>(uint id, out T result, RawRequestRunner requestRunner) where T : struct
    {
      byte[] output = GetByteArrayForStruct<T>();
      if (Request(id, null, output, requestRunner))
      {
        result = GetStruct<T>(output);
        return true;
      }
      result = default(T);
      return false;
    }

    public static bool RequestStruct<T>(uint id, T input, out T result, RawRequestRunner requestRunner) where T : struct
    {
      var sizeOfType = Marshal.SizeOf(typeof(T));
      var bytes = new byte[sizeOfType];
      IntPtr ptr = Marshal.AllocHGlobal(bytes.Length);
      Marshal.StructureToPtr(input, ptr, false);
      Marshal.Copy(ptr, bytes, 0, bytes.Length);
      Marshal.FreeHGlobal(ptr);

      byte[] output = new byte[sizeOfType];
      if (Request(id, bytes, output, requestRunner))
      {
        result = GetStruct<T>(output);
        return true;
      }
      result = default(T);
      return false;
    }

    #endregion
  }
}