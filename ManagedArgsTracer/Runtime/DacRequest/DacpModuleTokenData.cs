﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

#endregion

namespace ManagedArgsTracer.Runtime.DacRequest
{
  [StructLayout(LayoutKind.Sequential)]
  internal struct DacpModuleTokenData
  {
    public ulong Module;

    public uint Token;

    public ulong ReturnValue;
  }
}