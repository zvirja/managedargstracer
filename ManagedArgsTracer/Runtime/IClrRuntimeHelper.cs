﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Diagnostics.Runtime;

#endregion

namespace ManagedArgsTracer.Runtime
{
  public interface IClrRuntimeHelper
  {
    #region Public Properties

    IClrTypeCache ClrTypeCache { get; }

    ClrRuntime Runtime { get; }

    #endregion
  }
}