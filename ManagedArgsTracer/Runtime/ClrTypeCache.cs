﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using JetBrains.Annotations;

using ManagedArgsTracer.Helpers;

using Microsoft.Diagnostics.Runtime;

#endregion

namespace ManagedArgsTracer.Runtime
{
  internal class ClrTypeCache : IClrTypeCache
  {
    #region Constructors and Destructors

    public ClrTypeCache(ClrRuntimeHelper helper)
    {
      this.Helper = helper;
      this.Cache = new Dictionary<TypeKey, ClrType>();
      this.TypeModuleCache = new Dictionary<string, string>();
      this.Helper.Runtime.RuntimeFlushed += this.RuntimeOnRuntimeFlushed;
    }

    #endregion

    #region Properties

    private Dictionary<TypeKey, ClrType> Cache { get; set; }

    private ClrRuntimeHelper Helper { get; set; }

    private Dictionary<string, string> TypeModuleCache { get; set; }

    #endregion

    #region Public Methods and Operators

    public ClrType FindTypeByName(string fullTypeName, string moduleName = null)
    {
      var key = new TypeKey(fullTypeName, ulong.MaxValue, moduleName);
      ClrType resultType;
      if (this.Cache.TryGetValue(key, out resultType))
      {
        return resultType;
      }
      resultType = this.Helper.Runtime.GetHeap().GetTypeByName(fullTypeName);
      this.Cache.Add(key, resultType);

      return resultType;
    }

    public ClrType FindTypeByNameAppDomainSpecific(
      string fullTypeName,
      ulong appDomainId,
      bool sureThatDomainSpecific,
      [CanBeNull] ICleverFlusher clrRuntimeFlusher,
      string moduleName = null)
    {
      var key = new TypeKey(fullTypeName, appDomainId, moduleName);
      ClrType resultType;
      if (this.Cache.TryGetValue(key, out resultType))
      {
        return resultType;
      }

      resultType = this.GetTypeFromAppDomain(fullTypeName, appDomainId, moduleName, sureThatDomainSpecific);

      if (resultType != null)
      {
        this.Cache.Add(key, resultType);
        return resultType;
      }

      //If type wasn't found in cache, flush runtime. Do that stupidly and agressively, because type is appDomain specific
      bool runtimeWasFlushed = false;
      if (clrRuntimeFlusher == null)
      {
        this.Helper.Runtime.Flush();
        runtimeWasFlushed = true;
      }
      else
      {
        runtimeWasFlushed = clrRuntimeFlusher.FlushClrRuntime();
      }

      if (runtimeWasFlushed)
      {
        resultType = this.GetTypeFromAppDomain(fullTypeName, appDomainId, moduleName, sureThatDomainSpecific);
      }

      this.Cache.Add(key, resultType);
      return resultType;
    }

    #endregion

    #region Methods

    private ClrType GetTypeFromAppDomain(string typeName, ulong appDomainId, string moduleName, bool sureThatDomainSpecific)
    {
      var id = (int)appDomainId;
      var targetDomain = this.Helper.Runtime.AppDomains.FirstOrDefault(domain => domain.Id == id);
      if (targetDomain == null)
      {
        return null;
      }

      string moduleNameBoost = moduleName;
      if (moduleNameBoost.IsNullOrEmpty())
      {
        this.TypeModuleCache.TryGetValue(typeName, out moduleNameBoost);
      }

      if (moduleNameBoost.NotNullNotEmpty())
      {
        ClrModule moduleToLookIn = targetDomain.Modules.FirstOrDefault(mod => mod.AssemblyName.EqualsWithCase(moduleNameBoost, true));
        if (moduleToLookIn != null)
        {
          return moduleToLookIn.GetTypeByName(typeName);
        }
        return null;
      }

      var validModules = new HashSet<ClrModule>();

      foreach (ClrModule module in targetDomain.Modules)
      {
        validModules.Add(module);
      }

      foreach (ClrType clrType in this.Helper.Runtime.GetHeap().EnumerateTypes())
      {
        if (validModules.Contains(clrType.Module) && clrType.Name.EqualsWithCase(typeName, true))
        {
          this.TypeModuleCache[typeName] = clrType.Module.AssemblyName;
          return clrType;
        }
      }

      /*  //Cannot optimize somehow, because each clrModule.GetType() will load types to heap if they are not currently loaded.
        foreach (ClrModule clrModule in targetDomain.Modules)
        {
          var type = clrModule.GetTypeByName(typeName);
          if (type != null)
          {
            this.TypeModuleCache[typeName] = clrModule.AssemblyName;
            return type;
          }
        }*/

      return null;
    }

    private void RuntimeOnRuntimeFlushed(ClrRuntime runtime)
    {
      this.Cache.Clear();
    }

    #endregion

    private struct TypeKey
    {
      #region Fields

      public ulong AppDomainId;

      public string ModuleName;

      public string TypeName;

      #endregion

      #region Constructors and Destructors

      public TypeKey(string typeName, ulong appDomainId, string moduleName)
      {
        this.TypeName = typeName;
        this.AppDomainId = appDomainId;
        this.ModuleName = moduleName;
      }

      #endregion

      #region Public Methods and Operators

      public static bool operator ==(TypeKey left, TypeKey right)
      {
        return left.Equals(right);
      }

      public static bool operator !=(TypeKey left, TypeKey right)
      {
        return !left.Equals(right);
      }

      public bool Equals(TypeKey other)
      {
        return this.AppDomainId == other.AppDomainId && string.Equals(this.ModuleName, other.ModuleName) && string.Equals(this.TypeName, other.TypeName);
      }

      public override bool Equals(object obj)
      {
        if (ReferenceEquals(null, obj))
        {
          return false;
        }
        return obj is TypeKey && this.Equals((TypeKey)obj);
      }

      public override int GetHashCode()
      {
        unchecked
        {
          int hashCode = this.AppDomainId.GetHashCode();
          hashCode = (hashCode * 397) ^ (this.ModuleName != null ? this.ModuleName.GetHashCode() : 0);
          hashCode = (hashCode * 397) ^ (this.TypeName != null ? this.TypeName.GetHashCode() : 0);
          return hashCode;
        }
      }

      #endregion
    }
  }
}