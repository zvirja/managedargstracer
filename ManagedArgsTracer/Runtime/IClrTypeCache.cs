﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using JetBrains.Annotations;

using Microsoft.Diagnostics.Runtime;

#endregion

namespace ManagedArgsTracer.Runtime
{
  public interface IClrTypeCache
  {
    #region Public Methods and Operators

    ClrType FindTypeByName(string fullTypeName, [CanBeNull] string moduleName = null);

    ClrType FindTypeByNameAppDomainSpecific(
      string fullTypeName,
      ulong appDomainId,
      bool sureThanDomainSpecific,
      [CanBeNull] ICleverFlusher clrRuntimeFlusher,
      [CanBeNull] string moduleName = null);

    #endregion
  }
}