﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using JetBrains.Annotations;

using ManagedArgsTracer.Diagnostics;
using ManagedArgsTracer.Helpers;
using ManagedArgsTracer.Interop.Clr;
using ManagedArgsTracer.Runtime.DacRequest;

#endregion

namespace ManagedArgsTracer.Runtime.Wrappers
{
  internal class XClrDataModuleWrapper
  {
    #region Fields

    private ulong? m_moduleAddress;

    private string m_moduleFilePath;

    private string m_moduleName;

    #endregion

    #region Constructors and Destructors

    public XClrDataModuleWrapper(IXCLRDataModule nativeModule)
    {
      this.NativeModule = nativeModule;
    }

    #endregion

    #region Public Properties

    /// <summary>
    /// Get full image path. If unable to resolve, empty string is returned.
    /// </summary>
    public string FilePath
    {
      get
      {
        if (this.m_moduleFilePath != null)
        {
          return this.m_moduleFilePath;
        }
        this.m_moduleFilePath = ClrInteropHelper.GetName(this.NativeModule.GetFileName) ?? string.Empty;
        return this.m_moduleFilePath;
      }
    }

    public ulong ModuleAddress
    {
      get
      {
        if (this.m_moduleAddress != null)
        {
          return this.m_moduleAddress.Value;
        }

        DacpGetModuleAddress result;
        if (DacRequestManager.RequestStruct<DacpGetModuleAddress>(0xf0000000, out result, this.NativeModule.Request))
        {
          this.m_moduleAddress = result.ModulePtr;
        }
        else
        {
          this.m_moduleAddress = 0;
        }

        return this.m_moduleAddress.Value;
      }
    }

    public string Name
    {
      get
      {
        if (this.m_moduleName != null)
        {
          return this.m_moduleName;
        }

        this.m_moduleName = ClrInteropHelper.GetName(this.NativeModule.GetName) ?? string.Empty;
        return this.m_moduleName;
      }
    }

    public IXCLRDataModule NativeModule { get; set; }

    #endregion

    #region Public Methods and Operators

    public List<XClrDataTypeDefinitionWrapper> GetTypeDefinitions()
    {
      return
        ClrInteropHelper.EnumerateInstancesCustomSelector<XClrDataTypeDefinitionWrapper, IXCLRDataTypeDefinition>(
          this.NativeModule.StartEnumTypeDefinitions,
          this.NativeModule.EnumTypeDefinition,
          this.NativeModule.EndEnumTypeDefinitions,
          instance => new XClrDataTypeDefinitionWrapper(instance)).ToList();
    }

    public List<XClrDataTypeDefinitionWrapper> GetTypeDefinitionsByName(string name)
    {
      return
        ClrInteropHelper.EnumerateInstancesCustomSelector<XClrDataTypeDefinitionWrapper, IXCLRDataTypeDefinition>(
          (out ulong handle) => this.NativeModule.StartEnumTypeDefinitionsByName(name, 0, out handle),
          this.NativeModule.EnumTypeDefinitionByName,
          this.NativeModule.EndEnumTypeDefinitionsByName,
          instance => new XClrDataTypeDefinitionWrapper(instance)).ToList();
    }

    public List<XClrDataTypeInstanceWrapper> GetTypeInstances([NotNull] XClrDataAppDomainWrapper appDomain)
    {
      Assert.ArgumentNotNull(appDomain, "appDomain");
      return
        ClrInteropHelper.EnumerateInstancesCustomSelector<XClrDataTypeInstanceWrapper, IXCLRDataTypeInstance>(
          (out ulong handle) => this.NativeModule.StartEnumTypeInstances(appDomain.NativeAppDomain, out handle),
          this.NativeModule.EnumTypeInstance,
          this.NativeModule.EndEnumTypeInstances,
          instance => new XClrDataTypeInstanceWrapper(instance)).ToList();
    }

    public List<XClrDataTypeInstanceWrapper> GetTypeInstancesByName([CanBeNull] XClrDataAppDomainWrapper appDomain, string name)
    {
      return
        ClrInteropHelper.EnumerateInstancesCustomSelector<XClrDataTypeInstanceWrapper, IXCLRDataTypeInstance>(
          (out ulong handle) => this.NativeModule.StartEnumTypeInstancesByName(name, 0, appDomain != null ? appDomain.NativeAppDomain : null, out handle),
          this.NativeModule.EnumTypeInstanceByName,
          this.NativeModule.EndEnumTypeInstancesByName,
          instance => new XClrDataTypeInstanceWrapper(instance)).ToList();
    }

    #endregion
  }
}