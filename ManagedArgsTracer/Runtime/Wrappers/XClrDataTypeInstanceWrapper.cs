﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ManagedArgsTracer.Helpers;
using ManagedArgsTracer.Interop.Clr;

#endregion

namespace ManagedArgsTracer.Runtime.Wrappers
{
  internal class XClrDataTypeInstanceWrapper
  {
    #region Fields

    private XClrDataTypeInstanceWrapper m_baseType;

    private XClrDataTypeDefinitionWrapper m_definition;

    private string m_name;

    #endregion

    #region Constructors and Destructors

    public XClrDataTypeInstanceWrapper(IXCLRDataTypeInstance dataTypeInstance)
    {
      this.NativeInstance = dataTypeInstance;
    }

    #endregion

    #region Public Properties

    public XClrDataTypeInstanceWrapper BaseType
    {
      get
      {
        if (this.m_baseType != null)
        {
          return this.m_baseType;
        }

        this.m_baseType = ClrInteropHelper.GetInstanceCustomOrNull<XClrDataTypeInstanceWrapper, IXCLRDataTypeInstance>(
          this.NativeInstance.GetBase,
          instance => new XClrDataTypeInstanceWrapper(instance));
        return this.m_baseType;
      }
    }

    public XClrDataTypeDefinitionWrapper Definition
    {
      get
      {
        if (this.m_definition != null)
        {
          return this.m_definition;
        }
        this.m_definition = ClrInteropHelper.GetInstanceCustomOrNull<XClrDataTypeDefinitionWrapper, IXCLRDataTypeDefinition>(
          this.NativeInstance.GetDefinition,
          value => new XClrDataTypeDefinitionWrapper(value));
        return this.m_definition;
      }
    }

    public string Name
    {
      get
      {
        if (this.m_name != null)
        {
          return this.m_name;
        }
        this.m_name = ClrInteropHelper.GetName((uint len, out uint realLen, StringBuilder name) => this.NativeInstance.GetName(0, len, out realLen, name));
        if (this.m_name.IsNullOrEmpty())
        {
          return this.Definition.Name;
        }
        return this.m_name;
      }
    }

    public IXCLRDataTypeInstance NativeInstance { get; set; }

    #endregion

    #region Public Methods and Operators

    public List<XClrDataMethodInstanceWraper> FindMethodsByName(string methodName)
    {
      if (methodName.IndexOf('.') < 0)
      {
        return this.GetMethodsByName(methodName);
      }

      List<XClrDataMethodInstanceWraper> allMethods = this.GetMethods();

      return allMethods.Where(method => method.MethodName.EqualsWithCase(methodName, true)).ToList();
    }

    public List<XClrDataMethodInstanceWraper> GetMethods()
    {
      return ClrInteropHelper.EnumerateInstancesCustomSelector<XClrDataMethodInstanceWraper, IXCLRDataMethodInstance>(
        this.NativeInstance.StartEnumMethodInstances,
        this.NativeInstance.EnumMethodInstance,
        this.NativeInstance.EndEnumMethodInstances,
        instance => new XClrDataMethodInstanceWraper(instance)).ToList();
    }

    public List<XClrDataMethodInstanceWraper> GetMethodsByName(string methodName)
    {
      return ClrInteropHelper.EnumerateInstancesCustomSelector<XClrDataMethodInstanceWraper, IXCLRDataMethodInstance>(
        (out ulong handle) => this.NativeInstance.StartEnumMethodInstancesByName(methodName, 0, out handle),
        this.NativeInstance.EnumMethodInstanceByName,
        this.NativeInstance.EndEnumMethodInstancesByName,
        instance => new XClrDataMethodInstanceWraper(instance)).ToList();
    }

    #endregion
  }
}