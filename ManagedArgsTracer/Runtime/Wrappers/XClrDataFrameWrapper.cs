﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ManagedArgsTracer.Helpers;
using ManagedArgsTracer.Interop.Clr;

#endregion

namespace ManagedArgsTracer.Runtime.Wrappers
{
  internal class XClrDataFrameWrapper
  {
    #region Constructors and Destructors

    public XClrDataFrameWrapper(IXCLRDataFrame nativeFrame)
    {
      this.NativeFrame = nativeFrame;
    }

    #endregion

    #region Public Properties

    public IXCLRDataFrame NativeFrame { get; set; }

    #endregion

    #region Public Methods and Operators

    public string GetCodeName()
    {
      return ClrInteropHelper.GetName((uint len, out uint realLen, StringBuilder name) => this.NativeFrame.GetCodeName(0, len, out realLen, name));
    }

    public XClrDataStackWalkWrapper.DataFrameType? GetCurrentFrameType()
    {
      CLRDataSimpleFrameType simpleType;
      CLRDataDetailedFrameType detailedType;
      int retCode = this.NativeFrame.GetFrameType(out simpleType, out detailedType);

      if (retCode != 0)
      {
        return null;
      }
      else
      {
        return new XClrDataStackWalkWrapper.DataFrameType(simpleType, detailedType);
      }
    }

    #endregion
  }
}