﻿#region

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

using ManagedArgsTracer.Diagnostics;
using ManagedArgsTracer.Helpers;
using ManagedArgsTracer.Interop.Clr;
using ManagedArgsTracer.Runtime.DacRequest;

using Microsoft.Diagnostics.Runtime.Interop;

#endregion

namespace ManagedArgsTracer.Runtime.Wrappers
{
  internal class XClrDataProcessWrapper
  {
    #region Constructors and Destructors

    public XClrDataProcessWrapper(IXCLRDataProcess rawDataProcess)
    {
      this.RawXClrDataProcess = rawDataProcess;
      this.ISOSDac = rawDataProcess as ISOSDac;
    }

    #endregion

    #region Public Properties

    public ISOSDac ISOSDac { get; set; }

    public DataOtherNotifyFlag OtherNotificationFlag
    {
      get
      {
        DataOtherNotifyFlag flag;
        this.RawXClrDataProcess.GetOtherNotificationFlags(out flag);
        return flag;
      }
      set
      {
        var result = this.RawXClrDataProcess.SetOtherNotificationFlags(value);
        if (result != 0)
        {
          Tracer.PublicOutput(Tracer.UnexpectedErrorWrongWorkReportMessage);
          Tracer.PrivateOutput("Unable to enable CLR notifications. Response code: {0}".FormatWith(result));
        }
      }
    }

    public IXCLRDataProcess RawXClrDataProcess { get; set; }

    #endregion

    #region Public Methods and Operators

    public XClrDataTaskWrapper GetDataTaskByOSThreadID(uint osThreadId)
    {
      var nativeTask = ClrInteropHelper.GetInstanceOrNull<IXCLRDataTask>((out IXCLRDataTask value) => this.RawXClrDataProcess.GetTaskByOSThreadID(osThreadId, out value));
      if (nativeTask == null)
      {
        return null;
      }
      return new XClrDataTaskWrapper(nativeTask);
    }

    public ulong GetMethodTableForType(ulong moduleAddress, uint typeToken)
    {
      //For 4.5 use SOSDac
      if (this.ISOSDac != null)
      {
        ulong methodDesc;
        int retCode;
        if ((retCode = this.ISOSDac.GetMethodDescFromToken(moduleAddress, typeToken, out methodDesc)) == 0)
        {
          return methodDesc;
        }
        Tracer.ReportUnexpectedState("Unable to get MethodTable address for '{0}' type using ISOSDac. Ret code: " + retCode.ToString(CultureInfo.InvariantCulture));
        return 0UL;
      }

      var moduleTokenData = new DacpModuleTokenData();
      moduleTokenData.Module = moduleAddress;
      moduleTokenData.Token = typeToken;

      DacpModuleTokenData requestResult;
      if (DacRequestManager.RequestStruct((uint)DacRequestCodes.DACPRIV_REQUEST_MODULETOKEN_DATA, moduleTokenData, out requestResult, this.RawXClrDataProcess.Request))
      {
        return requestResult.ReturnValue;
      }
      Tracer.ReportUnexpectedState("Unable to get MethodTable address for '{0}' type using Request.");
      return 0;
    }

    public XClrDataModuleWrapper GetModuleByAddress(ulong address)
    {
      return
        ClrInteropHelper.GetInstanceCustomOrNull<XClrDataModuleWrapper, IXCLRDataModule>(
          (out IXCLRDataModule value) => this.RawXClrDataProcess.GetModuleByAddress(address, out value),
          module => new XClrDataModuleWrapper(module));
    }

    public List<XClrDataModuleWrapper> GetModules()
    {
      return ClrInteropHelper.EnumerateInstancesCustomSelector<XClrDataModuleWrapper, IXCLRDataModule>(
        this.RawXClrDataProcess.StartEnumModules,
        this.RawXClrDataProcess.EnumModule,
        this.RawXClrDataProcess.EndEnumModules,
        rawModule => new XClrDataModuleWrapper(rawModule));
    }

    public int Request(uint reqCode, uint inBufferSize, byte[] inBuffer, uint outBufferSize, byte[] outBuffer)
    {
      return this.RawXClrDataProcess.Request(reqCode, inBufferSize, inBuffer, outBufferSize, outBuffer);
    }

    public string ResolveMethodNameByAddress(ulong address)
    {
      ulong displacement;
      return
        ClrInteropHelper.GetName(
          (uint len, out uint realLen, StringBuilder name) => this.RawXClrDataProcess.GetRuntimeNameByAddress(address, 0, len, out realLen, name, out displacement));
    }

    public void TranslateExceptionRecordToNotification(ref EXCEPTION_RECORD64 record, [MarshalAs(UnmanagedType.Interface)] IXCLRDataExceptionNotification notify)
    {
      this.RawXClrDataProcess.TranslateExceptionRecordToNotification(ref record, notify);
    }

    #endregion
  }
}