﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ManagedArgsTracer.Common;
using ManagedArgsTracer.Diagnostics;
using ManagedArgsTracer.Helpers;
using ManagedArgsTracer.Interop.Clr;

#endregion

namespace ManagedArgsTracer.Runtime.Wrappers
{
  internal class XClrDataMethodInstanceWraper
  {
    #region Fields

    private List<FullTypeName> m_arguments;

    private XClrDataMethodDefinitionWraper m_definition;

    private CLRDataMethodFlag? m_isInstance;

    private string m_methodName;

    private string m_name;

    private string m_nameWithArgs;

    private int? m_numArguments;

    private TokenAndScopePair m_tokenAndScope;

    private XClrDataTypeInstanceWrapper m_typeInstance;

    private string m_typeName;

    #endregion

    #region Constructors and Destructors

    public XClrDataMethodInstanceWraper(IXCLRDataMethodInstance methodInstance)
    {
      this.NativeInstance = methodInstance;
    }

    #endregion

    #region Public Properties

    /// <summary>
    /// Arguments. If unable to fetch - null is returned.
    /// </summary>
    public List<FullTypeName> Arguments
    {
      get
      {
        if (this.m_arguments != null)
        {
          return this.m_arguments;
        }
        int indexOfParenthesis = this.NameWithArgs.IndexOf('(');
        Assert.IsTrue(indexOfParenthesis > -1, "Method name '{0}' doensn't contain parenthesis.".FormatWith(this.NameWithArgs));
        string[] arguments = this.NameWithArgs.Substring(indexOfParenthesis).Trim('(', ' ', ')').Split(",".ToCharArray());
        this.m_arguments = new List<FullTypeName>();
        //Check that args are not empty. If empty, they contain empty string (no args).
        if (!(arguments.Length == 1 && arguments[0].Equals(string.Empty)))
        {
          foreach (string argument in arguments)
          {
            this.m_arguments.Add(new FullTypeName(argument.Trim()));
          }
        }
        return this.m_arguments;
      }
    }

    public XClrDataMethodDefinitionWraper Definition
    {
      get
      {
        if (this.m_definition != null)
        {
          return this.m_definition;
        }
        this.m_definition = ClrInteropHelper.GetInstanceCustomOrNull<XClrDataMethodDefinitionWraper, IXCLRDataMethodDefinition>(
          this.NativeInstance.GetDefinition,
          definition => new XClrDataMethodDefinitionWraper(definition));
        return this.m_definition;
      }
    }

    public bool IsInstance
    {
      get
      {
        if (this.m_isInstance != null)
        {
          return this.m_isInstance.Value == CLRDataMethodFlag.CLRDATA_METHOD_HAS_THIS;
        }
        this.m_isInstance = ClrInteropHelper.GetInstanceCustomOrNull<CLRDataMethodFlag?, CLRDataMethodFlag>(this.NativeInstance.GetFlags, flag => flag);
        return this.m_isInstance != null && this.m_isInstance.Value == CLRDataMethodFlag.CLRDATA_METHOD_HAS_THIS;
      }
    }

    public string MethodName
    {
      get
      {
        if (this.m_methodName != null)
        {
          return this.m_methodName;
        }

        this.m_methodName = this.Name.Substring(this.TypeName.Length + 1);
        return this.m_methodName;
      }
    }

    public string Name
    {
      get
      {
        if (this.m_name != null)
        {
          return this.m_name;
        }
        this.m_name = this.NameWithArgs.GetSubStrTillChar('(');
        return this.m_name;
      }
    }

    public string NameWithArgs
    {
      get
      {
        if (this.m_nameWithArgs != null)
        {
          return this.m_nameWithArgs;
        }
        this.m_nameWithArgs = ClrInteropHelper.GetName((uint len, out uint realLen, StringBuilder name) => this.NativeInstance.GetName(0, len, out realLen, name));
        return this.m_nameWithArgs;
      }
    }

    public IXCLRDataMethodInstance NativeInstance { get; set; }

    /// <summary>
    /// Number of arguments. If unable to fetch, -1 is returned.
    /// </summary>
    public int NumArguments
    {
      get
      {
        if (this.m_numArguments != null)
        {
          return this.m_numArguments.Value;
        }
        this.m_numArguments = this.Arguments == null ? -1 : this.Arguments.Count;
        return this.m_numArguments.Value;
      }
    }

    public TokenAndScopePair TokenAndScope
    {
      get
      {
        if (this.m_tokenAndScope != null)
        {
          return this.m_tokenAndScope;
        }
        this.m_tokenAndScope = ClrInteropHelper.GetTokenAndScope(this.NativeInstance.GetTokenAndScope);
        return this.m_tokenAndScope;
      }
    }

    public XClrDataTypeInstanceWrapper TypeInstance
    {
      get
      {
        if (this.m_typeInstance != null)
        {
          return this.m_typeInstance;
        }
        this.m_typeInstance = ClrInteropHelper.GetInstanceCustomOrNull<XClrDataTypeInstanceWrapper, IXCLRDataTypeInstance>(
          this.NativeInstance.GetTypeInstance,
          instance => new XClrDataTypeInstanceWrapper(instance));

        return this.m_typeInstance;
      }
    }

    public string TypeName
    {
      get
      {
        if (this.m_typeName != null)
        {
          return this.m_typeName;
        }

        this.m_typeName = this.TypeInstance.NullOr(inst => inst.Name);

        if (this.m_typeName.IsNullOrEmpty())
        {
          this.m_typeName = this.Definition.NullOr(def => def.Name);
        }

        //Try use name parsing
        if (this.m_typeName.IsNullOrEmpty())
        {
          string methodName = this.Name;
          int typeMethodDelimiterInd = methodName.LastIndexOf('.');
          Assert.IsTrue(typeMethodDelimiterInd > 0, "Unable to get TypeName for the {0} method. Dot is not present".FormatWith(this.NameWithArgs));

          //Cover ..ctor
          if (methodName[typeMethodDelimiterInd - 1] == '.')
          {
            typeMethodDelimiterInd = typeMethodDelimiterInd - 1;
          }

          this.m_typeName = methodName.Substring(0, typeMethodDelimiterInd);
        }

        return this.m_typeName;
      }
    }

    #endregion

    #region Public Methods and Operators

    public ulong GetRepresentativeEntryAddress()
    {
      ulong result = ClrInteropHelper.GetInstanceCustomHandler<ulong, ulong>(
        this.NativeInstance.GetRepresentativeEntryAddress,
        (value, success) => success ? value : ulong.MaxValue);
      return result;
    }

    #endregion
  }
}