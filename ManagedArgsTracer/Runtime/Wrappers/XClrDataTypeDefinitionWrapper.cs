﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ManagedArgsTracer.Common;
using ManagedArgsTracer.Helpers;
using ManagedArgsTracer.Interop.Clr;
using ManagedArgsTracer.Session;

#endregion

namespace ManagedArgsTracer.Runtime.Wrappers
{
  internal class XClrDataTypeDefinitionWrapper
  {
    #region Fields

    private XClrDataTypeDefinitionWrapper m_baseType;

    private bool m_baseTypeResolved;

    private XClrDataModuleWrapper m_module;

    private string m_name;

    private TokenAndScopePair m_tokenAndScope;

    #endregion

    #region Constructors and Destructors

    public XClrDataTypeDefinitionWrapper(IXCLRDataTypeDefinition dataTypeDefinition)
    {
      this.NativeDefinition = dataTypeDefinition;
    }

    #endregion

    #region Public Properties

    /// <summary>
    /// Returned value could not be used for any purpose other than check whether base exists.
    /// </summary>
    public XClrDataTypeDefinitionWrapper BaseType
    {
      get
      {
        if (this.m_baseTypeResolved)
        {
          return this.m_baseType;
        }
        this.m_baseType = ClrInteropHelper.GetInstanceCustomOrNull<XClrDataTypeDefinitionWrapper, IXCLRDataTypeDefinition>(
          this.NativeDefinition.GetBase,
          value => new XClrDataTypeDefinitionWrapper(value));

        this.m_baseTypeResolved = true;
        return this.m_baseType;
      }
    }

    public XClrDataModuleWrapper Module
    {
      get
      {
        if (this.m_module != null)
        {
          return this.m_module;
        }

        this.m_module = ClrInteropHelper.GetInstanceCustomOrNull<XClrDataModuleWrapper, IXCLRDataModule>(this.NativeDefinition.GetModule, mod => new XClrDataModuleWrapper(mod));

        var addr = this.m_module.ModuleAddress;

        return this.m_module;
      }
    }

    public string Name
    {
      get
      {
        if (this.m_name != null)
        {
          return this.m_name;
        }
        this.m_name = this.GetName(true);

        return this.m_name;
      }
    }

    public IXCLRDataTypeDefinition NativeDefinition { get; set; }

    public TokenAndScopePair TokenAndScope
    {
      get
      {
        if (this.m_tokenAndScope != null)
        {
          return this.m_tokenAndScope;
        }

        this.m_tokenAndScope = ClrInteropHelper.GetTokenAndScope(this.NativeDefinition.GetTokenAndScope);

        if (this.m_module == null)
        {
          this.m_module = this.m_tokenAndScope.Module;
        }

        return this.m_tokenAndScope;
      }
    }

    #endregion

    #region Public Methods and Operators

    public List<XClrDataMethodDefinitionWraper> FindMethodDefinitionByName(string methodName)
    {
      if (methodName.IndexOf('.') < 0)
      {
        return this.GetMethodDefinitionsByName(methodName);
      }

      List<XClrDataMethodDefinitionWraper> allMethodDefs = this.GetMethodDefinitions();
      return allMethodDefs.Where(def => def.Name.EqualsWithCase(methodName, true)).ToList();
    }

    public List<XClrDataMethodDefinitionWraper> GetMethodDefinitions()
    {
      return ClrInteropHelper.EnumerateInstancesCustomSelector<XClrDataMethodDefinitionWraper, IXCLRDataMethodDefinition>(
        this.NativeDefinition.StartEnumMethodDefinitions,
        this.NativeDefinition.EnumMethodDefinition,
        this.NativeDefinition.EndEnumMethodDefinitions,
        nativeDef => new XClrDataMethodDefinitionWraper(nativeDef));
    }

    public List<XClrDataMethodDefinitionWraper> GetMethodDefinitionsByName(string methodName)
    {
      return ClrInteropHelper.EnumerateInstancesCustomSelector<XClrDataMethodDefinitionWraper, IXCLRDataMethodDefinition>(
        (out ulong handle) => this.NativeDefinition.StartEnumMethodDefinitionsByName(methodName, 0, out handle),
        this.NativeDefinition.EnumMethodDefinitionByName,
        this.NativeDefinition.EndEnumMethodDefinitionsByName,
        methodDef => new XClrDataMethodDefinitionWraper(methodDef));
    }

    public bool IsSameObject(XClrDataTypeDefinitionWrapper other)
    {
      return this.NativeDefinition.IsSameObject(other.NativeDefinition) == 0;
    }

    #endregion

    #region Methods

    private string GetName(bool useMTProbing)
    {
      if (this.m_name != null)
      {
        return this.m_name;
      }

      string resultName = ClrInteropHelper.GetName((uint len, out uint realLen, StringBuilder name) => this.NativeDefinition.GetName(0, len, out realLen, name));
      if (resultName.IsNullOrEmpty() && useMTProbing)
      {
        TokenAndScopePair tokenAndScope = this.TokenAndScope;
        if (tokenAndScope.IsValid)
        {
          ulong mt = GlobalSessionVars.XClrDataProcessWrapper.GetMethodTableForType(tokenAndScope.Module.ModuleAddress, tokenAndScope.Token);
          resultName = GlobalSessionVars.ClrRuntimeHelper.GetNameForMT(mt);
        }
      }

      return resultName;
    }

    #endregion
  }
}