﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ManagedArgsTracer.Diagnostics;
using ManagedArgsTracer.Helpers;
using ManagedArgsTracer.Interop.Clr;

#endregion

namespace ManagedArgsTracer.Runtime.Wrappers
{
  internal class XClrDataAppDomainWrapper
  {
    #region Fields

    private ulong? m_id;

    #endregion

    #region Constructors and Destructors

    public XClrDataAppDomainWrapper(IXCLRDataAppDomain nativeAppDomain)
    {
      this.NativeAppDomain = nativeAppDomain;
    }

    #endregion

    #region Public Properties

    public ulong ID
    {
      get
      {
        if (this.m_id != null)
        {
          return this.m_id.Value;
        }
        this.m_id = ClrInteropHelper.GetInstanceCustomHandler<ulong?, ulong>(this.NativeAppDomain.GetUniqueID, (value, success) => success ? (ulong?)value : null);
        if (this.m_id == null)
        {
          Tracer.ReportUnexpectedState("Unable to resolve ID for AppDomain.", true);
          return 0;
        }
        return this.m_id.Value;
      }
    }

    public IXCLRDataAppDomain NativeAppDomain { get; set; }

    #endregion
  }
}