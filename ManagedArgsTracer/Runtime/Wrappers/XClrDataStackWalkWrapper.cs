﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ManagedArgsTracer.Helpers;
using ManagedArgsTracer.Interop.Clr;

#endregion

namespace ManagedArgsTracer.Runtime.Wrappers
{
  internal class XClrDataStackWalkWrapper
  {
    #region Constructors and Destructors

    public XClrDataStackWalkWrapper(IXCLRDataStackWalk nativeStackWalk)
    {
      this.NativeStackWalk = nativeStackWalk;
    }

    #endregion

    #region Public Properties

    public IXCLRDataStackWalk NativeStackWalk { get; set; }

    #endregion

    #region Public Methods and Operators

    public XClrDataFrameWrapper GetCurrentFrame()
    {
      return ClrInteropHelper.GetInstanceCustomOrNull<XClrDataFrameWrapper, IXCLRDataFrame>(this.NativeStackWalk.GetFrame, frame => new XClrDataFrameWrapper(frame));
    }

    public DataFrameType? GetCurrentFrameType()
    {
      CLRDataSimpleFrameType simpleType;
      CLRDataDetailedFrameType detailedType;
      int retCode = this.NativeStackWalk.GetFrameType(out simpleType, out detailedType);

      if (retCode != 0)
      {
        return null;
      }
      else
      {
        return new DataFrameType(simpleType, detailedType);
      }
    }

    public ulong GetStackSizeSkipped()
    {
      ulong? result = ClrInteropHelper.GetInstanceCustomOrNull<ulong?, ulong>((out ulong value) => this.NativeStackWalk.GetStackSizeSkipped(out value), arg => arg);
      return result.HasValue ? result.Value : 0;
    }

    public bool Next()
    {
      return this.NativeStackWalk.Next() == 0;
    }

    #endregion

    public struct DataFrameType
    {
      #region Constructors and Destructors

      public DataFrameType(CLRDataSimpleFrameType simpleType, CLRDataDetailedFrameType detailedType)
        : this()
      {
        this.SimpleType = simpleType;
        this.DetailedType = detailedType;
      }

      #endregion

      #region Public Properties

      public CLRDataDetailedFrameType DetailedType { get; private set; }

      public CLRDataSimpleFrameType SimpleType { get; private set; }

      #endregion
    }
  }
}