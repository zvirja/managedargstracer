﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ManagedArgsTracer.Common;
using ManagedArgsTracer.Diagnostics;
using ManagedArgsTracer.Helpers;
using ManagedArgsTracer.Interop.Clr;

#endregion

namespace ManagedArgsTracer.Runtime.Wrappers
{
  internal class XClrDataMethodDefinitionWraper
  {
    #region Fields

    private string m_fullName;

    private bool? m_generic;

    private CLRDataMethodFlag? m_isInstance;

    private string m_name;

    private TokenAndScopePair m_tokenAndScope;

    #endregion

    #region Constructors and Destructors

    public XClrDataMethodDefinitionWraper(IXCLRDataMethodDefinition methodDefinition)
    {
      this.NativeDefinition = methodDefinition;
    }

    #endregion

    #region Public Properties

    public CLRDATA_METHNOTIFY CodeNotificationFlag
    {
      get
      {
        CLRDATA_METHNOTIFY flag;
        this.NativeDefinition.GetCodeNotification(out flag);
        return flag;
      }
      set
      {
        int result = this.NativeDefinition.SetCodeNotification(value);
        if (result != 0)
        {
          Tracer.PublicOutput("WARNING. Unable to enable JIT notifications. Application might not work correct.");
          Tracer.PrivateOutput("Method name: {0}, Flag: {1}, response code: {2}".FormatWith(this.FullName, value.ToString(), result));
        }
      }
    }

    public string FullName
    {
      get
      {
        if (this.m_fullName != null)
        {
          return this.m_fullName;
        }
        this.m_fullName = ClrInteropHelper.GetName((uint len, out uint realLen, StringBuilder name) => this.NativeDefinition.GetName(0, len, out realLen, name));
        int indexOfParenthesis = this.m_fullName.IndexOf('(');
        if (indexOfParenthesis > -1)
        {
          this.m_fullName = this.m_fullName.Substring(0, indexOfParenthesis);
        }
        return this.m_fullName;
      }
    }

    public bool Generic
    {
      get
      {
        if (this.m_generic != null)
        {
          return this.m_generic.Value;
        }
        this.m_generic = ClrInteropHelper.GetInstanceOrNull<bool>(this.NativeDefinition.HasClassOrMethodInstantiation);
        return this.m_generic.Value;
      }
    }

    public bool IsInstance
    {
      get
      {
        if (this.m_isInstance != null)
        {
          return this.m_isInstance.Value == CLRDataMethodFlag.CLRDATA_METHOD_HAS_THIS;
        }
        this.m_isInstance = ClrInteropHelper.GetInstanceCustomOrNull<CLRDataMethodFlag?, CLRDataMethodFlag>(this.NativeDefinition.GetFlags, flag => flag);
        return this.m_isInstance != null && this.m_isInstance.Value == CLRDataMethodFlag.CLRDATA_METHOD_HAS_THIS;
      }
    }

    public string Name
    {
      get
      {
        if (this.m_name != null)
        {
          return this.m_name;
        }

        string typeAndMethodName = this.FullName;
        int delimiterIndex = typeAndMethodName.LastIndexOf('.');
        if (typeAndMethodName[delimiterIndex - 1] == '.')
        {
          delimiterIndex -= 1;
        }

        this.m_name = typeAndMethodName.Substring(delimiterIndex + 1);
        return this.m_name;
      }
    }

    public IXCLRDataMethodDefinition NativeDefinition { get; set; }

    public TokenAndScopePair TokenAndScope
    {
      get
      {
        if (this.m_tokenAndScope != null)
        {
          return this.m_tokenAndScope;
        }
        this.m_tokenAndScope = ClrInteropHelper.GetTokenAndScope(this.NativeDefinition.GetTokenAndScope);
        return this.m_tokenAndScope;
      }
    }

    #endregion

    #region Public Methods and Operators

    public List<XClrDataMethodInstanceWraper> GetInstances()
    {
      return ClrInteropHelper.EnumerateInstancesCustomSelector<XClrDataMethodInstanceWraper, IXCLRDataMethodInstance>(
        (out ulong handle) => this.NativeDefinition.StartEnumInstances(null, out handle),
        this.NativeDefinition.EnumInstance,
        this.NativeDefinition.EndEnumInstances,
        instance => new XClrDataMethodInstanceWraper(instance)).ToList();
    }

    public ulong GetRepresentativeEntryAddress()
    {
      ulong result = ClrInteropHelper.GetInstanceCustomHandler<ulong, ulong>(
        this.NativeDefinition.GetRepresentativeEntryAddress,
        (value, success) => success ? value : ulong.MaxValue);
      return result;
    }

    #endregion
  }
}