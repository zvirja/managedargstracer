﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ManagedArgsTracer.Helpers;
using ManagedArgsTracer.Interop.Clr;

#endregion

namespace ManagedArgsTracer.Runtime.Wrappers
{
  internal class XClrDataTaskWrapper
  {
    #region Fields

    private XClrDataAppDomainWrapper m_appDomain;

    #endregion

    #region Constructors and Destructors

    public XClrDataTaskWrapper(IXCLRDataTask nativeDataTask)
    {
      this.NativeDataTask = nativeDataTask;
    }

    #endregion

    #region Public Properties

    public XClrDataAppDomainWrapper AppDomain
    {
      get
      {
        if (this.m_appDomain != null)
        {
          return this.m_appDomain;
        }
        this.m_appDomain = ClrInteropHelper.GetInstanceCustomOrNull<XClrDataAppDomainWrapper, IXCLRDataAppDomain>(
          this.NativeDataTask.GetCurrentAppDomain,
          domain => new XClrDataAppDomainWrapper(domain));
        return this.m_appDomain;
      }
    }

    public IXCLRDataTask NativeDataTask { get; set; }

    #endregion

    #region Public Methods and Operators

    public XClrDataStackWalkWrapper CreateStackWalk(CLRDataSimpleFrameType flags)
    {
      XClrDataStackWalkWrapper result =
        ClrInteropHelper.GetInstanceCustomOrNull(
          (out IXCLRDataStackWalk value) => this.NativeDataTask.CreateStackWalk(flags, out value),
          walk => new XClrDataStackWalkWrapper(walk));

      return result;
    }

    #endregion
  }
}