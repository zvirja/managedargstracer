﻿#region

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;

using ManagedArgsTracer.Breakpoints.Definition;
using ManagedArgsTracer.Common;
using ManagedArgsTracer.Diagnostics;
using ManagedArgsTracer.Helpers;
using ManagedArgsTracer.Properties;
using ManagedArgsTracer.Session;
using ManagedArgsTracer.Updater;

using Microsoft.Diagnostics.Runtime.Interop;

#endregion

namespace ManagedArgsTracer
{
  internal class Program
  {
    #region Static Fields

    private static volatile bool ExitWasRequired;

    private static volatile bool InterruptAllowed;

    #endregion

    #region Constructors and Destructors

    static Program()
    {
      //To resolve log4net and ClrMD from resources. Use static constructor because Main method (due to some reasons) is too late.
      ResourceAssembliesHook.InitializeHook();
    }

    #endregion

    #region Methods

    private static bool HandleHelpCall(string inputLine)
    {
      if (HandleOnlineHelpCall(inputLine))
      {
        return true;
      }

      var helpKeys = new string[] { "/?", "/help", "-help", "--help" };

      if (inputLine.IsNullOrEmpty() || helpKeys.Any(key => inputLine.EqualsWithCase(key, false)))
      {
        Console.WriteLine("Current vesrion: " + RecentVersionFetcher.GetCurrentVersion());
        Console.WriteLine();
        Console.WriteLine(Resources.HelpText);
        return true;
      }

      return false;
    }

    private static bool HandleOnlineHelpCall(string inputLine)
    {
      if (inputLine.EqualsWithCase("/ohelp", false) || inputLine.EqualsWithCase("/onlinehelp", false))
      {
        Process.Start("https://bitbucket.org/zvirja/managedargstracer/wiki/Home");
        Console.WriteLine("Wiki page with help has been opened.");
        return true;
      }
      return false;
    }

    private static void Main(string[] args)
    {
      string inputLine = args.JoinToString(" ");

      if (HandleHelpCall(inputLine))
      {
        return;
      }

      Tracer.PrivateOutput("Raw parameters: " + inputLine);
      try
      {
        var tracingTask = new TracingTask(inputLine);
        try
        {
          tracingTask.Parse();
          //Share current flags will all the application.
          GlobalSessionVars.CurrentFlags = tracingTask.SessionFlags;
        }
        catch (InvalidCommandLineInputException ex)
        {
          Log.Error("Unable to parse passed parameters. Raw parameters: '{0}'".FormatWith(inputLine), ex, typeof(Program));
          Tracer.PublicOutput(ex.Message);
          return;
        }

        //Trap Ctrl + C.
        Console.CancelKeyPress += OnCancelKeyPress;

        //Display info about recent version if need.
        string recentVersionMessage = RecentVersionFetcher.GetUpdateMessageIfNeed();
        if (!recentVersionMessage.IsNullOrEmpty())
        {
          Tracer.PublicOutput(recentVersionMessage);
        }

        using (var sessionManager = new SessionManager())
        {
          if (!sessionManager.StartSession(tracingTask))
          {
            return;
          }

          //Trace info only if session was initialized
          string traceInfo = ObtainTraceTaskInfo(tracingTask);
          Tracer.PublicOutput(traceInfo);

          PrivatelyTraceAdvanced(sessionManager);

          Console.WriteLine("Press Ctrl + C to gracefully stop the current session at any point. Never close the current window directly because that can kill the target process!");
          Console.WriteLine();
          Console.WriteLine("-----------------");

          sessionManager.BreakpointsManager.ResolveBreakpoints();

          //Resume initial suspend and let breakpoints to be hitted in future..
          sessionManager.NativeDebugInterfaces.DebugControl.SetExecutionStatus(DEBUG_STATUS.GO);

          //Run updater in background.
          RecentVersionFetcher.RunVersionFetch();

          InterruptAllowed = true;

          //Do only if Ctrl+C trap wasn't present before.
          if (!ExitWasRequired)
          {
            //Wait to the end. The only way to break it - set interrupt.
            //S_FALSE (1) is returned in case of timeout. We should continue only in this case, otherwise stop the session. 
            int lastRetCode = sessionManager.NativeDebugInterfaces.DebugControl.WaitForEvent(DEBUG_WAIT.DEFAULT, uint.MaxValue);

            //exit due to responce from debugger. Trace it.
            Tracer.PrivateOutput("Last return code from Native Debugger: {0}".FormatWith(lastRetCode.ToString("X")));
          }

          InterruptAllowed = false;

          if (ExitWasRequired)
          {
            Tracer.PublicOutput("Ctrl + C hit was trapped.");
          }

          sessionManager.BreakpointsManager.PrintStatistics();
        }

        Tracer.PublicOutput("Application is about to be closed. Check session log files to get more information about current session.");
      }
        //It's really unexpected to get exception here.
      catch (Exception ex)
      {
        //Who knows, probably issue is related to logging itself. So wrapped with empty catch.
        try
        {
          //Output that.
          Console.WriteLine("Unexpected error in application: " + ex);

          Log.Error("Exception in Main method.", ex, typeof(Program));
        }
        catch
        {
        }
      }
    }

    private static string ObtainTraceTaskInfo(TracingTask tracingTask)
    {
      var result = new StringBuilder();
      result.Append("TRACING TASK INFO");
      result.AppendLine();

      if (tracingTask.ProcessNameToAttach.NotNullNotEmpty())
      {
        result.Append("Process name: ");
        result.AppendLine(tracingTask.ProcessNameToAttach);
      }
      else
      {
        result.Append("Process ID: ");
        result.AppendLine(tracingTask.PidToAttach.ToString(CultureInfo.InvariantCulture));
      }

      if (tracingTask.SessionFlags != SessionFlags.NONE)
      {
        result.AppendLine("Flags: " + tracingTask.SessionFlags);
      }

      result.AppendLine();
      result.AppendLine("METHODS TO TRACE:");
      foreach (BreakpointDefinition breakpointDef in tracingTask.BreakpointDefinitions)
      {
        result.AppendLine("===========");
        result.AppendLine(breakpointDef.GetDefinitionInfo());
      }

      return result.ToString();
    }

    private static void OnCancelKeyPress(object sender, ConsoleCancelEventArgs consoleCancelEventArgs)
    {
      ExitWasRequired = true;
      IDebugControl debugControl = GlobalSessionVars.DebugControl;
      if (InterruptAllowed && debugControl != null)
      {
        //DEBUG_INTERRUPT_EXIT 2
        debugControl.SetInterrupt(DEBUG_INTERRUPT.ACTIVE);
      }

      consoleCancelEventArgs.Cancel = true;
    }

    private static void PrivatelyTraceAdvanced(SessionManager sessionManager)
    {
      var buffer = new StringBuilder("CURRENT SESSION INFO:");
      buffer.AppendLine();
      buffer.AppendFormat("Target process ID: {0}", sessionManager.TargetProcessInfo.Id);
      buffer.AppendLine();
      buffer.AppendFormat("Clr version: {0}", sessionManager.ClrVersion.Version);
      buffer.AppendLine();
      buffer.AppendFormat("DAC location: {0}", sessionManager.DacLocation);
      buffer.AppendLine();
      buffer.AppendFormat("Now: {0}", DateTime.Now.ToString("O"));
      buffer.AppendLine();

      Tracer.PrivateOutput(buffer.ToString());
    }

    #endregion
  }
}