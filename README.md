Managed Args Tracer
===================

Latest version: 1.1

Quick description
=================

ManagedArgsTracer tool allows to attach to process and trace values of particular method arguments. After you specify which methods and arguments should be traced, tool will wait until the method is called and trace argument values. Additionally, tool displays stack trace for method call, which might help you to quickly track root of method call.

Refer to the [Wiki](/zvirja/managedargstracer/wiki/Home) to get insight into the supported tool syntax.

Download link:
==============
[https://bitbucket.org/zvirja/managedargstracer-binaries/downloads](https://bitbucket.org/zvirja/managedargstracer-binaries/downloads)